Change log 0.17.2 - Fear Factor

* Fixes
- Fixe Grenade launcher grenade selection
-- Do not throw en electric grenade when pressing the left click
- Fixe "nofade" chat command
- Fixe score exception

* Features
- Adding hand grenade selection on the buy menu
-- Pulse/Cluster/Gaz would take the place of a welder or a mine

* Balances
- Change the faded selection system
-- The winner has 80% of the chance to be the Faded
-- If the Faded win again, he has 60% of chance

- Reduce electric grenade direct hit to 90 (from 110)
- Increase GL primary grenade explosion radius to 6.5 (from 4.8)
