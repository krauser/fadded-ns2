Change log 0.17 - Fear Factor

* Fixes
Fixed the "1HP" invincible bug (Eclipse/Raffinery/Docking/Descent)
Fixed Faded selection system
Fixed power node bug in the marine spawn room
Fixed "double start" game
Fixed minimap GUI for the Faded

* Features
Added Full Sanity game mechanic
Added Full Sanity Effects
Added Full Sanity GUI

Added Faded Growl abability (bind on "use")
Added Marine quick-talk menu to the Faded

Added dynamic Health and Armor on the Faded
Added dynamic round time

Added random spawn to the Fade and marines
- Marines are splitted into 2 groups (if more than 6 marines)
- The Faded can spawn on Techpoint and RessourcePoint

* Balance
Remove parasite effect on mine
Rewrite the Faded selection system
- 80% Chance on kill
- 20% Random

Increase Flamethrower damage to 7 (from 2.5)
Increase Flamethrower burn duration to 7 (from 2.5)
Decrease Flamethrower surface Friendly Fire damage (damage / 3)

* Details

Sanity. Each marine has a Sanity pool. If the Sanity pool is too low, random scary sound effets and halucination pops out.
TIP: If your flashlight is turned on, you can regenerate your mental health. (Various things influence the mental health, you will have to find out).
