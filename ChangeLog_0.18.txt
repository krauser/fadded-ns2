Change log 0.18 - Fear Factor

* Fixes
- Fixe Faded spawn without the "swipe" weapon
- Fixe exception when a marine suicide himself and then purchase a weapon

* Features
- Multi fade system (one fade every 12 marines)
- Adding team chat/all talk choice to the faded team to allow teamwork
- Adding name to hallucination to fake Faded death
