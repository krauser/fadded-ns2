Change log 0.19.1 - Veteran

* Fixes
- Fixe mine damage as Veteran
- Fixe "new round" bug:
- - Player is the faded 6+ times
- - Game start several times
- - Player kiked out
- Fixe Veteran endround choice (does not include the new Faded player)

* Feature
- Anti camp system added

* Balance
- Multiply by 2 the damage of the flamthrower (direct flame)
- Increase GL Grenade explosion radius to 7.5 (from 6.5)
- Increase nearby flashlight sanity bonus to 0.5/s (from 0.2)
- Decrease a bit welder damage
- Increase Veteran health to 110 (from 100)
- Increase marine buy time to 10s (from 8)
