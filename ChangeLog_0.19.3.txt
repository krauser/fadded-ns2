Change log 0.19.3 - Veteran

* Fixes
- Fixe "Auto join" bug for players in RR and spectator
- Fixe Veteran weapon GUI to be above the Veteran (and not behind)
- Fixe Veteran movement (Smooth move on Client side)
- Fixe Babbler exception
- Fixe minimap exception when spectating

* Feature
- Adding chat warning if the sanity pool is too low
- Adding decomposed body infestation/gaz/GUI
- Dead body now stay on the map (in preparation for the version 0.20)

* Design
- Changing minimap color for Veteran

* Balance
- Reduce Veteran Speed bonus to +8% (from +10%)
- Adding Veteran selection after a Draw
- Upgrade the Flamethrower to remove decomposed body area
