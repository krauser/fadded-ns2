Change log 0.19 - Veteran

* Fixes
- Fixe Faded spawn as a Marine
- Fixe Multi faded armor amount
- Fixe Fadedhelp chat command

* Features
- Flash light now increase nearby marine sanity
- Adding marinehelp chat command
- Adding Veteran marine system

* Balance
- Decrease Faded scale to 10 (from 12)
- Decrease round bonus time per marine to 30s (from 45s)
