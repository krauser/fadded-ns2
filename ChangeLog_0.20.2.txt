Change log 0.20.2 - Body snatcher

* Fixe
- Fixe flamethrower unable to burn a corpse
- Fixe faded eating multiple time the same corpse
- Fixe hallucination blue cloud visible by teammate
- Fixe pulse grenade damage on owner
- Fixe persistante attack order on Veteran (When a fake marine goes Fade while detected)
- Fixe hallucination suicide notification

* Features
- Rewrite the scoring system to avoid the 9999 bug and make it easier to understand
-- 100 points is the maximum reward
--> Ex: If the Faded has 100hp, and you deal 38, you will have 38pts

* Design
- Move the timer to the right to not block players name when spectating
- Add details error message when the Faded is unable to eat a body
- Print Faded health/armor when the round is over.

* Balance
- Reduce Veteran scale to 1 for 7 marines (down to 5)
- Decrease Veteran fake marine attack order radius to 8 (from 9)
- Reduce babblers number to 12 (from 15)
- Decrease long range visibility
- Slightly reduce fade long range visibility
- Decrease Veteran fake marine attack order radius to 5 (from 9)
- Remove alien Friendly Fire
- Hallucination now give a fake point indicator
- Change the hallucination spawn and attack to really fake a Faded
- Add a really low chance to spawn an hallucination in Stress mode

* Optimisation
-- Reduce by 2 the network traffic of spectator
