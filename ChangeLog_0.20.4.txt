Change log 0.20.4 - Body snatcher

* Fix
- Fixe Fake marine main weapon
- Fixe eating corpse check (now correclty use line of sight)
- Fixe bile bomb velocity
- Fixe swipe range

* Features
- Add a Stab ability to the Fake marine

* Balance
- Reduce the chance to have hallucination in stress Mode of 20%
- Change Fake marine footstep
- Increase Faded approach sound volume
- Increase mine explosion trigger radius
- Reduce energy drain of the Flamethrower
