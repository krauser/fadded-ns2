Change log 0.20.5.1 - Body snatcher

* Fixes
- Fix faded not able to Disguise himself
- Fix Leap consuming energy but not triggered
- Fix Faded stabbing a marine 10+ time in the same second

* Balance
- Decrease maximum sanity malus with the amount of players
- Increase energy after a stab to 25% (from 15%)
- Increase Stab sanity damage per sec to 3 (from 2)
- Decrease mine max damage to 100 (from 125)
- Decrease Faded scale to 9 (from 10)
- Denied the Faded to swipe 2s after Dropping his Fake Marine

* Optimisation
- Remove Hallucination corpse after 15s
