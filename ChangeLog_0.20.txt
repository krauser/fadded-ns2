Change log 0.20 - Body snatcher

* Fixes
- Fixe "Fade comming sound" of dead faded
- Fixe losing fade becoming fade again

* Features
- Changing the cloack ability of the Faded
- Adding to the Faded the ability to disguise with corpse
- Adding Faded disguise mecanic
-- Some marines are not really on your side

-- TIPS:
--- Trust Veteran
--- *They can easily detect the Fake Marines
--- *Veterans are always real Marine

* Balance (v0.20 feature in beta test)
- Remove Faded name when using a Fake Marine
- Remove minimap icon of the Fake marine when on sight
- Decrease pulse grenade damage to 50 (from 90)
- Increase pulse damage on GL owner (x2)
- Decrease mine number to 2 (from 3)
- Increase mine limitation radius to 25 (from 20)
