Change log 0.21.2 - Research & Destroy

* Features
- Add damage display when the observatory is up
- Add a second objectiv part with the prototype labs (Faded hunt)
- Add a new primary weapon: The heavy shotgun

* Balance
- Increase a bit the faded cloack amount
- Reduce base hp of the faded but increase hp bonus per marine
- Increase the sanity damages on a stab (3/s -> 5/s)
- Increase hand grenade number (2 --> 3)
- Increase battery icon range
- Remove anticamp nearby a marine building
- Increase by 2 the sanity pool
- Reduce by 10% flash light sanity regeneration
- A marine can't use his main weapon anymore when holding a battery
- Decrease mine minimum drop range limitation (25m -> 20m)
- Increase pulse grenade energy damages (0 -> 10)
- Marines building are now almost invulnerable (except PG & batteries)
- Increase a bit faded cloack
- Increase battery spawn time (1min30 -> 3min)
---> But spawn also a battery each time one is put in the main base

* Fixe
- Faded being fully invisible when revived
- Fixed score being reset on the next round
- Fixed battery spawning always at the same place
- Fixed observatory not giving the minimap anywhere
- Fixed the json configuration file when setting a value to 'false' or '0'
- Fixed the flameMines GUI that display ammo

* Other
- Reviving a fade give now 10 points
- Add a lots of variable in the json configuration files
