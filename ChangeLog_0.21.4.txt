Change log 0.21.4 - Research & Destroy

* Feature
- The faded can now create babblers against some hp
- The prototype labs now drops jetpack and exo

* Balance
- heavy shotgun improved (range & damage)
--> Reduce pellet number (from 100 to 75)
--> Increase damages per pellet (from 2 to 3)
--> Increase the pellet on a double shot (50% bonus to 60% bonus)
--> reduce max spread by approx 15%
--> Reduce weapon ammo (from 8 to 6)
- Turn on again anticamp in the main base
- Decrease anticamp radius (from 15 to 10m)
- Reduce max camp duration (from 60 to 45s)
- Reduce babblers lifetime (from 60s to 40s)
- Building auto kill a babbler if attacked
- Now 2 battery spawn when the round begin
- Divide by 2 the marine sanity pool
- Remove the observatory passive detection ability
- Reduce stab damages (from 25 to 10)
- Remove the wallgrip cost for the Faded
- Increase swipe cost (from 12 to 14)
- Marine are auto promoted to Veteran with at most 3 players in game
- Reduce battery weigth (from 0.5 to 0.4 (a shotgun is 0.14))
- Increase regular shotgun ammo (from 8 to 10)
- Increase flame mines num (from 1 to 2)
- Decrease battery health (from 1500/300 to 1300/200)

* Fixes
- Fixed Faded able to swipe without any stamina
- Fixed Faded location on entity change (marine <-> faded)
- Fixed lag when using the heavy shotgun alt fire
- Fixed marine using the hunter ability not having the minimap
- Fixed hint messages spamming server log
- Fixed flashlight only detecting the faded under the cursor
- Review the faded stab (is now triggered immediatly)
- Fixed building destroy animation not working
- Fixed fake marine not seeing damages with their weapon

* Other
- Update the "scan" weapon description (how to use it)
- Add a hint messages when a marine try to buy a weapon after the delay
- Add more configuration variable to the json
- Server side json modification are now also logged in the client console
