Change log 0.21.5 - Research & Destroy

* Features
- Add a new equipement: Napalm grenade !
- Completely rework the Faded camo
--> Now designed to react depending on marine proximity and speed
--> Allows the Faded to sneak much more, at closer range
- Rework the marine buy menu and add text details for the marine sanity bar
- The ambient light of the map now decreases with the sanity
- The Faded now see corpses on the minimap (white marine/faded blip)

* Balance
- Reduce burn damages (from 7/s to 6/s)
- Reduce by 10% heavy shotgun pellets (from 75 to 68)
- Restore the observatory passive detection
- Reduce Faded HP/AP and per marine bonus by 15%
- Increase hp bonus per marine (about 8%)
- Increase stab poison damages (75 -> 85) and duration (30s -> 25s)
- Add a +5 armor bonus for the Faded when eating a corpses
- Increase growl damages (17dmg -> 18dmg)
- Decrease growl rate (10s -> 9s)
- Decrease the Faded babbler ability cost (26 -> 24)
- Increase the number of babbler spawn by the ability (4 -> 5)
- Reduce swipe cost (14 -> 12)
- Increase blue flashlight intensity (22m -> 26m)
- Fake marine now appears in blue for regular marine when obs is up

* Fixes
- Fixed Growl damages triggered twice or more
- Fixed the hunter ability tracing hallucination
- Fixed all upgrade being reset when taking a jetpack or an exo
- Fixed sentry battery locked when dropped on an already powered up building
- Fixed building not spawning when the round start (pg & main building)
- Fixed Faded being the faded 5+ times in a row

* Others
- Add sanity bonus on hit for gas and cluster grenade
- Send auto chat messages when the anticamp is triggered
- Add a lots of variable in the json
- Optimisation of the Growl code
