Change log 0.22.2 Afterlife experiences

* Features
- Babbler have an almost instant respawn once the proto is up

* Balance
- Remove the useless robotics factory
- Battery spawn interval is a bit longer
- Babblers can only spawn on a location at least 25m away from any marine
- Increase jetpack fuel & charge
- Increase exo speed by 10%
- Increase flare intensity radius and fire rate
- Increase ammo drop on a babbler player kill (6% -> 10%)

* Fixes
- Fixed random marine spawn not beeing random
- Fixed phase gate wrong placement (can't phase throw)
- Fixed powernode issue (killed spam message on console & crashes)
- Fixed compatiblity with the build 275

* Regression
- Minimap is visible for everyone
- Scoreboard is hidden again & contain some printing mistakes
- Bilebomb is slower

* Others
- "Round time over" is now a victory for the marines
- Allow babblers to spawn on tech point too
- Add variables to the server json
