Change log 0.22.3 Afterlife experiences

* Balance
- Remove the whisper sound when the Faded is nearby
--> The Faded can trigger it for marines around with the voice menu
- Improve Faded camo
--> Max distance for being seen: 21m --> 19m
--> 10% less holes in the skin when leaping
--> 8% better camo while standing still
- Reduce by 7% Faded starting hp/ap
- Slitly reduce mines and flame damages

* Fixes
- Minimap has been fixed
--> Display only icons away for at most 25m
--> Fix babbler minimap icon
--> Veteran have the minimap by default

* Others
- Now by default the faded ratio will be a 4 marines for 1 fade
- Disable alltalk by default
- Crunching as a fade do not make you drop really fast anymore
