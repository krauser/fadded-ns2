Change log 0.22.6 Afterlife experiences

* Features
- Improve the napalm grenade:
--> The faded is now only put in fire if present in the cloud for more than 1.2s
--> The napalm is released faster and the cloud cover all the area travelled instead of the final point nade location.
- Rework the lighting system:
--> Map shadows are back :)
--> Rebalance bright and dark area
- Add an aura to the Faded (the closer to a marine the stronger):
--> Decrease sanity regeneration (for pressure)
--> Decrease ambiant light (except flare, flashlight and battery)

* Balance
- Increase flamed area lifetime (from 5.6s to 10s)
- Review the babblers spawn location algo
--> Spawn to the RT/corpses the closest to a marine but away from at least 22m

* Fixes
- Fixed flame mines acting as regular mines
- Fixed faded HP/AP bar not properly working
- Fixed the ugly light from the flare
- The flare light now disapears smoothly
- Fixed 2 server side console errors

* Others
- Add cloud effects after a stab
- The napalm cloud is now a light source
