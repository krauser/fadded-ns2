Change log 0.22.9 Afterlife experiences

* Features
- Add a "Medic" layout as an ability
--> Primary weapon  : GL with healing grenades
--> Secondary weapon: Pistol with explosive bullets and delayed detonation
--> Heal order if a wounded teammate ask for a medpack
--> Display "medic" on the scoreboard
- Display "Battery" on the socreboard if a marine is holding one
- Adding loading screens with a few hints

* Balance
- Removed gas grenades (useless)
- Reduce the HP cost of the Faded blink (from 8/s to 6/s)

* Fixes
- Healing grenades model are correctly destroyed (do not stay on screen forever)
- Bringing a battery back now correctly give 10 points
