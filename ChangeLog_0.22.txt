Change log 0.22 Afterlife experiences

* Features
- Add the ability for the marine to play as a babbler once dead
--> Auto respawn each "wave" of 50s
--> Auto respawn when the Faded spawn babblers
- Add a 70s delay after a changemap, and a 8s delay between each round
- Change the armory behaviour (players will buy weapon with PRes)
- The scoreboard is not anymore hidden (exept for the faded players as a marine)

* Balance
- Mines (both) do not explode anymore on babblers
- Battery are now visible on the minimap for marines

* Fixes
- Fixed some compatiblity issues
- Fixed battery locked sometime on an unpowered building
- Fixed marines being spammed on the chat when the round start
- Fixed faded FOV
- Fixed faded bilebomb fire rate being shared by all the Faded team
- Fixed Heavy shotgun displayed as a "shotgun" on the scoreboard

* Others
- Change marine/spectator 'x' menu to use command more easily
- Add variables to the json
- Optimise server side update loop
- Update the marine buy menu
--> Opened by default
--> Shows the client key to use it
- Add some client message to make the game more intuitive
- Each commands now work with the "!" at the beginning
