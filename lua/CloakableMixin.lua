-- ======= Copyright (c) 2003-2013, Unknown Worlds Entertainment, Inc. All rights reserved. =======
--
-- lua\CloakableMixin.lua
--
-- Handles both cloaking and camouflage. Effects are identical except cloaking can also hide
-- structures (camouflage is only for players).
--
--    Created by:   Charlie Cleveland (charlie@unknownworlds.com)
--
-- ========= For more information, visit us at http:--www.unknownworlds.com =====================

CloakableMixin = CreateMixin(CloakableMixin)
CloakableMixin.type = "Cloakable"

-- Uncloak faster than cloaking
CloakableMixin.kCloakRate = 2
CloakableMixin.kUncloakRate = 12
CloakableMixin.kTriggerCloakDuration = .6
CloakableMixin.kTriggerUncloakDuration = 2.5


-- Faded cloak amount (how visible he is whitout any modifier)
-- The more the better is the cloak (0.9 is barely visible)
-- How a marine see the Faded
-- 0 --> 0.5: lowest this value to make less part of the Faded visible
-- 0.5 --> 1: We see all the Faded, but the greater the harder to see (less distorsion)
kFadedMaxCloak = 0.95 -- max cloak amount
kFadedCloakAmount = 0.58 -- base cloak amount
kFadedVisuDistance = 19 -- max distance where a marine can see the faded
-- Maximum percent of bonus if the faded is slower than walking (crunching, not moving). 20% means 20% bonus from the range between kFadedCloakAmount and kFadedMaxCloak
kFadedMaxCloakBonusPercentSlowMovement = 18--%
kFadedMaxCloakMalusPercentSpeedMovement = 10 --% per unit above 1 (leap == 3)
kFadedLeapCloakMalus = 18 --%

-- Ajouter la sante mental en facteur ??

function UpCloack(player)
   if (Shared.GetCheatsEnabled()) then
      if (Server) then
         Server.SendNetworkMessage(player, "DEBUGCAMO_cloack_up", {}, true)
      else
         kFadedCloakAmount = kFadedCloakAmount + 0.1
         Print("Cloack now at " .. tostring(kFadedCloakAmount))
      end
   end
end

function DecCloack(player)
   if (Shared.GetCheatsEnabled()) then
      if (Server) then
         Server.SendNetworkMessage(player, "DEBUGCAMO_cloack_dec", {}, true)
      else
         kFadedCloakAmount = kFadedCloakAmount - 0.1
         Print("Cloack now at " .. tostring(kFadedCloakAmount))
      end
   end
end

function UpVisu(player)
   if (Shared.GetCheatsEnabled()) then
      if (Server) then
         Server.SendNetworkMessage(player, "DEBUGCAMO_visu_up", {}, true)
      else
         kFadedVisuDistance = kFadedVisuDistance + 2
         Print("Max visu distance now at " .. tostring(kFadedVisuDistance))
      end
   end
end
function DecVisu(player)
   if (Shared.GetCheatsEnabled()) then
      if (Server) then
         Server.SendNetworkMessage(player, "DEBUGCAMO_visu_dec", {}, true)
      else
         kFadedVisuDistance = kFadedVisuDistance - 2
         if (kFadedVisuDistance <= 0) then kFadedVisuDistance = 0.1 end
         Print("Max visu distance now at " .. tostring(kFadedVisuDistance))
      end
   end
end

local kEnemyUncloakDistanceSquared = 1.5 ^ 2

PrecacheAsset("cinematics/vfx_materials/cloaked.surface_shader")
PrecacheAsset("cinematics/vfx_materials/distort.surface_shader")
local kCloakedMaterial = PrecacheAsset("cinematics/vfx_materials/cloaked.material")
local kDistortMaterial = PrecacheAsset("cinematics/vfx_materials/distort.material")

local Client_GetLocalPlayer

if Client then
   Client_GetLocalPlayer = Client.GetLocalPlayer
end

CloakableMixin.expectedMixins =
   {
      EntityChange = "Required to update lastTouchedEntityId."
   }

CloakableMixin.optionalCallbacks =
   {
      OnCloak = "Called when entity becomes fully cloaked",
      GetSpeedScalar = "Called to figure out how fast we're moving, if we can move",
      GetIsCamouflaged = "Aliens that can evolve camouflage have this",
   }

CloakableMixin.networkVars =
   {
      -- set server side to true when cloaked fraction is 1
      fullyCloaked = "boolean",
      -- so client knows in which direction to update the cloakFraction
      cloakingDesired = "boolean",
      cloakRate = "integer (0 to 3)",
   }

function CloakableMixin:__initmixin()

   if Server then
      self.cloakingDesired = false
      self.fullyCloaked = false
   end

   self.desiredCloakFraction = 0
   self.timeCloaked = 0
   self.timeUncloaked = 0

   -- when entity is created on client consider fully cloaked, so units wont show up for a short moment when going through a phasegate for example
   self.cloakFraction = self.fullyCloaked and 1 or 0

end

function CloakableMixin:GetCanCloak()
   return true
end

CloakableMixinEntEndBlink = {}
function CloakableMixin:GetEndBlinkMalus()
   local opacity = 0
   if (self:isa("Fade")) then
      local malus = 3
      local ent_it = self:GetId()
      local ent_endtime = CloakableMixinEntEndBlink[ent_it]

      -- if (self.cloakEndEther == nil) then
      --    self.cloakEndEther = 0
      -- end
      -- if (self.ethereal) then
      --    self.cloakIsInEther = true
      --    Shared:FadedMessage("Enter ether")
      -- elseif (self.cloakIsInEther) then
      --    self.cloakEndEther = Shared.GetTime()
      --    self.cloakIsInEther = false
      --    Shared:FadedMessage("Leave ether")
      -- -- else
      --    -- We are after the blink
      -- end

      if (ent_endtime) then
         malus = malus - Clamp(Shared.GetTime() - ent_endtime, 0, malus)
      else
         malus = 0
      end
      -- if (ent_endtime and Shared.GetTime() - ent_endtime < 3) then
      --    Shared:FadedMessage("Blink endtime: " .. tostring(self.etherealEndTime)
      --                           .. " id: " .. tostring(self:GetId())
      --                           .. " malus: " .. tostring(malus))
      -- end
      malus = Clamp(malus, 0, 1)
      opacity = opacity + malus
   end
   return (opacity)
end

function CloakableMixin:GetIsCloaked()
   return true --self:GetEndBlinkMalus() == 0
end

function CloakableMixin:GetCloakFraction()
   if (self:isa("SentryBattery") and self:GetTeamNumber() == 2) then
      self.cloakFraction = kFadedMaxCloak
   end
   return (self.cloakFraction)
end

-- Function here to update the camo for each client independantly
function CloakableMixinUpdateCloakState(player, deltaTime)
   -- if (Shared.GetCheatsEnabled()) then
   --    self.cloakFraction = kFadedCloakAmount
   --    return
   -- end -- no up with cheat, for testing
   local cloak_bonus_for_distance = kFadedMaxCloak - kFadedCloakAmount
   local speed_bonus = 0
   local cloakFraction = nil
   local cloakOverLoadOnProximity = 0
   local entId = FindNearestEntityId("Marine", player:GetOrigin())
   local nearest_marine = Shared.GetEntity(entId)

   if (player.GetSpeedScalar) then
      local speed = player:GetSpeedScalar()
      if (speed <= 1) then -- Give at most N% of camo bonus
         speed_bonus = (1 - speed) * (kFadedMaxCloak - kFadedCloakAmount) * (kFadedMaxCloakBonusPercentSlowMovement / 100)
      elseif (speed >= 2) then
         speed_bonus = -((speed - 2) * (kFadedMaxCloak - kFadedCloakAmount) * (kFadedMaxCloakMalusPercentSpeedMovement / 100))
      end
   end

   if (nearest_marine) then
      -- Proximity reduction
      local missing_cloak_before_max = kFadedMaxCloak - kFadedCloakAmount
      local selforig = player:GetOrigin()
      local distance = nearest_marine:GetOrigin():GetDistanceTo(selforig)
      distance = Clamp(distance, 0, kFadedVisuDistance)

      if (kFadedVisuDistance <= 0) then kFadedVisuDistance = 0.1 end
      cloak_bonus_for_distance = (missing_cloak_before_max / kFadedVisuDistance) * distance
      -- Speed factor
      -- self:GetSpeedScalar() returns:
      -- on leap: around 3 --> no effect on cloak
      -- on walk: around 1 --> no effect on cloak
      -- on stand: 0       --> cloak bonus (0.2 speed ==> +0.08 cloak)
      --> cloak bonus (0.3 speed ==> +0.07 cloak)
      --> etc

      cloakFraction = kFadedCloakAmount + cloak_bonus_for_distance + speed_bonus
   else
      cloakFraction = kFadedMaxCloak + speed_bonus
   end

   cloakFraction = Clamp(cloakFraction, kFadedCloakAmount, kFadedMaxCloak)
   if (player:isa("SentryBattery")) then
      cloakFraction = kFadedMaxCloak
      -- player:TriggerCloak()
   end

   return cloakFraction, cloak_bonus_for_distance, speed_bonus
end


function CloakableMixin:OnUpdate(deltaTime)
   self.cloakFraction = CloakableMixinUpdateCloakState(self, deltaTime)
end

function CloakableMixin:OnProcessMove(input)
   self.cloakFraction = CloakableMixinUpdateCloakState(self, input.time)
end

function CloakableMixin:OnProcessSpectate(deltaTime)
   self.cloakFraction = CloakableMixinUpdateCloakState(self, deltaTime)
end

if Server then

   function CloakableMixin:OnEntityChange(oldId)

      if oldId == self.lastTouchedEntityId then
         self.lastTouchedEntityId = nil
      end

   end

elseif Client then

   function CloakableMixin:OnUpdateRender()

      PROFILE("CloakableMixin:OnUpdateRender")

      self:_UpdateOpacity()

      local model = self:GetRenderModel()

      if model then

         local player = Client_GetLocalPlayer()

         self:_UpdatePlayerModelRender(model)

         -- Now process view model
         if self == player then
            self:_UpdateViewModelRender()
         end

      end

   end

   -- Update holes in the camo according to speed
   function CloakableMixin:_UpdateOpacity()

      local player = Client_GetLocalPlayer()

      -- Only draw models when mostly uncloaked
      -- local albedoVisibility = 1 - Clamp(self.cloakFraction * 5, 0, 1)
      local albedoVisibility = 0

      if player and player:isa("AlienCommander") then
         albedoVisibility = 1
      end

      local opacity = albedoVisibility -- FADED: ici trou dans le camo
      if (player and self.GetSpeedScalar) then
         if (self:GetSpeedScalar() >= kFadedMinSpeedForOpacity) then
            opacity = 0 + ((self:GetSpeedScalar() - kFadedMinSpeedForOpacity) / 35) * kFadedModOpacity
         end
         -- Faded is trully visible at the end of a blink
      end

      opacity = opacity + self:GetEndBlinkMalus()
      if (not self:isa("Fade") and not (self:isa("SentryBattery") and self:GetTeamNumber() == 2)) then
         opacity = 1
      end
      self:SetOpacity(opacity, "cloak")

      if (self == player) then

         local viewModelEnt = self:GetViewModelEntity()
         if viewModelEnt then
            viewModelEnt:SetOpacity(opacity, "cloak")
         end

      end

   end

   -- Update how friend/enemy see the cloack
   function CloakableMixin:_UpdatePlayerModelRender(model)

      local player = Client_GetLocalPlayer()
      local hideFromEnemy = GetAreEnemies(self, player) or self:isa("SentryBattery")

      local useMaterial = (self.cloakingDesired or self:GetCloakFraction() ~= 0) and not hideFromEnemy

      if not self.cloakedMaterial and useMaterial then
         self.cloakedMaterial = AddMaterial(model, kCloakedMaterial)
      elseif self.cloakedMaterial and not useMaterial then

         RemoveMaterial(model, self.cloakedMaterial)
         self.cloakedMaterial = nil

      end

      if self.cloakedMaterial then

         -- show it animated for the alien commander. the albedo texture needs to remain visible for outline so we show cloaked in a different way here
         local distortAmount = self.cloakFraction
         if player and player:isa("AlienCommander") then
            distortAmount = distortAmount * 0.5 + math.sin(Shared.GetTime() * 0.05) * 0.05
         end

         -- Main material parameter that affects our appearance
         self.cloakedMaterial:SetParameter("cloakAmount", 0.8)
         -- self.cloakedMaterial:SetParameter("cloakAmount", self.cloakFraction)

      end

      local showDistort = self.cloakFraction ~= 0 and self.cloakFraction ~= 1
      if showDistort and not self.distortMaterial then
         self.distortMaterial = AddMaterial(model, kDistortMaterial )
      elseif not showDistort and self.distortMaterial then
         RemoveMaterial(model, self.distortMaterial)
         self.distortMaterial = nil
      end

      if self.distortMaterial then
         self.distortMaterial:SetParameter("distortAmount", self:GetCloakFraction())
      end

   end

   -- How the faded see himself
   function CloakableMixin:_UpdateViewModelRender()

      -- always show view model distort effect
      local viewModelEnt = self:GetViewModelEntity()
      if viewModelEnt and viewModelEnt:GetRenderModel() then

         -- Show view model as enemies see us, so we know how cloaked we are
         if not self.distortViewMaterial then
            self.distortViewMaterial = AddMaterial(viewModelEnt:GetRenderModel(), kDistortMaterial)
         end
         -- TODO: update ici aussi
         self.distortViewMaterial:SetParameter("distortAmount", self:GetCloakFraction())

      end

   end

end

-- Pass negative to uncloak
function CloakableMixin:OnScan()
end

function CloakableMixin:PrimaryAttack()
end

function CloakableMixin:OnGetIsSelectable(result, byTeamNumber)
   result.selectable = result.selectable and (byTeamNumber == self:GetTeamNumber() or not self:GetIsCloaked())
end

function CloakableMixin:SecondaryAttack()
end

function CloakableMixin:OnTakeDamage(damage, attacker, doer, point)
end

-- When he enter in colision when something
function CloakableMixin:OnCapsuleTraceHit(entity)

   -- PROFILE("CloakableMixin:OnCapsuleTraceHit")

   -- if GetAreEnemies(self, entity) then

   --     if self.fullyCloaked then
   --         TEST_EVENT("Uncloaked from being touched")
   --     end

   --     self:TriggerUncloak()
   --     self.lastTouchedEntityId = entity:GetId()

   -- end

end
