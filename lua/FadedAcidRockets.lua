// ===================== Faded Mod =====================
//
// lua\FadedAcidRockets.lua
//
//    Created by: Rio (rio@myrio.de)
//    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

Script.Load("lua/Weapons/Alien/LeapMixin.lua")

local floor = math.floor

local networkVars =
{
    firingPrimary = "boolean"
}

AddMixinNetworkVars(LeapMixin, networkVars)

local onCreate = BileBomb.OnCreate
function BileBomb:OnCreate()
   onCreate(self)
   InitMixin(self, LeapMixin)
end

function BileBomb:GetHUDSlot()
    return 3
end

function BileBomb:GetSecondaryTechId()
    return kTechId.Leap
end

-- -- Not even called
-- function BileBomb:GetPrimaryAttackAllowed()
-- end

local onPrimaryAttack = BileBomb.OnPrimaryAttack
-- As the Fade does not have a BileBomb animation,
-- we have to add a manual timer system for the firing rate
function BileBomb:OnPrimaryAttack(player)
   if (player:GetEnergy() >= kBileBombEnergyCost
          and player.lastBileBomb + kBileBombFireRate < Shared.GetTime())
   then
      player.lastBileBomb = Shared.GetTime()
      self.firingPrimary = true
   else
      self.firingPrimary = false
   end
end

-- -- Same as the default bilebomb but with the kBombVelocity increase
-- local kBombVelocity = kFadedModBombVelocity
-- function BileBomb:FireBombProjectile(player)

--    PROFILE("BileBomb:FireBombProjectile")

--    if Server then

--       local viewAngles = player:GetViewAngles()
--       local viewCoords = viewAngles:GetCoords()
--       local startPoint = player:GetEyePos() + viewCoords.zAxis * 1

--       local startPointTrace = Shared.TraceRay(player:GetEyePos(), startPoint, CollisionRep.Damage, PhysicsMask.Bullets, EntityFilterOne(player))
--       startPoint = startPointTrace.endPoint

--       local startVelocity = viewCoords.zAxis * kBombVelocity

--       local bomb = CreateEntity(Bomb.kMapName, startPoint, player:GetTeamNumber())
--       bomb:Setup(player, startVelocity, true, nil, player)

--    end

-- end

Shared.LinkClassToMap("BileBomb", BileBomb.kMapName, networkVars)
