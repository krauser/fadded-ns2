// ===================== Faded Mod =====================
//
// lua\FadedAlien.lua
//
//    Created by: Rio (rio@myrio.de)
//    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================


if (Server) then
   local locale = LibCache:GetLibrary("LibLocales-1.0")

   class 'FadedMod'

   -- Disable almost all regeneration for the Faded
   kAlienMinInnateRegeneration = 1
   kAlienMaxInnateRegeneration = 1

   local playerOnKill = Player.OnKill
   function Player:OnKill(attacker, doer, point, direction)
      -- Patch a nil exception for the stab poison
      -- (because doer is nil but not attacker)
      -- See "function Player:OnKill(killer, doer, point, direction)"
      if (attacker and doer == nil) then
         doer = attacker
      end

      -- Help message for friend
      if (self:GetTeamNumber() == 2 and self:isa("Fade") and self.isHallucination ~= true and kFadedRespawnedEnable) then
         for _, faded in pairs(GetGamerules():GetTeam2():GetPlayers()) do
            if (faded and not faded:isa("Skulk") -- no hint for babbler
               and faded:GetIsAlive() and self ~= faded) then
               faded:FadedHintMessage(
                  string.format(
                     locale:ResolveString("FADED_REVIVE_ORDER"),
                     self:GetName(),
                     self:GetLocationName(),
                     kFadedRespawnedFadeHealthCost
                  ))
               CreatePheromone(kTechId.NeedHealingMarker,
                               self:GetOrigin(),
                               self:GetTeamNumber())
               -- faded:GiveOrder(kTechId.Heal, nil, self:GetOrigin(),
               --                nil, true, false)
            end
         end
      end

      -- The player who killed the Faded has a higher chance
      -- to be the Faded next round
      -- If we kill an hallucination nothing is done
      if (self:GetTeamNumber() == 2
             and attacker and attacker:isa("Marine")
          and self.isHallucination ~= true) then
         fadedNextPlayersName[attacker:GetName()] = true
         if (GetGamerules():CheckIfFadedIsDead()) then
            attacker:FadedMessage(locale:ResolveString("FADED_SELECTION_MARINE"))
         end
      end
      playerOnKill(self, attacker, doer, point, direction)
   end

   function Alien:GetDarkVisionEnabled()
      return self.darkVisionOn
   end


elseif (Client) then
   function Alien:Buy() end
   function Fade:Buy()  end
   function Skulk:Buy()
      Marine.Buy(self)
   end
   function Skulk:CloseMenu()
      Marine.CloseMenu(self)
   end
   function Player:GetIsAllowedToBuy()
      return (self:GetTeamNumber() == 1 or (self:GetTeamNumber() == 2 and self:isa("Skulk")))
   end
end

-- function Alien:UpdateSilenceLevel()

--    self.silenceLevel = 3
--    -- if GetHasSilenceUpgrade(self) then
--    --    self.silenceLevel = GetVeilLevel(self:GetTeamNumber())
--    --  else
--    --      self.silenceLevel = 0
--    --  end

-- end

-- -- Get the opacity of the faded when he is moving
-- -- (small area of the body become visible)
-- function Alien:GetOpacityLevel()

--    kFadedModOpacity = kFadedModBaseOpacity

--    --if self.primaryAttacking then
--    --  kFadedModCloakedFraction = kFadedModCloakedFraction + kFadedModAttackingCloakedModificator
--    --end

--    kFadedModOpacity = kFadedModOpacity + ((math.abs(self.velocityLength) * kFadedModSpeedOpacitydModificator)/3)
--    kFadedModOpacity = kFadedModOpacity + ((math.abs(self.velocityYaw) * kFadedModSpeedOpacitydModificator)/3)
--    kFadedModOpacity = kFadedModOpacity + ((math.abs(self.velocityPitch) * kFadedModSpeedOpacitydModificator)/3)

--    if kFadedModOpacity <= 0  then
--       return 0
--    elseif kFadedModOpacity <= kFadedModLowestOpacity then
--       return kFadedModOpacity
--    else
--       return kFadedModLowestOpacity
--    end
-- end

if (Client) then
   -- -- Return the closest marine coord to the 'alien_orig' position
   -- -- Return nil if there are no one around
   -- local function _getClosestMarine(alien_orig)
   --    -- +1 to be above the maximum visibility range
   --    local distance = kFadedMarineMaximalDistanceOfDetection + 1
   --    local tmp = 0
   --    local closestMarine = nil
   --    local marine_orig = nil
   --    local marine_around = GetEntitiesForTeamWithinRange("Marine",
   --                               1, alien_orig,
   --                  kFadedMarineMaximalDistanceOfDetection)
   --    for _, marine in ipairs(marine_around) do
   --      if (marine and marine:GetIsAlive()) then
   --         if (marine_orig == nil) then
   --            marine_orig = marine:GetOrigin()
   --            closestMarine = marine
   --         end
   --         tmp = alien_orig:GetDistanceTo(marine:GetOrigin())
   --         if (tmp < distance) then
   --            marine_orig = marine:GetOrigin()
   --            distance = tmp
   --            closestMarine = marine
   --         end
   --      end
   --    end
   --    return marine_orig, closestMarine
   -- end

   -- -- Return the cloak amount of the Faded (from 0 to 1)
   -- local function _getCloakFraction(marine, marine_orig, alien_orig)
   --    local kFadedCloakFraction = kFadedBaseCloakFraction
   --    local kFadedClossestMarine = marine_orig:GetDistanceTo(alien_orig)

   --    local sanity = Clamp(marine:GetMentalHealth() + kFadedMinMissingSanityForMalus, kFadedMinViewQuality, 100)
   --    local max_view_distance = kFadedMarineMaximalDistanceOfDetection * (sanity / 100)
   --    -- Get the % of cloak
   --    if kFadedClossestMarine <= max_view_distance then
   --      local actual_percent = max_view_distance - kFadedClossestMarine
   --      local max_percent = max_view_distance - kFadedMarineMaximalDetectionDistance
   --      kFadedCloakFraction = (actual_percent)/(max_percent) * kFadedMinimalCloak
   --    end
   --    return (kFadedCloakFraction)
   -- end

   -- -- Get the cloak Fraction making it transparent at long range
   -- -- Return a value between 0 (full cloak) and 1 (visible)
   -- function Alien:GetCloakFraction()

   --    local kFadedClossestMarine = nil
   --    -- Default: Invisible
   --    local kFadedCloakFraction = 0
   --    -- The local player, not the alien (to update camo client side)
   --    local player = Client.GetLocalPlayer()
   --    local alien_orig = self:GetOrigin()
   --    local marine_orig = player:GetOrigin()

   --    if (player:GetTeamNumber() == 1 and player:isa("Marine"))
   --    then
   --      -- Local camo as seen by the marine or the local alien
   --      kFadedCloakFraction = _getCloakFraction(player, marine_orig, alien_orig)
   --    elseif (player:GetId() == self:GetId())
   --    then
   --      -- Camo as seen by the local Faded player (the visibility GUI)
   --      marine_orig, marine = _getClosestMarine(alien_orig)
   --      if (marine_orig) then
   --         kFadedCloakFraction = _getCloakFraction(marine, marine_orig, alien_orig)
   --      end
   --    else
   --      -- Local camo as seen by the others alien
   --      kFadedCloakFraction = 0.9
   --    end
   --    return kFadedCloakFraction
   -- end -- !function Alien:GetCloakFration()
end -- !Client
