// ===================== Faded Mod =====================
//
// lua\FadedAmbientSound.lua
//
//    Created by: Rio (rio@myrio.de)
//    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

local random = math.random

-- Sound list
local Sounds =
   {
      ["AmbientMusic"] = { -- Long Ambiant sound
         { sound = "sound/faded.fev/ambient/clank", length = 12382, volume = 0.4},
         { sound = "sound/faded.fev/ambient/alien_noise", length = 11415, volume = 0.4},
         { sound = "sound/faded.fev/ambient/alien_noise2", length = 26409, volume = 0.4},
         { sound = "sound/faded.fev/ambient/alien_noise2", length = 26409, volume = 0.4},
         { sound = "sound/faded.fev/ambient/horror_ambient", length = 38243, volume = 0.4},
      },
      ["AmbientEffects"] = { -- Effect short Ambiant sound
         { sound = "sound/NS2.fev/ambient/atmospheric clanks", length = 5000, volume = 1},
         { sound = "sound/NS2.fev/ambient/alien flavour", length = 5000, volume = 1},
      },
      ["FadeApproach"] = { -- Fade approach sound
         { sound = "sound/faded.fev/ambient/whisper_tail1", length = 4234, volume = 1},
         { sound = "sound/faded.fev/ambient/whisper_tail2", length = 3720, volume = 1},
         { sound = "sound/faded.fev/ambient/whisper_tail3", length = 5605, volume = 1},
      },
      ["AloneMarine"] = { -- When the marine is alone
         { sound = "sound/NS2.fev/alien/onos/idle", length = 5000, volume = 0.4},
      },
      ["LastMarine"] = { -- When the marine is alone
         { sound = "sound/NS2.fev/music/main_menu", length = 12000, volume = 1},
      },
      ["Others"] = { -- Uncommon NS2 sound, to be sure (used in other files)
         { sound = "sound/NS2.fev/alien/structures/death_large"},
         { sound = "sound/NS2.fev/alien/structures/healing_mound_heal"},
      },
   }

for _, list in pairs(Sounds) do
   for _, ambientSound in pairs(list) do
      Client.PrecacheLocalSound(ambientSound.sound)
   end
end

-- How much we have to play sound
local SoundInfo =
   {
      ["AmbientMusic"] = {played = 0, sound = nil, interval = 20},
      ["AmbientEffects"] = {played = 0, sound = nil, interval = 50},
      ["FadeApproach"] = {played = 0, sound = nil, interval = 4},
      ["AloneMarine"] = {played = 0, sound = nil, interval = 45},
      ["LastMarine"] = {played = 0, sound = nil, interval = 10},
   }

function kFadedResetSoundTime()
   for _, info in pairs(SoundInfo) do
      info.played = 0
      info.sound = nil
   end
end


-- Play a "fake" comming sound
function    ambiantPlaySound(player, id)
   if (id and SoundInfo[id] and Sounds[id]) then
      if (Shared.GetTime() - SoundInfo[id].played >= SoundInfo[id].interval)
      then
         local s = Sounds[id][random(1, #Sounds[id])]

         SoundInfo[id].sound = s.sound
         SoundInfo[id].played = Shared.GetTime() + s.length / 1000
         -- if (soundType == "Effect") then
         --    StartSoundEffect(s.sound)
         -- else

         Shared.PlaySound(player, s.sound, s.volume)
         -- end
      end
   else
      Print("Faded Warning: Ambiant sound '" .. tostring(id) .. "' not correctly loaded")
   end
end

-- Play a "fake" comming sound
function    MentalHealthEffect:kFadedPlayRandomSoundApproach(player)
   ambiantPlaySound(player, "FadeApproach")
end
-- Play ambient long sound
function    MentalHealthEffect:kFadedPlayRandomBackgroundSound(player)
   if (kFadedAmbiantSound) then
      ambiantPlaySound(player, "AmbientMusic")
   end
end
-- Sound when the marine is alone and there are no marines around
local function    kFadedPlayCompletelyAlone(player)
   if (kFadedAmbiantSound) then
      ambiantPlaySound(player, "AloneMarine")
   end
end
-- Short ambient effects
local function    kFadedPlayAmbiantEffects(player)
   ambiantPlaySound(player, "AmbientEffects")
end

-- Play ambient sound if with friend, if alone or last marine change music
local function chooseAmbiantSound(player)
   local marine_alive = 0
   local marine_around = GetEntitiesWithinRange("Marine",
                                                player:GetOrigin(),
                                                100)
   for _, marine in ipairs(marine_around) do
      if (marine and marine:GetIsAlive()) then
         marine_alive = marine_alive + 1
      end
   end
   if (marine_alive > 1) then
      MentalHealthEffect:kFadedPlayRandomBackgroundSound(player)
   else
      kFadedPlayCompletelyAlone(player)
   end
   kFadedPlayAmbiantEffects(player)
end

function FadedAmbiantSound(player, deltaTime)
   if (not player) then return end
   -- Print("FadedAmbiantSound")
   // Somehow the sound does still play if we have turned the volume to min
   if (OptionsDialogUI_GetSoundVolume() == 0) then return end

   -- if (kFadedAmbiantSound) then
      chooseAmbiantSound(player)
   -- end

   // Sound when the fade is close to the marine (see kFadedModDistortionRadius in FadedFlobals.lua)
   if (kFadedSoundIfFadeNearby and player:isa("Marine")) then
      -- player:TriggerEffects("shade_ink")
      -- player:TriggerEffects("tunnel_enter_2D")
      -- player:TriggerEffects("disorient_loop")
      player:ApplyMentalHealthEffect(deltaTime)

      local nearbyFade = GetEntitiesWithinRange("Fade", player:GetOrigin(), kFadedModDistortionRadius)
      for _, faded in ipairs(nearbyFade) do
         -- Do not play sound for dead faded
         if (faded and faded:GetIsAlive() == true) then
            MentalHealthEffect:kFadedPlayRandomSoundApproach(player)
         end
      end
   end
end

-- Définir une variable "volume original"
originalFunction = Class_ReplaceMethod( "AmbientSound", "OnLoad",
                                        function(self)
                                           originalFunction(self)
                                           self.volumeOriginal = self.volume
                                        end
)

-- Supprimer les sons d'ambiances
originalFunction2 = Class_ReplaceMethod( "AmbientSound", "StartPlaying",
                                         function(self)
                                            self.volume = self.volumeOriginal * 0.1

                                            -- originalFunction2(self)
                                         end
)
