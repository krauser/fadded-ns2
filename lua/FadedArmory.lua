// ===================== Faded Mod =====================
//
// lua\FadedArmsLab.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
//
// ===================== Faded Mod =====================

local locale = LibCache:GetLibrary("LibLocales-1.0")
local strformat = string.format

Script.Load("lua/Armory.lua")

local randomWeap = {
   LayMines.kMapName,
   -- LayFlameMines.kMapName,
   Rifle.kMapName,
   Shotgun.kMapName,
   -- HeavyShotgun.kMapName,
   Welder.kMapName,
   GrenadeLauncher.kMapName,
   Flamethrower.kMapName,
}
local randomSupply = {
   AmmoPack.kMapName,
   MedPack.kMapName,
}

if (Server) then
   local armoryResupplyPlayer = Armory.ResupplyPlayer
   local last_warning = 0
   function Armory:ResupplyPlayer(player)
      if (player and player:isa("Marine")) then
         if (player:GetArmoryUseLeft() > 0) then
            armoryResupplyPlayer(self, player)
            player:DecArmoryUseLeft()
            player:FadedMessage(strformat(
                                   locale:ResolveString("FADED_ARMORY_USE_LEFT"),
                                   player:GetArmoryUseLeft()))
            -- else
            --    if (last_warning < Shared.GetTime()) then
            --       player:FadedMessage(strformat(
            --                   locale:ResolveString("FADED_ARMORY_USE_NO_LEFT"),
            --                   player:GetArmoryUseLeft()))
            --       last_warning = Shared.GetTime() + 1
            --    end
         end
      end
   end
end


function Armory:GetItemList(forPlayer)
   local itemList = {
      kTechId.LayMines,
      -- kTechId.LayFlameMines,
      kTechId.Rifle,
      kTechId.Shotgun,
      kTechId.Welder,
      kTechId.ClusterGrenade,
      -- kTechId.GasGrenade,
      kTechId.PulseGrenade,
      -- kTechId.NapalmGrenade, -- Not working for now
      kTechId.GrenadeLauncher,
      kTechId.Flamethrower,
      -- kTechId.MedPack,
   }

   return itemList
end

-- Denie the legacy buy menu
function Armory:GetCanBeUsed(player, useSuccessTable)
   useSuccessTable.useSuccess = kFadedCanMarinesBuyWeapons
end

-- Check for Sentry Battery and power up if one is around
local armoryOnUpdate = Armory.OnUpdate
function Armory:OnUpdate(deltaTime)
   armoryOnUpdate(self, deltaTime)
   local oldPowerStatue = self:GetIsPowered()
   local bState = buildingCheckForBattery(self)

   if (oldPowerStatue == false and bState == true) then
      local team1 = GetGamerules():GetTeam(kTeam1Index)

      local i = 0
      local orig = self:GetOrigin()
      -- orig = orig + Vector(0, 0.2, 0) -- A bit above
      local numMarine = GetGamerules():GetTeam(kTeam1Index):GetNumPlayers()
      local nbDrop = numMarine * kFadedArmorySupplyAmount
      while (numMarine and i < nbDrop)
      do
     local max_range = 5
     local extents = GetExtents(kTechId.Armory)
     local entity = nil

     -- Team.RespawnPlayer(player:GetTeam(), player, orig, Angles(0, 0, 0)
     local spawnPoint = GetRandomBuildPosition(kTechId.Armory,
                           orig,
                           max_range)
     if spawnPoint then
        local randSupply = math.random(1, table.getn(randomSupply))
        local randWeap = math.random(1, table.getn(randomWeap))
        if (kFadedArmoryAllowWeapon and math.random(0, 9) < 3) then
           entity = CreateEntity(randomWeap[randWeap], spawnPoint, 1)
        elseif (kFadedArmoryAllowSupply) then
           entity = CreateEntity(randomSupply[randSupply], spawnPoint, 1)
        end
     end
     i = i + 1

      end
   end
end

function Armory:OnTakeDamage(damage, attacker, doer, point, direction, damageType, preventAlert)
   BuildingOnTakeDamage(self, damage, attacker, doer, point, direction, damageType, preventAlert)
end
