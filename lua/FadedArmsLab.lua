// ===================== Faded Mod =====================
//
// lua\FadedArmsLab.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
//
// ===================== Faded Mod =====================

Script.Load("lua/ArmsLab.lua")

-- Check for Sentry Battery and power up if one is around
local armsLabOnUpdate = ArmsLab.OnUpdate
function ArmsLab:OnUpdate(deltaTime)
   armsLabOnUpdate(self, deltaTime)
   local oldPowerStatue = self:GetIsPowered()
   local bState = buildingCheckForBattery(self)

   if (oldPowerStatue == false and bState == true) then
      local marinetechtree = GetTechTree(kTeam1Index)
      local team1 = GetGamerules():GetTeam(kTeam1Index)
      local upgrades = {}
      if (kFadedArmslabGiveWeapons1) then
     table.insert(upgrades, kTechId.Weapons1)
      end
      if (kFadedArmslabGiveArmor1) then
     table.insert(upgrades, kTechId.Armor1)
      end

      for _, up in ipairs(upgrades) do
     marinetechtree:GetTechNode(up):SetResearched(true)
     marinetechtree:QueueOnResearchComplete(up, self)
      end
   end
end

function ArmsLab:OnTakeDamage(damage, attacker, doer, point, direction, damageType, preventAlert)
   BuildingOnTakeDamage(self, damage, attacker, doer, point, direction, damageType, preventAlert)
end
