// ===================== Faded Mod =====================
//
// lua\FadedBabblerAbility.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

Script.Load("lua/Weapons/Alien/LeapMixin.lua")
Script.Load("lua/Weapons/Alien/BabblerPheromone.lua")
Script.Load("lua/Weapons/Alien/BabblerAbility.lua")

local networkVars =
{
}

AddMixinNetworkVars(LeapMixin, networkVars)

local babblerAbilityOnCreate = BabblerAbility.OnCreate
function BabblerAbility:OnCreate()
   babblerAbilityOnCreate(self)
   InitMixin(self, LeapMixin)
end

function BabblerAbility:GetSecondaryTechId()
    return kTechId.Leap
end

function BabblerAbility:GetHealthCost()
   return kFadedBabblerAbilityHealthCost
end

local babblerAbilityOnPrimaryAttack = BabblerAbility.OnPrimaryAttack
function BabblerAbility:OnPrimaryAttack(player)
   self.primaryAttacking = false
   if (Server) then
      for _, pheromone in ientitylist(Shared.GetEntitiesWithClassname("BabblerPheromone")) do
         if pheromone:GetOwner() == player then
            return
         end
      end

      if (player:GetHealth() > self:GetHealthCost()) then
         babblerAbilityOnPrimaryAttack(self, player)
         player:DeductHealth(self:GetHealthCost(), player, self)
      end
   end
end



if (Server) then
   local babblerPheromoneOnUpdate = BabblerPheromone.OnUpdate
   function BabblerPheromone:OnUpdate(deltaTime)
      self:CreatePhysics()
      if (self.physicsBody ~= nil) then
     babblerPheromoneOnUpdate(self, deltaTime)
      end
   end

   -- Do not destroy the ball on hit
   function BabblerPheromone:ProcessHit(entity)
   end

   local babblerPheromoneTimeUp = BabblerPheromone.TimeUp
   function BabblerPheromone:TimeUp()
      if (Server) then
     local nb = kFadedBabblerAbilityNumBabbler
     local bb_player = 0
     bb_player = GetGamerules():spawnBabblerPlayers(self:GetOrigin(), nb)
     nb = nb - bb_player -- How many babbler we must spawn
     for i = 1, nb do
        CreateEntity(Babbler.kMapName, self:GetOrigin(), 2)
     end
      end
      babblerPheromoneTimeUp(self)
   end
   Shared.LinkClassToMap("BabblerPheromone", BabblerPheromone.kMapName, {})
end


Shared.LinkClassToMap("BabblerAbility", BabblerAbility.kMapName, {})
