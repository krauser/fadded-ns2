-- / 4===================== Faded Mod =====================
--
-- lua/FadedBabblerEgg.lua
--
--    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

if (Server) then

   local babblerMoveRandom = Babbler.MoveRandom
   function Babbler:MoveRandom()
      if (self.camperId) then
         local marine = Shared.GetEntity(self.camperId)
         if (marine) then
            self:SetMoveType(kBabblerMoveType.Attack, marine, marine:GetOrigin())
            return self:GetIsAlive()
         else
            self:Kill()
            return false -- Kill anticamping babbler if target is too far or dead
         end
      end
      -- Regular babbler
      return babblerMoveRandom(self)
   end

   function BabblerEgg:SetCamperInfo(player)
      if (player and player:isa("Player") and player:GetIsAlive()) then
         self.camperId = player:GetId()
      end
   end

   local babblerEggOnConstructionComplete = BabblerEgg.OnConstructionComplete
   function BabblerEgg:OnConstructionComplete()
      if (babblerEggOnConstructionComplete) then
         babblerEggOnConstructionComplete(self)
         local marine = Shared.GetEntity(self.camperId)
         if (marine) then
            for _, babblerId in ipairs(self.trackingBabblerId) do

               local babbler = Shared.GetEntity(babblerId)
               if babbler then
                  babbler.camperId = self.camperId
               end


            end
         end

      end
   end

end -- Server
