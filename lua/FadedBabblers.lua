// ===================== Faded Mod =====================
//
// lua\FadedBabblers.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

local networkVars =
   {
      forPlayer = "integer",
   }


if (Server) then
   function Babbler:TimeUp()

      if (not self:GetOwner())
      then
         self:TriggerEffects("death", {effecthostcoords = Coords.GetTranslation(self:GetOrigin()) })
         DestroyEntity(self)
      end

   end
end

local onCreate = Babbler.OnCreate
function Babbler:OnCreate()
   onCreate(self)
   self.forPlayer = nil
   if (kFadedHallucinationMarineId) then
      self.forPlayer = kFadedHallucinationMarineId
      self:SetPropagate(Entity.Propagate_Callback)
   end
   if (Server) then
      -- local marines_building = GetEntitiesWithMixinWithinRange(
      --    "PowerConsumer", self:GetOrigin(), 15)
      -- if (#marines_building > 0) then
     self:AddTimedCallback(Babbler.TimeUp, kBabblersLifetime)
      -- else -- Babblers only stay 20s in the main
      --      self:AddTimedCallback(Babbler.TimeUp, kBabblersLifetime / 3)
      -- end
   end
   -- InitMixin(self, PlayerHallucinationMixin)
end

local onInit = Babbler.OnInitialized
function Babbler:OnInitialized()
   onInit(self)
end

function Babbler:GetBaseViewAngles()
   return (Angles(0,0,0))
end

function Babbler:OnGetIsRelevant(player)
   -- Allow or not the Faded to see marines hallucination
   if (kFadedAllowFadedToSeeHallucination) then
      if (player:GetTeamNumber() == 2) then
         return (true)
      end
   end
   return (self.forPlayer == player:GetId())
end

if (Server) then

   -- local babblerMove = Babbler.Move
   function Babbler:Move(targetPos, deltaTime)

      self:SetGroundMoveType(true)

      local prevY = self:GetOrigin().y
      local moveSpeed = 7
      if (self:GetOwner()) then
         moveSpeed = 7 + Clamp(targetPos:GetDistanceTo(self:GetOrigin()), 0, 7)
      end
      local done = self:MoveToTarget(PhysicsMask.AIMovement, targetPos, moveSpeed, deltaTime)

      local newOrigin = self:GetOrigin()
      local desiredY = newOrigin.y + Babbler.kRadius
      newOrigin.y = Slerp(prevY, desiredY, deltaTime * 3)

      self:SetOrigin(newOrigin)
      self.targetSelector:AttackerMoved()

      return done

   end

    local function GetBabblerBall(self)

        for _, ball in ipairs(GetEntitiesForTeamWithinRange("BabblerPheromone", self:GetTeamNumber(), self:GetOrigin(), 20)) do

            if ball:GetOwner() == self:GetOwner() and (ball:GetOrigin() - self:GetOrigin()):GetLength() > 4 then
                return ball
            end

        end

    end

    local function FindSomethingInteresting(self)

        PROFILE("Babbler:FindSomethingInteresting")

        local origin = self:GetOrigin()
        local searchRange = 7
        local targetPos = nil
        local randomTarget = self:GetOrigin() + Vector(math.random() * 4 - 2, 0, math.random() * 4 - 2)

        if math.random() < 0.2 then
            targetPos = randomTarget
        else

            local babblerBall = GetBabblerBall(self)

            if babblerBall then
                targetPos = babblerBall:GetOrigin()
            else

                local interestingTargets = { }
                table.copy(GetEntitiesWithMixinForTeamWithinRange("Live", self:GetTeamNumber(), origin, searchRange), interestingTargets, true)
                -- cysts are very attractive, they remind us of the ball we like to catch!
                table.copy(GetEntitiesForTeamWithinRange("Cyst", self:GetTeamNumber(), origin, searchRange), interestingTargets, true)

                local numTargets = #interestingTargets
                if numTargets > 1 then
                    targetPos = interestingTargets[math.random (1, numTargets)]:GetOrigin()
                elseif numTargets == 1 then
                    targetPos = interestingTargets[1]:GetOrigin()
                else
                    targetPos = randomTarget
                end

            end

        end

        return targetPos

    end


   function Babbler:MoveRandom()

      PROFILE("Babbler:MoveRandom")

      -- if self.moveType == kBabblerMoveType.None and self:GetIsOnGround() then
      if self:GetIsOnGround() then

         -- check for targets to attack
         local target = self.targetSelector:AcquireTarget()
         local owner = self:GetOwner()
         local activeWeapon = owner ~= nil and owner:GetActiveWeapon()
         local ownerOrigin = owner and ( not owner:isa("Commander") and owner:GetOrigin() or owner.lastGroundOrigin )

         local marines = 0
         if (ownerOrigin) then
            marines = GetEntitiesForTeamWithinRange("Player", 1, ownerOrigin, 8)
         end
         local follow_me = owner and ((owner:GetVelocity():GetLength() > 0.1
                                          and #marines == 0)
                                         or (ownerOrigin - self:GetOrigin()):GetLength() > 8)
         if owner and ownerOrigin
            and follow_me then
            -- mama gorge is too far away, hurry back to the warm glowy belly!
            -- self:SetMoveType(kBabblerMoveType.Move, nil, ownerOrigin)
            self.moveType = kBabblerMoveType.Move
            self.targetId = nil
            -- self.targetPosition = ownerOrigin
            local aheadDist = math.random(2.2, 5)
            self.targetPosition = owner:GetEyePos() + owner:GetViewAngles():GetCoords().zAxis * aheadDist

         elseif target then
            self:SetMoveType(kBabblerMoveType.Attack, target, target:GetOrigin())

         else

            -- nothing to do, find something "interesting" (maybe glowing)
            local targetPos = FindSomethingInteresting(self)

            if targetPos then
               self:SetMoveType(kBabblerMoveType.Move, nil, targetPos)
            end

         end

         -- jump randomly
         if not self:GetOwner() and math.random() < 0.1 then
            self:JumpRandom()
         end

      end

      if (self:GetIsAlive()) then
         self:AddTimedCallback(Babbler.MoveRandom, 0.05)
      end
      return false

   end

end -- Server

Shared.LinkClassToMap("Babbler", Babbler.kMapName, networkVars)
