// ===================== Faded Mod =====================
//
// lua\FadedBuildingShared.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// This files is used to stored shared function between all objective mod building
   //
   // ===================== Faded Mod =====================

   local locale = LibCache:GetLibrary("LibLocales-1.0")
   local strformat = string.format

   function BuildingOnTakeDamage(self, damage, attacker, doer, point, direction, damageType, preventAlert)
      self:ActivateNanoShield()
      if (Server and attacker and attacker:isa("Babbler")) then
         DestroyEntity(attacker)
      end
   end


   if (Server) then

      function FadedSpawnBuilding(techId, mapName, orig, coord_fix, max_range, force)
         if (max_range == nil) then
            max_range = 15
         end

         local extents = GetExtents(techId)
         local struct = nil
         local spawnPoint = nil
         if (force ~= true) then
            local i = 0
            while (i < 10 and not spawnPoint) do
               spawnPoint = GetRandomBuildPosition(techId,
                                                   orig,
                                                   max_range)
               orig = orig + Vector(0.1, 0, 0.1) -- so the coord are not "exactly" the same
               i = i + 1
            end
         else
            spawnPoint = orig
         end

         if spawnPoint then
            spawnPoint = spawnPoint + coord_fix
            struct = CreateEntity(mapName, spawnPoint, 1)
            if (struct) then
               SetRandomOrientation(struct)
               struct:SetConstructionComplete()
            end
         end
         return struct
      end

      function enableObjSecondPart()
         local isAllBuildingPowered = true
         local marine_building = GetEntitiesWithMixinForTeam("PowerConsumer", 1)

         for _, building in ipairs(marine_building) do
            if (building and building:GetIsPowered() == false
                   and (building:isa("ArmsLab")
                           or building:isa("Armory")
                           or building:isa("AdvancedArmory")
                           or building:isa("Observatory")
                        or building:isa("RoboticsFactory"))) then
               isAllBuildingPowered = false
               break
            end
         end
         if (isAllBuildingPowered) then
            if (#GetEntitiesForTeam("PrototypeLab", 1) == 0) then
               local proto = FadedSpawnBuilding(kTechId.PrototypeLab, PrototypeLab.kMapName, fadedMarinesMainBaseCoord, Vector(0, -0.6, 0))
               if (proto) then
                  Shared:FadedMessage(locale:ResolveString("FADED_PROTO_LABS_DROPPED"))
               end
            end
         end
      end
   end


   local function _givePointsToMarinesAround(self)
      for _, marine in ipairs(GetEntitiesForTeamWithinRange("Marine", 1,
                                                            self:GetOrigin(),
                                                            10))
      do
         if (marine and marine:GetIsAlive()) then
            marine:AddScore(10) -- Score added for a powerup
         end
      end
   end

   local kPowerSound = PrecacheAsset("sound/NS2.fev/marine/structures/power_up")
   -- Check if a sentry battery is around and powerOn or powerOff the structure
   function buildingCheckForBattery(self)
      local time = Shared.GetTime()

      if (self.lastBatteryCheckTime == nil
             or (time > self.lastBatteryCheckTime + 0.5))
      then
         -- Update if we're powered or not
         self.attachedToBattery = false

         local ents = GetEntitiesWithinRange("SentryBattery", self:GetOrigin(), SentryBattery.kRange)
         for index, ent in ipairs(ents) do

            if (GetIsUnitActive(ent)
                   and ent:GetLocationName() == self:GetLocationName()
                   and ent:isAttachedToBuildingName() == EntityToString(self))
            then
               self.attachedToBattery = true
               break
            end
         end
         self.lastBatteryCheckTime = time
         if (Server) then
            if (self.attachedToBattery) then
               if (self:GetIsPowered() == false) then -- No need to spam
                  Shared:FadedMessage(
                     strformat(
                        locale:ResolveString("FADED_BUILDING_POWERED"),
                        EntityToString(self)))
                  _givePointsToMarinesAround(self)

                  self:SetPowerOn()
                  StartSoundEffectAtOrigin(kPowerSound, self:GetOrigin())
                  if (kFadedSpawnBatteryOnBuildingPowerUp) then
                     GetGamerules():SpawnSentryBatteryOnMap(true)
                  end
                  enableObjSecondPart()
               end
               return (true)
            else
               self:SetPowerOff()
            end
         end
      end
      return (false)
   end
