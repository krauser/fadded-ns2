// ===================== Faded Mod =====================
//
// lua\FadedChatCommands.lua
//
//    Created by: Rio (rio@myrio.de)
//    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

-- Print("FadedChatCommands Start")
local locale = LibCache:GetLibrary("LibLocales-1.0")

FadedChatCommands = {
    chatCommands = {}
}

local kChatsPerSecondAdded = 1
local kMaxChatsInBucket = 5
local function CheckChatAllowed(client)

    client.chatTokenBucket = client.chatTokenBucket or CreateTokenBucket(kChatsPerSecondAdded, kMaxChatsInBucket)
    // Returns true if there was a token to remove.
    return client.chatTokenBucket:RemoveTokens(1)

end

local function GetChatPlayerData(client)

    local playerName = "Admin"
    local playerLocationId = -1
    local playerTeamNumber = kTeamReadyRoom
    local playerTeamType = kNeutralTeamType

    if client then

        local player = client:GetControllingPlayer()
        if not player then
            return
        end
        playerName = player:GetName()
        playerLocationId = player.locationId
        playerTeamNumber = player:GetTeamNumber()
        playerTeamType = player:GetTeamType()

    end

    return playerName, playerLocationId, playerTeamNumber, playerTeamType

end

local function DetaultOnChatReceived(client, message)

    if not CheckChatAllowed(client) then
        return
    end

    chatMessage = string.sub(message.message, 1, kMaxChatLength)
    if chatMessage and string.len(chatMessage) > 0 then

        local playerName, playerLocationId, playerTeamNumber, playerTeamType = GetChatPlayerData(client)

        if playerName then

            if message.teamOnly then

                local players = GetEntitiesForTeam("Player", playerTeamNumber)
                for index, player in ipairs(players) do
                    Server.SendNetworkMessage(player, "Chat", BuildChatMessage(true, playerName, playerLocationId, playerTeamNumber, playerTeamType, chatMessage), true)
                end

            else
                Server.SendNetworkMessage("Chat", BuildChatMessage(false, playerName, playerLocationId, playerTeamNumber, playerTeamType, chatMessage), true)
            end

            Shared.Message("Chat " .. (message.teamOnly and "Team - " or "All - ") .. playerName .. ": " .. chatMessage)

            // We save a history of chat messages received on the Server.
            Server.AddChatToHistory(chatMessage, playerName, client:GetUserId(), playerTeamNumber, message.teamOnly)

        end

    end

end

function FadedChatCommands:RegisterChatCommand(command, commandFunction)
    self.chatCommands[command] = commandFunction
end

function FadedChatCommands:TriggerChatCommand(chatMessage, player)
    local processed = false

    for command, commandFunction in pairs(self.chatCommands) do
        if (command:upper() == chatMessage:upper()) then
            commandFunction(player)
            processed = true
        end
    end

    return processed
end

local function OnChatReceived(client, message)
    chatMessage = string.sub(message.message, 1, kMaxChatLength)
    if (chatMessage and string.len(chatMessage) > 0) then
        local player = client:GetControllingPlayer()
        if (player) then
            local processed = FadedChatCommands:TriggerChatCommand(chatMessage, player)
            if (not processed) then
                DetaultOnChatReceived(client, message)
            end
        end
    end
end

// Friendly fire status command
FadedChatCommands:RegisterChatCommand("ff", function(player)
    if (kFadedModFriendlyFireEnabled) then
        player:FadedMessage(locale:ResolveString("FADED_CHAT_COMMANDS_FF_ENABLED"))
    else
        player:FadedMessage(locale:ResolveString("FADED_CHAT_COMMANDS_FF_DISABLED"))
    end
end)

-- No Faded command, the player wont be a faded
NS2Gamerules.noFaded = {}
function NoFadedChatCommand(player)
    local clientIndex = player:GetClientIndex()
    NS2Gamerules.noFaded[clientIndex] = not NS2Gamerules.noFaded[clientIndex] and true or false

    if (player and player.FadedMessage) then
       if (not NS2Gamerules.noFaded[clientIndex]) then
      player:FadedMessage(locale:ResolveString("FADED_NO_FADED_FALSE"))
       else
      player:FadedMessage(locale:ResolveString("FADED_NO_FADED_TRUE"))
       end
    end
end

function TogglerBeeperChatCommand(player)
   if (Player) then
      Server.SendNetworkMessage(player, "ToggleBeeper", {}, true)
   end
end

-- Print to the player a list of hint for the Faded
local function fadedhelp(player)
    local teamNumber = player:GetTeamNumber()
    local marine_list_msg =
       {
      "FADED_HELP_MESSAGE_MARINE_1",
      "FADED_HELP_MESSAGE_MARINE_2",
      "FADED_HELP_MESSAGE_MARINE_3",
       }
    local faded_list_msg =
       {
      "FADED_HELP_MESSAGE_FADED_1",
      "FADED_HELP_MESSAGE_FADED_2",
      "FADED_HELP_MESSAGE_FADED_3",
       }

    if (teamNumber == 1) then
       for k, v in pairs(marine_list_msg) do
      player:FadedMessage(locale:ResolveString(v))
       end
    elseif (teamNumber == 2) then
       for k, v in pairs(faded_list_msg) do
      player:FadedMessage(locale:ResolveString(v))
       end
    end

    player:FadedMessage(locale:ResolveString("FADED_HELP_MESSAGE_1"))
end

local function unstuck(player)
   SpawnSinglePlayer(player, player:GetOrigin())
   player:FadedMessage(locale:ResolveString("FADED_UNSTUCK"))
end

-- Print to the player a list of hint for the Marine
local function marinehelp(player)
   local teamNumber = player:GetTeamNumber()
   local list_msg =
      {
     "FADED_HELP_MESSAGE_MARINE_4",
     "FADED_HELP_MESSAGE_MARINE_5",
     "FADED_HELP_MESSAGE_MARINE_6",
      }
   if (teamNumber == 1) then
      for k, v in pairs(list_msg) do
     player:FadedMessage(locale:ResolveString(v))
      end
   end
end

function SpectateFadedChatCommand(player, force_remove)
   if (spectateFadedPlayers[player:GetName()]) then
      player:FadedMessage("You have been removed from the Faded spectator list")
      spectateFadedPlayers[player:GetName()] = nil
   else
      if (force_remove ~= true) then
     player:FadedMessage("You are considered as a Faded spectator")
     spectateFadedPlayers[player:GetName()] = true
      end
   end
end

function BabblerPlayerChatCommand(player, no_remove, force_remove)
   local k = -1
   for it, player_name in ipairs(babblerPlayers) do
      if (player_name == player:GetName()) then
     k = it
     break
      end
   end

   if (k ~= -1) then
      if (no_remove ~= true) then
     player:FadedMessage("You have been removed from the babbler list")
     table.remove(babblerPlayers, k)
      end
   else
      if (force_remove ~= true) then
     player:FadedMessage("You are now a babbler player")
     table.insert(babblerPlayers, player:GetName())
      end
   end
end

local discomusic = PrecacheAsset("sound/NS2.fev/ambient/descent/club_music")
local function DiscoMusic(player)
   if (Server) then
      local m = Server.CreateEntity(SoundEffect.kMapName)
      m:SetAsset(discomusic)
      m:SetParent(player)
      m:Start()
   end
end

local xmasmusic = PrecacheAsset("sound/NS2.fev/ambient/xmas_music")
local function XMasMusic(player)
   if (Server) then
      local m = Server.CreateEntity(SoundEffect.kMapName)
      m:SetAsset(xmasmusic)
      m:SetParent(player)
      m:Start()
   end
end

local function MoveAllBotToBabbler(player)
   if (Server) then
      for _, p in pairs(GetGamerules():GetTeam1():GetPlayers()) do
     BabblerPlayerChatCommand(p, true)
      end
      for _, p in pairs(GetGamerules():GetTeam2():GetPlayers()) do
         BabblerPlayerChatCommand(p, true)
      end
   end
end

local assault_id = 1
local animation =  PrecacheAsset("models/marine/male/male.animation_graph")

local models = {}
local models_name = {}

models["male"] = {}
models["female"] = {}
models_name["male"] = {}
models_name["female"] = {}

for _, gender in ipairs({"male", "female"})
do
   for _, model in ipairs(
      {
         {"models/marine/", "/", ".model"},
         {"models/marine/", "/", "_assault.model"},
         {"models/marine/", "/", "_kodiak.model"},
         {"models/marine/", "/", "_eliteassault.model"},
         {"models/marine/", "/", "_special.model"},
         {"models/marine/", "/", "_tundra.model"},
         {"models/marine/", "/", "_special_v1.model"},
      })
   do
      local model_path = model[1] .. gender .. model[2] .. gender .. model[3]
      table.insert(models[gender], PrecacheAsset(model_path))
      table.insert(models_name[gender], model_path)
   end
end

saveSkinChoiceModel = {}
saveSkinChoiceAnimation = {}

function reapplySkinChoice(player)
   if (player and player:isa("Marine")
       and saveSkinChoiceModel[player:GetName()]) then
      player:SetModel(saveSkinChoiceModel[player:GetName()],
                      saveSkinChoiceAnimation[player:GetName()])
      player:FadedMessage("You have the " .. saveSkinChoiceModel[player:GetName()]
                             .. " skin set")
   end
end

local function ChangeToBlackMarineSkin(player)
   local gender = "male"

   if (player.GetGenderString and
          (player:GetGenderString() == "male"
              or player:GetGenderString() == "female")) then
      gender = player:GetGenderString()
   end

   assault_id = (assault_id % #models[gender]) + 1
   model = models[gender][assault_id]
   player:SetModel(model, animation)
   player:FadedMessage(models_name[gender][assault_id])
   saveSkinChoiceModel[player:GetName()] = model
   saveSkinChoiceAnimation[player:GetName()] = animation

   -- for _, fromPlayer in ientitylist(allPlayers) do
   --    Server.SendNetworkMessage(fromPlayer, "SetMarineModel",
   --                              {},
   --                              true)
   -- end

end

-- local function BabblerPlayerUpdateHpAp(player)
--    if (Server) then
--       local scale = (player.modelSize * player.modelSize) * 5
--       if (scale > 1) then
--          player:SetMaxHealth(player:GetBaseHealth() * scale)
--          player:SetMaxArmor(player:GetBaseArmor() * scale)
--          player:SetHealth(player:GetBaseHealth() * scale)
--          player:SetArmor(player:GetBaseArmor() * scale)
--       end
--    end
-- end

-- local function BabblerPlayerUpdateHitBox(player)
--    if (player.hitBox) then
--       Shared.DestroyCollisionObject(player.hitBox)
--       player.hitBox = nil
--       player:OnUpdatePhysics()
--    end
-- end

-- local function BabblerPlayerBigger(player)
--    if (player and player:isa("Skulk") and player:GetIsAlive()) then
--       player.modelSize = player.modelSize * 1.5
--       BabblerPlayerUpdateHpAp(player)
--       BabblerPlayerUpdateHitBox(player)
--    end
-- end
-- local function BabblerPlayerSmaller(player)
--    if (player and player:isa("Skulk") and player:GetIsAlive()) then
--       if (player.modelSize > 0.1) then
--          player.modelSize = player.modelSize * 0.75
--          BabblerPlayerUpdateHpAp(player)
--          BabblerPlayerUpdateHitBox(player)
--       end
--    end
-- end

local function ToggleAllTalk(player)
   if (Shared.GetCheatsEnabled()) then
      kFadedModAllTalkEnabled = not kFadedModAllTalkEnabled
      player:FadedMessage("AlltalkEnable")
   end
end

FadedChatCommands:RegisterChatCommand("unstuck", unstuck)

FadedChatCommands:RegisterChatCommand("!nofade", NoFadedChatCommand)
FadedChatCommands:RegisterChatCommand("nofade", NoFadedChatCommand)
FadedChatCommands:RegisterChatCommand("nofaded", NoFadedChatCommand)

// Help
FadedChatCommands:RegisterChatCommand("fadedhelp", fadedhelp)
FadedChatCommands:RegisterChatCommand("fadehelp", fadedhelp)
FadedChatCommands:RegisterChatCommand("marinehelp", marinehelp)

FadedChatCommands:RegisterChatCommand("!spectatefade", SpectateFadedChatCommand)
FadedChatCommands:RegisterChatCommand("spectatefade", SpectateFadedChatCommand)
FadedChatCommands:RegisterChatCommand("spectatefaded", SpectateFadedChatCommand)

FadedChatCommands:RegisterChatCommand("!babblerplayer", BabblerPlayerChatCommand)
FadedChatCommands:RegisterChatCommand("babblerplayer", BabblerPlayerChatCommand)

local function medic_test(player)
   for _, player in ipairs (GetEntitiesForTeam("Marine", 1)) do
      if (math.random(1, 2) == 1) then
         player:PromoteToMedic()
      else
         player:ClearMedicAbility()
      end
   end
end

function spawnSquad(player, nb)
   local i = 0
   if (player) then
      local allBab = Shared.GetEntitiesWithClassname("Babbler")
      for _, bab in ientitylist(allBab) do
         if bab and bab:isa("Babbler") and bab:GetOwner()
         and bab:GetOwner():GetId() == player:GetId()
         then
            bab:Kill()
         end
      end

      while (i < nb) do
         local babbler = CreateEntity(Babbler.kMapName,
                                      player:GetOrigin() + Vector(0, -0.5, 0),
                                      player:GetTeamNumber())
         if (babbler) then
            babbler:SetOwner(player)
         end
         i = i + 1
      end
   end
end

local function spawnBabblerSquad(player)
   if (player) then

      for i = 1, 10 do
         local babbler = CreateEntity(Babbler.kMapName,
                                      player:GetOrigin() + Vector(0, -0.5, 0),
                                      player:GetTeamNumber())
         if (babbler) then
            babbler:SetOwner(player)
         end

      end
   end
end

local function blinkDance(player)
   if (player and player:isa("Fade")) then
      if (player.blinkDance == nil) then
         player.blinkDance = true
      else
         player.blinkDance = not player.blinkDance
      end
      Shared:FadedMessage("Blink dance: " .. tostring(player.blinkDance))
   end
end

FadedChatCommands:RegisterChatCommand("!medic_test", medic_test)

FadedChatCommands:RegisterChatCommand("!allbotbabbler", MoveAllBotToBabbler)
FadedChatCommands:RegisterChatCommand("allbotbabbler", MoveAllBotToBabbler)

FadedChatCommands:RegisterChatCommand("!nofade", NoFadedChatCommand)
FadedChatCommands:RegisterChatCommand("!beeper", TogglerBeeperChatCommand)
FadedChatCommands:RegisterChatCommand("!skin", ChangeToBlackMarineSkin)
FadedChatCommands:RegisterChatCommand("!faded_alltalk", ToggleAllTalk)
FadedChatCommands:RegisterChatCommand("!squad", spawnBabblerSquad)

FadedChatCommands:RegisterChatCommand("!blink", blinkDance)

-- FadedChatCommands:RegisterChatCommand("!b", BabblerPlayerBigger)
-- FadedChatCommands:RegisterChatCommand("!s", BabblerPlayerSmaller)

-- FadedChatCommands:RegisterChatCommand("discomusic", DiscoMusic)
-- FadedChatCommands:RegisterChatCommand("xmasmusic", XMasMusic)

-- FadedChatCommands:RegisterChatCommand("uc", UpCloack)
-- FadedChatCommands:RegisterChatCommand("dc", DecCloack)

-- FadedChatCommands:RegisterChatCommand("uv", UpVisu)
-- FadedChatCommands:RegisterChatCommand("dv", DecVisu)

Server.HookNetworkMessage("ChatClient", OnChatReceived)
-- Print("FadedChatCommands ENd")
