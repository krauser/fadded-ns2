// ===================== Faded Mod =====================
//
// lua\FadedClient.lua
//
//    Created by: Rio (rio@myrio.de)
//
// =====================================================

-- For extra class addition
-- Script.Load("lua/TechData.lua")
-- Script.Load("lua/TechTreeConstants.lua")

// Load the ns2 client script
Script.Load("lua/Client.lua")


// Load faded mod client scripts
Script.Load("lua/FadedShared.lua")
Script.Load("lua/FadedPlayer_Client.lua")

-- Script.Load("lua/FadedHUD.lua")
Script.Load("lua/FadedHelpMixin.lua")
Script.Load("lua/FadedAmbientSound.lua")
Script.Load("lua/FadedUpdateClient.lua")

-- Script.Load("lua/FadedScoreboard.lua")

-- Script.Load("lua/FadedPlayer_Client.lua")

-- -- Do not solve the "third person view" for weapon issue
-- function PlayerUI_GetCrosshairY()

--    local player = Client.GetLocalPlayer()

--    Print("PlayerUI_GetCrosshairY called")
--    if(player and not player:GetIsThirdPerson()) then

--       local weapon = player:GetActiveWeapon()
--       if(weapon ~= nil) then

--      -- Get class name and use to return index
--      local index
--      local mapname = weapon:GetMapName()

--      if mapname == Rifle.kMapName or mapname == HeavyRifle.kMapName then
--         index = 0
--      elseif mapname == Pistol.kMapName then
--         index = 1
--      elseif mapname == Shotgun.kMapName then
--         index = 3
--      elseif mapname == HeavyShotgun.kMapName then
--         Print("PlayerUI_GetCrosshairY called for the HEavy Shotgun")
--         index = 3
--      elseif mapname == Minigun.kMapName then
--         index = 4
--      elseif mapname == Flamethrower.kMapName or mapname == GrenadeLauncher.kMapName then
--         index = 5
--             -- All alien crosshairs are the same for now
--      elseif mapname == LerkBite.kMapName or mapname == Spores.kMapName or mapname == LerkUmbra.kMapName or mapname == Parasite.kMapName
--      or mapname == BileBomb.kMapName then
--         index = 6
--      elseif mapname == SpitSpray.kMapName or mapname == BabblerAbility.kMapName then
--         index = 7
--         -- Blanks (with default damage indicator)
--      else
--         index = 8
--      end

--      return index * 64

--       end

--    end

-- end


GetGUIManager():CreateGUIScript("FadedGUIFeedback")

// Only include the test script in the development version
if (kFadedModVersion:lower():find("development")) then
   Script.Load("lua/FadedTest.lua")
end
