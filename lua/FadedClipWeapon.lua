// ===================== Faded Mod =====================
//
// lua\FadedClipWeapon.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================


if Server then
   Script.Load("lua/Weapons/Weapon.lua")
   Script.Load("lua/Weapons/BulletsMixin.lua")
   Script.Load("lua/Weapons/Marine/ClipWeapon.lua")

   local function CancelReload(self)

      if self:GetIsReloading() then
         self.reloading = false
         if Client then
            self:TriggerEffects("reload_cancel")
         end
         if Server then
            self:TriggerEffects("reload_cancel")
         end
      end
   end

   local originalWeaponDropped
   -- Allow the player to drop a weapon but with no reserve clip
   -- (avoid early game full ammo refill)
   originalWeaponDropped = Class_ReplaceMethod(
      "ClipWeapon", "Dropped",
      function(self, prevOwner)

         Weapon.Dropped(self, prevOwner)
         CancelReload(self)

         local gameTime = GetGamerules():GetGameStartTime()
         if gameTime ~= 0 then
            gameTime = math.floor(Shared.GetTime()) - gameTime
         end

         if (gameTime >= kFadedModTimeInSecondsSelectingEquipmentIsAllowed) then
            return -- Drop ammo if buy time is over
         end

         -- Truncate ammo number to avoid spawn ammo shotgun spam
         if (self:GetAmmo() > 0) then
            self:SetClip(self:GetClipSize())
            -- else -- Keep bullet in clip
         end
         self.ammo = 0

         -- Fake marine does not drop any weapon
         -- A marine who drop his pistol/Mine, lose his pistol/Mine
         if (self:isa("LayMines") or self:isa("Pistol"))
         -- and self:GetHUDSlot() ~= kPrimaryWeaponSlot)
         then
            DestroyEntity(self)
         end
         -- local ammopackMapName = self:GetAmmoPackMapName()
         -- if ammopackMapName and self.ammo ~= 0 then
         --      local ammoPack = CreateEntity(ammopackMapName, self:GetOrigin(), self:GetTeamNumber())
         --      ammoPack:SetAmmoPackSize(self.ammo)
         --      self.ammo = 0
         -- end

      end)

   -- Drop the weapon but not the ammo
   -- function ClipWeapon:Dropped(prevOwner)
   -- end
   -- Shared.LinkClassToMap("ClipWeapon", ClipWeapon.kMapName, {})
end
