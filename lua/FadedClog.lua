Script.Load("lua/InfestationMixin.lua")

-- -- if (Server) then
--    local clogs_to_refresh = {}

--    function clearAllClogsWhip()
--       clogs_to_refresh = {}
--    end

   local function getChildPos(clog)
      local extents = GetExtents(kTechId.Clog)
      local direction = Vector(clog:GetAngles():GetCoords().yAxis)

      return (clog:GetOrigin() - direction * 1.1)
   end

   -- function refreshAllClogWhipPos()
   --    local i = 1
   --    while i <= #clogs_to_refresh do
   --       if (clogs_to_refresh[i]["triggerTime"] < Shared.GetTime()) then
   --          local clog = Shared.GetEntity(clogs_to_refresh[i]["clogId"])
   --          if (clog) then
   --             clog:SyncAttachPos()
   --          end
   --          -- if (clog and clog.clogParent) then
   --          --    local parent = Shared.GetEntity(clog.clogParent)
   --          --    if (parent) then
   --          --       clog:SetOrigin(getChildPos(parent))
   --          --       clog:SetAngles(SlerpAngles(clog:GetAngles(), parent:GetAngles(), 0.5))
   --          --    end
   --          --    -- setAllChildPos(child)
   --          -- end
   --          table.remove(clogs_to_refresh, i)
   --       else
   --          i = i + 1
   --       end
   --    end
   --    return true
   -- end

   -- local function registerAllChildPos(clog, triggerTime)
   --    if (clog and clog.clogChild) then
   --       local new_triggerTime = triggerTime + 2
   --       -- Entity.AddTimedCallback(Shared.GetEntity(clog.clogChild),
   --       --                         setAllChildPos, 1)
   --       -- setAllChildPos()
   --       table.insert(clogs_to_refresh,
   --                    {
   --                       ["triggerTime"] = new_triggerTime,
   --                       ["clogId"] = clog.clogChild
   --                    } )
   --       registerAllChildPos(Shared.GetEntity(clog.clogChild), new_triggerTime)
   --    end
   --    -- if (clog and clog.clogParent) then
   --    --    local parent = Shared.GetEntity(clog.clogParent)
   --    --    clog:SetAngles(parent:GetAngles())
   --    -- end
   -- end

   local function _createClogWhip(parent, nb)
      local new_clog = CreateEntity(Clog.kMapName, getChildPos(parent),
                                    parent:GetTeamNumber())
      if (new_clog) then
         -- new_clog.modelSize = Clamp(parent.modelSize / 1.1, 0.2, 2)
         parent.clogChild = new_clog:GetId()
         new_clog.clogParent = parent:GetId()

         -- new_clog:SetParent(parent)
         -- new_clog:SetAttachPoint("babbler_attach1")
         -- new_clog:SetAttachPoint(parent:GetOrigin() + Vector(0, extents.y, 0))

         -- Shared:FadedMessage("Clog created (left: " .. tostring(nb))
         if (nb > 0) then
            new_clog.moveSpeed = new_clog.moveSpeed * 1.1
            new_clog.xSeed = new_clog.xSeed / 1.1
            new_clog.ySeed = new_clog.ySeed / 1.1
            if (math.random() <= 0.1) then
               new_clog.clogOwnAngle = true
            end
            _createClogWhip(new_clog, nb - 1)
         end
      else
         Shared:FadedMessage("Failed to create Clog")
      end
   end


   function Clog:GetInfestationRadius()
      return (kFadedDeadBodyInfestationRadius)
   end
   function Clog:GetAttached()
      return nil
   end


   function Clog:createClogWhip(nb)
      self.clogRootChildNb = nb
      self.clogRoot = true
      -- _createClogWhip(self, nb) -- Will be called latter on the update
   end

   local function getCosSeed(self, deltaTime, nb_light_on)
      -- return ()
      -- return self.clogCosSeed = Shared.GetTime() - self.angleSeed
      if (not self.clogCosSeed) then
         self.clogCosSeed = Shared.GetTime() - self.angleSeed
      end
      nb_light_on = Clamp(nb_light_on, 0, 2)
      self.clogCosSeed = self.clogCosSeed + deltaTime + nb_light_on / 8
      return self.clogCosSeed
   end
   -- function Clog:UpdatePos(deltaTime, nb_light_on)
   --    if (self.clogInitDone) then
   --       -- Divide to slow down the movement
   --       local seed = getCosSeed(self, deltaTime, nb_light_on) * (self.moveSpeed)
   --       -- Divide to reduce range interval
   --       local range1 = math.cos(seed) * self.xSeed
   --       local range3 = math.sin(seed) * self.ySeed
   --       -- local vector_diff = Vector(0, 0, range)
   --       -- self:SetOrigin(self.clogCreationOrig + vector_diff)

   --       -- SlerpAngles(self:GetAngles(), Angles(0, 0, range), 1)
   --       local parent = nil
   --       if (self.clogParent) then
   --          parent = Shared.GetEntity(self.clogParent)
   --       end
   --       if (not parent or self.clogOwnAngle) then
   --          self:SetAngles(Angles(range1, 0, range3))
   --       else
   --          self:SetAngles(parent:GetAngles())
   --       end
   --       -- registerAllChildPos(self, Shared.GetTime())
   --    end
   --    -- if (self.clogChild) then
   --    --    local child = Shared.GetEntity(self.clogChild)
   --    --    child.clogCreationOrig = self:GetOrigin() + vector_diff
   --    -- end
   -- end

   -- function Clog:OnAdjustModelCoords(modelCoords)
   --    local coords = modelCoords
   --    local scale = self.modelsize
   --    if (scale == nil) then
   --       if (self.clogParent) then
   --       end
   --       scale = 1
   --    end
   --    -- local rand = ((math.random(1, 10) - 5) / 10)
   --    coords.xAxis = coords.xAxis * scale -- Largeur
   --    coords.yAxis = coords.yAxis * scale -- Hauteur
   --    coords.zAxis = coords.zAxis * scale -- Profondeur
   --    return coords
   -- end

   local clogOnCreate = Clog.OnCreate
   function Clog:OnCreate()
      clogOnCreate(self)
      local precision = 100
      -- self.modelSize = 1
      self.clogRoot = false
      local angle = 1
      self.clogRefreshPos = Shared.GetTime()
      self.clogTimePassed = 0
      self.xSeed = angle - math.random()
      self.ySeed = angle - self.xSeed
      self.angleSeed = Shared.GetTime()-- + math.random(1, 100)
      self.moveSpeed = math.random(0.1 * precision, 0.5 * precision) / precision
      -- self.clogPos = math.cos(getCosSeed(self))
      self.clogChild = nil
      self.clogParent = nil
      self.clogInitDone = true
      InitMixin(self, InfestationMixin)

   end

   local clogOnInitialized = Clog.OnInitialized
   function Clog:OnInitialized()

      clogOnInitialized(self)

   end

   -- function Clog:OnUpdate2(deltaTime)
   --    if (self.clogDelta == nil) then
   --       self.clogDelta = FindNearestEntityId("Clog", self:GetOrigin())
   --    end
   --    if (self.clogDelta) then
   --       local clog = Shared.GetEntity(self.clogDelta)
   --       if (clog) then
   --          local direction = Vector(clog:GetAngles():GetCoords().xAxis)

   --          self:SetOrigin(clog:GetOrigin() - direction * 1.2)
   --          local angle = clog:GetAngles()
   --          angle.roll = angle.roll + math.rad(90)
   --          -- angle.pitch = angle.pitch + math.rad(90)
   --          -- angle.yaw = angle.yaw + math.rad(90)
   --          self:SetAngles(angle)
   --       else
   --          self.clogDelta = nil
   --       end
   --    end
   -- end

   // Return entity number or -1 if not found
   function FindNearestPathEntityId(className, location, max_dist)

      local points = nil
      local entityId = -1
      local shortestDistance = nil

      for index, current in ipairs(GetEntitiesWithinRange("Player", location, max_dist))
      -- for index, current in ientitylist(Shared.GetEntitiesWithClassname(className))
      do
         --local distance = 0--(current:GetOrigin() - location):GetLength()
         points = PointArray()
         Pathing.GetPathPoints(location, current:GetOrigin(), points)
         local distance = #points
         if(shortestDistance == nil or distance < shortestDistance) then

            entityId = current:GetId()
            shortestDistance = distance
         end
      end
      return entityId, points

   end


   function Clog:SyncAttachPos(timePassed, nb_light_on)
      if (self.clogParent) then
         -- local is_seen = false
         -- local marines_around = GetEntitiesWithinRange("Player",
         --                                              self:GetOrigin(),
         --                                              40)
         -- for _, m in ipairs(marines_around) do
         --    if (m and m:GetIsAlive()
         --        and not GetWallBetween(self:GetOrigin(), m:GetOrigin())) then
         --       is_seen = true
         --       break
         --    end
         -- end

         local is_seen = true
         if (is_seen) then
            local parent = Shared.GetEntity(self.clogParent)
            -- local extents = GetExtents(kTechId.Clog)

            if (parent)
            then
               local seed = getCosSeed(self, timePassed, nb_light_on) * (self.moveSpeed)
               -- Divide to reduce range interval
               local range1 = math.cos(seed) * self.xSeed
               -- local range3 = math.sin(seed) * self.ySeed

               -- local vector_diff = Vector(0, 0, range)
               -- self:SetOrigin(self.clogCreationOrig + vector_diff)

               -- SlerpAngles(self:GetAngles(), Angles(0, 0, range), 1)

               -- if (self:GetOrigin():GetDistanceTo(getChildPos(parent)) > 1.2)
               -- then
               local new_pos = getChildPos(parent)
               self:SetOrigin(new_pos)
               self:SetAngles(parent:GetAngles())
               local pa = self:GetAngles()
               pa.pitch = pa.pitch + range1
               pa.yaw = pa.yaw + range1
               pa.roll = pa.roll + range1
               self:SetAngles(pa)
               -- self:SetAngles(SlerpAngles(parent:GetAngles(), Angles(-1,0,-1), 0.5))
               -- end
            end
         end

      elseif (self.clogRoot) then
         if (self.clogRootDeleteChildOutLOSOpti == nil) then
            self.clogRootDeleteChildOutLOSOpti = Shared.GetTime()
         end
         if (self.clogRootDeleteChildOutLOSOpti < Shared.GetTime()) then
            self.clogRootDeleteChildOutLOSOpti = Shared.GetTime() + 1
            local clog_create = false
            local clog_destroy = false
            local max_dist = 40
            local id = FindNearestEntityId("Player", self:GetOrigin())
            if (id ~= -1) then
               local player = Shared.GetEntity(id)
               local is_seen = false
               if (player and player:GetIsAlive()
                      and not GetWallBetween(self:GetOrigin(),
                                             player:GetModelOrigin())) then
                  is_seen = true
               end
               local points = PointArray()
               Pathing.GetPathPoints(self:GetOrigin(), player:GetOrigin(), points)

               if (self.clogChild == nil) then
                  if (is_seen or (0 < #points and #points < 80)) then
                     clog_create = true
                  end
               elseif (is_seen == false and (#points >= 85 or #points == 0)) then
                  clog_destroy = true
               end
            end
            if (clog_destroy) then
               local child = Shared.GetEntity(self.clogChild)
               self.clogChild = nil
               child:Kill()
            elseif (clog_create) then
               _createClogWhip(self, self.clogRootChildNb)
            end
         end
      end
   end

   function Clog:OnUpdate(timePassed)
      -- self:OnUpdate2(timePassed)
      -- if (nextRefresh < Shared.GetTime()) then
      --    nextRefresh = Shared.GetTime() + 1

      --    if (self.clogRoot and not self.clogRootInit) then
      --       createClogWhip(self, 8)
      --       self.clogRoot = true
      --       self.clogRootInit = true
      --    end
      -- end

      -- Move faster with flashlight
      -- if (self.nb_light_on == nil) then
      --    self.nb_light_on = 0
      --    self.nb_light_on_last_check = Shared.GetTime()
      -- end
      -- if (self.nb_light_on_last_check < Shared.GetTime()) then
      --    self.nb_light_on = 0
      --    self.nb_light_on_last_check = Shared.GetTime() + 2
      --    for _, marine in ipairs(GetEntitiesForTeamWithinRange("Marine", 1, self:GetOrigin(), 10))
      --    do
      --       if (GetWallBetween(marine:GetModelOrigin(), self:GetOrigin())) then
      --          if (marine:GetFlashlightOn()) then
      --             self.nb_light_on = self.nb_light_on + 1
      --          end
      --       end
      --    end
      -- end

      -- if (Server and self.clogRoot) then
      --    self.clogTimePassed = self.clogTimePassed + timePassed
      --    if (self.clogRefreshPos < Shared.GetTime()) then
      --       self.clogRefreshPos = Shared.GetTime() + 5
      --       self:UpdatePos(self.clogTimePassed, 0)
      --       self.clogTimePassed = 0
      --    end
      -- else
         self:SyncAttachPos(timePassed, 0)
      -- end


      -- if (self:GetOwnerEntityId() == nil) then
      --    Shared:FadedMessage("Creating 5 clogs")
      -- end
   end

   function Clog:OnKill()

      if (Server) then
         if (self.clogParent) then
            local parent = Shared.GetEntity(self.clogParent)
            if (parent) then
               parent.clogChild = nil
            end
         end
         if (self.clogChild) then
            local child = Shared.GetEntity(self.clogChild)
            if (child) then
               child:Kill()
            end
         end
         self:TriggerEffects("death")
         DestroyEntity(self)
      end

   end
-- elseif Client then

   function Clog:GetShowHealthFor()
      return false
   end
-- end
