// ===================== Faded Mod =====================
//
// lua\FadedConsoleCommands.lua
//
//    Created by: Rio (rio@myrio.de)
//
// =====================================================

local function OnCommandFaded(player)
    Shared.Message(string.format("Faded mod: version %s", kFadedModVersion or "error"))
end

// No Faded console command
local function NoFadedCommand(player)
   Shared.Message("Faded mod: To be removed from de Faded list, "
                     .. "type 'nofade' on the chat")
end

local function UnstuckCommand(player)
   Shared.Message("Faded mod: This command has to be typed in the main chat.")
end

local function SpectateFadedCommand(player)
   Shared.Message("Faded mod: This command has to be typed in the main chat.")
end

Event.Hook("Console_unstuck", UnstuckCommand)

Event.Hook("Console_spectatefade", SpectateFadedCommand)
Event.Hook("Console_spectatefaded", SpectateFadedCommand)

Event.Hook("Console_faded", OnCommandFaded)
Event.Hook("Console_nofade", NoFadedCommand)
Event.Hook("Console_nofaded", NoFadedCommand)
