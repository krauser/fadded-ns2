local controlledButtonEmitterGetCanBeUsed = ControlledButtonEmitter.GetCanBeUsed
function ControlledButtonEmitter:GetCanBeUsed(player, useSuccessTable)
   controlledButtonEmitterGetCanBeUsed(self, player, useSuccessTable)
   if (useSuccessTable.useSuccess == true) then
      useSuccessTable.useSuccess = player and (player:GetTeamNumber() == 1 or player:isa("Marine"))
   end
end
