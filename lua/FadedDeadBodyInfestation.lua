// ===================== Faded Mod =====================
//
// lua\FadedDeadBodyInfestation.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
// This manage manage infestation and gaz with dead body
//
// =====================================================

function InfestationMixin:OnUpdate(deltaTime)
    if not Predict then
       if (self and self:isa("Ragdoll") and self:isAMarine()) then
      -- Print("==> Marine " .. self:GetName() .. " Is Dead")
      self:UpdateInfestation(deltaTime)
      -- Spawn toxic gaz
      local randnb = math.random(1, 1000)
      if (Server and randnb <= kFadedDeadBodyToxicGazChance) then
         CreateEntity(SporeCloud.kMapName, self:GetOrigin(),
              2)
      end
       end
    end
end

-- Infestation growth rate
-- Default NS2 value: 0.25
function InfestationMixin:GetInfestationGrowthRate()
   return kFadedDeadBodyInfestationRate
end

-- function InfestationMixin:GetDestructionAllowed(destructionAllowedTable)
--     destructionAllowedTable.allowed = destructionAllowedTable.allowed and self.allowDestruction
-- end
