// ===================== Faded Mod =====================
//
// lua\FadedDisolveMixin.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

local kDissolveDelay = 60

function DissolveMixin:OnKillClient()

   // Start the dissolve effect
   local now = Shared.GetTime()
   self.dissolveStart = now + kDissolveDelay
   self:InstanceMaterials()
end
