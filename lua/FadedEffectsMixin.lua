// ===================== Faded Mod =====================
//
// lua\FadedEffectsMixin.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

-- function Team:TriggerEffects(eventName)

   -- Print("Event called with " .. tostring(eventName))
   -- local function TriggerEffects(player)
   --    player:TriggerEffects(eventName)
   --  end

   -- self:ForEachPlayer(TriggerEffects)
-- end

-- local EffectManagerTriggerEffects = EffectManager.TriggerEffects
-- function EffectManager:TriggerEffects(effectName, tableParams, triggeringEntity)
--    -- Print("Event2 called with " .. tostring(eventName))
--    -- EffectManagerTriggerEffects(self, effectName, tableParams, triggeringEntity)
-- end

-- Remove the blue cloud when an hallucination die
local oldeffect = ""
local EffectsMixinTriggerEffects = EffectsMixin.TriggerEffects
function EffectsMixin:TriggerEffects(effectName, tableParams)
   if (effectName ~= "death_hallucination") then
      if (oldeffect ~= effectName) then
     -- Print("Effect is " .. tostring(effectName))
         oldeffect = effectName
      end
      EffectsMixinTriggerEffects(self, effectName, tableParams)
   end
end
