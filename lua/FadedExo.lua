
local kWalkMaxSpeed = 3.7 * kFadedExoSpeedBonusFactor -- default
local kMaxSpeed = 5.75 * kFadedExoSpeedBonusFactor -- default

local kThrusterUpwardsAcceleration = 2 * kFadedExoSpeedBonusFactor
local kThrusterHorizontalAcceleration = 23 * kFadedExoSpeedBonusFactor
-- added to max speed when using thrusters
local kHorizontalThrusterAddSpeed = 2.5 * kFadedExoSpeedBonusFactor

-- Same as ns2
function Exo:GetMaxSpeed(possible)

    if possible then
        return kWalkMaxSpeed
    end

    local maxSpeed = kMaxSpeed * self:GetInventorySpeedScalar()

    if self.catpackboost then
        maxSpeed = maxSpeed + kCatPackMoveAddSpeed
    end

    return maxSpeed

end

-- same as ns2
local kUpVector = Vector(0, 1, 0)
function Exo:ModifyVelocity(input, velocity, deltaTime)

    if self.thrustersActive then

        if self.thrusterMode == kExoThrusterMode.Vertical then

       velocity:Add(kUpVector * kThrusterUpwardsAcceleration * deltaTime)
       velocity.y = math.min(1.5, velocity.y)

        elseif self.thrusterMode == kExoThrusterMode.Horizontal then

       input.move:Scale(0)

       local maxSpeed = self:GetMaxSpeed() + kHorizontalThrusterAddSpeed
       local wishDir = self:GetViewCoords().zAxis
       wishDir.y = 0
       wishDir:Normalize()

       local currentSpeed = wishDir:DotProduct(velocity)
       local addSpeed = math.max(0, maxSpeed - currentSpeed)

       if addSpeed > 0 then

          local accelSpeed = kThrusterHorizontalAcceleration * deltaTime
          accelSpeed = math.min(addSpeed, accelSpeed)
          velocity:Add(wishDir * accelSpeed)

       end

        end

    end

end
