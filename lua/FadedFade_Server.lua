// ===================== Faded Mod =====================
//
// lua\FadedFade_Server.lua
//
//    Created by: Rio (rio@myrio.de)
//
// =====================================================

function Fade:InitWeapons()
   Alien.InitWeapons(self)

   self:GiveItem(SwipeBlink.kMapName)
   self:GiveItem(SwipeLeap.kMapName)
   self:SetActiveWeapon(SwipeBlink.kMapName)
   -- self:SetActiveWeapon(SwipeLeap.kMapName)
end

function Fade:GetTierTwoTechId()
   return kTechId.BileBomb
end

function Fade:GetTierThreeTechId()
   -- return kTechId.Stab
   if (kFadedBabblerAbilityEnable) then
      return kTechId.BabblerAbility
   else
      return kTechId.None
   end
end
