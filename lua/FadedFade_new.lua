// ======= Copyright (c) 2003-2011, Unknown Worlds Entertainment, Inc. All rights reserved. =======
//
// lua\Fade.lua
//
//    Created by:   Charlie Cleveland (charlie@unknownworlds.com) and
   //                  Max McGuire (max@unknownworlds.com)
//
// Role: Surgical striker, harassment
//
// The Fade should be a fragile, deadly-sharp knife. Wielded properly, it's force is undeniable. But
// used clumsily or without care will only hurt the user. Make sure Fade isn't better than the Skulk
// in every way (notably, vs. Structures). To harass, he must be able to stay out in the field
// without continually healing at base, and needs to be able to use blink often.
//
// ========= For more information, visit us at http://www.unknownworlds.com =====================

Script.Load("lua/FadedSwipeLeap.lua")
Script.Load("lua/FadedAcidRockets.lua")
Script.Load("lua/Weapons/PredictedProjectile.lua")

local locale = nil
local strformat = string.format
-- Prevent chat message flood (1msg per sec at most)
local canSeeYouHelpTime = Shared.GetTime()
if (Server) then
   locale = LibCache:GetLibrary("LibLocales-1.0")

   Script.Load("lua/FadedFade_Server.lua")
elseif (Client) then
   Script.Load("lua/FadedFade_Client.lua")
end

Fade.kLeapForce = 23
Fade.kLeapTime = 0.2
-- Id of the marine who has an hallucination
-- Use in the OnCreate
kFadedHallucinationMarineId = nil
kFadedStabTriggered = false

local networkVars =
   {
      lastBileBomb = "time",

      isScanned = "boolean",
      shadowStepping = "boolean",
      timeShadowStep = "private compensated time",
      shadowStepDirection = "private vector",
      shadowStepSpeed = "private compensated interpolated float",

      etherealStartTime = "time",
      etherealEndTime = "time",

      -- True when we're moving quickly "through the ether"
      ethereal = "boolean",

      landedAfterBlink = "private compensated boolean",

      timeMetabolize = "private compensated time",

      timeOfLastPhase = "time",
      hasEtherealGate = "boolean",

      -- Wall grip. time == 0 no grip, > 0 when grip started.
      wallGripTime = "private compensated time",
      -- the normal that the model will use. Calculated the same way as the skulk
      wallGripNormalGoal = "private compensated vector",
      wallGripAllowed = "private compensated boolean",

      leaping = "compensated boolean",
      timeOfLeap = "private compensated time",
      timeOfLastJumpLand = "private compensated time",

      forPlayer = "integer",
      lastTimeGrowled = "time",

      kFadedLocalVocalChat = "boolean",
      -- En lien avec FadedMarine.lua
      kFadedTransformation = "boolean",
      kFadedTimeout = "time",
      kFadedCode1 = "boolean",
      kFadedCode2 = "boolean",
      kFadedCode3 = "boolean",

      kFadedCorpseMessageHelp = "boolean",
      kFadedEatingStartTime = "time",
      kFadedDeniedSwipeAfterStab = "time",

      kFadedKnockback = "private vector",
   }

-- AddMixinNetworkVars(BaseMoveMixin, networkVars)
-- AddMixinNetworkVars(GroundMoveMixin, networkVars)
-- AddMixinNetworkVars(CameraHolderMixin, networkVars)
-- AddMixinNetworkVars(DissolveMixin, networkVars)

-- if the user hits a wall and holds the use key and the resulting speed is < this, grip starts
Fade.kWallGripMaxSpeed = 10
-- once you press grip, you will slide for this long a time and then stop. This is also the time you
-- have to release your movement keys, after this window of time, pressing movement keys will release the grip.
Fade.kWallGripSlideTime = 0.2
-- after landing, the y-axis of the model will be adjusted to the wallGripNormal after this time.
Fade.kWallGripSmoothTime = 0.6

-- how to grab for stuff ... same as the skulk tight-in code
Fade.kWallGripRange = 0.2
Fade.kWallGripFeelerSize = 0.25



local onCreate = Fade.OnCreate
function Fade:OnCreate()
   onCreate(self)
   InitMixin(self, WallMovementMixin)
   InitMixin(self, PredictedProjectileShooterMixin)

   -- self:SetHasUmbra(true, 1000)
   -- self:SetMaxHealth(100)
   -- self:SetHealth(13)

   self.wallGripTime = 0
   self.silenceLevel = 3
   self.lastBileBomb = 0

   self.kFadedKnockback = Vector(0, 0, 0)
   self.leaping = false
   self.lastTimeGrowled = Shared.GetTime()
   self.darkVisionEnabled = false
   self:SetDarkVision(true)
   self.kFadedLocalVocalChat = false
   self.kFadedTransformation = false
   self.kFadedTimeout = Shared.GetTime()
   self.kFadedCode1 = false
   self.kFadedCode2 = false
   self.kFadedCode3 = false
   self.kFadedCorpseMessageHelp = true
   self.kFadedEating = false
   kFadedEatingStartTime = nil
   self.kFadedDeniedSwipeAfterStab = Shared.GetTime()
   if (Client and Client.GetLocalPlayer() == self)
   then
      self.GUIAboveHeadIcon = GetGUIManager():CreateGUIScript("FadedGUIAboveHeadIcon")
      self.GUICloak = GetGUIManager():CreateGUIScript("FadedGUICloak")
      self.GUICorpses = GetGUIManager():CreateGUIScript("FadedGUICorpses")
   end

   self.forPlayer = nil
   if (kFadedSpawnFadeHallucination == true) then
      -- Print("Hallu has callback")
      -- self:SetName("Hallucination")
      -- self.kMapName = "Hallucination"
      -- self:SetMaxHealth(26)
      -- self:SetHealth(26)
      -- self:SetMaxArmor(0)
      -- self:SetArmor(0)
      self.forPlayer = kFadedHallucinationMarineId
      -- else
      -- Print("Real faded called")
   end
   self:SetPropagate(Entity.Propagate_Callback)
   self.etherealEndTime = 0
end


local fadeOnDestroy = Fade.OnDestroy
function Fade:OnDestroy()
   fadeOnDestroy(self)
   if (Client) then
      GetGUIManager():DestroyGUIScript(self.GUIAboveHeadIcon)
      GetGUIManager():DestroyGUIScript(self.GUICloak)
      GetGUIManager():DestroyGUIScript(self.GUICorpses)
   end
end

function Fade:GetMaxArmor()
   return (self:GetBaseArmor())
end
function Fade:GetMaxHealth()
   return (self:GetBaseHealth())
end

-- Start amount of armor of the Faded
-- The faded receive a bonus for each additional marine in game
function Fade:GetBaseArmor()
   local fadeArmor = kFadeBaseArmor + kMarineNb * kFadeArmorPerMarine
   fadeArmor = fadeArmor / kFadeNb
   if (kMultiFadeMaluce) then
      if (kFadeNb > 1) then
         fadeArmor = fadeArmor / kFadedHPReductionPerFade
      end
   end
   return fadeArmor
end

-- Start amount of Health of the Faded
-- The faded receive a bonus for each additional marine in game
function Fade:GetBaseHealth()
   local fadeHealth = kFadeBaseHealth + kMarineNb * kFadeHealthPerMarine
   fadeHealth = fadeHealth / kFadeNb
   if (kMultiFadeMaluce) then
      if (kFadeNb > 1) then
         fadeHealth = fadeHealth / kFadedHPReductionPerFade
      end
   end
   return fadeHealth
end

function Fade:GetArmorFullyUpgradedAmount()
   return kFadeBaseArmor + kMarineNb * kFadeArmorPerMarine
end

-- local fadeGetCrouchSpeedScalar = Fade.GetCrouchSpeedScalar
-- function Fade:GetCrouchSpeedScalar()
--    if (self.ethereal or not fadeGetCrouchSpeedScalar) then
--       return (0)
--    else
--       return (fadeGetCrouchSpeedScalar(self))
--    end
-- end

local fadeOnInitialize = Fade.OnInitialized
function Fade:OnInitialized()
   fadeOnInitialize(self)

   if (Client) then
      if (Client.GetLocalPlayer() == self) then
         -- local Drop = Client.GetOptionString("input/Drop", "Drop")
         -- Client.SendNetworkMessage("OnChatCallBack",
         --               { msg = "FADED_LOCAL_CHAT_KEY", local_key = Drop },
         --               true)
      end

      -- self:AddHelpWidget("FadedGUIFadeLeapHelp", kFadedModGUIHelpLimit)
      -- self:AddHelpWidget("FadedGUIFadeWallgripHelp", kFadedModGUIHelpLimit)
      -- self:AddHelpWidget("FadedGUIFadeVisionHelp", kFadedModGUIHelpLimit)
   end
end

-- -- TODO: a checker
-- function Fade:SetDarkVision(state)
--    Alien.SetDarkVision(self, state)
--    self.darkVisionEnabled = state
-- end

-- function Fade:GetIsLeaping()
--    return self.leaping
-- end

function Fade:GetIsWallGripping()
   return self.wallGripTime ~= 0
end

-- function Fade:OverrideUpdateOnGround(onGround)
--    return (onGround or self:GetIsWallGripping())
-- end

-- function Fade:GetCanStep()
--    return self:GetIsOnGround() and not self:GetIsWallGripping()
-- end

function Fade:ModifyGravityForce(gravityTable)
   if self:GetIsWallGripping() or self:GetIsOnGround() or self:GetIsBlinking() then
      gravityTable.gravity = 0
      -- elseif self:GetCrouching() then
      --    gravityTable.gravity = gravityTable.gravity * 4

   end
end


function Fade:OnWorldCollision(normal)
   self.wallGripAllowed = normal.y < 0.5 and not self:GetCrouching()
end


function Fade:CalcWallGripSpeedFraction()

   local dt = (Shared.GetTime() - self.wallGripTime)
   if dt > Fade.kWallGripSlideTime then
      return 0
   end
   local k = Fade.kWallGripSlideTime
   return (k - dt) / k

end

local postUpdateMove = Fade.PostUpdateMove
function Fade:PostUpdateMove(input, runningPrediction)
   postUpdateMove(self, input, runningPrediction)

   -- local buttonDown = (bit.band(input.commands, Move.MovementModifier) ~= 0)
   -- if (buttonDown and not self.ethereal
   --     and self:GetActiveWeapon() and self:GetActiveWeapon():isa("SwipeLeap")) then
   --    if (Client) then

   --       -- local direction = self:GetCoords().zAxis
   --       -- if self.cursor then
   --       --    direction = self.cursor:GetDirection()
   --       -- end

   --       local direction = GetNormalizedVector(self:GetViewCoords():TransformVector(input.move))
   --       Client.SendNetworkMessage("TriggerShadowStep",
   --                                 {
   --                                    direction = direction
   --                                 }, true)
   --       self:FadedTriggerShadowStep(direction)
   --    end
   --    -- Entity.AddTimedCallback(self, FadedTriggerShadowStep, 0)
   -- end

   -- if (self:GetIsWallGripping()) then
   --    if (Shared.GetTime() > (self.wallGripTime + 0.08)) then
   --      local energyCost = input.time * kWallGripEnergyCost
   --      if (self:GetEnergy() < energyCost) then
   --         self.wallGripTime = 0
   --         self.wallGripOrigin = nil
   --      else
   --         self:DeductAbilityEnergy(energyCost)
   --      end
   --    end
   -- end

   -- wallgrip, recheck wallwalknormal as soon as the slide has stopped
   if self:GetIsWallGripping() and not self.wallGripRecheckDone and self:CalcWallGripSpeedFraction() == 0 then
      self.wallGripNormalGoal = self:GetAverageWallWalkingNormal(Lerk.kWallGripRange, Lerk.kWallGripFeelerSize)
      self.wallGripRecheckDone = true

      if not self.wallGripNormalGoal then
         self.wallGripTime = 0
         self.wallGripOrigin = nil
         self.wallGripRecheckDone = false
      end
   end

   if self.leaping and self:GetIsOnGround() and (Shared.GetTime() > self.timeOfLeap + Fade.kLeapTime) then
      self.leaping = false
   end
end

-- local velocity = self:GetVelocity() * 0.5
-- local forwardVec = self:GetViewAngles():GetCoords().zAxis
-- local newVelocity = velocity + GetNormalizedVectorXZ(forwardVec) * kLeapForce

-- // Add in vertical component.
-- newVelocity.y = kLeapVerticalForce * forwardVec.y + kLeapVerticalForce * 0.5 + ConditionalValue(velocity.y < 0, velocity.y, 0)

-- self:SetVelocity(newVelocity)

-- self.leaping = true
-- self.wallWalking = false
-- self:DisableGroundMove(0.2)

-- self.timeOfLeap = Shared.GetTime()

-- function Fade:GetPrimaryAttackAllowed()
--    local weap = self:GetWeapon(SwipeBlink.kMapName)
--    if (weap and weap.GetIsBlinking) then
--       return (not self:GetIsBlinking())
--    end
--    return (true)
-- end

function Fade:GetBlinkAllowed()
   return true
end

function Fade:OnLeap()
   if (self:GetEnergy() > kLeapEnergyCost) then
      local newVelocity = self:GetViewCoords().zAxis * self.kLeapForce * 1
      self:SetVelocity(newVelocity)

      self.leaping = true
      self.onGround = false
      self.onGroundNeedsUpdate = true

      self.timeOfLeap = Shared.GetTime()
      self.timeOfLastJump = Shared.GetTime()
      if (Server) then
         self:DeductAbilityEnergy(kLeapEnergyCost)
      end
   end
end

function Fade:GetAirMoveScalar()

   if self:GetVelocityLength() < 8 then
      return 1.0
   elseif self.leaping then
      return 0.3
   end

   return 0
end

function Fade:GetCanBeSetOnFire()
   return kFadedModFadeCanBeSetOnFire
end

function Fade:GetHasShadowStepAbility()
   return true
end
function Fade:GetHasShadowStepCooldown()
   return false
end
function Fade:GetRecentlyShadowStepped()
   return (false)
end

-- local function restoreVelocity(player)
--    local new_velocity = 0
--    if (player) then
--       player:SetVelocity(player:GetVelocity() * 0.95)
--    end
--    if (player:GetSpeedScalar() > 1) then
--       Entity.AddTimedCallback(player, restoreVelocity, 0.05)
--    end
--    return false
-- end

function Fade:GetPositionForShadownStep(range)

    local isPositionValid = false
    local foundPositionInRange = false
    local structPosition = nil

    local origin = self:GetEyePos() + self:GetViewAngles():GetCoords().zAxis * range

    -- Trace short distance in front
    local trace = Shared.TraceRay(self:GetEyePos(), origin, CollisionRep.Default,
                                  PhysicsMask.Bullets,
                                  EntityFilterOneAndIsa(self, "Entity"))

    local displayOrigin = trace.endPoint

    -- If we hit nothing, trace down to place on ground
    if trace.fraction == 1 then

        origin = self:GetEyePos() + self:GetViewAngles():GetCoords().zAxis * range
        trace = Shared.TraceRay(origin, origin - Vector(0, range, 0), CollisionRep.Default, PhysicsMask.Bullets, EntityFilterOneAndIsa(self, "Entity"))

    end


    -- If it hits something, position on this surface (must be the world or another structure)
    if trace.fraction < 1 then

        foundPositionInRange = true

        if trace.entity == nil then
            isPositionValid = true
        elseif not trace.entity:isa("ScriptActor") and not trace.entity:isa("Clog") and not trace.entity:isa("Web") then
            isPositionValid = true
        end

        displayOrigin = trace.endPoint

        -- Don't allow placing above or below us and don't draw either
        local structureFacing = self:GetViewAngles():GetCoords().zAxis

        if math.abs(Math.DotProduct(trace.normal, structureFacing)) > 0.9 then
            structureFacing = trace.normal:GetPerpendicular()
        end

        if trace.normal:DotProduct(Vector(0, 1, 0)) < .5 then
            isPositionValid = false
        end
        -- Coords.GetLookIn will prioritize the direction when constructing the coords,
        -- so make sure the facing direction is perpendicular to the normal so we get
        -- the correct y-axis.
        local perp = Math.CrossProduct(trace.normal, structureFacing)
        structureFacing = Math.CrossProduct(perp, trace.normal)

        structPosition = Coords.GetLookIn(displayOrigin, structureFacing, trace.normal)

    end

    return foundPositionInRange, structPosition, isPositionValid

end

local function disableEthereal(player)
   local weap = player:GetActiveWeapon()
   if (weap) then
      weap:SetEthereal(player, false)
   end
   return false
end

function Fade:FadedTriggerShadowStep(direction)

   -- local weap = self:GetActiveWeapon()
   -- if (weap and not self.ethereal) then
   --    weap:SetEthereal(self, true, direction)
   --    Entity.AddTimedCallback(self, disableEthereal, 0.3)
   -- end

    -- if not self:GetHasMovementSpecial() then
    --     return
    -- end

    -- if direction:GetLength() == 0 then
    --     direction.z = 1
    -- end
    -- /*
    -- if direction.z == 1 then
    --     direction.x = 0
    -- end
    -- */
    -- local movementDirection = self:GetViewCoords():TransformVector(direction)
    -- movementDirection:Normalize()

    -- if not self:GetIsBlinking() and not self:GetHasShadowStepCooldown() and self:GetEnergy() > kFadeShadowStepCost then

    --    local found, pos, isValid = self:GetPositionForShadownStep(6)

    --    if (found and isValid) then
    --       self:TriggerEffects("shadow_step", { effecthostcoords = Coords.GetLookIn(self:GetOrigin(), movementDirection) })
    --       if (Server) then
    --          self:DeductAbilityEnergy(3)
    --          -- local new_orig = GetValidFadeSpawnPlaceAround(pos.origin)
    --          -- if (new_orig) then
    --          --    self:SetOrigin(new_orig)
    --          -- end
    --          self:SetCoords(pos)
    --       end
    --    end



    --    if (Server) then
    --       if (hit) then
    --          range = Clamp(dist - 1, 0, range)
    --       end
    --    end


    --     -- local celerityAddSpeed = (GetHasCelerityUpgrade(self) and GetSpurLevel(self:GetTeamNumber()) or 0) * 0.7

    --     -- // add small force in the direction we are stepping
    --     -- local currentSpeed = movementDirection:DotProduct(self:GetVelocity())
    --     -- local shadowStepStrength = math.max(currentSpeed, 11 + celerityAddSpeed) + 0.5
    --     -- self:SetVelocity(self:GetVelocity() * 6)
    --     -- Entity.AddTimedCallback(self, restoreVelocity, 0)

    --     -- self.timeShadowStep = Shared.GetTime()
    --     -- self.shadowStepSpeed = kShadowStepSpeed
    --     -- self.shadowStepping = true
    --     -- self.shadowStepDirection = Vector(movementDirection)

    --     -- self:TriggerUncloak()

    --     -- local velocity = self:GetVelocity() * 0.5
    --     -- local forwardVec = self:GetViewAngles():GetCoords().zAxis
    --     -- local forward_force = 7.6
    --     -- local newVelocity = velocity + GetNormalizedVectorXZ(forwardVec) * forward_force

    --     -- -- Add in vertical component.
    --     -- local verticalForce = 10.5
    --     -- newVelocity.y = verticalForce * forwardVec.y + verticalForce * 0.5 + ConditionalValue(velocity.y < 0, velocity.y, 0)

    --     -- self:SetVelocity(newVelocity)

    --     -- self:DisableGroundMove(0.2)
    --     -- if (Server) then
    --     --    Shared:FadedMessage("Server")
    --     -- elseif (Client) then
    --     --    Shared:FadedMessage("Client")
    --     -- else
    --     --    Shared:FadedMessage("Predict")
    --     -- end
    -- end
    return false
end

-- -- No shadow step!
-- local fadeTriggerShadowStep = Fade.TriggerShadowStep
-- function Fade:FadedTriggerShadowStep(direction)
--    local weap = self:GetActiveWeapon()

--    Shared:FadedMessage("Shadow step")
--    -- if (weap and weap:isa("SwipeLeap")) then
--       fadeTriggerShadowStep(self, direction)
--    -- end
-- end

-- -- Make the Faded silent
function Alien:UpdateSilenceLevel()
   self.silenceLevel = 3
end

function Fade:UpdateSilenceLevel()
   self.silenceLevel = 3
end

function Fade:GetEffectParams(tableParams)
   tableParams[kEffectFilterSilenceUpgrade] = true
end

function Alien:GetEffectParams(tableParams)
   tableParams[kEffectFilterSilenceUpgrade] = true
end

-- Speed of the Fade
local fadeGetMaxSpeed = Fade.GetMaxSpeed
function Fade:GetMaxSpeed(possible)
   local max_speed = 6.8

   return (max_speed)
end

-- Return the number of Fade alive in the team
function Fade:getNbFadedAlive()
   local kFadeNbAlive = 0
   local faded_team = GetEntitiesForTeamWithinRange("Fade", 2,
                                                    self:GetOrigin(),
                                                    kMultiFadedGrowlMalusDistance)
   for _, fade in ipairs(faded_team) do
      if (fade and fade:GetIsAlive()) then
         kFadeNbAlive = kFadeNbAlive + 1
      end
   end
   -- Safety check
   if (kFadeNbAlive == 0) then
      kFadeNbAlive = 1
   end
   return (kFadeNbAlive)
end

-- MentalHealth impact on the growl
-- The Growl reduce nearby marine MentalHealth
-- Return the growl sound if the faded has enough energy, taunt otherwise
function Fade:FadedGrowl()
   local    damage = 0
   local    distance = 0

   if (self:GetEnergy() > kFadedGrowlEnergyCost) then
      self:DeductAbilityEnergy(kFadedGrowlEnergyCost)
      distance = kFadedGrowlMentalHealthDamage / kFadedGrowlDiminutionByMeter
      -- Make a round for each range level and applie to all marine
      -- So a marine nearby will receive N*level he is in.
      -- ex: for 30dmg and 2dmg reduction
      -- * 30/2 => 15 loop, each loop substract 2 damages
      -- * Marine at 1 meter will receive 15 time 2 damage ==> 30dmg
      local marines_around = GetEntitiesWithinRange("Marine",
                                                    self:GetOrigin(),
                                                    distance)
      local kFadeNbAlive = self:getNbFadedAlive()
      for index, marine in ipairs(marines_around) do
         if (marine and marine:GetIsAlive()) then
            -- 1/ Get numbers of level the marine is in
            -- 2/ Multiply by the damage per meter
            -- 3/ Divide by the number of Faded around
            local damages = ((distance - self:GetOrigin():GetDistanceTo(marine:GetOrigin())) * kFadedGrowlDiminutionByMeter) / kFadeNbAlive
            -- if (Server) then
            --    Shared:FadedMessage("Inflict " .. tostring(damages) .. " to "
            --                   .. marine:GetName())
            -- end
            marine:DecreaseMentalHealthByVal(damages)
            -- CreateEntity(StormCloud.kMapName, self:GetOrigin(), 2)
         end
      end

      -- while (distance > 0) do
      --      local marines_around = GetEntitiesWithinRange("Marine",
      --                                self:GetOrigin(),
      --                                distance)
      --      for index, marine in ipairs(marines_around) do
      --         -- Divise growl damage, so its not Over powered
      --         local growl_damage = kFadedGrowlDiminutionByMeter / kFadeNbAlive
      --         marine:DecreaseMentalHealthByVal(growl_damage)
      --      end
      --      distance = distance - kFadedGrowlDiminutionByMeter
      -- end


      return ("sound/faded.fev/alien/voiceovers/growl")
   end
   return (nil)
end


function ViewModel:OnGetIsRelevant(player)
   return self:GetParent() == player-- or self.ethereal
end

function Fade:OnGetIsRelevant(player)
   local see = false
   -- Allow or not the Faded to see marines hallucination
   if (kFadedAllowFadedToSeeHallucination) then
      if (player:GetTeamNumber() == 2) then
         return (true)
      end
   end
   if (self.forPlayer) then
      see = (self.forPlayer == player:GetId())
   else
      see = not self.ethereal
   end
   return (see)
end

function Alien:IsUsingTeamVocalChat()
   return (self.kFadedLocalVocalChat)
end

-- -- Custom FOV for real view detection (160 degrees)
-- -- local function customPlayerFOV()
-- --    return (160)
-- -- end


function Player:SetKnockBack(velocity)
   self.kFadedKnockback = velocity
end
function Player:GetKnockBack()
   return (self.kFadedKnockback)
end

-- Return true if there is a marine of team1
-- at less than distance who view the Player
function Player:isAliveMarineAroundInView(distance)
   local function _isSeen(seen, viewver)
      for _, entitySeen in ipairs(seen) do
         if (entitySeen:isa("Player")
             and GetAreEnemies(viewver, entitySeen)) then
            return true, viewver
         end
      end
      return false, nil
   end

   local alive_marine = GetEntitiesWithinRange("Marine",
                                               self:GetOrigin(),
                                               distance)
   -- Is there an alive marine nearby who see a Faded ?
   for _, amarine in ipairs(alive_marine) do
      if (amarine and amarine:GetIsAlive()
          and GetAreEnemies(amarine, self)) then

         -- Trix to use a 160 field of view
         -- local old_GetFov = amarine.GetFov
         -- local old_GetMinimapFov = amarine.GetMinimapFov
         -- amarine.GetFov = customPlayerFOV
         -- amarine.GetMinimapFov = customPlayerFOV
         local seen = GetEntitiesWithinRangeInView("Player",
                                                   distance,
                                                   amarine)
         -- amarine.GetFov = old_GetFov
         -- amarine.GetMinimapFov = old_GetMinimapFov
         local ret, entity = _isSeen(seen, amarine)
         --------- If not found check just around
         --------- Because "beeing glue" on a marine is considered as not viewed
         if (ret == false) then
            seen = GetEntitiesWithinRange("Player",
                                          amarine:GetOrigin() + Vector(0, 0, 2),
                                          1.6)
            ret, entity = _isSeen(seen, amarine)
         end
         if (ret) then
            return true, entity
         end
         ---------

      end
   end
   return false, nil
end

-- Check for nearby corpse and change the Faded
function Fade:useMarineCorpse(input)
   local marineWhoSeeYou = nil
   local marine_alive_around = false
   local key = (bit.band(input.commands, Move.Use) ~= 0)
   local status, ent = false
   if (key and self.kFadedTransformation == false) then
      -- local dead_marine = GetEntitiesWithinRange("Marine",
      --                          self:GetOrigin(),
      --                          kFadedEatUseDistance)
      status, ent = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
      -- Is there a body to take ?
      -- for _, marine in ipairs(dead_marine) do
      if (status and (ent:isAMarine())) then
         haveCorpseAround = true
         -- marine_alive_around, marineWhoSeeYou = self:isAliveMarineAroundInView(kFadedTakeBodyDistance)
         -- -- If no, transform and destroy the body
         -- if (marine_alive_around == false) then
         ent.fadedCorpseAlreadyUsed = true
         if (Client) then
            local msg = {corpse_name = ent:GetPlayerName()}
            Client.SendNetworkMessage("OnTriggerTransform", msg)
         end
         self.kFadedTransformation = true
      end
      -- break
      -- end
      -- end
      -- if (Server and self.kFadedTransformation == false
      --       and status == true
      --       and canSeeYouHelpTime < Shared.GetTime() - 1) then
      --      self:FadedMessage(strformat(locale:ResolveString("FADED_CANT_USE_CORPSE"), marineWhoSeeYou:GetName()))
      --      -- Server.SendNetworkMessage(self, "ResetEatingTimer", {}, true)
      --      canSeeYouHelpTime = Shared.GetTime()
      -- end

      -- self:CheckTransformation()
   end
end


-- Called when the eating process is completed
-- Get the corpse by hand to be sure there are no confusion
function Fade:DevourMarineCorpse()
   local status, corpse = nil
   status, corpse = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
   if (status and corpse:isAMarine()) then
      self:ResetEatingFraction()
      corpse.fadedCorpseAlreadyUsed = true
      if (Client) then
         local msg = {corpse_name = corpse:GetPlayerName()}
         Client.SendNetworkMessage("OnTriggerEating", msg)
         return (true)
      end
   end
   return (false)
end

-- Called when the eating process is completed
-- Get the corpse by hand to be sure there are no confusion
function Fade:ReviveFadedCorpse()
   local status, corpse = nil
   if (kFadedRespawnedEnable == false) then return end
   status, corpse = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
   if (status and corpse:isAFaded()) then
      if (self:GetHealth() >= kFadedRespawnedFadeHealthCost) then
         self:ResetEatingFraction()
         corpse.fadedCorpseAlreadyUsed = true
         if (Client) then
            local msg = {corpse_name = corpse:GetPlayerName()}
            Client.SendNetworkMessage("OnTriggerReviveFaded", msg)
            return (true)
         end
      else
         Client.SendNetworkMessage("OnChatCallBack",
                                   { msg = "FADED_REVIVE_NOT_ENOUGHT_HEALTH",
                                     local_key = corpse:GetPlayerName() },
                                   true)
      end
   end
   return (false)
end

-- Process to eat a corpse
-- the Server notify
function Fade:eatMarineOrReviveFadedCorpse(onlyContinue)
   -- local dead_marine = GetEntitiesWithinRange("Marine",
   --                           self:GetOrigin(),
   --                           kFadedEatUseDistance)
   -- Is there à corpse to devour
   local status, ent = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
   if (status and ent and ent.fadedCorpseAlreadyUsed ~= true) then
      if (onlyContinue == nil and self.kFadedEatingStartTime == nil) then
         local start = Shared.GetTime()
         self:SetEatingStartTime(start)
         -- Shared:FadedMessage("Start eating")
      else
         if (self:HasFinishedToEat()) then
            -- Shared:FadedMessage("Eating finished")
            if (ent:isAMarine()) then
               self:DevourMarineCorpse()
            elseif (ent:isAFaded() and kFadedRespawnedEnable) then
               self:ReviveFadedCorpse()
               --else
               -- -- Its an hallucination
            end
         end
      end
   else
      self:ResetEatingFraction()
   end
   return (nil)
end

function Fade:HandleButtons(input)
   local wallGripPressed = bit.band(input.commands, Move.MovementModifier) ~= 0 and bit.band(input.commands, Move.Jump) == 0

   if not self:GetIsWallGripping() and wallGripPressed and self.wallGripAllowed then

      -- check if we can grab anything around us
      local wallNormal = self:GetAverageWallWalkingNormal(Fade.kWallGripRange, Fade.kWallGripFeelerSize)

      if wallNormal then

         self.wallGripTime = Shared.GetTime()
         self.wallGripNormalGoal = wallNormal
         self:SetVelocity(Vector(0,0,0))

      end

   else

      -- we always abandon wall gripping if we flap (even if we are sliding to a halt)
      local breakWallGrip = bit.band(input.commands, Move.Jump) ~= 0 or input.move:GetLength() > 0 or self:GetCrouching() or bit.band(input.commands, Move.SecondaryAttack) ~= 0

      if breakWallGrip then

         self.wallGripTime = 0
         self.wallGripNormal = nil
         self.wallGripAllowed = false

      end

   end

   Alien.HandleButtons(self, input)
end

-- See PlayerInput.lua (of NS2) for the key id
-- Note: Do not use this function anymore, try to use SendKeyEvent()
--       unless you need to run Server side code
local last_change = Shared.GetTime()
local playerHandleButtons = Fade.HandleButtons
function Fade:HandleButtons(input)
   playerHandleButtons(self, input)
   -- alienHandleButtons(self, input)
   -- Print("OnSendKeyEvent called")
   local msg = nil
   -- local player = Client.GetLocalPlayer()


   -- -- No need for local chat anymore
   -- local buyButtonPressed = bit.band(input.commands, Move.Drop) ~= 0
   -- if (buyButtonPressed and last_change < Shared.GetTime() + 0.5) then
   --    self.kFadedLocalVocalChat = not self.kFadedLocalVocalChat
   --    if (self.kFadedLocalVocalChat == true) then
   --       msg = "Team vocal chat enable"
   --    else
   --       msg = "Team vocal chat disable"
   --    end
   --    if (Server) then
   --       self:FadedMessage(msg)
   --    end
   --    last_change = Shared.GetTime()
   -- end

   self:useMarineCorpse(input)
end

-- Return the eating progress of the Faded (0.00 --> 1.00)
function Fade:GetEatingFraction()
   -- local ents = GetEntitiesWithinRange("Ragdoll",
   --                        self:GetOrigin(),
   --                        kFadedEatUseDistance)
   if (self:GetIsOnFire() == false) then
      if (self.kFadedEatingStartTime) then
         local current = Shared.GetTime() - self.kFadedEatingStartTime
         local end_time = kFadedEatingTime
         local progress = ((current / end_time) * 100) / 100
         if (progress > 1) then
            progress = 1
         end
         return (progress)
      end
   else
      self:ResetEatingFraction()
   end
   return (0)
end

-- Return true if the Faded has finished to eat a corpse
function Fade:HasFinishedToEat()
   if (self:GetEatingFraction() >= 1) then
      return (true)
   else
      return (false)
   end
end

-- Set the timer when the Faded start eating and notify the Server
function Fade:SetEatingStartTime(_time)
   self.kFadedEatingStartTime = _time
   -- if (Client) then
   --    Client.SendNetworkMessage("IsFadeEating", {time = _time})
   -- end

end

-- Reset the eating timer and notify the Server
function Fade:ResetEatingFraction()
   -- if (Client) then
   --    Client.SendNetworkMessage("IsFadeEating", {time = nil})
   -- end
   -- Shared:FadedMessage("Eating fraction reset")
   self.kFadedEatingStartTime = nil
end

if (Client) then
   -- This function manage the display of Help icon for the Faded
   -- Its Display:
   -- * "Press 'use'" / Stab / Eat / Eat progress Bar Button
   local switch_icon_time = Shared.GetTime()
   local icon = 0
   local fadeOnProcessMove = AlienActionFinderMixin.OnProcessMove
   function AlienActionFinderMixin:OnProcessMove(input)
      fadeOnProcessMove(self, input)
      local player = Client.GetLocalPlayer()
      if (switch_icon_time < Shared.GetTime()) then
         icon = (icon + 1) % 3
         if (icon == 2) then -- More time for the swipe_corpse help msg
            switch_icon_time = Shared.GetTime() + 2.5
         else
            switch_icon_time = Shared.GetTime() + 1
         end
      end
      -- for _, corpse in ipairs(GetEntitiesWithinRange("Marine",
      --                              player:GetOrigin(),
      --                              kFadedEatUseDistance)) do
      local status, corpse = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
      if (status and player.GetEatingFraction
             and self:GetActiveWeapon()
             and self:GetActiveWeapon():isa("SwipeBlink")
             and not self:GetActiveWeapon():isa("SwipeLeap")
      ) then
         local eatingFraction = player:GetEatingFraction()
         if (corpse:isAMarine()) then
            if (eatingFraction == 0) then -- MARINE
               if (icon == 0) then
                  self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("Use"), nil, "FADED_DISGUISE_HELP", nil)
               elseif (icon == 1) then
                  self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("RequestHealth"), nil, "FADED_HEAL_HELP", nil)
               else
                  self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("PrimaryAttack"), nil, "FADED_PICK_CORPSE_HELP", nil)
               end
            else -- Show eating progress
               self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("RequestHealth"), nil, "FADED_HEAL_HELP", eatingFraction)
            end
         elseif (corpse:isAFaded() and kFadedRespawnedEnable) then -- FADED player (not hallucination)
            self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("RequestHealth"), nil, "FADED_REVIVE_HELP", eatingFraction)
         end -- end corpse check
         player:eatMarineOrReviveFadedCorpse(true) -- do not start eat fraction
      else
         if (player.ResetEatingFraction) then
            player:ResetEatingFraction() -- Aim at nothing
         end
      end -- end status
   end -- end fct
end

-- end

-- Called when a key is pressed
-- Note: GetIsBinding() is only available on Client side
-- Note2: If the key is overloaded in VoiceOver.lua, the event
--        when the key is pressed will not be catched here.
--        (Only when its released)

-- '_hasReleasetheKey' is here to force the "eatMarineOrReviveFadedCorpse()" to be
-- called. If the player press an other key, the key is not the same,
-- (and the eating stop) so we have to track event "down" and "up"
-- and not "until".
local fadeSendKeyEvent = Fade.SendKeyEvent
function Fade:SendKeyEvent(key, down)
   if (down and GetIsBinding(key, "RequestHealth")) then
      self:eatMarineOrReviveFadedCorpse(nil)
      -- else
      --    local status, ent = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
      --    if (status == nil) then -- we do not aim anymore
      --      self:ResetEatingFraction()
      --    end
   end

   -- if (down and GetIsBinding(key, "MovementModifier")
   --     and self:GetActiveWeapon() and self:GetActiveWeapon():isa("SwipeLeap")) then
   --    if (Client) then

   --       local direction = self:GetCoords().zAxis
   --       if self.cursor then
   --          direction = self.cursor:GetDirection()
   --       end

   --       Client.SendNetworkMessage("TriggerShadowStep",
   --                                 {
   --                                    direction = direction
   --                                 }, true)
   --       self:FadedTriggerShadowStep(direction)
   --    end
   --    -- Entity.AddTimedCallback(self, FadedTriggerShadowStep, 0)
   -- end

   -- elseif (down == false and GetIsBinding(key, "RequestHealth")) then
   --    Print("Request Heal Event called Client up")
   -- end
   if (fadeSendKeyEvent) then
      fadeSendKeyEvent(self, key, down)
   end
end

-- function Fade:GetHasMetabolizeAnimationDelay()
--    return 0
-- end

function Fade:GetHasMovementSpecial()
   return (true)
end

function Fade:GetMovementSpecialCooldown()
   return (0)
end

function Fade:GetMovementSpecialTechId()
   -- if (self:GetActiveWeapon()
   --        and self:GetActiveWeapon():isa("SwipeLeap")) then
   --    return kTechId.ShadowStep
   -- end
   return kTechId.Cling
end

function Fade:GetCanMetabolizeHealth()
--   return self:GetHasTwoHives()
   return false
end

-- Check if a marine see the Faded and reset the eating timer client side
-- Only send the message at most once per second
local _notifyOncePerSec = 0
local function _CheckIfAMarineSeeMe(self, deltatime)
   _notifyOncePerSec = _notifyOncePerSec + deltatime
   local dead_marine = GetEntitiesWithinRange("Marine",
                                              self:GetOrigin(),
                                              kFadedEatUseDistance + 0.5)
   if (#dead_marine > 0) then
      local marine_alive_around, marineWhoSee = self:isAliveMarineAroundInView(kFadedTakeBodyDistance)
      if (marine_alive_around == true) then
         -- Reset client side timer
         if (Server) then
            Server.SendNetworkMessage(self, "IsFadeEating",
                                      {time = nil}, true)
            if (_notifyOncePerSec > 1 and marineWhoSee) then
               _notifyOncePerSec = 0
               self:FadedMessage(strformat(locale:ResolveString("FADED_CANT_USE_CORPSE"), marineWhoSee:GetName()))
            end
         end
      end
   end
end

-- function Fade:DanceOnUpdate(deltaTime)
--    if (self.blinkDance) then
--       local weap = self:GetWeaponInHUDSlot(2)

--       for _, m in ipairs(GetEntitiesForTeamWithinRange("Marine", 1,
--                                                        self:GetOrigin(), 30))
--       do
--          if (m and m:GetIsAlive()) then
--             if (self.blinkDanceLastMove == nil
--                 or self.blinkDanceLastMove < Shared.GetTime() - 1) then
--                self.blinkDanceLastMove = Shared.GetTime()
--                weap:SetEthereal(self, true)
--                for i = 0, 100 do
--                   local orig = GetLocationAroundFor(m:GetOrigin(), kTechId.Fade, 3)
--                   if (orig and GetWallBetween(orig, m:GetOrigin()) == false) then
--                      self:SetOrigin(orig)

--                      local coords = Coords.GetLookIn(orig, m:GetOrigin() - orig)
--                      local angles = Angles()
--                      angles:BuildFromCoords(coords)

--                      self:SetViewAngles(angles)
--                      self:SetAngles(angles)

--                      Server.SendNetworkMessage(self, "setViewAngles", {
--                                                   pitch = angles.pitch,
--                                                   raw = angles.raw,
--                                                   roll = angles.roll
--                                                                       }, true)
--                      weap:SetEthereal(self, false)
--                      break
--                   end
--                end
--                break
--             end
--          end
--       end
--    end
-- end

-- Update function for every Faded entity
-- Check if the process is finished
-- Called by OnUpdateClient()
local force_ether = 0
local state = true
function FadedOnUpdate(self, deltatime)
   -- if (self.blinkDance) then
   --    local weap = self:GetWeaponInHUDSlot(2)
   --    weap:SetEthereal(self, state)

   --    for _, m in ipairs(GetEntitiesForTeamWithinRange("Marine", 1,
   --                                                     self:GetOrigin(), 30))
   --    do
   --       if (m and m:GetIsAlive()) then
   --          local orig = GetLocationAroundFor(m:GetOrigin(), kTechId.Armory, 3)
   --          self:SetOrigin(orig)
   --          break
   --       end
   --    end
   -- end
   force_ether = force_ether + deltatime
   if (force_ether >= 5) then
      force_ether = 0
      if (self and self:isa("Fade")) then
         local weap = self:GetWeaponInHUDSlot(2)
         weap:SetEthereal(self, state)
      end
      state = not state
   end
end

-- Event.Hook("SendKeyEvent", OnSendKeyEvent)

-- function Fade:SetTargetPlayer(forPlayerId)
--    Print("SetTargetPlayer called with :" .. tostring(forPlayerId))
--    self.forPlayer = forPlayerId
-- end

-- function Fade:MovementModifierChanged(newMovementModifierState, input)

-- if newMovementModifierState and self:GetActiveWeapon() ~= nil then
--    local weaponMapName = self:GetActiveWeapon():GetMapName()
--    local metabweapon = self:GetWeapon(Metabolize.kMapName)
--    if metabweapon and not metabweapon:GetHasAttackDelay() and self:GetEnergy() >= metabweapon:GetEnergyCost() then
--      self:SetActiveWeapon(Metabolize.kMapName)
--      self:PrimaryAttack()
--          if weaponMapName ~= Metabolize.kMapName then
--              self.previousweapon = weaponMapName
--          end
--      end
--  end

-- end

Shared.LinkClassToMap("Fade", Fade.kMapName, networkVars)
