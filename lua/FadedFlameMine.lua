Script.Load("lua/Mine.lua")

class 'FlameMine' (Mine)

FlameMine.kMapName = "active_flamemine"

local kTimeArmed = 0.15 -- Default 0.17

-- FlameMine.kModelName = PrecacheAsset("models/marine/mine/mine.model")

local networkVars =
{
}


local function Detonate(self, armFunc)
   local hitEntities = GetEntitiesWithMixinWithinRange("Live", self:GetOrigin(), kFadedFlameMinesRadius)

   for _, ent in ipairs(hitEntities) do
      if (HasMixin(ent, "Fire")) then
         ent:SetOnFire(self:GetOwner(), self)
      end
   end

   local params = {}
   params[kEffectHostCoords] = Coords.GetLookIn( self:GetOrigin(), self:GetCoords().zAxis )

   if GetIsVortexed(self) then
      params[kEffectSurface] = "ethereal"
   else
      params[kEffectSurface] = "metal"
   end

   self:TriggerEffects("mine_explode", params)

   DestroyEntity(self)
end

-- Same as legacy NS2
local function Arm(self)

    if not self.armed then

       self:AddTimedCallback(function() Detonate(self, Arm) end, kTimeArmed)

       self:TriggerEffects("mine_arm")

        self.armed = true

    end

end

-- Same as legacy NS2
local function CheckEntityExplodesMine(self, entity)

   if not self.active then
      return false

   end

   if entity:isa("Hallucination") or entity.isHallucination then
      return false
   end

   if not HasMixin(entity, "Team") or GetEnemyTeamNumber(self:GetTeamNumber()) ~= entity:GetTeamNumber() then
      return false
   end

   if not HasMixin(entity, "Live") or not entity:GetIsAlive() or not entity:GetCanTakeDamage() then
      return false
   end

   if not (entity:isa("Player") or entity:isa("Whip") or entity:isa("Babbler")) then
      return false
   end

   if entity:isa("Commander") then
      return false
   end

    if entity:isa("Skulk") or entity:isa("Babbler") then -- Faded condition added here
        return false
    end

   if entity:isa("Fade") and entity:GetIsBlinking() then

      --TEST_EVENT("Blinking Fade doesn't detonate mine")
      return false

   end

   local minePos = self:GetEngagementPoint()
   local targetPos = entity:GetEngagementPoint()
   -- Do not trigger through walls. But do trigger through other entities.
   if not GetWallBetween(minePos, targetPos, entity) then

      -- If this fails, targets can sit in trigger, no "polling" update performed.
      Arm(self)
      return true

   end

   return false

end

-- Same as legacy NS2
local function CheckAllEntsInTriggerExplodeMine(self)

   local ents = self:GetEntitiesInTrigger()
    for e = 1, #ents do
       CheckEntityExplodesMine(self, ents[e])
    end

end

if Server then
   /*
   ** We need to check when there are entities within the trigger area often.
   */
   function FlameMine:OnUpdate(dt)

      local now = Shared.GetTime()
      self.lastMineUpdateTime = self.lastMineUpdateTime or now
      if now - self.lastMineUpdateTime >= 0.5 then

     CheckAllEntsInTriggerExplodeMine(self)
     self.lastMineUpdateTime = now

      end

   end


   function FlameMine:OnTriggerEntered(entity)
      CheckEntityExplodesMine(self, entity)
   end

end

function FlameMine:OnCreate()
   Mine.OnCreate(self)
end

function FlameMine:OnInitialized()

   ScriptActor.OnInitialized(self)

   if Server then

      InitMixin(self, InfestationTrackerMixin)

      self.active = false

      local activateFunc = function(self)
         self.active = true
         CheckAllEntsInTriggerExplodeMine(self)
      end
      self:AddTimedCallback(activateFunc, kMineActiveTime)

      self.armed = false
      self:SetHealth(self:GetMaxHealth())
      self:SetArmor(self:GetMaxArmor())
      self:TriggerEffects("mine_spawn")

      InitMixin(self, TriggerMixin)
      self:SetSphere(kMineTriggerRange)

   end

   self:SetModel(Mine.kModelName)

end

GetTexCoordsForTechId(kTechId.Mine)
gTechIdPosition[kTechId.LayFlameMines] = kDeathMessageIcon.Mine

Shared.LinkClassToMap("FlameMine", FlameMine.kMapName, networkVars)
