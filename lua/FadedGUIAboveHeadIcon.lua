// ===================== Faded Mod =====================
//
// lua\FadedGUIAboveHeadIcon.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
// Notes: This file is the same as "GUIPickups" except:
// Each line with "This line has been changed"
--[[ In short:
   * Change 'Update' function to loop over Veteran
   instead of 'pickup' entity.
   * Change The GUI origin in the Update function
   - replace GetOrigin() by GetAttachPointOrigin(Jetpack.kAttachPoint)
   * Change 'pickup:isa(pickupType)'to GetActiveWeapon():isa(pickupType)
   in the 'GetPickupTextureCoordinates' function
   ==> We want to show the veteran active weapon
   + Init yOffset to a default value
   + Check if the veteran has a weapon (GetActiveWeapon) ~= nil
   * Increase kPickupsVisibleRange to 30
--]]
//
// =====================================================

class 'FadedGUIAboveHeadIcon' (GUIPickups)

-- This line has been change (original valu = 15)
local kPickupsVisibleRange = 30

local kPickupTextureYOffsets = { }
kPickupTextureYOffsets["AmmoPack"] = 0
kPickupTextureYOffsets["CatPack"] = 11
kPickupTextureYOffsets["MedPack"] = 1
kPickupTextureYOffsets["Rifle"] = 2
kPickupTextureYOffsets["HeavyRifle"] = 2
kPickupTextureYOffsets["Shotgun"] = 3
kPickupTextureYOffsets["Pistol"] = 4
kPickupTextureYOffsets["Flamethrower"] = 5
kPickupTextureYOffsets["GrenadeLauncher"] = 6
kPickupTextureYOffsets["Welder"] = 7
kPickupTextureYOffsets["Builder"] = 7
kPickupTextureYOffsets["Jetpack"] = 8
kPickupTextureYOffsets["LayMines"] = 9
kPickupTextureYOffsets["Exosuit"] = 10
kPickupTextureYOffsets["CatPack"] = 11


local kPickupIconHeight = 64
local kPickupIconWidth = 64

local function hasIconAboveHead(localPlayer, pickup)
   local isPickupVeteran = false
   local isMedicAndPickupWounded = false
   local isFadedAndPiclupMedic = false
   if (pickup and pickup:GetIsAlive()
          and pickup:GetMaxArmor() == kFadedVeteranArmor
       and pickup:GetMaxHealth() == kFadedVeteranHealth) then
      isPickupVeteran = true
   end

   if (localPlayer:GetTeamNumber() == 1 and localPlayer:isa("Marine") -- Medic
         and localPlayer:IsMedic() and localPlayer:GetCanBeHealed())
   then
      isMedicAndPickupWounded = true
   end

   if (localPlayer:GetTeamNumber() == 2 and pickup and pickup:GetIsAlive()
          and pickup:isa("Marine") and pickup:IsMedic()) then
      isFadedAndPiclupMedic = true
   end

   return isPickupVeteran, isMedicAndPickupWounded, isFadedAndPiclupMedic
end

local function GetPickupTextureCoordinates(pickup, vet, wounded_and_medic, alien_and_medic)

   -- Faded: This line has bee changed
   local yOffset = 4
   local localPlayer = Client.GetLocalPlayer()

   if (wounded_and_medic or alien_and_medic)
   then
      yOffset = kPickupTextureYOffsets["MedPack"]
   elseif (vet) then -- Veteran
      for pickupType, pickupTextureYOffset in pairs(kPickupTextureYOffsets) do

         -- Faded: This line has been changed
         -- Original: if pickup:isa(pickupType) then
         if (pickup:GetActiveWeapon()
             and pickup:GetActiveWeapon():isa(pickupType)) then

            yOffset = pickupTextureYOffset
            break

         end

      end
   else
      assert(0) -- should never happend (checked in the main loop)
      -- Faded: This line has been changed
      -- assert(yOffset)
   end


   return 0, yOffset * kPickupIconHeight, kPickupIconWidth, (yOffset + 1) * kPickupIconHeight

end

local kMinPickupSize = 16
local kMaxPickupSize = 48
-- Note: This graphic can probably be smaller as we don't need the icons to be so big.
local kTextureName = "ui/drop_icons.dds"
local kIconWorldOffset = Vector(0, 0.5, 0)
local kBounceSpeed = 2
local kBounceAmount = 0.05

function FadedGUIAboveHeadIcon:Initialize()

   self.allPickupGraphics = { }

end

function FadedGUIAboveHeadIcon:Uninitialize()

   for i, pickupGraphic in ipairs(self.allPickupGraphics) do
      GUI.DestroyItem(pickupGraphic)
   end
   self.allPickupGraphics = { }

end

function FadedGUIAboveHeadIcon:GetFreePickupGraphic()

   for i, pickupGraphic in ipairs(self.allPickupGraphics) do

      if pickupGraphic:GetIsVisible() == false then
         return pickupGraphic
      end

   end
   local newPickupGraphic = GUIManager:CreateGraphicItem()
   newPickupGraphic:SetAnchor(GUIItem.Left, GUIItem.Top)
   newPickupGraphic:SetTexture(kTextureName)
   newPickupGraphic:SetIsVisible(false)

   table.insert(self.allPickupGraphics, newPickupGraphic)

   return newPickupGraphic

end

function FadedGUIAboveHeadIcon:Update(deltaTime)

   PROFILE("GUIPickups:Update")

   local localPlayer = Client.GetLocalPlayer()
   -- Line added
   local origin = localPlayer:GetOrigin()

   if localPlayer then

      for i, pickupGraphic in ipairs(self.allPickupGraphics) do
         pickupGraphic:SetIsVisible(false)
      end

      -- Line Changed
      -- local nearbyPickups = GetNearbyPickups()
      -- for i, pickup in ipairs(nearbyPickups) do
      for _, pickup in ipairs(GetEntitiesWithinRange("Marine", origin, kPickupsVisibleRange))
      do
         -- Line added (trix to avoid network msg to client)
         local vet, wounded_and_medic, alien_and_medic = hasIconAboveHead(localPlayer, pickup)
         if (vet or wounded_and_medic or alien_and_medic) then
            -- Update marine skin
            -- if (pickup.kVeteranSkinUpdated == false) then
            --    kFadedIsVeteran = true
            --    pickup:ResetSkin()
            --    pickup.kVeteranSkinUpdated = true
            --    kFadedIsVeteran = false
            -- end
            // Check if the pickup is in front of the player.
            local playerForward = localPlayer:GetCoords().zAxis
            local playerToPickup = GetNormalizedVector(pickup:GetOrigin() - localPlayer:GetOrigin())
            local dotProduct = Math.DotProduct(playerForward, playerToPickup)

            if dotProduct > 0 then

               local freePickupGraphic = self:GetFreePickupGraphic()
               freePickupGraphic:SetIsVisible(true)

               local distance = pickup:GetDistanceSquared(localPlayer)
               distance = distance / (kPickupsVisibleRange * kPickupsVisibleRange)
               distance = 1 - distance
               freePickupGraphic:SetColor(Color(1, 1, 1, distance))

               local pickupSize = kMinPickupSize + ((kMaxPickupSize - kMinPickupSize) * distance)
               freePickupGraphic:SetSize(Vector(pickupSize, pickupSize, 0))

               local bounceAmount = math.sin(Shared.GetTime() * kBounceSpeed) * kBounceAmount
               -- Changed
               -- local pickupWorldPosition = pickup:GetOrigin() + kIconWorldOffset + Vector(0, bounceAmount, 0)
               local pickupWorldPosition = pickup:GetOrigin() + kIconWorldOffset + Vector(0, bounceAmount + 1.6, 0)
               local pickupInScreenspace = Client.WorldToScreen(pickupWorldPosition)
               // Adjust for the size so it is in the middle.
               pickupInScreenspace = pickupInScreenspace + Vector(-pickupSize / 2, -pickupSize / 2, 0)
               freePickupGraphic:SetPosition(pickupInScreenspace)

               freePickupGraphic:SetTexturePixelCoordinates(GetPickupTextureCoordinates(pickup, vet, wounded_and_medic, alien_and_medic))

            end

         end
      end
   end
end
