// ===================== Faded Mod =====================
//
// lua\FadedGUIBabblerXP.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

class 'FadedGUIBabblerXP' (GUIJetpackFuel)

function FadedGUIBabblerXP:Initialize()
   GUIJetpackFuel.Initialize(self)

   self.XPTitle = GetGUIManager():CreateTextItem()
   self.XPTitle:SetFontName("fonts/AgencyFB_small.fnt")
   self.XPTitle:SetFontIsBold(false)
   self.XPTitle:SetFontSize(8)
   self.XPTitle:SetAnchor(GUIItem.Middle, GUIItem.Top)
   self.XPTitle:SetTextAlignmentX(GUIItem.Align_Center)
   self.XPTitle:SetTextAlignmentY(GUIItem.Align_Max)
   self.XPTitle:SetColor(kMarineTeamColorFloat)
   -- self.XPTitle:SetText(Locale.ResolveString("FADED_"))
   local player = Client.GetLocalPlayer()
   self.XPTitle:SetText("XP")
   self.background:AddChild(self.XPTitle)


   self.babblerXPText = GetGUIManager():CreateTextItem()
   self.babblerXPText:SetFontName("fonts/AgencyFB_Tiny.fnt")
   self.babblerXPText:SetFontIsBold(false)
   self.babblerXPText:SetFontSize(2)
   self.babblerXPText:SetPosition(Vector(50, -3, 0))
   self.babblerXPText:SetAnchor(GUIItem.Middle, GUIItem.Bottom)
   self.babblerXPText:SetTextAlignmentX(GUIItem.Align_Center)
   self.babblerXPText:SetTextAlignmentY(GUIItem.Align_Max)
   self.babblerXPText:SetColor(kMarineTeamColorFloat)
   -- self.babblerXPText:SetText(Locale.ResolveString("FADED_"))
   self.babblerXPText:SetText("")
   self.background:AddChild(self.babblerXPText)
end

function FadedGUIBabblerXP:Uninitialize()
   GUIJetpackFuel.Uninitialize(self)
   self.babblerXPText = nil
   self.XPTitle = nil
end

function FadedGUIBabblerXP:Update(deltaTime)
   local player = Client.GetLocalPlayer()

   if (player and Client.GetLocalPlayer() == self and self.babblerXPText) then
      local finalStr = string.format(
         "%s: %d\n" ..
            "%s: %d\n -- ",
         "Level", player:GetXPLevel(),
         "Damages", kBiteDamage + babbler_level[player:GetXPLevel()].dmg_bonus)

      self.babblerXPText:SetText(finalStr)
      self.background:SetIsVisible(true)
      self:SetFuel(player:GetXPPercent() / 100)
   end
end
