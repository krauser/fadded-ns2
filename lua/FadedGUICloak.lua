------------------------Faded Mod-----------------------
--
--    lua\FadedGUICloak.lua    
--
--        Created by D'oh (vector.thor37@hotmail.fr
--    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
--------------------------------------------------------

class 'FadedGUICloak' (GUIJetpackFuel)

local kAdrenalineCloakColor = Color(1, 121/255, 12/255, 1)

function FadedGUICloak:Initialize()
   GUIJetpackFuel.Initialize(self)
   self.background:SetColor(kAdrenalineCloakColor)

   self.cloakTitle = GetGUIManager():CreateTextItem()
   self.cloakTitle:SetFontName("fonts/AgencyFB_small.fnt")
   self.cloakTitle:SetFontIsBold(false)
   self.cloakTitle:SetFontSize(10)
   self.cloakTitle:SetAnchor(GUIItem.Middle, GUIItem.Top)
   self.cloakTitle:SetTextAlignmentX(GUIItem.Align_Center)
   self.cloakTitle:SetTextAlignmentY(GUIItem.Align_Max)
   self.cloakTitle:SetColor(kAlienTeamColorFloat)
   -- self.cloakTitle:SetText(Locale.ResolveString("FADED_"))
   self.cloakTitle:SetText("Cloak")
   self.background:AddChild(self.cloakTitle)

   self.cloakText = GetGUIManager():CreateTextItem()
   self.cloakText:SetFontName("fonts/AgencyFB_Tiny.fnt")
   self.cloakText:SetFontIsBold(false)
   self.cloakText:SetFontSize(2)
   self.cloakText:SetPosition(Vector(55, -3, 0))
   self.cloakText:SetAnchor(GUIItem.Middle, GUIItem.Bottom)
   self.cloakText:SetTextAlignmentX(GUIItem.Align_Center)
   self.cloakText:SetTextAlignmentY(GUIItem.Align_Max)
   self.cloakText:SetColor(kAlienTeamColorFloat)
   self.cloakText:SetText("")
   self.background:AddChild(self.cloakText)

end

function FadedGUICloak:SetFuel(fraction)
   self.fuelBar:SetSize( Vector(GUIJetpackFuel.kBarWidth, -GUIJetpackFuel.kBarHeight * fraction, 0) )
   self.fuelBar:SetColor(kAdrenalineCloakColor)
end

local function setGUICloakText(self, player, cloak_percent, proximity, speed)
   if (self.cloakText) then
      local speed_bonus_ratio = (speed * 100) / (kFadedMaxCloak - kFadedCloakAmount)
      local proximity_bonus_ratio = (proximity * 100) / (kFadedMaxCloak - kFadedCloakAmount)

      local finalStr = string.format(
     "Cloak: %d%%\n" ..
        "Speed: %+d%%\n"..
        "Proximity: %+d%%\n"
     , cloak_percent * 100, speed_bonus_ratio, proximity_bonus_ratio)
      self.cloakText:SetText(finalStr)
   end
end

function FadedGUICloak:Update(deltaTime)
   local player = Client.GetLocalPlayer()
   local canSeeGUI = player

   -- if (player.GUICloak == nil) then
   --    player.GUICloak = GetGUIManager():CreateGUIScript("FadedGUICloak")
   -- end
   if (canSeeGUI) then
      self.background:SetIsVisible(true)
      -- We want the GUI cloak meter to be usefull
      -- it will be zero (fully visible) below kFadedCloakAmount
      -- and full at kFadedMaxCloak
      local cloak_percent = 0
      local cloak, proximity, speed = CloakableMixinUpdateCloakState(player, nil)
      if (cloak <= kFadedCloakAmount) then
     self:SetFuel(0)
      else
     local bonus_cloak = Clamp(cloak - kFadedCloakAmount, 0, kFadedMaxCloak - kFadedCloakAmount)
     cloak_percent = bonus_cloak / (kFadedMaxCloak - kFadedCloakAmount)
     self:SetFuel(cloak_percent)
      end
      setGUICloakText(self, player, cloak_percent, proximity, speed)
   else
      self.background:SetIsVisible(false)
   end
   -- Print("Update")
   -- if (self:isa("Marine")) then
   -- Print("Set fuel to " .. tostring(self:GetMentalHealth()))
   -- FadedGUIMentalHealth:SetFuel(self:GetMentalHealth() / 100)
   -- end
end

function FadedGUICloak:Uninitialize()
   GUIJetpackFuel.Uninitialize(self)
   cloakTitle = nil
end
