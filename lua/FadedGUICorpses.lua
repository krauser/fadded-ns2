// ===================== Faded Mod =====================
//
// lua\FadedGUICorpses.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
// Notes: This file is the same as "GUIPickups" except:
// Each line with "This line has been changed"
--[[ In short:
   * Change 'Update' function to loop over Veteran
   instead of 'pickup' entity.
   * Change The GUI origin in the Update function
   - replace GetOrigin() by GetAttachPointOrigin(Jetpack.kAttachPoint)
   * Change 'pickup:isa(pickupType)'to GetActiveWeapon():isa(pickupType)
   in the 'GetPickupTextureCoordinates' function
   ==> We want to show the veteran active weapon
   + Init yOffset to a default value
   + Check if the veteran has a weapon (GetActiveWeapon) ~= nil
   * Increase kPickupsVisibleRange to 30
--]]
//
// =====================================================

class 'FadedGUICorpses' (GUIPickups)

-- This line has been change (original valu = 15)
local kPickupsVisibleRange = 15

local kPickupTextureYOffsets = { }
kPickupTextureYOffsets["AmmoPack"] = 0
kPickupTextureYOffsets["CatPack"] = 11
kPickupTextureYOffsets["MedPack"] = 1
kPickupTextureYOffsets["Rifle"] = 2
kPickupTextureYOffsets["HeavyRifle"] = 2
kPickupTextureYOffsets["Shotgun"] = 3
kPickupTextureYOffsets["Pistol"] = 4
kPickupTextureYOffsets["Flamethrower"] = 5
kPickupTextureYOffsets["GrenadeLauncher"] = 6
kPickupTextureYOffsets["Welder"] = 7
kPickupTextureYOffsets["Builder"] = 7
kPickupTextureYOffsets["Jetpack"] = 8
kPickupTextureYOffsets["LayMines"] = 9
kPickupTextureYOffsets["Exosuit"] = 10
kPickupTextureYOffsets["CatPack"] = 11


local kPickupIconHeight = 64
local kPickupIconWidth = 64

local function GetPickupTextureCoordinates(pickup)

   -- Faded: This line has bee changed
   local yOffset = 1
   -- for pickupType, pickupTextureYOffset in pairs(kPickupTextureYOffsets) do

   --    -- Faded: This line has been changed
   --    -- Original: if pickup:isa(pickupType) then
   --    if (pickup:GetActiveWeapon()
   --        and pickup:GetActiveWeapon():isa(pickupType)) then

   --       yOffset = pickupTextureYOffset
   --       break

   --    end

   -- end
   -- Faded: This line has been changed
   -- assert(yOffset)

   return 0, yOffset * kPickupIconHeight, kPickupIconWidth, (yOffset + 1) * kPickupIconHeight

end

local kMinPickupSize = 16
local kMaxPickupSize = 48
-- Note: This graphic can probably be smaller as we don't need the icons to be so big.
local kTextureName = "ui/drop_icons.dds"
local kIconWorldOffset = Vector(0, 0.5, 0)
local kBounceSpeed = 2
local kBounceAmount = 0.05

function FadedGUICorpses:Initialize()

   self.allPickupGraphics = { }

end

function FadedGUICorpses:Uninitialize()

   for i, pickupGraphic in ipairs(self.allPickupGraphics) do
      GUI.DestroyItem(pickupGraphic)
   end
   self.allPickupGraphics = { }

end

function FadedGUICorpses:GetFreePickupGraphic()

   for i, pickupGraphic in ipairs(self.allPickupGraphics) do

      if pickupGraphic:GetIsVisible() == false then
         return pickupGraphic
      end

   end
   local newPickupGraphic = GUIManager:CreateGraphicItem()
   newPickupGraphic:SetAnchor(GUIItem.Left, GUIItem.Top)
   newPickupGraphic:SetTexture(kTextureName)
   newPickupGraphic:SetIsVisible(false)

   table.insert(self.allPickupGraphics, newPickupGraphic)

   return newPickupGraphic

end

function FadedGUICorpses:Update(deltaTime)

   PROFILE("GUIPickups:Update")

   local localPlayer = Client.GetLocalPlayer()
   -- Line added
   local origin = localPlayer:GetOrigin()

   if localPlayer then

      for i, pickupGraphic in ipairs(self.allPickupGraphics) do
         pickupGraphic:SetIsVisible(false)
      end

      -- Line Changed
      -- local nearbyPickups = GetNearbyPickups()
      -- for i, pickup in ipairs(nearbyPickups) do
      for _, pickup in ipairs(GetEntitiesWithinRange("Ragdoll", origin, kPickupsVisibleRange))
      do
         -- Line added (trix to avoid network msg to client)
         if (pickup) then
            -- Update marine skin
            -- if (pickup.kVeteranSkinUpdated == false) then
            --    kFadedIsVeteran = true
            --    pickup:ResetSkin()
            --    pickup.kVeteranSkinUpdated = true
            --    kFadedIsVeteran = false
            -- end
            // Check if the pickup is in front of the player.
            local playerForward = localPlayer:GetCoords().zAxis
            local playerToPickup = GetNormalizedVector(pickup:GetOrigin() - localPlayer:GetOrigin())
            local dotProduct = Math.DotProduct(playerForward, playerToPickup)

            if dotProduct > 0 then

               local freePickupGraphic = self:GetFreePickupGraphic()
               freePickupGraphic:SetIsVisible(true)

               local distance = pickup:GetDistanceSquared(localPlayer)
               distance = distance / (kPickupsVisibleRange * kPickupsVisibleRange)
               distance = 1 - distance
               freePickupGraphic:SetColor(Color(1, 1, 1, distance))

               local pickupSize = kMinPickupSize + ((kMaxPickupSize - kMinPickupSize) * distance)
               freePickupGraphic:SetSize(Vector(pickupSize, pickupSize, 0))

               local bounceAmount = math.sin(Shared.GetTime() * kBounceSpeed) * kBounceAmount
               -- Changed
               -- local pickupWorldPosition = pickup:GetOrigin() + kIconWorldOffset + Vector(0, bounceAmount, 0)
               local pickupWorldPosition = pickup:GetOrigin() + kIconWorldOffset + Vector(0, bounceAmount + 1.6, 0)
               local pickupInScreenspace = Client.WorldToScreen(pickupWorldPosition)
               // Adjust for the size so it is in the middle.
               pickupInScreenspace = pickupInScreenspace + Vector(-pickupSize / 2, -pickupSize / 2, 0)
               freePickupGraphic:SetPosition(pickupInScreenspace)

               freePickupGraphic:SetTexturePixelCoordinates(GetPickupTextureCoordinates(pickup))

            end

         end
      end
   end
end
