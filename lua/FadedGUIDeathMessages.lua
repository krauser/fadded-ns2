// ===================== Faded Mod =====================
//
// lua\FadedGUIDeathMessages.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

-- Only print death notification if the victim is around
local GUIDeathMessagesAddMessage = GUIDeathMessages.AddMessage
function GUIDeathMessages:AddMessage(killerColor, killerName, targetColor, targetName, iconIndex, targetIsPlayer)
   local _printDeathMessage = false
   local p = Client.GetLocalPlayer()
   if (p:GetTeamNumber() == 1) then
      if (p) then
     for _, player in ipairs(GetEntities("Player")) do
        local orig1 = p:GetOrigin()
        local orig2 = player:GetOrigin()
        local distance = orig1:GetDistanceTo(orig2)
        -- Absolute value
        if (distance < 0) then distance = distance * -1 end
        if (player:GetName() == targetName
           and distance < kFadedDeathMessageDistance
           and killerName and killerName ~= "")
        then
           _printDeathMessage = true
           break
        end
     end
      end
   else
      if (targetIsPlayer == true) then
         _printDeathMessage = true
      end
   end
   if (_printDeathMessage) then
      GUIDeathMessagesAddMessage(self, killerColor, killerName, targetColor, targetName, iconIndex, targetIsPlayer)
   end
end
