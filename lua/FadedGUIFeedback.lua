// ===================== Faded Mod =====================
//
// lua\FadedGUIFeedback.lua
//
//    Created by: Rio (rio@myrio.de)
//
// =====================================================

Script.Load("lua/GUIFeedback.lua")

local GUIFeedbackInitialized = GUIFeedback.Initialize
function GUIFeedback:Initialize()
   GUIFeedbackInitialized(self)
   self.buildText:SetText("Faded Mod " .. kFadedModVersion)
end
