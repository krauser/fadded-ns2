// ===================== Faded Mod =====================
//
// lua\FadedGUIMentalHealth.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

class 'FadedGUIMentalHealth' (GUIJetpackFuel)

local sanityAmountStr    = "Sanity"
local sanityLevelNameStr = "Level  " -- space to adjust in screen with sanity
local flashlightAmountStr = "Light"

function FadedGUIMentalHealth:Initialize()
   GUIJetpackFuel.Initialize(self)

   self.sanityTitle = GetGUIManager():CreateTextItem()
   self.sanityTitle:SetFontName("fonts/AgencyFB_small.fnt")
   self.sanityTitle:SetFontIsBold(false)
   self.sanityTitle:SetFontSize(8)
   self.sanityTitle:SetAnchor(GUIItem.Middle, GUIItem.Top)
   self.sanityTitle:SetTextAlignmentX(GUIItem.Align_Center)
   self.sanityTitle:SetTextAlignmentY(GUIItem.Align_Max)
   self.sanityTitle:SetColor(kMarineTeamColorFloat)
   -- self.sanityTitle:SetText(Locale.ResolveString("FADED_"))
   local player = Client.GetLocalPlayer()
   if (player and player:GetIsAlive()
          and player:GetMaxArmor() == kFadedVeteranArmor
       and player:GetMaxHealth() == kFadedVeteranHealth) then
      self.sanityTitle:SetText("Veteran\n Sanity")
   else
      self.sanityTitle:SetText("Sanity")
   end
   self.background:AddChild(self.sanityTitle)


   self.fadedMarineText = GetGUIManager():CreateTextItem()
   self.fadedMarineText:SetFontName("fonts/AgencyFB_Tiny.fnt")
   self.fadedMarineText:SetFontIsBold(false)
   self.fadedMarineText:SetFontSize(2)
   self.fadedMarineText:SetPosition(Vector(50, -3, 0))
   self.fadedMarineText:SetAnchor(GUIItem.Middle, GUIItem.Bottom)
   self.fadedMarineText:SetTextAlignmentX(GUIItem.Align_Center)
   self.fadedMarineText:SetTextAlignmentY(GUIItem.Align_Max)
   self.fadedMarineText:SetColor(kMarineTeamColorFloat)
   -- self.fadedMarineText:SetText(Locale.ResolveString("FADED_"))
   self.fadedMarineText:SetText("")
   self.background:AddChild(self.fadedMarineText)
end

function FadedGUIMentalHealth:Uninitialize()
   GUIJetpackFuel.Uninitialize(self)
   self.fadedMarineText = nil
   self.sanityTitle = nil
end

function FadedGUIMentalHealth:Update(deltaTime)
   local player = Client.GetLocalPlayer()

   -- if (player.GUIMentalHealth == nil) then
   --    player.GUIMentalHealth = GetGUIManager():CreateGUIScript("FadedGUIMentalHealth")
   -- end
   local canSeeGUI = player and self.fadedMarineText
      and player.kFadedLightBatteryLeft
      and player.GetMentalHealth and (not player:isa("Marine") or player:GetTeamNumber() == 1)
   if (canSeeGUI) then
      local lightPercent = (player.kFadedLightBatteryLeft / kFadedLightTime) * 100
      lightPercent = Clamp(lightPercent, 0, 100)
      local finalStr = string.format(
     "%s: %d%%\n" ..
        "%s: %d%%\n"..
        "%s: %s",
     flashlightAmountStr, lightPercent,
     sanityAmountStr, player:GetMentalHealth(),
     sanityLevelNameStr, player:GetMentalLevelStats().name)

      if (player and player:GetIsAlive()
             and player:GetMaxArmor() == kFadedVeteranArmor
          and player:GetMaxHealth() == kFadedVeteranHealth) then
         finalStr = string.format(
            "%s\n" ..
               "Speed: %+d%%\n", finalStr, kFadedVeteranSpeedBonus * 100)
      end


      self.fadedMarineText:SetText(finalStr)
   end
   if (canSeeGUI) then
      self.background:SetIsVisible(true)
      self:SetFuel(player:GetMentalHealth() / 100.0)
   else
      self.background:SetIsVisible(false)
   end
end
