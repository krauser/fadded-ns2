// ===================== Faded Mod =====================
//
// lua\FadedGamerules.lua
//
//    Created by: Rio (rio@myrio.de)
//    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

local locale = LibCache:GetLibrary("LibLocales-1.0")
local strformat = string.format
local floor = math.floor
local random = math.random
local tinsert = table.insert

local alive_marine = {}

-- List of spectate fade player
spectateFadedPlayers = {}

-- List of players already auto added once
babblerPlayersAutoJoin = {}
-- List of players in the babbler player list
babblerPlayers = {}
babblerNextSpawnWave = 0

-- Next veteran marine (random on alive player)
-- Note: I use table.insert() / table.remove() for this table
fadedNextVeteranPlayersName = {}
fadedVeteranPlayersName = {}

-- Player who kill fade or winning faded alive
fadedNextPlayersName = {}
-- Faded of the current round (for selection chance)
fadedPlayersName = {}
fadedMarinesMainBase = ""
fadedMarinesMainBaseCoord = Vector(0, 0, 0)

-- fadedNextPlayerName = nil
-- fadedPlayerName = nil

local fadedPregameTenSecMessage = false
local fadedPregameFiveSecMessage = false
local fadedModLastTimeTwoPlayersToStartMessage = nil

local fadedMapTime = 0
local startingBatteryDropped = false

local cycle = MapCycle_GetMapCycle()
local mod_loaded_time = Shared.GetTime()
local first_start_in_msg_refresh = Shared.GetTime()

if (Server) then
   -- Checks if the faded is dead
   -- Does not count hallucination
   function NS2Gamerules:CheckIfFadedIsDead()
      for _, faded in pairs(self.team2:GetPlayers()) do
         if (faded ~= nil and faded:isa("Player") and faded:GetIsAlive()
                and not faded:isa("Skulk") -- Do not count babbler players
                and faded.isHallucination ~= true)
         then
            return (false)
         end
      end
      return (true)
   end

   -- Checks if the marines are dead
   function NS2Gamerules:CheckIfMarinesAreDead()
      for _, marine in pairs(self.team1:GetPlayers()) do
         if (marine ~= nil and (marine:isa("Player") and not marine:isa("Skulk"))
             and marine:GetIsAlive()) then
            return (false)
         end
      end
      return (true)
   end

   // Only allow players to join the marine and random team
   function NS2Gamerules:GetCanJoinTeamNumber(teamNumber)
      return true //(teamNumber ~= 2)
   end

   local joinTeam = NS2Gamerules.JoinTeam
   function NS2Gamerules:JoinTeam(player, newTeamNumber, force)
      if (newTeamNumber == 2 and not force) then
         newTeamNumber = 1
      end
      -- If in ready room, remove spectatefade and babbler
      if (newTeamNumber == kTeamReadyRoom) then
         SpectateFadedChatCommand(player, true)
         BabblerPlayerChatCommand(player, false, true)
         player = player:Replace(Marine.kMapName, 1, false, player:GetOrigin())
      end

      return joinTeam(self, player, newTeamNumber, force)
   end

   // Allow friendly fire to help the Faded a bit
   function NS2Gamerules:GetFriendlyFire() return kFadedModFriendlyFireEnabled end
   function GetFriendlyFire()
      return kFadedModFriendlyFireEnabled
   end

   // Display the Faded health and armor left when the round
   function NS2Gamerules:printFadedHealthArmorLeft()
      for _, faded in ipairs(self.team2:GetPlayers()) do
         if (faded and faded:GetIsAlive()
                and not faded:isa("Skulk")
             and faded.isHallucination ~= true) then
            local hp = string.format("%.0f", faded:GetHealth())
            local ap = string.format("%.0f", faded:GetArmor())
            Shared:FadedMessage("Faded '" .. faded:GetName() .. "'"
                                   .. " has " .. hp .. "/" .. ap
                                   .. " (hp/ap)")
         end
      end
   end

   // Define the game ending rules
   function NS2Gamerules:CheckGameEnd()
      -- Print("Faded NS2Gamerules:CheckGameEnd()")
      if (self:GetGameStarted()
             and self.timeGameEnded == nil
             and not self.preventGameEnd
          and Shared.FadedMessage) then
         if (self.timeLastGameEndCheck == nil
                or (Shared.GetTime() > self.timeLastGameEndCheck + 1))
         then
            local team1Players = self.team1:GetNumPlayers()
            local team2Players = self.team2:GetNumPlayers()

            if (team1Players == 0) then
               Shared:FadedMessage(locale:ResolveString("FADED_NO_MARINES_LEFT"))
               self:EndGame(self.team2)
            elseif (team2Players == 0) then
               Shared:FadedMessage(locale:ResolveString("FADED_NO_FADED_LEFT"))
               self:EndGame(self.team1)
            elseif (self:CheckIfFadedIsDead() == true) then
               Shared:FadedMessage(locale:ResolveString("FADED_MARINES_VICTORY"))
               -- self:ChooseVeteranPlayers()
               self:SaveEndRoundAliveMarine()
               self:EndGame(self.team1)
            elseif (self:CheckIfMarinesAreDead() == true) then
               Shared:FadedMessage(locale:ResolveString("FADED_VICTORY"))

               -- If the Faded wins, the player has a higher chance to be the Faded next round
               -- Only add alive fade to the list
               for faded in pairs(fadedPlayersName) do
                  if (faded ~= nil) then
                     local player = Shared:GetPlayerByName(faded)
                     if (player and player:GetIsAlive()) then
                        fadedNextPlayersName[faded] = true
                        -- Print("Faded Player won: " .. faded
                        --      .. " is next")
                        player:FadedMessage(locale:ResolveString("FADED_SELECTION_ALIEN"))
                     end
                  end
               end
               self:EndGame(self.team2)
            elseif (FadedRoundTimer:GetIsRoundTimeOver()) then
               Shared:FadedMessage(locale:ResolveString("FADED_ROUND_TIME_OVER"))
               self:SaveEndRoundAliveMarine()
               self:EndGame(self.team1)
               --self:DrawGame()
            end
            -- If the game state has change (win/lose/draw)
            if self:GetGameState() ~= kGameState.Started then
               self:printFadedHealthArmorLeft()
            end
            self.timeLastGameEndCheck = Shared.GetTime()
         end
      end
   end


   function NS2Gamerules:SpawnClogMonster(orig)
      -- for _, spawn_point in ipairs(alien_spawn_points) do
      local main_base_coord = orig--spawn_point:GetOrigin()
      for i = 1, 3 do
         local orig = GetLocationAroundFor(main_base_coord, kTechId.Clog, 5)
         local ground_orig = orig
         if (orig) then
            local trace = Shared.TraceRay(orig, orig + Vector(0, 30, 0), CollisionRep.Damage, PhysicsMask.Flame, EntityFilterAll())
            --orig = orig - Vector(0, 0.6, 0)
            if (trace.fraction < 1 and trace.endPoint) then
               orig = trace.endPoint - Vector(0, 0.2, 0)
               if (orig) then
                  local clog = CreateEntity(Clog.kMapName, orig, 2)
                  local clog_nb = math.floor(ground_orig:GetDistanceTo(orig) / 1.3)
                  if (clog) then
                     clog:createClogWhip(Clamp(clog_nb, 3, 12))
                  end
               end
            end
         end
      end
      -- end
   end

   -- Spawn a set of structure at the marine main base
   -- for the objectif mode.
   function NS2Gamerules:SpawnMainBaseBuilding(marine_spawn_points)
      local struct = nil
      if (kFadedDisableObjectivMod) then return end
      -- Spawn a building at the valid places, coord_fix is a patch
      -- to fix the building beeing placed in the air
      ----------


      local rand_it = math.random(1, #marine_spawn_points)
      local main_base_coord = marine_spawn_points[rand_it]:GetOrigin()
      fadedMarinesMainBase = marine_spawn_points[rand_it]:GetLocationName()
      fadedMarinesMainBaseCoord = main_base_coord
      -- local main_base_coord = marine_spawn_points[1]:GetOrigin()
      -- FadedSpawnBuilding(kTechId.CommandStation, CommandStation.kMapName, main_base_coord, Vector(0, -1.6, 0))
      FadedSpawnBuilding(kTechId.Armory, Armory.kMapName, main_base_coord, Vector(0, -0.6, 0))
      -- FadedSpawnBuilding(kTechId.RoboticsFactory, RoboticsFactory.kMapName, main_base_coord, Vector(0, -0.6, 0))
      FadedSpawnBuilding(kTechId.Observatory, Observatory.kMapName, main_base_coord, Vector(0, -0.6, 0))
      FadedSpawnBuilding(kTechId.ArmsLab, ArmsLab.kMapName, main_base_coord, Vector(0, -0.6, 0))


      if (table.getn(marine_spawn_points) >= 2) then
         for elem in pairs(marine_spawn_points) -- Spawn phase gate
         do -- place for a CC, so we do not get stuck in something
            FadedSpawnBuilding(kTechId.Armory, PhaseGate.kMapName, marine_spawn_points[elem]:GetOrigin(), Vector(0, -0.6, 0))
         end
      end

      -- FadedSpawnBuilding(kTechId.SentryBattery, SentryBattery.kMapName, main_base_coord, Vector(0, -0.6, 0))

   end -- endfct

   -- Return fade_spawn_coord, {marine_spawn1, ...}
   function NS2Gamerules:GetBothTeamSpawnPoint()
      local spawn_point = {}
      local aliens_spawn_places = {}
      local marines_spawn_places = Shared.GetEntitiesWithClassname("TechPoint")

      for index, entity in ientitylist(marines_spawn_places) do
         table.insert(spawn_point, math.random(1, #spawn_point + 1), entity)
      end

      local fade_spawn_it = math.random(1, table.getn(spawn_point))
      local fade_spawn_coord = spawn_point[fade_spawn_it]

      -- Give at least 1 TP to aliens
      table.insert(aliens_spawn_places, spawn_point[fade_spawn_it])
      table.remove(spawn_point, fade_spawn_it)
      if (table.getn(spawn_point) > 4) then -- Let 2 TP for faded if many TP
         local rand_tp = math.random(1, #spawn_point)
         table.insert(aliens_spawn_places, spawn_point[rand_tp])
         table.remove(spawn_point, rand_tp)
      end
      return fade_spawn_coord, spawn_point, aliens_spawn_places
   end

   -- Check if there is room for at least a building (so we are sure)
   function GetLocationAroundFor(orig, tech_id, max_range)
      local max_range = 15
      local extents = GetExtents(kTechId.Armory)
      local struct = nil

      orig = orig + Vector(0, 0.2, 0) -- A bit above
      -- Team.RespawnPlayer(player:GetTeam(), player, orig, Angles(0, 0, 0)
      local spawnPoint = GetRandomBuildPosition(tech_id, orig, max_range)
      return spawnPoint
   end

   function SpawnSinglePlayer(player, orig)
      spawnPoint = GetLocationAroundFor(orig, kTechId.Armory, 15)
      if spawnPoint then
         player:SetOrigin(spawnPoint)
      else
         Team.RespawnPlayer(player:GetTeam(), player, orig, Angles(0, 0, 0))
      end
   end

   // Spawn at one point, standart system
   -- // Return spawn point not used by the faded
   function NS2Gamerules:FadedSpawnAtOnePoint(fade_spawn,
                                              marine_spawn_points,
                                              team1Players,
                                              team2Players)
      -- -- Print("Faded NS2Gamerules:FadedSpawnAtOnePoint()")
      -- For safety, spawn Fade on a free spawn point
      for _, faded in ipairs(team2Players) do
         -- Team.RespawnPlayer(faded:GetTeam(), faded, fade_spawn:GetOrigin(),
         --             Angles(0, 0, 0))
         SpawnSinglePlayer(faded, fade_spawn:GetOrigin())
      end
      for _, player in ipairs(team1Players) do
         local new_coord = marine_spawn_points[1]:GetOrigin()
         -- Team.RespawnPlayer(player:GetTeam(), player, new_coord, Angles(0, 0, 0))
         SpawnSinglePlayer(player, new_coord)
      end
   end

   // Dispatch marine around the map
   function NS2Gamerules:FadedSpawnMarineAroundMap(faded_coord, marine_spawn_points)
      -- Print("Faded NS2Gamerules:FadedSpawnMarineAroundMap()")
      local team1Players = self.team1:GetPlayers()
      local team2Players = self.team2:GetPlayers()

      self:FadedSpawnAtOnePoint(faded_coord, marine_spawn_points,
                                team1Players, team2Players)
      if (KFadedRandomDispatchSpawn == false) then return end

      -----------------------------------------
      -- Spawn Random for marine if above scale
      if (self.team1:GetNumPlayers() >= kFadedRandomDispatchMinPlayer)
      then
         for _, player in ipairs(team1Players)
         do
            local new_coord = math.random(1, table.getn(marine_spawn_points))
            new_coord = marine_spawn_points[new_coord]:GetOrigin()
            SpawnSinglePlayer(player, new_coord)
            -- Team.RespawnPlayer(player:GetTeam(), player, new_coord, Angles(0, 0, 0))
         end
      end

      -- Faded
      local rt_point = {}
      local rt_places = Shared.GetEntitiesWithClassname("ResourcePoint")

      for index, entity in ientitylist(rt_places) do
         table.insert(rt_point, entity)
      end

      -----------------------------------------
      -- For challenge, 1 out of 2 time
      -- change fade to a RT location if no marine around
      ----------------------------------------
      -- Spawn random for Fade
      if (math.random(0, 1) == 0) then return end
      local rand_rt = math.random(1, table.getn(rt_point))
      rand_rt = rt_point[rand_rt]:GetOrigin()
      local marine_around = GetEntitiesWithinRange("Marine",
                                                   rand_rt,
                                                   kFadedSafetySpawnRadius)
      -- If no marine around
      if (#marine_around == 0) then
         for _, faded in ipairs(team2Players) do
            local new_coord = rand_rt
            SpawnSinglePlayer(faded, new_coord)
            -- Team.RespawnPlayer(faded:GetTeam(), faded, new_coord, Angles(0, 0, 0))
         end
      end
   end

   // Define the rules for the game start
   function NS2Gamerules:CheckGameStart()
      -- Print("Faded NS2Gamerules:CheckGameStart()")
      local team1Players = self.team1:GetNumPlayers()
      local team2Players = self.team2:GetNumPlayers()

      local first_round_time = mod_loaded_time + kFadedNextMapWaitingTime
      if (Shared.GetTime() < first_round_time)
      then
         if (first_start_in_msg_refresh < Shared.GetTime())
         then
            local start_in = string.format("%.0f", first_round_time - Shared.GetTime())
            first_start_in_msg_refresh = first_start_in_msg_refresh + kFadedNextMapMessageRefresh
            Shared:FadedMessage(
               string.format("First round starting in %ds", start_in))
         end
         return
      end

      if (team1Players + team2Players == 1
          and Shared.GetTime() - (fadedModLastTimeTwoPlayersToStartMessage or 0) > kFadedModTwoPlayerToStartMessage) then
         -- if (Shared.FadedMessage) then
         --    Shared:FadedMessage(locale:ResolveString("FADED_GAME_NEEDS_TWO_PLAYERS"))
         -- end
         fadedModLastTimeTwoPlayersToStartMessage = Shared.GetTime()
         return
      end

      if self:GetGameState() == kGameState.NotStarted then
         if (team1Players > 1 or (team1Players > 0 and team2Players > 0))
         then
            self:SetGameState(kGameState.PreGame)
         end
      elseif self:GetGameState() == kGameState.PreGame then
         if (team1Players + team2Players == 1) then
            self:SetGameState(kGameState.NotStarted)
         end
      end
   end


   function NS2Gamerules:SaveEndRoundAliveMarine()
      -- Print("Faded NS2Gamerules:SaveEndRoundAliveMarine()")
      -- Get alive marine
      alive_marine = {}
      for _, marine in pairs(self.team1:GetPlayers()) do
         if (marine ~= nil and marine:GetIsAlive())
         then
            table.insert(alive_marine, marine:GetName())
         end
      end
   end

   -- If marine win, choose N veteran among the alive marines
   function NS2Gamerules:ChooseVeteranPlayers()
      -- Print("Faded NS2Gamerules:ChooseVeteranPlayers()")
      local cpt = 0
      local rand_nb = 0
      local max_veteran = kMarineNb / kFadedVeteranScale

      if (self.team1:GetNumPlayers() < 1) then return end
      -- If a veteran is alive, he keep his veteran
      -- If the veteran is the next Faded, skip
      for it, veteran_name in ipairs(alive_marine) do
         if (fadedVeteranPlayersName[veteran_name] == true
             and fadedPlayersName[alive_marine[rand_nb]] ~= true) then
            fadedNextVeteranPlayersName[alive_marine[it]] = true
            table.remove(alive_marine, it)
            cpt = cpt + 1
         end
      end

      -- Choose random veteran marine among survivor
      -- If the marine is the Faded or a Veteran, choose a new one
      while (cpt < max_veteran and table.getn(alive_marine) > 0) do
         rand_nb = math.random(1, table.getn(alive_marine))
         if (fadedPlayersName[alive_marine[rand_nb]] ~= true
                and not fadedNextVeteranPlayersName[alive_marine[rand_nb]])
         then
            fadedNextVeteranPlayersName[alive_marine[rand_nb]] = true
            cpt = cpt + 1
         end
         table.remove(alive_marine, rand_nb)
         -- end
      end
      alive_marine = {}
   end

   function NS2Gamerules:ChooseFadedPlayers()
      -- Print("Faded NS2Gamerules:ChooseFadedPlayers()")
      -- Choose a random player as Faded
      local cpt = 0
      local _marine_nb = GetGamerules():GetTeam1():GetNumPlayers()
      local new_faded_list = {}
      -- Print("NS2Gamerules:ChooseFaded()")
      while (_marine_nb > 0 and (cpt * kFadedAddFadeMarineScale) < _marine_nb) do
         local player = nil
         -- Retrieve the next player selected, if any
         for nextFadedName, value in pairs(fadedNextPlayersName) do
            if (value == true) then
               player = self:GetSingleFaded(nextFadedName)
               fadedNextPlayersName[nextFadedName] = nil
               break
            end
         end
         -- No more next faded at this point, reset
         if (player == nil) then
            fadedPlayersName = {}
         end
         -- If we choose the same player, choose again
         for i = 0, 10 do
            if (player == nil or new_faded_list[player:GetName()]) then
               player = self:GetSingleFaded(nil)
               break
            end
         end
         -- Add player to the list of new faded
         if (player) then
            new_faded_list[player:GetName()] = true
         end
         cpt = cpt + 1
      end
      fadedPlayersName = {} -- Reset
      fadedPlayersName = new_faded_list
      fadedNextPlayersName = {}
      -- Print("NS2Gamerules:ChooseFaded() END")
   end

   ------------

   function NS2Gamerules:RemoveBuilding()
      -- Print("Faded NS2Gamerules:RemoveBuilding()")
      -- Remove all resource points
      -- for index, entity in ientitylist(Shared.GetEntitiesWithClassname("ResourcePoint")) do
      --     DestroyEntity(entity)
      -- end

      -- Turn off the lights by removing all powernode
      for index, entity in ientitylist(Shared.GetEntitiesWithClassname("PowerPoint")) do
           if (entity.Kill and entity.SetConstructionComplete
               and entity.SetLightMode) then
              -- entity:SetLightMode(kLightMode.NoPower)
              entity:SetConstructionComplete()
              entity:Kill()
           end
           -- entity:SetModel(nil)
           -- DestroyEntity(entity)
      end
   end

   -- Print the mod version
   function NS2Gamerules:PrintModVersion()
      if (kFadedModVersion:lower():find("development")) then
         Shared:FadedMessage("Warning! This is a development version! It's for testing purpose only!")
      elseif (kFadedModVersion:lower():find("alpha")) then
         Shared:FadedMessage("Warning! This is an alpha version, which means it's not finished yet!")
      elseif (kFadedModVersion:lower():find("beta")) then
         Shared:FadedMessage("This mod is in beta. Feel free to leave feedback on the Steam workshop page.")
      end
   end

   -- Some info messages
   function NS2Gamerules:PrintInfoMessageAtRoundStart()
      -- Print("Faded NS2Gamerules:PrintInfoMessageAtRoundStart()")

      -- -- No need to tell the marines who is the Faded, check the scoreboard !
      -- for faded_name in pairs(fadedPlayersName) do
      --      self:GetTeam1():FadedMessage(strformat(locale:ResolveString("FADED_PLAYER_IS_NOW_THE_FADED"), faded_name))
      -- end

      if (kFadedModFriendlyFireEnabled) then
         self:GetTeam1():FadedMessage(locale:ResolveString("FADED_MARINE_GAME_STARTED_2"))
      end
      self:GetTeam1():FadedMessage(locale:ResolveString("FADED_MARINE_GAME_STARTED_1"))
   end

   local playerScores = {}
   function NS2Gamerules:SaveScore(allPlayers)
      -- Print("Faded NS2Gamerules:SaveScore()")
      playerScores = {}
      for _, fromPlayer in ientitylist(allPlayers) do
         local score = 0
         if HasMixin(fromPlayer, "Scoring") then
            score = fromPlayer:GetScore()
         end

         local playerScore = {
            Score = score,
            Kills = fromPlayer:GetKills(),
            Deaths = fromPlayer:GetDeaths()
         }

         local clientIndex = fromPlayer:GetClientIndex()
         playerScores[clientIndex] = playerScore
      end
   end

   function NS2Gamerules:RestoreScore(allPlayers)
      -- Print("Faded NS2Gamerules:RestoreScore()")
      for _, fromPlayer in ientitylist(allPlayers) do
         local clientIndex = fromPlayer:GetClientIndex()

         if HasMixin(fromPlayer, "Scoring") and playerScores[clientIndex] then
            -- fromPlayer.score = playerScores[clientIndex].Score
            fromPlayer:AddScore(playerScores[clientIndex].Score)
         end
         if (playerScores[clientIndex]) then
            fromPlayer.kills = playerScores[clientIndex].Kills
            fromPlayer.deaths = playerScores[clientIndex].Deaths
         end
      end
   end

   --------------

   function NS2Gamerules:SwitchFadedToMarineTeam()
      -- Print("Faded NS2Gamerules:SwitchFadedToMarineTeam()")
      -- Switch the faded to the marine team
      if (self.team2:GetNumPlayers() > 0) then
         -- for i, playerId in ipairs(self.team2.playerIds) do
         --    local alienPlayer = Shared.GetEntity(playerId)
         --    if (alienPlayer) then
         --       self:JoinTeam(alienPlayer, 1, false)
         --    end
         -- end
         local faded_list = self.team2:GetPlayers()
         for _, faded in pairs(faded_list) do
            self:JoinTeam(faded, 1, false)
         end
      end
   end

   -- Move marine into a Veteran marine
   function NS2Gamerules:MoveMarineToVeteran()
      -- Print("Faded NS2Gamerules:MoveMarineToVeteran()")
      local player = nil
      fadedVeteranPlayersName = {}
      if (self.team1:GetNumPlayers() <= 3) then
         for _, marine in pairs(self.team1:GetPlayers()) do
            fadedNextVeteranPlayersName[marine:GetName()] = true
         end
      end
      for k, v in pairs(fadedNextVeteranPlayersName) do
         player = Shared:GetPlayerByName(k)
         -- if (player and player:isa("Marine")) then
         --    Print("Replace marine to veteran player: " .. player:GetName())
         -- end
         if (player and player:isa("Marine")) then
            fadedVeteranPlayersName[player:GetName()] = true
            local list_msg =
               {
                  "FADED_VETERAN_INF0_1",
                  "FADED_VETERAN_INF0_2",
                  "FADED_VETERAN_INF0_3",
                  "FADED_VETERAN_INF0_4",
               }
            VeteranMarine:UpgradeMarine(player)
            -- -- Disable messages for Veteran (it spam the chat just too much)
            -- for k, v in pairs(list_msg) do
            --    player:FadedMessage(locale:ResolveString(v))
            -- end
            -- Shared:FadedMessage(strformat(locale:ResolveString("FADED_PLAYER_IS_NOW_A_VETERAN"), player:GetName()))
         end
      end
      -- Safety reset
      fadedNextVeteranPlayersName = {}
      alive_marine = {}
   end

   local function updateSpectatorBabblerIndicator(self)
      local table_ref = {}
      for _, name in ipairs(babblerPlayers) do
         table_ref[name] = true
      end
      for _, str in ipairs({"Spectator"}) do
         for _, spectator in ientitylist(Shared.GetEntitiesWithClassname(str)) do
            if (spectator and spectator.GetName
                   and table_ref[spectator:GetName()]
                and fadedPlayersName[spectator:GetName()] ~= true) then
               local fmtstr = locale:ResolveString("FADED_BABBLERPLAYER_RESPAWN_IN")
               local delay = babblerNextSpawnWave - Shared.GetTime()
               delay = string.format("%.1f", delay)
               local final_msg = string.format(fmtstr, delay)
               spectator:FadedMessage(final_msg)
            end
         end
      end
      Entity.AddTimedCallback(self, updateSpectatorBabblerIndicator, kFadedBabblerPlayerRespawnMsg)
   end

   local _babblerPlayerNextSpawnLocation = {}
   local function triggerBabblersPlayerRespawn(self)
      if (#_babblerPlayerNextSpawnLocation > 0) then
         local spawn_loc = _babblerPlayerNextSpawnLocation[1]
         table.remove(_babblerPlayerNextSpawnLocation, 1)
         GetGamerules():spawnBabblerPlayers(spawn_loc, 3)
         Entity.AddTimedCallback(self, triggerBabblersPlayerRespawn, 0.4)
      end
   end

   local function _getDistFromNeareastMarine(rt)
      if (rt) then
         local id = FindNearestEntityId("Marine", rt:GetOrigin())
         local nearest_marine = Shared.GetEntity(id)
         if (nearest_marine) then
            return (rt:GetOrigin():GetDistanceTo(nearest_marine:GetOrigin()))
         end
      end
      return (0)
   end

   local function sortSpawnByMarineProximity(rt_point)
      local i = 1
      local sorted_rt_point = {}
      local added = false
      for _, rt in ipairs(rt_point) do
         i = 0
         local m1 = _getDistFromNeareastMarine(rt)
         for it, rt2 in ipairs(sorted_rt_point) do
            local m2 = _getDistFromNeareastMarine(rt2)
            if (m1 < m2) then
               table.insert(sorted_rt_point, it, rt)
               added = true
               break
            end
         end
         if (added == false) then
            table.insert(sorted_rt_point, #sorted_rt_point + 1, rt)
         end
      end
      -- for it, rt in ipairs(sorted_rt_point) do
      --    Shared:FadedMessage("Nearest marine from rt n'" .. tostring(it)
      --                           .. " is at " .. _getDistFromNeareastMarine(rt) .. "m")
      -- end
      return (sorted_rt_point)
   end

   local function defaultBabblersPlayerRespawn(self)

      -- only spawn again if we are not currently spawning entities
      if (#_babblerPlayerNextSpawnLocation == 0) then
         local rt_point = {}
         local l_rt_places = {
            Shared.GetEntitiesWithClassname("TechPoint"),
            Shared.GetEntitiesWithClassname("ResourcePoint"),
            -- Shared.GetEntitiesWithClassname("Ragdoll")
         }
         for _, rt_places in ipairs(l_rt_places) do
            for index, entity in ientitylist(rt_places) do
               local marines = nil
               marines = GetEntitiesForTeamWithinRange("Marine", 1,
                                                       entity:GetOrigin(),
                                                       kFadedMinDistFromMarineToRespw)
               if (#marines == 0)
               then
                  table.insert(rt_point, entity)
                  -- Shared:FadedMessage("Insert done")
               else
                  -- Shared:FadedMessage("Marine around: " .. tostring(#marines))
               end
            end
         end
         local i = 0
         rt_point = sortSpawnByMarineProximity(rt_point)
         local rt_nb = 0--math.random(1, table.getn(rt_point))
         if (#rt_point > 0) then
            while (i < #babblerPlayers) do
               rt_nb = ((rt_nb + 1) % #rt_point) + 1
               table.insert(_babblerPlayerNextSpawnLocation,
                            rt_point[rt_nb]:GetOrigin())
               -- GetGamerules():spawnBabblerPlayers(rt_point[rt_nb]:GetOrigin(), 1)
               i = i + 3
            end
            triggerBabblersPlayerRespawn(self)
         end
      end

      local fade_nb = 0
      for _ in pairs(fadedPlayersName) do
         fade_nb = fade_nb + 1
      end
      local skulk_nb = self.team2:GetNumPlayers() - fade_nb
      skulk_nb = Clamp(skulk_nb, 0, kFadedMaxBabblerPlayerSpawnDelay)
      -- babblerNextSpawnWave = Shared.GetTime() + kFadedBabblerPlayerSpawnInterval
      local wave_time = 1 + skulk_nb * kFadedDelayOnSpawnWavePerBabbler
      babblerNextSpawnWave = Shared.GetTime() + wave_time

      ---
      local proto = GetEntitiesForTeam("PrototypeLab", 1)
      if (kFadedInstantBabblerSpawnWithProto
          and #proto > 0 and proto[1]:GetIsPowered() == true) then
         Entity.AddTimedCallback(self, defaultBabblersPlayerRespawn, 5)
      else
         Entity.AddTimedCallback(self, defaultBabblersPlayerRespawn, wave_time)
      end

   end

   local function movePlayerToFadedSpectator(self)
      local player = nil
      for player_name, v in pairs(spectateFadedPlayers) do
         player = Shared:GetPlayerByName(player_name)
         if (v and player and player:GetIsAlive() == false
                and fadedPlayersName[player:GetName()] ~= true
             and player:GetTeamNumber() ~= kSpectatorIndex) then
            self:JoinTeam(player, kSpectatorIndex)
         end
      end
      Entity.AddTimedCallback(self, movePlayerToFadedSpectator, 3)
   end

   local InitCallbackDone = false
   function NS2Gamerules:InitCallback()
      if (InitCallbackDone == false) then
         Entity.AddTimedCallback(self, defaultBabblersPlayerRespawn, 3)
         Entity.AddTimedCallback(self, updateSpectatorBabblerIndicator, kFadedBabblerPlayerRespawnMsg)
         Entity.AddTimedCallback(self, movePlayerToFadedSpectator, 3)
         Entity.AddTimedCallback(self, SpawnSentryBatteryOnMap, kFadedBatterySpawnDelay)
         -- Entity.AddTimedCallback(self, refreshAllClogWhipPos, 0.1)
         InitCallbackDone = true
         babblerNextSpawnWave = Shared.GetTime() + 3
      end
   end

   local kFadedLastMarineEvent = false
   local lastEpiqueMusic = nil
   local music = {
      PrecacheAsset("sound/NS2.fev/music/main_menu"),
      -- PrecacheAsset("sound/NS2.fev/ambient/spooky_music_summit"),
      -- PrecacheAsset("sound/NS2.fev/common/explore")
   }

   local resetGame = NS2Gamerules.ResetGame
   function NS2Gamerules:ResetGame()
      -- Print("Faded NS2Gamerules:ResetGame()")
      -- Disable auto team balance
      -- See  FadedRagdoll.lua
      -- if (self:GetGameStarted()) then
      --      self:SpawnSentryBatteryOnMap(true)
      -- end
      -- clearAllClogsWhip()
      startingBatteryDropped = false
      kFadedLastMarineEvent = false
      Server.SetConfigSetting("auto_team_balance", nil)

      -- Move all previous alien/Faded spectator to marine team
      local function _MoveToMarineTeam(player)
         -- Do not move spectator and admin in RR into game
         local isPlaying = player:GetTeamNumber() == 2 or player:GetTeamNumber() == 1
         local isFadedSpectator = spectateFadedPlayers[player:GetName()]
         if (player and (isPlaying or isFadedSpectator))
         then
            player:SetCameraDistance(0)
            self:JoinTeam(player, 1, true)
            if (babblerPlayersAutoJoin[player:GetName()] ~= true)
            then -- auto add once in the babbler player list
               babblerPlayersAutoJoin[player:GetName()] = true
               BabblerPlayerChatCommand(player, true)
            end
         end
      end
      Server.ForAllPlayers(_MoveToMarineTeam)

      local allPlayers = Shared.GetEntitiesWithClassname("Player")
      -- self:SaveScore(allPlayers)
      resetGame(self)
      -- self:RestoreScore(allPlayers)

      -- Marine count and faded nb count
      kMarineNb = GetGamerules():GetTeam1():GetNumPlayers()
      if (kMarineNb < 0) then kMarineNb = 0 end
      if (kMarineNb <= 0) then
         Print("Faded error: Try to reset a game with no marine")
         return
      end
      self:InitCallback()

      kPlayerHallucinationNumFraction = kMarineNb

      -- Count fade number for the next round
      kFadeNb = 0
      while ((kFadeNb * kFadedAddFadeMarineScale) < kMarineNb) do
         kFadeNb = kFadeNb + 1
      end

      -- Choose Faded
      self:ChooseFadedPlayers()

      -- local function _updateFadedList(player)
      --      for _, faded in ipairs(fadedPlayersName) do
      --         Server.SendNetworkMessage(player, "AddScoreboardFadeList",
      --                       { name = faded:GetName()}, true)
      --      end
      -- end
      -- local function _resetFadedScoreBoardList(player)
      --      Server.SendNetworkMessage(player, "ResetFadedScoreboardList",
      --                    {}, true)
      -- end

      -- -- Check if we have at least 1 Faded
      -- Server.ForAllPlayers(_resetFadedScoreBoardList)
      -- Server.ForAllPlayers(_updateFadedList)
      for faded in pairs(fadedPlayersName) do
         -- Move selected Faded to alien team

         local function _MoveToAlienTeam(player)
            if (fadedPlayersName[player:GetName()] == true
                and self.team2:GetNumPlayers() <= kFadeNb) then
               player:SetCameraDistance(0)
               self:JoinTeam(player, 2, true)
            end
         end
         Server.ForAllPlayers(_MoveToAlienTeam)

         -- Convert alien to Fade
         local function _SpawnFaded(player)
            if (player:GetTeamNumber() == 2) then
               kFadedSpawnFadeHallucination = false
               -- local p = player:Replace(Marine.kMapName) -- Fake marine test
               -- if (math.random(1, 2) == 1) then
               --       p:GiveItem(Flamethrower.kMapName)
               -- else
               --       p:GiveItem(Shotgun.kMapName)
               -- end
               -- player:Replace(Skulk.kMapName)
               player:Replace(Fade.kMapName)
               DestroyEntity(player)
               kFadedSpawnFadeHallucination = true
            end
         end
         Server.ForAllPlayers(_SpawnFaded)

         -- Create veteran among alive marine
         self:ChooseVeteranPlayers()
         self:MoveMarineToVeteran()
         -- Some info messages
         self:PrintInfoMessageAtRoundStart()
         self:PrintModVersion()
         break
      end

      -- Spawn player around the map
      local faded_coord, marine_spawn_points, unused_tp = self:GetBothTeamSpawnPoint()
      self:SpawnMainBaseBuilding(marine_spawn_points)

      -- self:SpawnClogMonster(faded_coord)
      self:FadedSpawnMarineAroundMap(faded_coord, marine_spawn_points)
      ---------------------------
      -- for _, tp in ipairs(unused_tp) do
      --    self:SpawnClogMonster(tp:GetOrigin()) -- One clog monster per unused TP
      -- end
      -- local rt = {}
      -- for _, _rt in ientitylist(Shared.GetEntitiesWithClassname("ResourcePoint")) do
      --    table.insert(rt, _rt)
      -- end
      -- for i = 1, #rt/2 do
      --    local _rt1 = rt[math.random(1, #rt)]
      --    local _rt2 = rt[math.random(1, #rt)]
      --    local points = PointArray()
      --    Pathing.GetPathPoints(_rt1:GetOrigin(), _rt2:GetOrigin(), points)
      --    if (points and #points > 0) then
      --       self:SpawnClogMonster(points[math.random(1, #points)])
      --    end
      -- end
      ---------------------------

      -- Reset the mod timer
      FadedRoundTimer:Reset()
      -- Init Timer GUI for all player with the new time
      for _, fromPlayer in ientitylist(allPlayers) do
         reapplySkinChoice(fromPlayer)
         Server.SendNetworkMessage(fromPlayer, "RoundTime",
                                   {time = kFadedModRoundTimerInSecs},
                                   true)
         Server.SendNetworkMessage(fromPlayer, "kMarineNb_kFadeNB",
                                   {kMarineNb = kMarineNb, kFadeNb = kFadeNb},
                                   true)
      end


      self:RemoveBuilding()
   end

   -- Return the Ids of players in a marine team without the last fade
   -- Note: Player who type "nofade" are removed
   function NS2Gamerules:GetPossiblePlayerIdsForTeam()
      local playerIds = {}
      for _, player in ipairs(self.team1:GetPlayers()) do
         local playerId = player:GetId()
         local clientIndex = player:GetClientIndex()

         -- Exclude Faded player
         if (not self.noFaded[clientIndex]
                and fadedNextPlayersName[player:GetName()] ~= true
             and fadedPlayersName[player:GetName()] ~= true) then
            tinsert(playerIds, playerId)
         end
      end

      return playerIds
   end

   -- Return the Ids of players in marine team
   function NS2Gamerules:GetPossiblePlayerIds()
      -- Print("Faded NS2Gamerules:GetPossiblePlayerIds()")
      local playerTeam = self:GetPossiblePlayerIdsForTeam()
      return playerTeam
   end

   -- Faded selection
   -- The variable "fadedNextPlayer" contain the name of the player
   -- who won the round, which can be:
   -- * The Faded
   -- * The marine who kill the Faded.
   -- The selection is divided in 2 part:
   -- 1/ Check if the winner can be the faded
   -- ** (according to "faded_selection_chance")
   -- 2/ If not, pick a random player
   function NS2Gamerules:GetSingleFaded(fadedNextPlayerName)
      -- Print("Faded NS2Gamerules:GetSingleFaded()")
      local player = nil
      -- Print("NS2Gamerules:GetSingleFaded()")
      if (not FadedMod.nextFaded) then
         -- Use a random number to check if the winner can be the Faded
         -- Random return a float, range: (min:0.0, max:0,9)
         if (fadedNextPlayerName ~= nil) then
            local randomNumber = math.random(0, 100)
            if (fadedPlayersName[fadedNextPlayerName] == nil) then
               if (randomNumber <= kFadedModFadedSelectionChance)
               then -- If we just kill a fade
                  -- Print("Choice 80: " .. tostring(fadedNextPlayerName))
                  player = Shared:GetPlayerByName(fadedNextPlayerName)
               end
            else
               if (randomNumber <= kFadedModFadedNextSelectionChance)
               then -- Chance to be fade again
                  -- Print("Choice 60: " .. tostring(fadedNextPlayerName))
                  player = Shared:GetPlayerByName(fadedNextPlayerName)
               end
            end
         end

         -- Remove winner if they have typed nofaded
         if (player and self.noFaded[player:GetClientIndex()]) then
            player = nil
         end
         -- If not choose a random player
         -- Remove the fadedNextPlayer from the random choice
         -- so he get exactly FadedModFadedSelectionChance chance
         local player_list = self:GetPossiblePlayerIds()
         if (not player and player_list and #player_list >= 1)
         then
            if (Shared.GetRandomPlayer) then
               player = Shared:GetRandomPlayer(player_list)
            end
            -- Print("Random choice: " .. player:GetName())
         end
      end
      -- Print("Faded NS2Gamerules:GetSingleFaded() END")
      return (player)
   end

   // Reset the game on round end.
   function NS2Gamerules:UpdateToReadyRoom()
      -- Print("Faded NS2Gamerules:UpdateToReadyRoom()")
      local state = self:GetGameState()
      if (state == kGameState.Team1Won or state == kGameState.Team2Won or state == kGameState.Draw) then
         // Check if we need to change the map
         if (fadedMapTime >= (cycle.time * 60) - 10 and fadedMapTime >= 0) then
            local function _MoveToRR(player)
               self:JoinTeam(player, kTeamReadyRoom)
            end
            Server.ForAllPlayers(_MoveToRR)
            -- self.timeToCycleMap = Shared.GetTime() + kFadedModTimeTillMapChange
            fadedMapTime = -1
         elseif (fadedMapTime > 0) then
            if (self.timeSinceGameStateChanged >= kFadedModTimeTillNewRound) then
               // Reset the game
               self:ResetGame()

               // See if there are enough players, otherwise stop the game
               local team1Players = self.team1:GetNumPlayers()
               local team2Players = self.team2:GetNumPlayers()

               if (team1Players + team2Players <= 1) then
                  // Set game state to not started
                  self:SetGameState(kGameState.NotStarted)
                  self.countdownTime = 6
                  self.lastCountdownPlayed = nil
                  self:EndGame(self.team1)
               else
                  // Set game state to countdown
                  self:SetGameState(kGameState.Countdown)
                  self.countdownTime = 6
                  self.lastCountdownPlayed = nil
               end
            end
         end
      end
      -- Print("Faded NS2Gamerules:UpdateToReadyRoom() END")
   end

   // Display a timer on pre game
   local updatePregame = NS2Gamerules.UpdatePregame
   function NS2Gamerules:UpdatePregame(timePassed)
      -- Print("Faded NS2Gamerules:UpdatePregame()")
      if (self:GetGameState() == kGameState.PreGame) then
         local preGameTime = kFadedModPregameLength

         if (self.timeSinceGameStateChanged > preGameTime) then
            fadedPregameTenSecMessage = false
            fadedPregameFiveSecMessage = false
         else
            if (fadedPregameFiveSecMessage == false and floor(preGameTime - self.timeSinceGameStateChanged) == 5) then
               Shared:FadedMessage(strformat(locale:ResolveString("FADED_GAME_STARTS_IN"), 5))
               fadedPregameFiveSecMessage = true
               fadedPregameTenSecMessage = true
            elseif (fadedPregameTenSecMessage == false and floor(preGameTime - self.timeSinceGameStateChanged) == 10) then
               Shared:FadedMessage(strformat(locale:ResolveString("FADED_GAME_STARTS_IN"), 10))
               fadedPregameTenSecMessage = true
            end
         end
      end

      updatePregame(self, timePassed)
      -- Print("Faded NS2Gamerules:UpdatePregame() END")
   end

   -- Send to each player the new name
   function NS2Gamerules:networkBroadCastName(marine,
                                              newform_name, corpse_name)

      -- for _, marine in pairs(self.team1:GetPlayers()) do
      -- Print("Sending to " .. marine:GetName())
      Server.SendNetworkMessage(
         marine, "setFakeMarineName",
         {
            fakeMarineRealName = newform_name,
            fakeMarineName = corpse_name,
         }, true)
   end

   function NS2Gamerules:networkBroadCastFakeMarineName(marine)
      for _, fakeMarine in pairs(self.team2:GetPlayers()) do
         if (fakeMarine:isa("Marine") and fakeMarine:GetIsAlive()) then
            self:networkBroadCastName(marine,
                                      fakeMarine:GetName(),
                                      fakeMarine:GetFakeMarineName())
         end
      end
   end

   -- MH stand for MentalHealth
   -- Update all marine health

   function NS2Gamerules:UpdateSpectatorMentalBar(marine)
      local owner = nil
      for _, str in ipairs({"Spectator"}) do
         for _, spectator in ientitylist(Shared.GetEntitiesWithClassname(str)) do
            owner = Server.GetOwner(spectator)
            -- if (spectator and spectator.GetName) then
            --    Shared:FadedMessage("Is spectator: " .. spectator:GetName() .. " spectating " .. marine:GetName())
            -- end
            if spectator and marine and owner
            and marine == owner:GetSpectatingPlayer() then
               Server.SendNetworkMessage(
                  spectator, "MentalHealthSystem",
                  {
                     MentalHealthLevel = marine:GetMentalLevel(),
                     MentalHealth = marine:GetMentalHealth(),
                     MentalRege = marine:GetMentalRege(),
                     MentatUse = marine:GetMentalUse(),
                     LightLeft = marine.kFadedLightBatteryLeft,
                  }, true)
               for _, fakeMarine in pairs(self.team2:GetPlayers()) do
                  if (fakeMarine:isa("Marine") and fakeMarine:GetIsAlive()) then
                     self:networkBroadCastName(spectator,
                                               fakeMarine:GetName(),
                                               fakeMarine:GetFakeMarineName())
                  end
               end -- endfor
            end -- endif
         end -- end for2
      end -- end for1
   end -- endfct


   -- Update rate of message sent over the network to the client
   local kFadedNetworkSend = 0
   local kFadedNetworkSendRate = 0.5
   -- Decrease spectator mental health network
   -- local kFadedNetworkSpecSend = 2
   -- local kSpecCpt = 0
   function NS2Gamerules:UpdateMarineMentalHealth(timePassed)
      kFadedNetworkSend = kFadedNetworkSend + timePassed
      for _, marine in pairs(self.team1:GetPlayers()) do
         -- Only update mental health if he is alive
         if (marine ~= nil and marine:GetIsAlive()
             and marine.UpdateMentalHealthByVal) then
            local update_val = MentalHealthEffect:GetMentalHealthUpdate(
               marine,
               self.team1:GetPlayers(),
               timePassed)
            marine:UpdateMentalHealthByVal(update_val)
            marine:ApplyMentalHealthEffect(timePassed)
            -- Send to client there MentalHealth
            if (kFadedNetworkSend > kFadedNetworkSendRate) then
               -- Player stay often a long time at 0% or 100% of sanity, to not send to network
               Server.SendNetworkMessage(
                  marine, "MentalHealthSystem",
                  {
                     MentalHealthLevel = marine:GetMentalLevel(),
                     MentalHealth = marine:GetMentalHealth(),
                     MentalRege = marine:GetMentalRege(),
                     MentatUse = marine:GetMentalUse(),
                     LightLeft = marine.kFadedLightBatteryLeft,
                  }, true)
               -- kSpecCpt = kSpecCpt + 1
               -- if (kSpecCpt >= kFadedNetworkSpecSend) then
               --       kSpecCpt = kSpecCpt - kFadedNetworkSpecSend
               self:UpdateSpectatorMentalBar(marine)
               -- end
            end
         end
      end
      if (kFadedNetworkSend > kFadedNetworkSendRate) then
         for _, marine in pairs(self.team1:GetPlayers()) do
            self:networkBroadCastFakeMarineName(marine)
         end
         for _, marine in pairs(self.team2:GetPlayers()) do
            self:networkBroadCastFakeMarineName(marine)
         end

         kFadedNetworkSend = kFadedNetworkSend - kFadedNetworkSendRate
      end
      MentalHealthEffect:PostUpdate(timePassed)
   end

   function NS2Gamerules:TriggerAntiCamp(player)
      -- no anticamp when cheats are one
      if (kFadedAntiCampEnable == false or Shared.GetCheatsEnabled()) then return end
      if (self:GetGameStarted()) then
         local babbler = GetEntitiesForTeam("Babbler", 2)
         -- limit of babbler to avoid server crash
         if (#babbler < kMaxBabblerOnMap) then
            local cpt = 0
            player:FadedMessage(locale:ResolveString("FADED_ANTI_CAMP_WARNING"))
            while (cpt < kFadedNumBabblersPerEgg) do
               local egg = CreateEntity(BabblerEgg.kMapName, player:GetOrigin(), 2)
               if (egg) then
                  egg:SetCamperInfo(player)
               end
               cpt = cpt + kNumBabblersPerEgg
            end
            self:spawnBabblerPlayers(player:GetOrigin(), kFadedNumBabblersPerEgg)
         end
      end
   end

   -- Anti camp system
   -- If a marine stay in the same N meters during kCampMaxTime time,
   -- a number of bablers egs spawn. If he continue to stay on this zone,
   -- babler will then poped out every M seconds
   local bablerAntiCamp_time_delay = 0
   function NS2Gamerules:BabblerAntiCamp(timePassed)
      if (kFadedAntiCampEnable == false or Shared.GetCheatsEnabled()) then return end
      bablerAntiCamp_time_delay = bablerAntiCamp_time_delay + timePassed
      if (bablerAntiCamp_time_delay > 1) then
         for _, marine in pairs(self.team1:GetPlayers()) do
            -- local marines_building = GetEntitiesWithMixinWithinRange(
            --    "PowerConsumer", marine:GetOrigin(), 15)
            if (marine:GetIsAlive() and marine:isa("Marine")) then
               local origin = marine:GetOrigin()
               local distance = origin:GetDistanceTo(marine.kFadedLastCampPosition)
               if (distance < 0) then
                  distance = distance * -1
               end
               if (distance <= kAntiCampRadius) then
                  marine.kFadedCampDuration = marine.kFadedCampDuration + bablerAntiCamp_time_delay
               else
                  marine.kFadedLastCampPosition = marine:GetOrigin()
                  marine.kFadedCampDuration = 0
               end
               if (marine.kFadedCampDuration >= kCampMaxTime) then
                  -- Only substract 10s
                  marine.kFadedCampDuration = kCampMaxTime - 10
                  self:TriggerAntiCamp(marine)
               end
               -- Add shadow design to babbler
               for _, babbler in ipairs(GetEntitiesForTeam("Babbler", 2)) do
                  babbler:SetVariant(kGorgeVariant.shadow)
               end
            end
         end

         bablerAntiCamp_time_delay = bablerAntiCamp_time_delay - 1
      end
      -- Force egs construction
      for _, egs in ipairs(GetEntitiesForTeam("BabblerEgg", 2)) do
         egs:Construct(timePassed)
      end
   end

   -- -- Send a chat message to fade to teach how to use corpse
   -- function NS2Gamerules:fadeDisguiseMessageHelp(timePassed)
   --    for _, faded in ipairs(GetEntitiesForTeam("Fade", 2)) do
   --      if (faded.kFadedCorpseMessageHelp) then
   --         local corpse_around = 0
   --         corpse_around = GetEntitiesWithinRange("Marine", faded:GetOrigin(), 10)
   --         for _, m in ipairs(corpse_around) do
   --            if (m:GetIsAlive() == false) then
   --           -- local msg = m:GetClient().SubstituteBindStrings("Press BIND_Drop to use your Faded back")
   --           -- Print(msg)

   --           -- faded:FadedMessage("Press 'Use'(E) on a corpse to disguise your self")
   --           -- faded:FadedMessage("Or Press 'Request Medpack'(A) to eat the corpse an regen health.")

   --           faded.kFadedCorpseMessageHelp = false
   --           break
   --            end
   --         end
   --      end
   --    end
   -- end

   -- local _BurnBodiesTimeInterval = 0
   -- function NS2Gamerules:burnBodiesOrder(timePassed)
   --    _BurnBodiesTimeInterval = _BurnBodiesTimeInterval + timePassed
   --    if (_BurnBodiesTimeInterval > 10) then
   --      _BurnBodiesTimeInterval = 0
   --      for _, marine in pairs(self.team1:GetPlayers()) do
   --         local active_weap = marine:GetActiveWeapon()
   --         if (marine:GetIsAlive() and active_weap
   --            and active_weap:GetMapName() == Flamethrower.kMapName) then
   --            -- marine:GiveOrder()
   --            -- marine:GiveOrder(kTechId.Attack, newTarget:GetId(), nil)
   --            marine:GiveOrder(kTechId.Attack, newTarget:GetId(), nil)
   --         end
   --      end
   --    end
   -- end

   -- Raise an alert to veteran Marine if a Fake Marine is nearby
   local _AttackOrderTimeInterval = kVeteranAlertRefreshTime
   function NS2Gamerules:alertVeteranOfNearbyFakeMarine(timePassed)
      _AttackOrderTimeInterval = _AttackOrderTimeInterval - timePassed
      if (_AttackOrderTimeInterval < 0) then
         _AttackOrderTimeInterval = kVeteranAlertRefreshTime
         local nearbyFakeMarine = 0
         local veteran = nil
         -- For each Veteran
         for _, marine in pairs(self.team1:GetPlayers()) do
            if (marine:GetMaxHealth() == kFadedVeteranHealth
                   and marine:GetMaxArmor() == kFadedVeteranArmor
                and marine:GetIsAlive()) then
               -- For each Fake marine alive
               local hasOrder = false
               for _, fakeMarine in pairs(self.team2:GetPlayers()) do
                  if (fakeMarine:GetIsAlive() and fakeMarine:isa("Marine"))
                  then
                     -- Check distance and give order
                     local orig1 = marine:GetOrigin()
                     local orig2 = fakeMarine:GetOrigin()
                     local distance = orig1:GetDistanceTo(orig2)
                     if (distance < 0) then
                        distance = distance * -1
                     end
                     if (distance <= kVeteranFakeMarineRangeDetection) then
                        marine:GiveOrder(kTechId.Attack, fakeMarine:GetId(), orig2, nil, true, false)
                        fakeMarine:FadedMessage("Warning: " .. marine:GetName() .. " is a veteran, he can detect you.")
                        hasOrder = true
                     end

                  end
               end
               -- Clear the order if the fake marine is not around anymore
               if (hasOrder == false) then
                  marine:ClearOrders()
               end
            end
         end
      end
   end

   function NS2Gamerules:_test()

      local numPlayers = 0
      local numRookies = 0

      -- Player may have been deleted this tick, so check id to make sure player count is correct)
      for index, player in ipairs(GetEntities("Entity")) do

         if (player ~= nil and player:GetClientIndex() == -1) then
            numPlayers = numPlayers + 1
            Print("EntityName == " .. EntityToString(player))
         end
      end

      if (numPlayers > 0) then
         Print("Num RAgdol == " .. tostring(numPlayers))
      end
   end

   local refresh_LastMarineEvent_time = 0
   function NS2Gamerules:triggerLastMarineEvent(timePassed)
      refresh_LastMarineEvent_time = refresh_LastMarineEvent_time + timePassed
      if (refresh_LastMarineEvent_time > 3) then
         refresh_LastMarineEvent_time = refresh_LastMarineEvent_time - 3
         local last_marine = nil
         -- Print("Faded NS2Gamerules:triggerLastMarineEvent()")
         if (kFadedLastMarineEvent == false) then
            local nb_marine_alive = 0
            for _, marine in ipairs(self.team1:GetPlayers()) do
               if (marine and marine:GetIsAlive()) then
                  nb_marine_alive = nb_marine_alive + 1
                  last_marine = marine
               end
            end

            local nb_players = self.team1:GetNumPlayers() + self.team2:GetNumPlayers()
            -- Only make the epic music if we have some people in the game
            if (nb_marine_alive == 1 and nb_players >= 10) then
               kFadedLastMarineEvent = true
               lastEpiqueMusic = Server.CreateEntity(SoundEffect.kMapName)
               lastEpiqueMusic:SetAsset(music[random(1, #music)])
               lastEpiqueMusic:SetParent(last_marine)
               lastEpiqueMusic:Start()
               -- for _, fromPlayer in ientitylist(allPlayers) do
               --    Server.SendNetworkMessage(fromPlayer, "LastMarineAlive",
               --                  {},
               --                  true)
               -- end
            end
         end
      end
   end



   -- coord: where to spawn them
   -- nb: how many slot max
   function NS2Gamerules:spawnBabblerPlayers(coord, nb)
      local player = nil
      local it_player_respawn = {}
      for it, player_name in ipairs(babblerPlayers) do
         if (nb <= 0) then
            break
         end
         player = Shared:GetPlayerByName(player_name)
         -- Shared:FadedMessage("Respawning " .. player_name
         --             .. " nb = " .. tostring(nb)
         --             .. " v = " .. tostring(v)
         --             .. " isnull = " .. tostring(player)
         --             .. " is alive = " .. tostring(player:GetIsAlive()))
         if (nb > 0 and player and player:GetIsAlive() == false) then
            -- Shared:FadedMessage("Can respawn")
            local success = true
            if (fadedPlayersName[player:GetName()] ~= true) then
               if (player:GetTeamNumber() ~= 2) then
                  success, player = self:JoinTeam(player, 2, true)
                  -- else -- simple respawn
                  --       if (player.Reset) then
                  --          player:Reset()
                  --          if player.OnReset then
                  --         player:OnReset()
                  --          end
                  --       end
               end

               if (success and player) then
                  local randx = (math.random(0, 1) / 2) * (-1 * math.random(0, 1))
                  local randy = (math.random(0, 1) / 2) * (-1 * math.random(0, 1))
                  local new_coord = coord + Vector(randx, 2, randy)
                  local new_player = player:Replace(
                     Skulk.kMapName, 2, false, new_coord)
                  if (new_player) then
                     -- SpawnSinglePlayer(new_player, new_coord)
                     nb = nb - 1
                     table.insert(it_player_respawn, it)
                     spawnSquad(new_player, new_player:GetXPLevel() - 1)
                     babbler_xeno_byte[new_player:GetName()] = 0
                  end
                  -- if (new_player) then
                  --    new_player:SetOrigin(coord)
                  -- end
               end
            end
         end
      end
      for _, it in ipairs(it_player_respawn) do
         -- Put at the end of the list to rotate
         table.insert(babblerPlayers, babblerPlayers[it])
         table.remove(babblerPlayers, it)
      end
      return #it_player_respawn
   end

   function SpawnSentryBatteryOnMap(self, force, nocallback)
      if (kFadedDisableObjectivMod) then return end
      if (force == true or self:GetGameStarted()) then
         local rt_point = {}
         local rt_places = Shared.GetEntitiesWithClassname("ResourcePoint")
         for index, entity in ientitylist(rt_places) do
            table.insert(rt_point, entity)
         end
         local i = 0
         while (i < 10 and table.getn(rt_point) > 1) do -- 10 try for the battery spawn
            local rand_rt = math.random(1, table.getn(rt_point))
            local st = nil
            local force_coord = true

            i = i + 1
            -- No sentry battery in main base
            if (rt_point[rand_rt]:GetLocationName() ~= fadedMarinesMainBase) then
               st = FadedSpawnBuilding(kTechId.SentryBattery,
                                       SentryBattery.kMapName,
                                       rt_point[rand_rt]:GetOrigin(),
                                       Vector(0, 0, 0), 5, force_coord)
            end

            if (st) then
               Shared:FadedMessage(string.format(
                                      locale:ResolveString("FADED_BATTERY_SPAWN"),
                                      st:GetLocationName()))
               break
            end
         end
      end
      if (nocallback ~= true) then
         Entity.AddTimedCallback(self, SpawnSentryBatteryOnMap, kFadedBatterySpawnDelay)
      end
   end

   local randomHintMessage_delay = 0
   -- local hint_nb_displayed = 0
   local hint_messages =
      {
         -- Marines
         "You have more chance to become the faded if you kill it.",
         "As a marine, you can purchase a weapon by pressing 'buy' when the round begin.",
         "Staying in group is better for your sanity and safety, but if someone die it will hurt the whole group sanity an lead to a panic situation.",
         "If your sanity pool is too low, you will start seeing hallucination.",
         -- "Ambiant light intensity decrease with sanity, keep it up.",
         "As a veteran, you have access to the minimap. Lead the group.",
         "Do not go alone, you will quicky die.",
         "As a marine, keep your sanity up with the flashlight.",
         -- Obj,
         "Find battery and power up the main base, it will give you up & weapons.",
         "If you see an alive marine marked as dead in the scoreboard, kill him, its the faded.",
         -- Weapons
         "If you have a flamethrower, remember to burn corpses.",
         "Mines are good to trap dead bodies and hit the faded where it hurts.",
         "Using the right click with the GL will throw a pulse grenade.",
         "Using the right click with the Pistol will throw flare.",
         -- Faded
         "As a Faded, you can use corpses to heal yourself or disguise.",
         "As a fake marine, you can stab from behind.",
         "As a fake marine, you can hear marines voice chat.",
         "The faded stab instantly kills a marine if he is alone",
         "As a fake marine, your weapon does not deal any damages.",
         "As a fake marine, do not approach Veteran, they can see you if you are too close.",
         "As a fake marine, you can take phase gate. Use them.",
         "The Faded has leap, blink, wallgrip, and bilebomb.",
         "The flashlight turns red if the faded is in front of you.",
         -- Other
         "If you stay for too long in a room, babblers will spawn. Keep moving.",
         -- "Want to turn off those hint ? type 'no_hint' in the chat.",
         "The 'spectatefade' chat command is usefull. If dead you turns into a spectator, and back again as a marine the next round.",
         "Stuck on a wall ? type 'unstuck' on the chat.",
      }
   local hint_messages_tmp = {}
   function NS2Gamerules:randomHintMessage(timePassed)
      if (kFadedHintEnable == true) then
         randomHintMessage_delay = randomHintMessage_delay + timePassed
         if (randomHintMessage_delay > 60) then
            randomHintMessage_delay = 0
            if (table.getn(hint_messages_tmp) == 0) then
               for k, v in ipairs(hint_messages) do
                  hint_messages_tmp[k] = v
               end
            end
            -- hint_nb_displayed = hint_nb_displayed + 1
            -- if (hint_messages[hint_nb_displayed] == nil) then
            --    hint_nb_displayed = 1
            -- end
            local msg_nb = math.random(1, table.getn(hint_messages_tmp))
            Shared:FadedHintMessage(hint_messages_tmp[msg_nb])
            table.remove(hint_messages_tmp, msg_nb)
         end
      end
   end

   function NS2Gamerules:SpawnStartupBattery()
      if (self:GetGameStarted() == true and startingBatteryDropped == false) then
         startingBatteryDropped = true
         local i = 0
         while (i < kFadedBatterySpawnAtRoundStart) do
            SpawnSentryBatteryOnMap(self, false, true)
            i = i + 1
         end
      end
   end

   -- Hook into the update .function, so we can update our mod
   -- local babblerModelSizeUpTime = 0
   local onUpdate = NS2Gamerules.OnUpdate
   function NS2Gamerules:OnUpdate(timePassed)
      onUpdate(self, timePassed)
      self:SpawnStartupBattery()
      self:randomHintMessage(timePassed)
      -- self:fadeDisguiseMessageHelp()
      self:alertVeteranOfNearbyFakeMarine(timePassed)
      -- Only update Mental Health if the game has started
      if (GetGamerules():GetGameState() == kGameState.Started) then
         self:UpdateMarineMentalHealth(timePassed)
      end
      self:triggerLastMarineEvent(timePassed)
      if (fadedMapTime >= 0) then
         fadedMapTime = fadedMapTime + timePassed
      end

      self:BabblerAntiCamp(timePassed)
      -- for _, marine in pairs(self.team1:GetPlayers()) do
      --    marine:SetDetected(true)
      -- end
      for _, faded in ipairs(self.team2:GetPlayers()) do
         if (faded and faded:isa("Player") and faded:GetIsAlive()) then
            faded:CheckTransformation()
            -- if (faded:isa("Fade")) then
            --    faded:DanceOnUpdate(timePassed)
            -- end
         end
      end

      -- if (babblerModelSizeUpTime < Shared.GetTime()) then
      --    babblerModelSizeUpTime = Shared.GetTime() + 2

      --    local allPlayers = Shared.GetEntitiesWithClassname("Player")
      --    for _, babblerPlayer in ipairs(GetEntities("Skulk")) do
      --       if (babblerPlayer and babblerPlayer:GetIsAlive()
      --           and babblerPlayer.modelSize ~= 1) then
      --          for _, toPlayer in ientitylist(allPlayers) do
      --             Server.SendNetworkMessage(toPlayer, "ChangeModelSize",
      --                                       {modelSize = babblerPlayer.modelSize,
      --                                        id = babblerPlayer:GetId()},
      --                                       true)
      --          end
      --       end
      --    end
      -- end

      -- self:burnBodiesOrder(timePassed)
   end

   // get ALL the tech!
   function NS2Gamerules:GetAllTech()
      return true
   end

   -- Welcome message!
   function NS2Gamerules:OnClientConnect(client)
      Gamerules.OnClientConnect(self, client)

      local player = client:GetControllingPlayer()

      if (player and player.FadedMessage) then
         player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_1"))
         player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_2"))
         player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_3"))
         -- player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_4"))
         -- player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_5"))

         Server.SendNetworkMessage(player, "RoundTime", { time = kFadedModRoundTimerInSecs }, true)
         Server.SendNetworkMessage(player, "ConfigModification",
                                   {
                                      kFadedNoCustomFlashlight = kFadedNoCustomFlashlight,
                                      kFadedRespawnedEnable = kFadedRespawnedEnable,
                                      kFadedGLCheatEnable = kFadedGLCheatEnable,
                                      kFadedHSGCheatEnable = kFadedHSGCheatEnable,
                                   }, true)
         -- send log to each client, so they known what have been changed
         for _, str in ipairs(all_change_string_log) do
            Server.SendNetworkMessage(player, "ConfigModificationLog",
                                      {
                                         str = str,
                                      }, true)

         end
      end
   end

   -- Dont't let people spawn if they go to the ready room and back in game just after the game started
   function NS2Gamerules:GetCanSpawnImmediately()
      local game_state = self:GetGameState()
      if ((game_state ~= kGameState.Countdown and not self:GetGameStarted())
          or Shared.GetCheatsEnabled()) then
         return (true)
      else
         return (false)
      end
   end

   function NS2Gamerules:GetCanPlayerHearPlayer(listenerPlayer, speakerPlayer)
      if (listenerPlayer and speakerPlayer) then
         if (listenerPlayer:GetTeamNumber() == speakerPlayer:GetTeamNumber()) then
            return (true)
            -- If alien player want to use local, do not forward to marines
         elseif (speakerPlayer:GetTeamNumber() == 2
                    and listenerPlayer:GetTeamNumber() == 1
                    and speakerPlayer.IsUsingTeamVocalChat ~= nil
                    and speakerPlayer:IsUsingTeamVocalChat() == true)
         then
            return (false)
         end
      end
      -- Print("Alltalk mode")
      if (listenerPlayer:GetTeamNumber() == 2 and listenerPlayer:isa("Marine"))
      then
         -- Fake marines hear marine's communication
         return true
      end
      return kFadedModAllTalkEnabled
   end
end



