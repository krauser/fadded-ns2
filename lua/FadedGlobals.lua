// ===================== Faded Mod =====================
//
// lua\FadedGlobals.lua
//
//    Created by: Rio (rio@myrio.de)
//    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

-- kFadedModVersion = "0.17 Fear Factor"
-- kFadedModVersion = "0.18 MultiFade"
-- kFadedModVersion = "0.19.3 Veteran"
-- kFadedModVersion = "0.20 Body snatcher"
-- kFadedModVersion = "0.21.5 Research & Destroy"
kFadedModVersion = "0.22.11 Afterlife experiences"

-----------------------------------------------
------------------ Fade global ----------------
-----------------------------------------------

kFadedModPregameLength = 1
kFadedModTimeTillMapChange = 10

-- Time the server waits to start the first round (so people can join)
kFadedNextMapWaitingTime = 80
-- Time to refresh the "first game in" message
kFadedNextMapMessageRefresh = 10
-- Delay between two round
kFadedModTimeTillNewRound = 8

kFadedHintEnable = true

-------- Fade global for the cloak configuration

-- kFadedModMaxCloakedFraction = 0.98
--Values related to the faded Cloack ability
-- kFadedModBaseOpacity = -0.20
-- Opacity of the faded camo (skin hole in the camo)
-- 1 is the default, 2 means twice more hole in the skin, 0.5 is half
kFadedModOpacity = 2
kFadedMinSpeedForOpacity = 2.2
-- kFadedModSpeedOpacitydModificator = 0.05
--kFadedModAttackingOpacityModificator = 0.3
-- kFadedModLowestOpacity = 0.1

-- The detection distance of the marine light in meter
kFadedLightDetectDist = 30
-- Time in second the light can be used
kFadedLightTime = 50
kFadedLightRegePerSec = 0.6

kFadedLightNoDetectColor = Color(.1, .1, 1)
kFadedLightDetectColor = Color(1, 0, 0)

-- Amont of sanity (2 means all rege & damage are divided by 2)
kFadedSanityAmountScale = 1

-- Infinite parasite duration
kParasiteDuration = -1
-- Sanity for the cloak malus
-- Minimum percent of distance view when the Marine is low in sanity
-- Ex: for value 70
--     If the marine, full sanity see at 100 meters, at 0 if will see 70
-- kFadedMinViewQuality = 70
-- Minimum amount of sanity lost to start the progressive view distance
-- reduction.
-- Ex: If 40, the sanity will reduce from 60 to 0
kFadedMinMissingSanityForMalus = 40

-- kFadedBaseCloakFraction = 0
-- kFadedCloakFraction = 0
-- kFadedMinimalCloak = 0.04
-- kFadedMarineMaximalDistanceOfDetection = 23
-- kFadedMarineMaximalDetectionDistance = 2
kFadedClossestMarineFound = true


----------------
-- Hunt ability

-- Maximum points on the path
kFadedHuntMaxPointOnPath = 100
-- Max distance to show the "Use" bar
kFadedHuntingRayRange = 1.5
-- Time to reveal the path in second
kFadedHuntTime = 3
-- Time before we clear the path
kFadedHuntPathDuration = 8

------------------
-- Spectator babblers
kBiteDamage = 3
kFadedSpectatorBabblerHealth = 10
kFadedSpectatorBabblerArmor = 0
kBiteEnergyCost = 80
kFadedBabblerJumpHeigh = 2
kFadedBabblerPlayerJumpCost = 20
-- Time in second before the next babbler spawn wave
-- kFadedBabblerPlayerSpawnInterval = 40

-- Time added to the babbler spawn wave per babbler
-- so, with a few they will respawn a lot by themselves, and lesser
-- in endgame
kFadedInstantBabblerSpawnWithProto = true
kFadedDelayOnSpawnWavePerBabbler = 4
kFadedMaxBabblerPlayerSpawnDelay = 60
-- Minimum distance to have when respawning as a babbler
kFadedMinDistFromMarineToRespw = 20

-- Delay in sec between 2 chat msg "you will respawn in"
kFadedBabblerPlayerRespawnMsg = 15
kFadedBabblerPlayerEnergyRegen = 40

kFadedBabblerPlayerWalkSpeed = 4.9
kFadedBabblerPlayerJumpSpeed = 12

kFadedBabblerOnKillScoreReward = 2

-- Percent of chances to drop a ammo pack when killing a babbler
kFadedBabblerAmmoPackSpawnChances = 8.5

-----------------------------------------------
-----------------------------------------------
-- Objective mod

kFadedDisableObjectivMod = false

kFadedCanUsePrimaryWithBattery = false
-- Set to true if you want a battery to be dropped each time a building is powered up
kFadedSpawnBatteryOnBuildingPowerUp = false

-- Weight of the sentryBattery (for reference:
-- shotgun = 0.14, rifle = 0.13, pistol = 0.00
kFadedSentryBatteryWeight = 0.40
kSentryBatteryHealth = 1600 -- Default 600
kSentryBatteryArmor = 400 -- Default 200

kPhaseGateHealth = 1300 -- 2000
kPhaseGateArmor = 200 -- 550

kFadedObsEnablePassiveDetection = true

kPowerPointHealth = LiveMixin.kMaxHealth - 1
kPowerPointArmor = LiveMixin.kMaxArmor - 1

kObservatoryHealth = LiveMixin.kMaxHealth - 1
kObservatoryArmor = LiveMixin.kMaxArmor - 1
kArmoryHealth = LiveMixin.kMaxHealth - 1
kArmoryArmor = LiveMixin.kMaxArmor - 1
kAdvancedArmoryHealth = LiveMixin.kMaxHealth - 1
kAdvancedArmoryArmor = LiveMixin.kMaxArmor - 1
kRoboticsFactoryHealth = LiveMixin.kMaxHealth - 1
kRoboticsFactoryArmor = LiveMixin.kMaxArmor - 1
kArmsLabHealth = LiveMixin.kMaxHealth - 1
kArmsLabArmor = LiveMixin.kMaxArmor - 1

-- Max range to display the icon of the sentyr battery
kFadedSentryBatteryVisibleRange = 35

-- Spawn a battery on battery each N second
kFadedBatterySpawnDelay = 55
kFadedBatterySpawnAtRoundStart = 1

-- Give 2 item per marines (30% weap / 70% supply (ammo/medpack))
-- If 0, nothing is given
kFadedArmorySupplyAmount = 2
kFadedArmoryAllowWeapon = true  -- Allows the armory to drop weapon
kFadedArmoryAllowSupply = false -- Allows the armory to drop supply (medpack/ammo)
-- Number of time a marine can heal/get ammo on the armory automaticaly
kFadedArmoryHealthAmmoSupply = 2

kFadedCanMarinesBuyWeapons = true
kShotgunCost = 20
kClusterGrenadeCost = 10
kGasGrenadeCost = 10
kPulseGrenadeCost = 10
kMineCost = 20
kFlamethrowerCost = 20
kGrenadeLauncherCost = 20
kWelderCost = 10

kFadedGrenadeSecondary = true -- TODO: add to the server.json

kFadedObsGiveMinimap = true
kFadedObsGiveDamageDisplay = true

kFadedRoboticsFactoryGiveMac = true

kFadedArmslabGiveWeapons1 = true
kFadedArmslabGiveArmor1 = true

-----------------------------------------------
-----------------------------------------------

-- Faded capability
kFadedRespawnedEnable = true
kFadedRespawnedFadeHealth = 30
kFadedRespawnedFadeEnergy = 1
kFadedRespawnedFadeHealthCost = 50

kFadedReviveScore = 15

-----------------------------------------------


kFadedModTwoPlayerToStartMessage = 20


local fadedReductionFactor = 0.80
-- Base amount of healt/armor the fade has
kFadeBaseHealth = tonumber(string.format("%.2f", 55*fadedReductionFactor)) -- old 100
kFadeBaseArmor = tonumber(string.format("%.2f", 10*fadedReductionFactor))  -- old 50

-- Number of marine is the team,
-- this is updated at the start of the round (in FadedAlien.lua)
kMarineNb = 0
-- Number of fade
kFadeNb = 0
-- Number of marine per fade
-- Ex: If 10: the second fade will come with 11 marines
kFadedAddFadeMarineScale = 4
-- Extra division perform when we have multiple fade (to balance DPS)
-- Ex: if value is "1.2", fade hp/armor will be divided by 1.2 (20%)
-- If true, the maluce is enable
kMultiFadeMaluce = false
kFadedHPReductionPerFade = 1.2

kFadedAmbiantSound = false
kFadedSoundIfFadeNearby = false

-- Amount of healt/armor given to the fade for each marine
kFadeHealthPerMarine = tonumber(string.format("%.2f", 37*fadedReductionFactor)) -- 35
kFadeArmorPerMarine = tonumber(string.format("%.2f", 18*fadedReductionFactor)) -- 10

//kFadedModMaxMarinePlayersUntilFadeScale = 6

-- Total amount of Point to dispatch when a Faded die
-- If a marine deal 10% damage to the Faded, he will have 10 points
kFadedModScorePointsForFadeKill = 100
-- Point to give if a Faded kill the marine
kFadedModScorePointsForAllMarineKill = 100


kFadedModFadeCanBeSetOnFire = true

kEnergyUpdateRate = 0.7

-----------------------------------------------
------------------ Game Balance global --------
-----------------------------------------------

----------------------------

--Values related to the Devour Corps Ability

--kFadedEatingStopedTime = 3
kFadedEatingRegen = 35
kFadedEatingRegenArmor = 5
kFadedEatingTime = 4

-------------------------------

kJetpackUseFuelRate = 0.15  -- 0.21 -- ns2 default

kFadedExoSpeedBonusFactor = 1.1 -- add 10% speed

-------------------------------

-- Cost of the bile bomb
kBileBombEnergyCost = 20
-- Bile bomb fire rate in second
kBileBombFireRate = 3
-- Bile bomb speed
kFadedModBombVelocity = 23

-- Max distance in which the marine receive death notification
kFadedDeathMessageDistance = 25


--------- Mines
--
-- Restriction of the number of mine in an area
-- for ex:
-- * kFadedModMinesRestrictionCount = 1
-- * kFadedModMinesRestrictionRange = 20
-- Mean: Allow at max 1 mine each 20 meter
--

kFadedModMinesRestrictionCount = 1
kFadedModMinesRestrictionRange = 20
-- Number of mine per marine
kNumMines = 2
-- Trigger radius for the mine to explode
kMineTriggerRange = 3
-- Default: 125
kMineDamage = 94
kMineAlertTime = 4 -- Default 8

-- Flame

kFadedFlameMinesNum = 2
kFadedFlameMinesRadius = 8

-----------

kFadedCanDropWelder = false

kFadedBabblerAbilityEnable = true
kFadedBabblerAbilityHealthCost = 20
kFadedBabblerAbilityNumBabbler = 5
-- Maximum number of babbler alive on the map
kMaxBabblerOnMap = 100
-- Build time of babbler egs
kBabblerEggBuildTime = 5
-- Babblers lifetime
kBabblersLifetime = 40*1

kFadedAntiCampEnable = false
-- Radius de la zone pour le camp
kAntiCampRadius = 10
-- Number of babbler when the anti camp trigger
kFadedNumBabblersPerEgg = 6

-- Do not use this one, it work but there is an unknown exception
-- when using this NS2 global
-- kNumBabblersPerEgg = 15

-- Max camp time before babbler spawn
kCampMaxTime = 45

-- Dispatch marine around the map
-- Allow also the Faded to be spawn on Resource Point
KFadedRandomDispatchSpawn = true
-- Minimum distance between the Faded and marine when the round start
kFadedSafetySpawnRadius = 40
-- Minimum number of marine before dispatch
kFadedRandomDispatchMinPlayer = 6

---------------- Flame

-- Energy drain (default = 3)
kFlameThrowerEnergyDamage = 2
-- Damage of the fire surface on ground
Flame.kDamage = 2
Flame.kLifeTime = 10 -- Default is 5.6
-- Burn damage over time
kBurnDamagePerSecond = 5.5
-- Burn duration
kFlamethrowerBurnDuration = 7
-- Direct burn damage
kFlamethrowerDamage = 6
-- Reduction de la taille chargeur du lance flame (original = 50)
kFlamethrowerClipSize = 30
-- Reduce the damage of flame on marines
-- If its equal to "2", flame damage on ground are divided by 2
kFadedFlameFriendlyFireDamageReductionFactor = 2

---------------- All grenades

-- Maximum numbers of grenades
kMaxHandGrenades = 3

----------------- Cluster Grenade

kClusterGrenadeDamageRadius = 11 -- Default 10
kClusterGrenadeDamage = 90 --55
kClusterFragmentDamageRadius = 7 -- 6
kClusterFragmentDamage = 40 -- 20

----------------- Flare Grenade and light

kFadedFlareEnable = true
kFlareLifeTime = 20
kFadedFlareAmmoCost = 2
kFadedFlareIntensity = 6
kFadedFlareRadius = 12

kFadedBatteryLightColor = Color(1, 1, 1)
kFadedBatteryLightIntensity = 7
kFadedBatteryLightRadius = 14

----------------- Heal Grenade
kFadedMaxHealGrenades = 3
kFadedHealCloudRadius = 7


-- Max HP per grenade
-- Heal once on detonation, and once each Xs
kFadedHealCloudLifetime = 2
kFadedHealCloudRefreshRate = 0.80
kFadedHealCloudHpPerRefresh = 15
-- Reduction for the GL of the medic
kFadedGLHealCloudHpPerRefresh = 6

----------------- Napalm Grenade

kFadedMaxNapalmGrenades = 2
kFadedNapalmDamagePerSecond = 10

kFadedNapalmCloudLifetime = 6 -- (default nerve gaz => 6s)
kFadedNapalmCloudRadius = 7 -- default (same as nerve gaz cloud)
-- Radius within the Faded goes auto in flame
-- kFadedBurnRadius = 2
kFadedNapalmPutInFireDelay = 0.85

-- Marine do not burn, to compensate and make the cloud dangerous
-- (so you can't just walk in and out easy), make more damages
kFadedOnMarineDamageMultiplyer = 1

--- Babbler anticamp only attack the camper
--- heavyshptgun double shot, check if deployed
--- cheatcode for the mega babler for pheonix (scaled)

-------------------- Medic Pistol

kFadedMedicPistolDmg = 20
kFadedMedicPistolBlastRadius = 4
kFadedMedicPistolDetonationDelay = 1.7

kXenocideRange = 7
kXenocideDamage = 11

-------------------- Rifle
-- Increase damage to be more usefull in combat
-- SG can deal    170dmg/0.88s ==>        193dps
--> Usefull in close and fast engagement, can deal 170dmg instant
--> Can reload in combat and shoot if necessary
-- Rifle can deal 18bullets/1s ==> 18*13.5 =    243dps
--> Long distance, but hard to deal all the damage in close combat + long reload
--> Non stopable reload
-- Shotgun time to empty a full clip: 6*0.88 = 5.28 (0.88 == rate of fire)
-- Rifle   time to empty a full clip: 75/18  = 3.60 (18 == Bullets per seconde)
kRifleCost = 20
kRifleDamage = 13.5
kRifleClipSize = 65
-- kRifleDamageType = kDamageType.Normal
-- kRifleClipSize = 50

-- PulseGrenade damage (base = 110)
kPulseGrenadeDamage = 40
kPulseGrenadeDamageRadius = 10 -- default 7
kPulseGrenadeEnergyDamage = 10

kGrenadeLauncherGrenadeDamage = 165

------------------------------
-- Warning: Remember to update en gamestring/enUS.txt for weapon description
-- --> WEAPON_DESC_HEAVYSHOTGUN <--
kFadedHSGCheatEnable = false
kFadedHeavyShotgunDamage = 3
kFadedHeavyShotgunKnockBack = 19
kFadedHeavyShotgunPelletPerShot = 68
-- Fire rate compared to the regular shotgun (2 means 2 type slower)
kFadedHeavyShotgunFireRate = 1.4
-- Number of pellet compared to the single shot
-- 2 means twice more pellets
kFadedHeavyShotgunDoubleShotPelletRatio = 1.6
------------------------------

kBlinkEnergyCost = 12
kFadedBlinkHpCost = 4
kStartBlinkEnergyCost = 3
kFadedEnzimeAfterBlink = true

kWallGripEnergyCost = 5
kLeapEnergyCost = 14

-- Cost in energy of the Growl
kFadedGrowlEnergyCost = 24
-- Time between Growl in second
kFadedGrowlInterval = 9
-- The growl damage is reduce by the number of Faded in the same are
-- Ex for 40: If there are two fade close from each others of 40m, then
--            the growl is reduced in damage by 2
kMultiFadedGrowlMalusDistance = 15

-- Correspond the the damage max of the growl and the radius
-- if he "Growl", the second global indicate the power diminution
-- over distance. When Damage is "0" the propagation stops
kFadedGrowlMentalHealthDamage = 18
kFadedGrowlDiminutionByMeter = 0.33

-- Bonus of presence to rege mental health
-- Ex: 1 mean you count as 1 marine (for group)
-- The higher it is, the faster you get sanity back
kFadedMarineMentalStrenght = 1
-- Direct Mental health bonus for flashlight around
-- Ex: 0.2 mean: rege 0.2 per second for each flashlight on
kFadedFriendFlashLight = 0.4
-- Number of marine per veteran allowed
-- (5 means: 1 veteran every 5 marines)
kFadedVeteranScale = 5
-- Mental strenght aura of veteran
-- Ex: If 3, they count for 3 marines in groupe sum
kFadedVeteranMarineMentalStrenght = 3
-- If 1.1, increase damage by 10%
-- If 1.6, increase damage by 60%
-- (damage = damage * kFadedVeteranDamageBonusFactor)
kFadedVeteranDamageBonusFactor = 1.1
-- Armor amount/health of Veteran
kFadedVeteranArmor = 50
kFadedVeteranHealth = 110
-- Speed bonus of a veteran (Ex: if 0.1, give 10% speed bonus)
kFadedVeteranSpeedBonus = 0.09

-- Damage dealed by each swip of the fade on a Marine
kFadedSwipMentalHealthDamage = 15
-- Default of 37.5, does 75 damages on a good hit
kSwipeDamage = 37.5
kSwipeDamageType = kDamageType.Normal
kSwipeEnergyCost = 12

-- 1 ==> Fake marine can takes PG, anything else it can't
kFadedAllowFakeMarineToTakePG = 1

-- Instant stab only if a marine is not visible or away from at least this range
kFadedInstantStabMinUnseenDist = 30

-- If the a Marine view a Fake marine less than N meters, he can't stab
-- Ex: 2 ==> The fade can stab if no one see him below 2 meters
kFadedStabViewDistance = 2
-- Direct damage from the stab
kFadedStabDamage = 10
-- Poison damage to the marine (3.4*25 == 85.0 health damages)
kFadedStabPoisonDamage = 3.4
kBitePoisonDamage = kFadedStabPoisonDamage -- per second
kPoisonBiteDuration = 25
-- Min amount of life of the marine with the poison
kPoisonDamageThreshhold = 0
-- Damage per second on the sanity of the marine
kFadedPoisonSanityDamagePerSec = 5 -- Per second


kFadedStabRange = 2
kFadedSwipeRange = 1.6
-- Time to denied swipe after stab to force the fade to flie away
kFadedDeniedSwipeAfterStab = 0
-- Do not edit this global
SwipeBlink.kRange = kFadedSwipeRange
SwipeLeap.kRange = kFadedSwipeRange * 1.10

-- Damage amount on nearby marine when a marine die
kFadedMarineMentalHealthDeathDamage = 20
-- Radius in which marine are affected by the death of a friend
kFadedMarineDeathRadius = 20

-- Radius of an explosive grenade (original: 4.8)
-- I increase this one to balance the fact that the fade is fast
-- and to help for cover (otherwise, except for direct hit its not
-- very usefull)
kGrenadeLauncherGrenadeDamageRadius = 7.5

kFadedGLCheatEnable = false
-- Damage reduction on Friendly fire of the GL
-- but not on the owner of the GL
kFadedGLFriendlyFireDamageReductionFactor = 2
kFadedGLSecondary = true

-- Radius of a marine's group (to calculate Mentalhealth factor)
kFadedMarineGroupRadius = 15

-- Bonus on hit by marine weapon
kFadedShotgunOnHitMentalHealthBonus = math.floor(((20 / kShotgunBulletsPerShot)))
kFadedPistolAltCheatEnable = false
kFadedPistolOnHitMentalHealthBonus = 4
kFadedRifleOnHitMentalHealthBonus = 1
kFadedFlameThrowerOnHitMentalHealthBonus = 0
kFadedFlameOnHitMentalHealthBonus = 3.6
kFadedGLOnHitMentalHealthBonus = 25
kFadedClusterOnHitMentalHealthBonus = 4
kFadedGazOnHitMentalHealthBonus = 4
kFadedNapalmOnHitMentalHealthBonus = 4

kFadedModGUIHelpLimit = 3

kFadedModDistortionRadius = 16
kFadedModDistortionIntensity = 16
kFadedModDistortionAcceleration = 1.6

kFadedModLightColorScale = 0.10
kFadedModLightIntensity = 0.5

kFadedModAllTalkEnabled = false

-- Unused
-- kFadedModFairFadedRotation = true

kTimeStartFade = 10
kTimeEndFade = 11

----------------------------------------------
----------------- Ragdol gobal
----------------------------------------------

-- Min distance to take/eat away from view of a marine
kFadedTakeBodyDistance = 25
-- Max distance for the fade to eat/use a body
-- or to burn a body
kFadedEatUseDistance = 2.5
-- Range of detection for veteran of any Fake Marine
kVeteranFakeMarineRangeDetection = 8
-- Every N second, the Veteran is alerted if a Fake Marine
-- is nearby of M meter.
kVeteranAlertRefreshTime = 5
-- % of Energy left when a Fake marine get Fade
-- Ex: 0.3 mean 30% of his max energy
kFadedTransformationEnergyPenality = 0.35
-- % of Energy left when the Fake marine stab
kFadedTransformationOnStabEnergyPenality = 0.20
-- Infestation radius generated by dead body
-- For ex: Hive infestation is 20
kFadedDeadBodyInfestationRadius = 4
-- Default of NS2: 0.25
kFadedDeadBodyInfestationRate = 0.1
kFadedDeadBodyLifeTime = 60*10
-- Radius to give order if the player has a flame weapon
kFadedBurnOrderRadius = 10

-- Gaz above dead marine
-- Pourcent of chance of a gaz spawn
-- A value of '1' mean 1/1000
kFadedDeadBodyToxicGazChance = 1
kSporesDustDamagePerSecond = 4
kSporesDustCloudRadius = 2.5
kSporesDustCloudLifetime = 8

---------------------------

-----------------------------------------------
------------------ MentalHealth system global -
-----------------------------------------------

kFadedMarineMentalHealth = 100

----
kFadedNoCustomFlashlight = false
-- Regeneration when FLashLight is On
-- Note: Do not chance until you know what you are doing,
-- this value work with the formula in FadedMentalHealthMecanic.lua
-- Changing this value could break the game balance
kFadedMarineMentalRege = 3.5
-- Dynamicaly updated with a group coef
kFadedMarineMentalUse = kFadedMarineMentalRege
----

-- Defined in the file Balance.lua
-- kPlayerHallucinationNumFraction is dynamicaly updated in FadedAlien.lua
-- in the function FadedMod:SpawnAsFade()

-- Maximum number of hallucination on map
kPlayerHallucinationNumFraction = nil
-- Life time in second of an hallucination
kHallucinationLifeTime = 10
-- HP of hallucination
kHallucinationMaxHealth = 50

-- kHallucinationHealthFraction = 0.01
-- -- No armor for hallucination
-- kHallucinationArmorFraction = 0.01
-- No longueur used (replace with clean hallucination spawn)
-- Force the hallucination to duplicated faded copie on all the map
-- HallucinationCloud.kRadius = 600

kFadedAllowFadedToSeeHallucination = true

-----------------------------------------------
------- Mental Health global ------------------
-----------------------------------------------

kFadedAdreBonusOnKill = 20

-- RATE
-- Number of second between tried (depending then on Chance)
-- Ex: Value 3 mean "try to spawn an hallucination every 3 sec"
-- CHANCE
-- Chance every 3 second to spawn an hallucination
-- 0    => No hallucination
-- 0.5  => 50% Hallucination chance
-- 1    => Full chance

-- Stress level
kFadedStressLvlApproachSoundRate = 3
kFadedStressLvlApproachSoundChance = 0.3
kFadedStressLvlHallucinationChance = 0.03

-- Panic level
kFadedPanicLvlHallucinationRate = 3
kFadedPanicLvlHallucinationChance = 0.35

-- Heavy Panic level
-- Note: This chance are added to the panic level
-- because panic level effects are also played in heavy Panic
kFadedHeavyPanicLvlHallucinationRate = 2.6
kFadedHeavyPanicLvlHallucinationChance = 0.35

-- kFadedHeavyPanicLvlGrowlRate = 3
kFadedHeavyPanicLvlGrowlChance = 0.1


-- See FadedMentalHealthMecanic.lua for the Effect table and function

-----------------------------------------------
------- Hallucination global (do not touch) ---
-----------------------------------------------

kFadedSpawnFadeHallucination = nil
-- Remove hallucination cloud effect
HallucinationCloud.kSplashEffect = nil
EnzymeCloud.kSplashEffect = nil
EnzymeCloud.kRepeatEffect = nil

-- Remove spawn sound of hallucination
-- Return the place in the table of each element
-- (See  GeneralEffects.lua)
local function removeSpawnSoundFor(entityName)
   local soundIt = 1
   local soundItMax = #kGeneralEffectData.spawn.spawnSoundEffects
   local cineIt = 1
   local cineItMax = #kGeneralEffectData.spawn.spawnEffects
   -- Disable sound
   while (soundIt <= soundItMax)
   do
      local tmp = kGeneralEffectData.spawn.spawnSoundEffects[soundIt]
      if (tmp and tmp.classname == entityName) then
         table.remove(kGeneralEffectData.spawn.spawnSoundEffects, soundIt)
         -- kGeneralEffectData.spawn.spawnSoundEffects[soundIt].sound = nil
         break
      end
      soundIt = soundIt + 1
   end
   -- Disable Effect
   while (cineIt <= cineItMax)
   do
      local tmp = kGeneralEffectData.spawn.spawnEffects[cineIt]
      if (tmp and tmp.classname == entityName) then
         -- kGeneralEffectData.spawn.spawnEffects[cineIt].cinematic = nil
         table.remove(kGeneralEffectData.spawn.spawnEffects, cineIt)
         break
      end
      cineIt = cineIt + 1
   end
   return soundIt, cineIt
end

local function removeSoundForSkulk()
   local t = kPlayerEffectData.land.landSoundEffects
   for _, sub_table in ipairs(t) do
      if (sub_table.player_sound == "sound/NS2.fev/alien/skulk/land")
      then
         sub_table.player_sound = ""
      end
   end

   t = kGeneralEffectData.death.deathSoundEffects
   for _, sub_table in ipairs(t) do
      if (sub_table.sound == "sound/NS2.fev/alien/skulk/death")
      then
         sub_table.sound = ""
      end
   end

   t = kDamageEffects.flinch.flinchEffects
   for _, sub_table in ipairs(t) do
      if (sub_table.sound == "sound/NS2.fev/alien/skulk/wound"
             or sub_table.sound == "sound/NS2.fev/alien/skulk/wound_serious")
      then
         sub_table.sound = ""
      end
   end

   t = kDamageEffects.damage_sound.damageSounds
   for _, sub_table in ipairs(t) do
      if (sub_table.sound and sub_table.doer == "BiteLeap") then
         sub_table.sound = ""
      end
   end
end

-- -- Hallucination cloud sound/cloud effect disable
-- -- note: The letter case are correct
removeSpawnSoundFor("Hallucination")
removeSpawnSoundFor("Fade")
removeSpawnSoundFor("Skulk")
removeSpawnSoundFor("SentryBattery")
removeSoundForSkulk()

----------------------------- Remove Fake marine footstep specific sound
local function removeFootStep()
   local it = 1
   local max = #kPlayerEffectData.footstep.footstepSoundEffects
   while (it <= max) do
      local data = kPlayerEffectData.footstep.footstepSoundEffects[it]
      if (data and string.find(data.sound, "sprint_")or string.find(data.sound, "footstep_")) then
         data.sound = string.gsub(data.sound, "_for_enemy", "")
      end
      it = it + 1
   end
end
removeFootStep()
-- sprint_
-- footstep_
-- kPlayerEffectData.footstep.footstepSoundEffects

-----------------------------------------------
------------------ Server config global -------
-----------------------------------------------
-- Server global
kItemStayTime = 30

-- Base timer for the mod
kFadedModBaseRoundTimerInSecs = (60*3 + 45)
-- Max amount of time for a round
kFadedMaxRoundTimer = (60*12)
kFadedModBonusTimePerMarine = 25
kFadedModRoundTimerInSecs = 0

kFadedModSpawnProtectionEnabled = true
kFadedModSpawnProtectionTime = 8

kFadedModFriendlyFireEnabled = true
kFriendlyFireScalar = 0.5

kFadedModFadedSelectionChance = 80
kFadedModFadedNextSelectionChance = 10

-- Time during which the marine is allowed to purchase weapon
-- when the round begin
kFadedModTimeInSecondsSelectingEquipmentIsAllowed = 16

-- See FadedServerConfig.lua for round global
-- Script.Load("lua/FadedCustomGlobals.lua")
