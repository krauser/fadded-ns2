// ===================== Faded Mod =====================
//
// lua\FadedGrenadeLauncher.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

local kFadedGrenadeType = ""
local kGrenadeSpeed = 25

-- Allow alternative fire
function GrenadeLauncher:GetHasSecondary(player)
   return kFadedGLSecondary
end


-- This function is the same as ShootGrenade of the grenade launcher
-- I change "grenade" to 'Pulsegrenade'
local function ShootGrenade(self, player, babbler_mod)

   PROFILE("ShootGrenade")

   self:TriggerEffects("grenadelauncher_attack")

   if Server or (Client and Client.GetIsControllingPlayer()) then

      local viewCoords = player:GetViewCoords()
      local eyePos = player:GetEyePos()

      local startPointTrace = Shared.TraceCapsule(eyePos, eyePos + viewCoords.zAxis, 0.2, 0, CollisionRep.Move, PhysicsMask.PredictedProjectileGroup, EntityFilterTwo(self, player))
      local startPoint = startPointTrace.endPoint

      local direction = viewCoords.zAxis

      if startPointTrace.fraction ~= 1 then
         direction = GetNormalizedVector(direction:GetProjection(startPointTrace.normal))
      end

      -- Those lines has been added
      kFadedGrenadeType = "Grenade"
      if (player.GrenadeLauncherRegular == false) then
         kFadedGrenadeType = "PulseGrenade"
         -- kFadedGrenadeType = "Flare"
         -- fire_flare_grenade(player)
         -- return
      end

      if (player:IsMedic() == true) then
         kFadedGrenadeType = "HealGrenade"
      end

      if (player.GrenadeLauncherRegular == true or babbler_mod ~= true) then
         local grenade = player:CreatePredictedProjectile(kFadedGrenadeType, startPoint, direction * kGrenadeSpeed, 0.7, 0.45)
      else
         if (Server and babbler_mod == true) then
            local viewCoords = player:GetViewCoords()
            local impulse = 0.1
            local base_vect = direction * kGrenadeSpeed * 1.2
            local bab1 = CreateEntity(Babbler.kMapName, startPoint + Vector(0, -0.5, 0), 2)
            if (bab1 ~= nil) then bab1:Jump(base_vect) end
            local bab2 = CreateEntity(Babbler.kMapName, startPoint + Vector(0, 0.5, 0), 2)
            if (bab2 ~= nil) then bab2:Jump(base_vect) end
         end
      end
      -- We shoot Pulse, reset normal grenade
      player.GrenadeLauncherRegular = true

   end

   -- TEST_EVENT("Grenade Launcher primary attack")
   player.GrenadeLauncherRegular = true
end

-- Hook to use our own "ShootGrenade" function with the hook
function GrenadeLauncher:FirePrimary(player)
   ShootGrenade(self, player, false)
end

local onPrimaryAttack = GrenadeLauncher.OnPrimaryAttack
function GrenadeLauncher:OnPrimaryAttack(player)
   player.GrenadeLauncherRegular = true
   if (player.kFadedReserved or kFadedGLCheatEnable) then
      ShootGrenade(self, player)
   else
      onPrimaryAttack(self, player)
   end
end

-- Replace temporaly the Grenade type for the alternative fire
-- and restore it after
function GrenadeLauncher:OnSecondaryAttack(player)
   player.GrenadeLauncherRegular = false
   if (self:GetHasSecondary(player)) then
      if (player.kFadedReserved or kFadedGLCheatEnable) then
         ShootGrenade(self, player, true)
      else
         onPrimaryAttack(self, player)
      end
   end
end
