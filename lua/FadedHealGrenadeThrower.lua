// ======= Copyright (c) 2003-2011, Unknown Worlds Entertainment, Inc. All rights reserved. =======
//
// lua\Weapons\Marine\GasGrenadeThrower.lua
//
//    Created by:   Andreas Urwalek (andi@unknownworlds.com)
//
//    Throws gas grenades.
//
// ========= For more information, visit us at http://www.unknownworlds.com =====================

Script.Load("lua/Weapons/Marine/GrenadeThrower.lua")
Script.Load("lua/FadedHealGrenade.lua")

local networkVars =
   {
   }

class 'HealGrenadeThrower' (GrenadeThrower)

HealGrenadeThrower.kMapName = "healgrenade"

local kModelName = PrecacheAsset("models/marine/grenades/gr_nerve.model")
local kViewModels = GenerateMarineGrenadeViewModelPaths("gr_nerve")
local kAnimationGraph = PrecacheAsset("models/marine/grenades/grenade_view.animation_graph")

function HealGrenadeThrower:OnCreate()
   GrenadeThrower.OnCreate(self)
   self.grenadesLeft = kFadedMaxHealGrenades
end

function HealGrenadeThrower:GetThirdPersonModelName()
   return kModelName
end

function HealGrenadeThrower:GetViewModelName(sex, variant)
   return kViewModels[sex][variant]
end

function HealGrenadeThrower:GetAnimationGraphName()
   return kAnimationGraph
end

function HealGrenadeThrower:GetGrenadeClassName()
   return "HealGrenade"
end

Shared.LinkClassToMap("HealGrenadeThrower", HealGrenadeThrower.kMapName, networkVars)
