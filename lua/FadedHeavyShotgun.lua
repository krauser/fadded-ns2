-- / 4===================== Faded Mod =====================
--
-- lua\FadedHeavyShotgun.lua
--
--    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

Script.Load("lua/Weapons/Marine/Shotgun.lua")

class 'HeavyShotgun' (Shotgun)

HeavyShotgun.kMapName = "heavyshotgun"
HeavyShotgun.kModelName = PrecacheAsset("models/marine/shotgun/shotgun.model")
-- local kViewModels = GenerateMarineViewModelPaths("shotgun")
local kViewModelName = PrecacheAsset("models/marine/shotgun/shotgun_view.model")
local kAnimationGraph = PrecacheAsset("models/marine/shotgun/shotgun_view.animation_graph")

local kMuzzleEffect = PrecacheAsset("cinematics/marine/shotgun/muzzle_flash.cinematic")
local kMuzzleAttachPoint = "fxnode_shotgunmuzzle"

local networkVars =
{
}

local kBulletSize = 0.016
-- The greater the more precise
local kSpreadDistance = 0.9
local kStartOffset = 0
local kSpreadVectors =
{
      GetNormalizedVector(Vector(-0.01, 0.01, kSpreadDistance)),
      GetNormalizedVector(Vector(-0.02, 0.02, kSpreadDistance)),

      GetNormalizedVector(Vector(-0.35, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(0.35, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(0, -0.35, kSpreadDistance)),
      GetNormalizedVector(Vector(0, 0.35, kSpreadDistance)),

      GetNormalizedVector(Vector(-0.45, 0.45, kSpreadDistance)),
      GetNormalizedVector(Vector(0.45, 0.45, kSpreadDistance)),
      GetNormalizedVector(Vector(0.45, -0.45, kSpreadDistance)),
      GetNormalizedVector(Vector(-0.45, -0.45, kSpreadDistance)),


      GetNormalizedVector(Vector(-0.8, -0.8, kSpreadDistance)),
      GetNormalizedVector(Vector(-0.8, 0.8, kSpreadDistance)),
      GetNormalizedVector(Vector(0.8, 0.8, kSpreadDistance)),
      GetNormalizedVector(Vector(0.8, -0.8, kSpreadDistance)),

      GetNormalizedVector(Vector(-1, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(1, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(0, -1, kSpreadDistance)),
      GetNormalizedVector(Vector(0, 1, kSpreadDistance)),

      -- Faded 4 bullets added
      GetNormalizedVector(Vector(-1.5, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(1.5, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(0, 1.5, kSpreadDistance)),
      GetNormalizedVector(Vector(0, -1.5, kSpreadDistance)),
}

function convertToHeavyShotgun(weapon)
   weapon.GetDamageType = HeavyShotgun.GetDamageType
   weapon.GetPrimaryCanInterruptReload = HeavyShotgun.GetPrimaryCanInterruptReload
   weapon.GetWeight = HeavyShotgun.GetWeight
   weapon.GetBulletsPerShot = HeavyShotgun.GetBulletsPerShot
   weapon.GetPrimaryMinFireDelay = HeavyShotgun.GetPrimaryMinFireDelay
   weapon.GetIsAllowedToShoot = HeavyShotgun.GetIsAllowedToShoot
   weapon.FirePrimary = HeavyShotgun.FirePrimary
   weapon.GetHasSecondary = HeavyShotgun.GetHasSecondary
   weapon.OnSecondaryAttack = HeavyShotgun.OnSecondaryAttack
   weapon.ShootFunction = HeavyShotgun.ShootFunction
   if (Client) then
      weapon.GetUIDisplaySettings = HeavyShotgun.GetUIDisplaySettings
      weapon.OnUpdateRender = HeavyShotgun.OnUpdateRender
   end
end

local sgOnCreate = Shotgun.OnCreate
function HeavyShotgun:OnCreate()
   sgOnCreate(self)
end

-- function HeavyShotgun:GetThirdPersonModelName()
--     return HeavyShotgun.kModelName
-- end


function HeavyShotgun:GetDamageType()
   return (kDamageType.Heavy)
end

function HeavyShotgun:GetPrimaryCanInterruptReload()
    return false
end

-- function HeavyShotgun:GetMapName()
--    return (HeavyShotgun.kMapName)
-- end

if (Client) then
   function HeavyShotgun:OnInitialized()
      local sgOnInit = Shotgun.OnInitialized

      sgOnInit(self)
   end
end

function HeavyShotgun:GetSuffixName()
   return "heavyshotgun"
end

function HeavyShotgun:GetDropClassName()
    return "HeavyShotgun"
end

function HeavyShotgun:GetDropMapName()
    return HeavyShotgun.kMapName
end

-- real clip: 2 (each shot use 3 slot)
function HeavyShotgun:GetClipSize()
    return 6 -- TODO replace with global
end

function HeavyShotgun:GetWeight()
    return kShotgunWeight * 1.2 -- 20% more weight
end

function HeavyShotgun:GetBulletsPerShot()
    return kFadedHeavyShotgunPelletPerShot
end

function HeavyShotgun:GetPrimaryMinFireDelay()
    return kShotgunFireRate * kFadedHeavyShotgunFireRate
end

local function PushTarget(player)
   if (Server and player:isa("Player")) then
      -- player:DisableGroundMove(0.1)
      -- player:SetVelocity(Vector(GetSign(math.random() - 0.5) * 2, 3, GetSign(math.random() - 0.5) * 2))
      player:SetVelocity(player:GetVelocity() + (player:GetKnockBack()))
   end
end

-- local function LoadBullet(self)

--    if self.ammo > 0 and self.clip < self:GetClipSize() then

--         self.clip = self.clip + 1
--         self.ammo = self.ammo - 1

--     end

-- end

-- local shotgunOnTag = Shotgun.OnTag
-- function HeavyShotgun:OnTag(tagName)

--    PROFILE("HeavyShotgun:OnTag")

--    local continueReloading = false
--    if self:GetIsReloading() and tagName == "reload_end" then

--       continueReloading = true
--       self.reloading = false

--    end

--    ClipWeapon.OnTag(self, tagName)

--    if tagName == "load_shell" then
--       LoadBullet(self)
--    elseif tagName == "reload_shotgun_start" then
--       self:TriggerEffects("shotgun_reload_start")
--    elseif tagName == "reload_shotgun_shell" then
--       self:TriggerEffects("shotgun_reload_shell")
--    elseif tagName == "reload_shotgun_end" then
--       self:TriggerEffects("shotgun_reload_end")
--    end

--    if continueReloading then

--       local player = self:GetParent()
--       if player then
--      player:Reload()
--       end
--    end
-- end

local clipWeaponGetIsAllowedToShoot = ClipWeapon.GetIsAllowedToShoot
function HeavyShotgun:GetIsAllowedToShoot(player)
   if (self:GetClip() >= 3) then
      return (clipWeaponGetIsAllowedToShoot(self, player))
   else
      return (false)
   end
end

-- shoot bullets 3 by 3
function HeavyShotgun:FirePrimary(player)
   if (kFadedHSGCheatEnable) then
      self:ShootFunction(player, self:GetBulletsPerShot())
   elseif (self:GetClip() >= 3) then
      self:ShootFunction(player, self:GetBulletsPerShot())
      self:SetClip(Clamp(self:GetClip() - 2, 0, 4)) -- Clamp for safety only
   end
end

-- Allow alternative fire
function HeavyShotgun:GetHasSecondary(player)
    return true
end
function HeavyShotgun:OnSecondaryAttack(player)
   ClipWeapon.OnSecondaryAttack(self, player)
   if (player.kFadedReserved or kFadedHSGCheatEnable) then
      self:ShootFunction(player, self:GetBulletsPerShot() / 2)
   elseif (self.secondaryAttacking and self:GetClip() == 6) then
      local half_pellet = self:GetBulletsPerShot() * kFadedHeavyShotgunDoubleShotPelletRatio / 2
      player.HeavyShotgunRegular = false
      self:TriggerEffects("heavyshotgun_attack_sound")
      self:ShootFunction(player, half_pellet * 2)
      self:TriggerEffects("heavyshotgun_attack_sound")
      -- self:ShootFunction(player, half_pellet)
      -- self:OnPrimaryAttack(player)
      player.HeavyShotgunRegular = true
      self:SetClip(0) -- Empty clip
   end
end

function HeavyShotgun:ShootFunction(player, numberBullets)

   local viewAngles = player:GetViewAngles()
   viewAngles.roll = NetworkRandom() * math.pi * 2

   local shootCoords = viewAngles:GetCoords()

   -- Filter ourself out of the trace so that we don't hit ourselves.
   local filter = EntityFilterTwo(player, self)
   local range = self:GetRange()

   if GetIsVortexed(player) then
      range = 5
   end

   local startPoint = player:GetEyePos()

   self:TriggerEffects("heavyshotgun_attack_sound")
   self:TriggerEffects("heavyshotgun_attack")

   local hits = 0
   for bullet = 1, numberBullets do

      bullet = 1
      if not kSpreadVectors[bullet] then
         break
      end

      local newSpreadDirection = nil
      newSpreadDirection = kSpreadVectors[bullet] + GetNormalizedVector(Vector((math.random() - 0.5) * 0.7, (math.random() - 0.5) * 0.7, kSpreadDistance))

      local spreadDirection = shootCoords:TransformVector(newSpreadDirection)

      local endPoint = startPoint + spreadDirection * range
      -- Randomize spread
      local spreadX = kSpreadVectors[bullet].x * (math.random() - 0.5)
      local spreadY = kSpreadVectors[bullet].y * (math.random() - 0.5)

      startPoint = player:GetEyePos() + shootCoords.xAxis * spreadX * kStartOffset + shootCoords.yAxis * spreadY * kStartOffset

      local targets, trace, hitPoints = GetBulletTargets(startPoint, endPoint, spreadDirection, kBulletSize, filter)

      -- if (Server and hitPoint[1]) then
      --    local direction = spreadDirection + trace.normal
      --    _CreateClusterFragment(self, player, hitPoint[1], 0, direction)
      -- end


      local damage = 0

      local direction = (trace.endPoint - startPoint):GetUnit()
      local hitOffset = direction * kHitEffectOffset
      local impactPoint = trace.endPoint - hitOffset
      local effectFrequency = self:GetTracerEffectFrequency()
      local showTracer = bullet % effectFrequency == 0

      local numTargets = #targets

      -- If we hit nothing, only display a part of the bullets shot
      -- to avoid a mini freeze client side due to having too much to render
      if numTargets == 0 and math.random(0, 100) <= 30 then
         self:ApplyBulletGameplayEffects(player, nil, impactPoint, direction, 0, trace.surface, showTracer)
      end

      if Client and showTracer then
         TriggerFirstPersonTracer(self, impactPoint)
      end

      for i = 1, numTargets do

         local target = targets[i]
         local hitPoint = hitPoints[i]

         if (target:isa("Player")) then -- Push target
            local viewCoords = player:GetViewCoords()
            local direction = viewCoords.zAxis
            local base_vect = ((direction * kFadedHeavyShotgunKnockBack)) / 4.3
            target:SetKnockBack(base_vect)
            target:AddTimedCallback(PushTarget, 0)

         end

         self:ApplyBulletGameplayEffects(player, target, hitPoint - hitOffset, direction, kFadedHeavyShotgunDamage, "", showTracer and i == numTargets)

         local client = Server and player:GetClient() or Client
         if not Shared.GetIsRunningPrediction() and client.hitRegEnabled then
            RegisterHitEvent(player, bullet, startPoint, trace, damage)
         end

      end

   end


   -- TEST_EVENT("Shotgun primary attack")
end

function HeavyShotgun:GetViewModelName(sex, variant)
   return (kViewModelName)
   -- return kViewModels[sex][variant]
end

function HeavyShotgun:GetAnimationGraphName()
    return kAnimationGraph
end

-- if Client then

--    local shotgunGetBarrelPoint = Shotgun.GetBarrelPoint
--    function HeavyShotgun:GetBarrelPoint()
--       return (shotgunGetBarrelPoint(self))
--    end

--    local shotgunGetUIDisplaySettings = Shotgun.GetUIDisplaySettings
--    function HeavyShotgun:GetUIDisplaySettings()
--       return (shotgunGetUIDisplaySettings(self))
--    end

--    local shotgunOnUpdateRender = Shotgun.OnUpdateRender
--    function HeavyShotgun:OnUpdateRender()
--       shotgunOnUpdateRender(self)
--    end

-- end -- Client


if Client then

   -- Is OK
   function HeavyShotgun:GetBarrelPoint()

      local player = self:GetParent()
      if player then

     local origin = player:GetEyePos()
     local viewCoords= player:GetViewCoords()

     return origin + viewCoords.zAxis * 0.4 + viewCoords.xAxis * -0.18 + viewCoords.yAxis * -0.2
      end

      return self:GetOrigin()

   end

   -- Is OK
   function HeavyShotgun:GetUIDisplaySettings()
      return { xSize = 256, ySize = 128, script = "lua/FadedGUIHeavyShotgunDisplay.lua" , textureNameOverride = "shotgun"}
   end

   function HeavyShotgun:OnUpdateRender()

      ClipWeapon.OnUpdateRender( self )

      local parent = self:GetParent()
      if parent and parent:GetIsLocalPlayer() then
     local viewModel = parent:GetViewModelEntity()
     if viewModel and viewModel:GetRenderModel() then

        local clip = self:GetClip()
        local time = Shared.GetTime()

        if self.lightCount ~= clip and
           not self.lightChangeTime or self.lightChangeTime + 0.15 < time
        then
           self.lightCount = clip
           self.lightChangeTime = time
        end

        viewModel:InstanceMaterials()
        viewModel:GetRenderModel():SetMaterialParameter("ammo", self.lightCount or 6 )

     end
      end
   end

end

-- Annexes changes

GetTexCoordsForTechId(kTechId.Shotgun)
gTechIdPosition[kTechId.HeavyShotgun] = kDeathMessageIcon.Shotgun
-- local originalMarineGetPlayerStatusDesc
-- originalMarineGetPlayerStatusDesc
--    = Class_ReplaceMethod("Marine", "GetPlayerStatusDesc",
--                          function(self)
--                             local weapon = self:GetWeaponInHUDSlot(1)
--                             if (weapon) and self:GetIsAlive() then
--                                if (weapon:isa("HeavyShotgun")) then
--                                   return "HShotgun"--kPlayerStatus.HShotgun
--                                end
--                             end
--                             return originalMarineGetPlayerStatusDesc(self)
--                          end
--                         )

-- local oldOnCommandScores = OnCommandScores
-- function OnCommandScores(scoreTable)

--    local status = kPlayerStatus[scoreTable.status]
--    if scoreTable.status == kPlayerStatus.Shotgun then
--       status = "HShotgun"

--       Scoreboard_SetPlayerData(scoreTable.clientId, scoreTable.entityId, scoreTable.playerName, scoreTable.teamNumber, scoreTable.score,
--                                scoreTable.kills, scoreTable.deaths, math.floor(scoreTable.resources), scoreTable.isCommander, scoreTable.isRookie,
--                                status, scoreTable.isSpectator, scoreTable.assists, scoreTable.clientIndex)

--    else
--       oldOnCommandScores(scoreTable)
--    end

-- end
Shared.LinkClassToMap("HeavyShotgun", HeavyShotgun.kMapName, networkVars)
