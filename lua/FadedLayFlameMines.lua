
Script.Load("lua/Weapons/Marine/LayMines.lua")
Script.Load("lua/FadedFlameMine.lua")

class 'LayFlameMines' (LayMines)

LayFlameMines.kMapName = "flamemine"


-- local kDropModelName = PrecacheAsset("models/marine/mine/mine_pile.model")
-- local kHeldModelName = PrecacheAsset("models/marine/mine/mine_3p.model")

-- local kViewModels = GenerateMarineViewModelPaths("mine")
-- local kAnimationGraph = PrecacheAsset("models/marine/mine/mine_view.animation_graph")

local networkVars =
   {
   }

function LayFlameMines:OnCreate()
   LayMines.OnCreate(self)
   self:Refill(kFadedFlameMinesNum)
end

function LayFlameMines:GetDropStructureId()
   return (kTechId.FlameMine)
end

function LayFlameMines:GetSuffixName()
   return "flamemine"
end


function LayFlameMines:GetDropClassName()
   return "FlameMine"
end

function LayFlameMines:GetDropMapName()
   return FlameMine.kMapName
end

if Client then

   function LayFlameMines:GetUIDisplaySettings()
      return { xSize = 256, ySize = 417, script = "lua/GUIMineDisplay.lua", textureNameOverride = "mine" }
   end

end


-- Same as orig ns2 (except CreateEntity)
local function DropStructure(self, player)

   if Server then

      local showGhost, coords, valid = self:GetPositionForStructure(player)
      if valid then

         --  Create mine.
         -- Faded: changing this (getdropmapname is not called, do not know why)
         local mine = CreateEntity(FlameMine.kMapName, coords.origin, player:GetTeamNumber())
         if mine then

            mine:SetOwner(player)

            --  Check for space
            if mine:SpaceClearForEntity(coords.origin) then

               local angles = Angles()
               angles:BuildFromCoords(coords)
               mine:SetAngles(angles)

               player:TriggerEffects("create_" .. self:GetSuffixName())

               --  Jackpot.
               return true

            else

               player:TriggerInvalidSound()
               DestroyEntity(mine)

            end

         else
            player:TriggerInvalidSound()
         end

      else

         if not valid then
            player:TriggerInvalidSound()
         end

      end

   elseif Client then
      return true
   end

   return false

end

-- Same as orig NS2
function LayFlameMines:PerformPrimaryAttack(player)

   local success = true

   if self.minesLeft > 0 then

      player:TriggerEffects("start_create_" .. self:GetSuffixName())

      local viewAngles = player:GetViewAngles()
      local viewCoords = viewAngles:GetCoords()

      success = DropStructure(self, player)

      if success and not player:GetDarwinMode() then
         self.minesLeft = Clamp(self.minesLeft - 1, 0, kNumMines)
      end

   end

   return success

end



Shared.LinkClassToMap("LayFlameMines", LayFlameMines.kMapName, networkVars)
