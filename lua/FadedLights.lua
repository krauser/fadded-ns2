// ===================== Faded Mod =====================
//
// lua\FadedLights.lua
//
//    Created by: Rio (rio@myrio.de)
//
// =====================================================

Script.Load("lua/FadedPowerPointLightHandler.lua")

-- -- Les aliens voient mieux dans le noir (couleur verte)
-- PowerPoint.kDisabledColorForAlien = Color(0.0015*2, 0.015*2, 0.0015*2)
-- -- PowerPoint.kDisabledColorForMarine = Color(0.014, 0.014, 0.014)
-- PowerPoint.kDisabledColorForMarine = Color(0.0015*2, 0.015*2, 0.0015*2)

-- return a percent [1.00, 0.00], 1.00 is far, 0.00 is really close
function GetFadeDistancePercent(coord, max_dist)
   local diminish_percent = 1
   -- Faded: Light are darker if a fade is nearby
   -- if (player and player:GetTeamNumber() == 1) then
   local entId = FindNearestEntityId("Fade", coord)
   local nearest_fade = Shared.GetEntity(entId)
   if (nearest_fade) then
      local diminish_dist = coord:GetDistanceTo(nearest_fade:GetOrigin())

      -- diminish_dist = Clamp(diminish_dist, 0, max_dist)
      -- if (max_dist == 15) then
      --    Shared:FadedMessage("Fade found. dist: " .. tostring(diminish_dist))
      -- end
      if (diminish_dist <= max_dist) then
         diminish_percent = ((diminish_dist * 100) / max_dist) / 100
      end
      -- end
   end
   --
   return (diminish_percent)
end

local function updateLightColor(color)
   return (color)
end


local lastShutdownLights = nil
function ShutdownLights()
   local lights_changed = {} -- Store lights to not change them more than once
   -- if (lastShutdownLights and (Shared.GetTime() - lastShutdownLights) < 1) then return end
   local local_player = Client.GetLocalPlayer()
   lastShutdownLights = Shared.GetTime()

   local color = Color(kFadedModLightColorScale, kFadedModLightColorScale, kFadedModLightColorScale)
   color = PowerPoint.kDisabledColorForMarine

   local lightRegressionFactor = 1 -- Faded proximity from light
   local faded_proximity = 1 -- Faded proximity from player
   local lightIntensity = kFadedModLightIntensity
   local is_marine = local_player and local_player:GetTeamNumber() == 1

   if (is_marine) then
      faded_proximity = GetFadeDistancePercent(local_player:GetOrigin(), 20)
      faded_proximity = Clamp(faded_proximity * 100, 10, 100) / 100
   end

   -- if (local_player and local_player:isa("Marine") and local_player:GetIsAlive())
   -- then -- The lower the sanity is, the darker the room is
   --    -- the room is completely dark before we reach 0 sanity
   --    -- if we are at 50% sanity, the room intensity is 50%-12% ==> 62% darker
   --    -- and below 75% it's completely dark
   --    local sanity = (local_player:GetMentalHealth() / 100)
   --    lightRegressionFactor = sanity - (1 - sanity) / 4
   --    -- if (lightRegressionFactor <= 0) then lightRegressionFactor = 0 end
   --    -- lightIntensity = lightIntensity * lightRegressionFactor
   -- end

   if (is_marine) then
      for index, renderLight in ipairs(Client.lightList) do
         local dist_to_light = renderLight:GetCoords().origin:GetDistanceTo(local_player:GetOrigin())

         local maxDistance = 34
         local light_proximity = (100 - ((100 / maxDistance) * dist_to_light)) / 100

         -- lightRegressionFactor = GetFadeDistancePercent(renderLight:GetCoords().origin, 30)
         -- lightRegressionFactor = lightRegressionFactor * lightRegressionFactor

         -- lightRegressionFactor = 0
         -- renderLight:SetIntensity(renderLight.originalIntensity * light_proximity * faded_proximity * kFadedModLightIntensity)

         renderLight:SetIntensity(renderLight.originalIntensity
                                     * faded_proximity
                                     * lightRegressionFactor
                                     * light_proximity)

         -- end
         -- color = renderLight:GetColor()
         -- renderLight:SetColor(updateLightColor(renderLight:GetColor()))




         -- if renderLight:GetType() == RenderLight.Type_AmbientVolume
         -- then
            renderLight:SetDirectionalColor(RenderLight.Direction_Right,    color)
            renderLight:SetDirectionalColor(RenderLight.Direction_Left,     color)
            renderLight:SetDirectionalColor(RenderLight.Direction_Up,       color)
            renderLight:SetDirectionalColor(RenderLight.Direction_Down,     color)
            renderLight:SetDirectionalColor(RenderLight.Direction_Forward,  color)
            renderLight:SetDirectionalColor(RenderLight.Direction_Backward, color)
         -- end




         -- local location = GetLocationForPoint(renderLight:GetCoords().origin)
         -- local lightLocation = location and location:GetName() or ""
         -- location = GetLocationForPoint(local_player:GetOrigin())
         -- local playerLocation = location and location:GetName() or ""
         -- if (Shared.GetMapName() == "ns2_summit") then
         --    Shared:FadedMessage("Location: " .. lightLocation
         --                           .. "player loc: " .. playerLocation
         --                           .. " (same room)")

         --    for _, loc_ref in ipairs(
         --       {
         --          "Upper Rapids", "Lower Rapids", "Southern Biosphere"
         --       })
         --    do
         --       if (true or locationName == loc_ref) then
         --          light:SetIntensity(light.originalIntensity * 3)
         --          Shared:FadedMessage("Light sets")
         --          break;
         --       end
         --    end
         -- end

      end
   end

end

-- local location = GetLocationForPoint(local_player:GetOrigin())
-- local locationName = location and location:GetName() or ""
-- if (map == "ns2_kodiak") then
--    for _, loc_ref in ipairs(
--       {
--          "Upper Rapids", "Lower Rapids", "Southern Biosphere"
--       })
--    do
--       if (locationName == loc_ref) then
--          local lights = GetLightsForLocation(locationName)
--          for _, light in ipairs(lights) do
--             light:SetIntensity(light.originalIntensity * 3)
--             Shared:FadedMessage("Light sets")
--          end
--          break
--       end
--    end
-- end

Event.Hook("UpdateClient", ShutdownLights)

-- Dont't reset the scoreboard on game reset
function OnCommandOnResetGame()
   ResetLights()
end
