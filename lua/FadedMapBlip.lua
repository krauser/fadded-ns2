// ===================== Faded Mod =====================
//
// lua\FadedMapBlip.lua
//
//    Created by: Rio (rio@myrio.de)
//    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================
--[[
   Notes:
   This file manage the minimap of players.
   With new NS2 build you must take care of updating variable like:
   * blip_block_size
   * * The number of blip per block of element
   * blip_team_it
   * * The index in a block where the team of the element is stored

   Exemple of code in the "PlayerUI_GetStaticMapBlips" function in
   "Player_Client.lua".
   [...]
   blipsData[i + 1] = blipOrig.x
   blipsData[i + 2] = blipOrig.z
   blipsData[i + 3] = GetMapBlipRotation(blip)
   blipsData[i + 4] = 0
   blipsData[i + 5] = 0
   blipsData[i + 6] = GetMapBlipType(blip)
   blipsData[i + 7] = blipTeam
   blipsData[i + 8] = GetMapBlipIsInCombat(blip)
   blipsData[i + 9] = isSteamFriend
   blipsData[i + 10] = blip.isHallucination == true
   [...]

   After reading this i assume there are 10 elem per block size,
   also i can find that the team is at index '7'.
   Recap:
   1-5  * X pos, Y pos, rotation, X texture offset, Y texture offset,
   6-8  * kMinimapBlipType, kMinimapBlipTeam, Combat blip,
   9-10 * SteamFriend, isHallucination

--]]
-- local tinsert = table.insert

-- local kMinimapBlipTeamAlien = kMinimapBlipTeam.Alien
-- local kMinimapBlipTeamMarine = kMinimapBlipTeam.Marine
-- local kMinimapBlipTeamFriendAlien = kMinimapBlipTeam.FriendAlien
-- local kMinimapBlipTeamFriendMarine = kMinimapBlipTeam.FriendMarine
-- local kMinimapBlipTeamInactiveAlien = kMinimapBlipTeam.InactiveAlien
-- local kMinimapBlipTeamInactiveMarine = kMinimapBlipTeam.InactiveMarine
-- local kMinimapBlipTeamFriendly = kMinimapBlipTeam.Friendly
-- local kMinimapBlipTeamEnemy = kMinimapBlipTeam.Enemy
-- local kMinimapBlipTeamNeutral = kMinimapBlipTeam.Neutral


-- if (Client) then
--    local blip_block_size = 10
--    local blip_team_it = 7
--    local blip_type_it = 6

--    -- local function removeBlipBlock(blipsData, index)
--    --    for i = 0, 9 do
--    --      return (table.remove(blipsData, index + i))
--    -- end

--    -- Find blip and players with the same x/z coord, and edit minimap color avatar
--    local function isPlayerVeteran(player, x, z)
--       for _, marine in ipairs(GetEntitiesForTeam("Marine", 1)) do
--          if (marine and marine:GetMaxArmor() == kFadedVeteranArmor
--              and marine:GetMaxHealth() == kFadedVeteranHealth) then
--             local x_diff = x - marine:GetOrigin().x
--             local z_diff = z - marine:GetOrigin().z
--             -- Check abs value (as the player has moved a bit)
--             if (-1 < x_diff and x_diff < 1
--                    and -1 < z_diff and z_diff < 1)
--             then
--                return (true)
--             end
--          end
--       end
--       return (false)
--    end

--    -- Find fake marine on minimap
--    local function isPlayerFakeMarine(player, x, z)
--       for _, marine in ipairs(GetEntitiesForTeam("Marine", 2)) do
--          if (marine) then
--             local x_diff = x - marine:GetOrigin().x
--             local z_diff = z - marine:GetOrigin().z
--             -- Check abs value (as the player has moved a bit)
--             if (-1 < x_diff and x_diff < 1
--                    and -1 < z_diff and z_diff < 1)
--             then
--                return (true)
--             end
--          end
--       end
--       return (false)
--    end

--    -- Remove blip_block_size of block index
--    local function removeBlipBlock(blipsData, blockNb)
--       local it = 0
--       while (it < blip_block_size) do
--          table.remove(blipsData, blockNb * blip_block_size)
--          -- Print("Removed done for item " .. tostring(it))
--          it = it + 1
--       end
--    end

--    -- Remove fake marine BlipMap
--    local function removeFakeMarineMinimapIcon(blipsData, i, index)
--       local isFakeMarine = false
--       local player = Client.GetLocalPlayer()
--       if (blipsData[index + blip_team_it] == kMinimapBlipTeamAlien) then
--          isFakeMarine = isPlayerFakeMarine(player, blipsData[index + 1],
--                                            blipsData[index + 2])
--          if (isFakeMarine) then
--             removeBlipBlock(blipsData, i)
--          end
--       end
--    end

--    -- Change Veteran color on Minimap
--    local function changeVeteranMinimapColor(blipsData, index, x)
--       local isVeteran = false
--       local player = Client.GetLocalPlayer()
--       if (x == blip_team_it) then
--          local _blipsData = blipsData[index + x]
--          -- Check for minimap color
--          isVeteran = false
--          if (_blipsData == kMinimapBlipTeamMarine) then
--             isVeteran = isPlayerVeteran(player, blipsData[index + 1],
--                                         blipsData[index + 2])
--          end
--          if (isVeteran) then
--             blipsData[index + x] = kMinimapBlipTeamFriendAlien
--          end
--       end
--    end

--    -- Change in blue the color of fake marines on the minimap for non veteran
--    local function changeFakeMarineColorForNonVeteran(player, blipsData, isVeteran)
--       if (player:GetTeamNumber() == 1 and isVeteran == false) then
--          local index = nil
--          local numBlips = #blipsData / blip_block_size
--          -- Only edit marines minimap
--          if (player and player:GetIsAlive()) then
--             -- For each bitmap block
--             for i = 0, numBlips do
--                index = (i * blip_block_size)
--                if (blipsData[index + blip_team_it] == kMinimapBlipTeamAlien) then
--                   blipsData[index + blip_team_it] = kMinimapBlipTeamMarine
--                end
--             end
--          end
--       end
--    end

--    local function removeCorpsesForMarines(player, blipsData)
--       -- Print("removeCorpsesForMarines called")
--       if (player:GetTeamNumber() == 1) then
--          -- Print("Pass the if")
--          local index = nil
--          local numBlips = #blipsData / blip_block_size
--          -- Only edit marines minimap
--          if (player and player:GetIsAlive()) then
--             -- Print("Pass the second if")
--             -- For each bitmap block
--             for i = 0, numBlips do
--                index = (i * blip_block_size)
--                local blip_team = blipsData[index + blip_team_it]
--                local blip_type = blipsData[index + blip_type_it]
--                -- Print("team = " .. tostring(blipsData[index + blip_team_it])
--                --         .. "type = " .. tostring(blipsData[index + blip_type_it])
--                --         .. "x = " .. tostring(blipsData[index + 1])
--                --         .. "z = " .. tostring(blipsData[index + 2]))
--                if (blip_team == kMinimapBlipTeam.Neutral
--                       and (blip_type == kMinimapBlipType.Marine
--                            or blip_type == kMinimapBlipType.Fade)) then
--                   removeBlipBlock(blipsData, i)
--                   -- Table changed, reparse all
--                   removeCorpsesForMarines(player, blipsData)
--                   return
--                end
--             end -- end for
--          end -- end alive
--       end -- end if team
--    end

--    local function changeSkulkBlipType(player, blipsData)
--       if (player:GetTeamNumber() ~= 1) then
--          local index = nil
--          local numBlips = #blipsData / blip_block_size
--          for i = 0, numBlips do
--             index = (i * blip_block_size)
--             local blip_type = blipsData[index + blip_type_it]
--             if (blip_type == kMinimapBlipType.Skulk) then
--                blipsData[index + blip_type_it] = kMinimapBlipType.Undefined
--             end
--          end
--       end
--    end

--    function filterOnlyBatteryBlock(player, blipsData)
--       local batteryOnly = {}
--       local index = nil
--       local numBlips = #blipsData / blip_block_size
--       -- Only edit marines minimap
--       if (player and player:GetIsAlive()) then
--          -- Print("Pass the second if")
--          -- For each bitmap block
--          for i = 0, numBlips do
--             index = (i * blip_block_size)
--             local blip_team = blipsData[index + blip_team_it]
--             local blip_type = blipsData[index + blip_type_it]
--             if (blip_type == kMinimapBlipType.SentryBattery) then
--                local it = 1
--                while (it <= blip_block_size) do
--                   table.insert(batteryOnly, blipsData[index + it])
--                   it = it + 1
--                end
--             end
--          end -- end for
--       end -- end alive
--       return (batteryOnly)
--    end

--    --[[
--       This function change the content of the original
--       playerUIGetStaticMapBlips() function.
--       Change:
--       * Remove all minimap item for non Veteran exept battery (whitout obs)
--       * Change from red to blue fake marine for non Veteran (with and without obs)
--       * Remove corpses icon for all the marine team
--    --]]
--    local playerUIGetStaticMapBlips = PlayerUI_GetStaticMapBlips
--    function PlayerUI_GetStaticMapBlips()
--       -- Print("PlayerUI_GetStaticMapBlips called")

--       local blipsData = playerUIGetStaticMapBlips()
--       local player = Client.GetLocalPlayer()
--       local isSpectator = player:isa("Spectator") or player:isa("FilmSpectator")
--       local isVeteran = player:GetMaxHealth() == kFadedVeteranHealth
--          and player:GetMaxArmor() == kFadedVeteranArmor
--       changeFakeMarineColorForNonVeteran(player, blipsData, isVeteran)
--       removeCorpsesForMarines(player, blipsData)
--       changeSkulkBlipType(player, blipsData)
--       if (isSpectator == true or isVeteran
--                or player:GetTeamNumber() ~= 1
--                or player.kFadedIsObsUp
--                or player:isa("Exo") -- Exo always have minimap
--              or player:isa("Skulk") -- player babbler have the minimap
--       ) then
--            return (blipsData)
--       end
--       return (filterOnlyBatteryBlock(player, blipsData))
--    end
-- end


-- local mapBlipUpdateRelevancy = MapBlip.UpdateRelevancy
-- function MapBlip:UpdateRelevancy()
--    local owner = Shared.GetEntity(self.ownerEntityId)
--    local isVeteran = false
--    if (owner) then
--       isVeteran = owner:GetMaxHealth() == kFadedVeteranHealth
--          and owner:GetMaxArmor() == kFadedVeteranArmor
--    end

--    if (owner == nil) then
--       return (mapBlipUpdateRelevancy(self))
--    end
--    if (owner:GetTeamNumber() ~= 1 or isVeteran) then
--       if (mapBlipUpdateRelevancy) then
--          mapBlipUpdateRelevancy(self)
--       end
--    else
--       self:SetRelevancyDistance(Math.infinity)

--       local mask = 0

--       -- if self.mapBlipTeam == kTeam1Index or self.mapBlipTeam == kTeamInvalid or self:GetIsSighted() then
--       --    mask = bit.bor(mask, kRelevantToTeam1)
--       -- end
--       -- if self.mapBlipTeam == kTeam2Index or self.mapBlipTeam == kTeamInvalid or self:GetIsSighted() then
--       --    mask = bit.bor(mask, kRelevantToTeam2)
--       -- end

--       self:SetExcludeRelevancyMask( mask )

--    end

-- end

