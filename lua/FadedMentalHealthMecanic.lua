// ===================== Faded Mod =====================
//
// lua\FadedSpawnHallucination.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================


class 'MentalHealthEffect'

-- Calculate the change on a marine health depending on
-- the number of marine around and the total marine alive on the map
function MentalHealthEffect:GetMentalHealthUpdate(player,
                                                  marine_team,
                                                  timePassed)
   if (player:isa("Marine") == false) then
      Print("Player is not a marine ! " .. tostring(player:GetTechId()))
   end
   local alive_marines_nb = 0
   local alive_marine_nearby_nb = 0
   local nearby_flashlight_on = 0
   for _, marine in pairs(marine_team) do
      if (marine ~= nil and marine:GetIsAlive()) then
         alive_marines_nb = alive_marines_nb + 1
      end
      if (alive_marines_nb >= 22) then
         break
      end
   end
   -- if (Client) then
   --    Print("alive_marines_nb = " .. tostring(alive_marines_nb))
   -- end
   local _marines_around = GetEntitiesWithinRange("Marine",
                                                  player:GetOrigin(),
                                                  kFadedMarineGroupRadius)
   -- Marine around excluding ourself
   local marines_around = 0
   for index, marine in ipairs(_marines_around) do
      marines_around = marines_around + marine.kFadedMentalStrenght
      -- Marine with flashlight on count as 1.5
      if (marine:GetFlashlightOn() and player:GetId() ~= marine:GetId())
      then
         nearby_flashlight_on = nearby_flashlight_on + kFadedFriendFlashLight
      end
   end

   -- 23 - (Player Vivant dans la Team marine) + (Taille du groupe)
   local X = 23 - alive_marines_nb + marines_around
   local mentalHealhUpdateVal = (-1/72)*(X*X) + (7/12)*X - 3
   -- Add flashlight factor + time
   if (player:GetFlashlightOn()) then
      mentalHealhUpdateVal = ((kFadedMarineMentalRege + mentalHealhUpdateVal) / kFadedSanityAmountScale) / 1.1 -- Sanity from light regen 10% slower
   else
      mentalHealhUpdateVal = (-(kFadedMarineMentalUse - mentalHealhUpdateVal)) / kFadedSanityAmountScale
   end
   local poison_malus = 0
   if (player and player:isa("Marine") and player.poisoned) then
      poison_malus = kFadedPoisonSanityDamagePerSec
   end

   mentalHealhUpdateVal = mentalHealhUpdateVal
      + nearby_flashlight_on * kFadedFriendFlashLight
      - poison_malus
   mentalHealhUpdateVal = mentalHealhUpdateVal * timePassed
   if (mentalHealhUpdateVal > 0) then -- Decrease regen if fade is nearby
      mentalHealhUpdateVal = mentalHealhUpdateVal
      -- Allow at least N% of actual regen
      -- * GetFadeDistancePercent(player:GetOrigin(), 40)
         * (Clamp(
               GetFadeDistancePercent(player:GetOrigin(), 40) * 100,
               20, 100) / 100)
   end
   return (mentalHealhUpdateVal)
end

if (Server) then
   Script.Load("lua/bots/Bot.lua")

   local kHallucinationList = {}


   --------------------------------------------------
   ------- Hallucination spawn management
   --------------------------------------------------

   -- Hallucination Spawn & management function
   -- Add an hallucination for the corresponding player
   function MentalHealthEffect:AddHallucinationToList(player)
      table.insertunique(kHallucinationList, {player:GetId(),
                                              player:GetOrigin()})
   end
   function MentalHealthEffect:ClearHallucinationList()
      kHallucinationList = {}
   end

   -- Patch function
   -- The hallucination cloud drop hive, we have to remove them
   -- No need for harvester, they need infestation
   function MentalHealthEffect:RemoveHiveHallucination()
      for _, techPoint in ientitylist(Shared.GetEntitiesWithClassname("TechPoint")) do

         if techPoint:GetAttached() ~= nil then
            DestroyEntitySafe(techPoint:GetAttached())
         end

      end

   end

   -- Set the hallucination name to one of the Faded in team 2
   function MentalHealthEffect:ChooseHallucinationName(hallucinatedPlayer)
      // Pseudo table random for setting hallucination name
      local faded_player_random = nil
      for _, hallucinatedPlayer in ipairs(hallucinatedPlayer) do
         for _, faded in pairs(GetGamerules():GetTeam2():GetPlayers()) do
            if (math.random(0, 1) == 0 and faded) then
               faded_player_random = faded
               break
            end
         end
         if (faded_player_random) then
            hallucinatedPlayer:SetName(faded_player_random:GetName())
            hallucinatedPlayer:SetHallucinatedClientIndex(faded_player_random:GetClientIndex())
         end
      end
   end

   function MentalHealthEffect:_SpawnFakeMarineHallucination(hallu_origin)
      local hallucinatedPlayer = {}
      table.insert(hallucinatedPlayer, CreateEntity(Fade.kMapName,
                                                    hallu_origin, 2))
      self:ChooseHallucinationName(hallucinatedPlayer)
      return (hallucinatedPlayer)
   end

   function MentalHealthEffect:_SpawnBabblersHallucination(hallu_origin)
      local hallucinatedPlayer = {}
      local random_number = math.random(2, 15)
      local total_babbler = GetEntitiesForTeam("Babbler", 2)
      -- Safety check to avoid babblers flood
      if (#total_babbler + random_number < kMaxBabblerOnMap) then
         for i = 1, random_number do
            local babbler = CreateEntity(Babbler.kMapName, hallu_origin, 2)
            babbler.brain = SkulkBrain()
            table.insert(hallucinatedPlayer, babbler)
         end
      end
      return (hallucinatedPlayer)
   end

   function MentalHealthEffect:_SpawnFadeHallucination(hallu_origin)
      local hallucinatedPlayer = {}
      local team_nb = 2
      table.insert(hallucinatedPlayer, CreateEntity(Fade.kMapName,
                                                    hallu_origin, team_nb))
      self:ChooseHallucinationName(hallucinatedPlayer)
      return (hallucinatedPlayer)
   end

   function MentalHealthEffect:_GetRandomEntity()
      local chance = math.random(0, 100)
      local hallucination_entity = {
         [1] = {e = Fade.kMapName, c = 80}, -- 0-80 (80% chance)
         [2] = {e = Babbler.kMapName, c = 100}} -- 80-100 (20% chance)
      -- [Marine.kMapName] = 15}
      for _, entity in ipairs(hallucination_entity) do
         if (chance < entity.c) then
            return (entity.e)
         end
      end
      return (Fade.kMapName)
   end

   function MentalHealthEffect:SpawnHallucinationEntity(marine_id, marine_origin)
      local newAlienExtents = LookupTechData(kTechId.Fade, kTechDataMaxExtents)
      local capsuleHeight, capsuleRadius = GetTraceCapsuleFromExtents(newAlienExtents)
      --function GetRandomSpawnForCapsule(capsuleHeight, capsuleRadius, origin, minRange, maxRange, filter, validationFunc)
      hallu_origin = GetRandomSpawnForCapsule(newAlienExtents.y, capsuleRadius, marine_origin, 10, 15)
      if (hallu_origin == nil) then
         hallu_origin = marine_origin + Vector(0, 0, -0.2)
      end

      local hallucinatedPlayer = {}
      -- Spawn the selected hallucination
      local entity = self:_GetRandomEntity()
      kFadedHallucinationMarineId = marine_id
      if (entity == Marine.kMapName) then
         hallucinatedPlayer = self:_SpawnFakeMarineHallucination(hallu_origin)
      elseif (entity == Babbler.kMapName) then
         hallucinatedPlayer = self:_SpawnBabblersHallucination(hallu_origin)
      else
         hallucinatedPlayer = self:_SpawnFadeHallucination(hallu_origin)
      end
      -- Add mixin and order stuff
      kFadedHallucinationMarineId = nil
      for _, hallu in ipairs(hallucinatedPlayer) do
         hallu:SetMaxHealth(kHallucinationMaxHealth)
         hallu:SetHealth(kHallucinationMaxHealth)
         hallu:SetMaxArmor(0)
         hallu:SetArmor(0)
         -- Force crouching in ventilation, so hallucination is not stuck
         hallu.crouching = true

         hallu.isHallucination = true
         InitMixin(hallu, PlayerHallucinationMixin)
         InitMixin(hallu, SoftTargetMixin)
         InitMixin(hallu, OrdersMixin, { kMoveOrderCompleteDistance = kPlayerMoveOrderCompleteDistance })
         hallu:GiveOrder(kTechId.Attack, marine_id, marine_origin, nil, true, false)
      end
   end

   -- Spawn hallucination with a single cloud on all the map
   function MentalHealthEffect:ApplyHallucinationList()
      local    i = 0
      local    size
      local    fade_copy = {}

      size = table.getn(kHallucinationList)
      while (i < size and i < kPlayerHallucinationNumFraction) do
         i = i + 1
         local marine_id = (kHallucinationList[i])[1]
         local marine_origin = (kHallucinationList[i])[2]

         -- Used to bind the hallucination revelancy to the marine
         self:SpawnHallucinationEntity(marine_id, marine_origin)

         ----------------
         -- local hallu_name = self:ChooseHallucinationName(hallucinatedPlayer)

         -- hallucinatedPlayer:SetName("Hallucination")
         -- hallucinatedPlayer.kMapName = "Hallucination"
         -- end
      end

      self:ClearHallucinationList()
   end

   --------------------------------------------------
   ------- Post update
   --------------------------------------------------

   -- This function is call at the end of the server loop
   -- to apply block effect
   -- local kFadedtimetmp = 0
   function MentalHealthEffect:PostUpdate(timePassed)
      MentalHealthEffect:ApplyHallucinationList()
   end
end

--------------------------------------------------
------- kFadedMentalHealthLevels callback function
--------------------------------------------------

-- Manage the rate of effects every "rate" second
-- Call function if time is passed
local function FadedManageEffectsRate(player, timePassed,
                                      lvl, rate, functor)
   if (kFadedMentalHealthLevels[lvl].cpt == nil) then
      kFadedMentalHealthLevels[lvl].cpt = 0
   end

   kFadedMentalHealthLevels[lvl].cpt =
      kFadedMentalHealthLevels[lvl].cpt + timePassed

   if (kFadedMentalHealthLevels[lvl].cpt > rate) then
      functor(player, timePassed)

      kFadedMentalHealthLevels[lvl].cpt =
         kFadedMentalHealthLevels[lvl].cpt - rate
   end
end

------------------------
local function FadedMHSafeLevel(player, timePassed)
   -- Print("Safe effect")
end
------------------------

local function FadedMHStressLevelEffects(player, timePassed)
   if (Server) then
      local chance = kFadedStressLvlHallucinationChance
      if (math.random() < chance) then
         MentalHealthEffect:AddHallucinationToList(player)
      end
   end
   if (Client) then
      local chance = kFadedStressLvlApproachSoundChance
      if (math.random() < chance) then
         MentalHealthEffect:kFadedPlayRandomSoundApproach(player)
      end
   end
   FadedMHSafeLevel(player, timePassed)
end

local function FadedMHStressLevel(player, timePassed)
   FadedManageEffectsRate(player, timePassed,
                          1, -- Level in the table (starting at 0)
                          kFadedStressLvlApproachSoundRate,
                          FadedMHStressLevelEffects)
end

------------------------

local function FadedMHPanicLevelEffects(player, timePassed)
   if (Server) then
      local chance = kFadedPanicLvlHallucinationChance
      if (math.random() < chance) then
         MentalHealthEffect:AddHallucinationToList(player)
      end
   end
   FadedMHStressLevelEffects(player, timePassed)
end

local function FadedMHPanicLevel(player, timePassed)
   FadedManageEffectsRate(player, timePassed,
                          2, -- Level in the table (starting at 0)
                          kFadedPanicLvlHallucinationRate,
                          FadedMHPanicLevelEffects)
end

------------------------

local function FadedMHHeavyPanicLevelEffects(player, timePassed)
   if (Server) then
      local chance = kFadedHeavyPanicLvlHallucinationChance
      if (math.random() < chance)
      then
         MentalHealthEffect:AddHallucinationToList(player)
      end
   elseif (Client) then
      local chance = kFadedHeavyPanicLvlGrowlChance
      if (math.random() < chance) then
         Shared.PlaySound(player, "sound/faded.fev/alien/voiceovers/growl")
      end
   end
   FadedMHPanicLevel(player, timePassed)
end

local function FadedMHHeavyPanicLevel(player, timePassed)
   FadedManageEffectsRate(player, timePassed,
                          3, -- Level in the table (starting at 0)
                          kFadedHeavyPanicLvlHallucinationRate,
                          FadedMHHeavyPanicLevelEffects)
end

------------------------------------------

-- Steps of the marine mental health
-- {name, l, h, fct}
-- * name ==> Arbitrary name for the level
-- * l    ==> This value correspond to the limit to be reached
--             before going on the next level
-- * h    ==> This value correspond to the limit to be reached
--            before going on a safer level
-- * fct  ==> Callback function wich apply effect of the level
--
-- Warning: The First level must have a Hight bound equal to 100 !!
-- Warning: The last level must have a lower bound equal to 0 !!
kFadedMentalHealthLevels =
   {
      -- Effect: None
      {name = "Safe",    l = 60,    h = 100, fct = FadedMHSafeLevel},
      -- Effect: Hear random comming fade noise
      {name = "Stress",    l = 20,    h = 80, fct = FadedMHStressLevel},
      -- Effect: 30% hallucination chance every "kFadedHallucinationRate"
      {name = "Panic",    l = 1,    h = 20, fct = FadedMHPanicLevel},
      -- Effect: 30% added to Hallucination chance
      -- Effect: 10% Growl (no damage)(hear in client) every "kFadedHallucinationRate"
      {name = "Heavy Panic", l = 0, h = 20, fct = FadedMHHeavyPanicLevel},
   }
