// ===================== Faded Mod =====================
//
// lua\FadedFade.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

//
// lua\Weapons\Alien\Metabolize.lua

Script.Load("lua/Weapons/Alien/Ability.lua")
Script.Load("lua/Weapons/Alien/Blink.lua")

class 'Metabolize' (Blink)

Metabolize.kMapName = "metabolize"

local networkVars =
   {
      lastPrimaryAttackTime = "time"
   }

local kMetabolizeDelay = 2.0
local kMetabolizeEnergyRegain = 35
local kMetabolizeHealthRegain = 15

local kAnimationGraph = PrecacheAsset("models/alien/fade/fade_view.animation_graph")

local onCreate = Metabolize.OnCreate
function Metabolize:OnCreate()
   onCreate(self)
end

function Metabolize:GetAnimationGraphName()
   -- return nil
   return kAnimationGraph
end

function Metabolize:GetEnergyCost(player)
   return 1000
end

function Metabolize:GetHUDSlot()
   return 0
end

function Metabolize:GetDeathIconIndex()
   return kDeathMessageIcon.Metabolize
end

function Metabolize:GetBlinkAllowed()
   return false
end

function Metabolize:GetAttackDelay()
   return kMetabolizeDelay
   -- return 0
end

function Metabolize:GetLastAttackTime()
   -- return self.lastPrimaryAttackTime
   return 0
end

function Metabolize:GetSecondaryTechId()
   -- return kTechId.Blink
   return kTechId.Leap
end

function Metabolize:GetHasAttackDelay()
   return true
end

local onPrimaryAttack = Metabolize.OnPrimaryAttack
function Metabolize:OnPrimaryAttack(player)
   -- onPrimaryAttack(self, player)
end

local onPrimaryAttackEnd = Metabolize.OnPrimaryAttackEnd
function Metabolize:OnPrimaryAttackEnd()
   -- onPrimaryAttackEnd(self)
end

local onHolster = Metabolize.OnHolster
function Metabolize:OnHolster(player)
   -- onHolster(self, player)
end

local onTag = Metabolize.OnTag
function Metabolize:OnTag(tagName)
   -- onTag(self, tagName)
end

local onUpdateAnimationInput = Metabolize.OnUpdateAnimationInput
function Metabolize:OnUpdateAnimationInput(modelMixin)
   -- onUpdateAnimationInput(self, modelMixin)
end

Shared.LinkClassToMap("Metabolize", Metabolize.kMapName, networkVars)
