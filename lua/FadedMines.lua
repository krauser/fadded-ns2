// ===================== Faded Mod =====================
//
// lua\FadedMines.lua
//
//    Created by: Rio (rio@myrio.de)
//
// =====================================================

if (Server) then
   function Mine:OnTouchInfestation()
      -- Arm(self) -- Do not explode on infest, allow body mine trap
   end
end


local layMinesGetPositionForStructure = LayMines.GetPositionForStructure
function LayMines:GetPositionForStructure(player)
   local foundPositionInRange, position, valid = layMinesGetPositionForStructure(self, player)

   local nearbyMines = GetEntitiesWithinRange("Mine", player:GetOrigin(), kFadedModMinesRestrictionRange)

   valid = #nearbyMines + 1 <= kFadedModMinesRestrictionCount
   if (GetGamerules() and GetGamerules():GetGameState() == kGameState.NotStarted) then
      valid = true
   end

   return foundPositionInRange, position, valid
end
