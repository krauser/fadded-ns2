// ======= Copyright (c) 2003-2013, Unknown Worlds Entertainment, Inc. All rights reserved. =======
//
// lua\Weapons\Marine\GasGrenade.lua
//
//    Created by:   Andreas Urwalek (andi@unknownworlds.com)
//
// ========= For more information, visit us at http://www.unknownworlds.com =====================

Script.Load("lua/Weapons/PredictedProjectile.lua")
Script.Load("lua/OwnerMixin.lua")

class 'NapalmGrenade' (PredictedProjectile)

NapalmGrenade.kMapName = "napalmgrenadeprojectile"
NapalmGrenade.kModelName = PrecacheAsset("models/marine/grenades/gr_nerve_world.model")
NapalmGrenade.kUseServerPosition = true

NapalmGrenade.kRadius = 0.085
NapalmGrenade.kClearOnImpact = false
NapalmGrenade.kClearOnEnemyImpact = false

local networkVars =
   {
      releaseGas = "boolean",
      nextRelease = "time"
   }

local kLifeTime = 12
local kGasReleaseDelay = 0.34

AddMixinNetworkVars(BaseModelMixin, networkVars)
AddMixinNetworkVars(ModelMixin, networkVars)
AddMixinNetworkVars(TeamMixin, networkVars)

local function TimeUp(self)
   DestroyEntity(self)
end

function NapalmGrenade:OnCreate()

   PredictedProjectile.OnCreate(self)

   InitMixin(self, BaseModelMixin)
   InitMixin(self, ModelMixin)
   InitMixin(self, TeamMixin)
   InitMixin(self, DamageMixin)

   if Server then

      self:AddTimedCallback(TimeUp, kLifeTime)
      self:AddTimedCallback(NapalmGrenade.ReleaseGas, kGasReleaseDelay)
      -- self:AddTimedCallback(NapalmGrenade.UpdateNerveGas, 1)
   end

   -- self:SetUpdates(true)
   self.releaseGas = false
   self.nextRelease = Shared.GetTime() + kGasReleaseDelay
   self.clientGasReleased = false
end

function NapalmGrenade:ProcessHit(targetHit, surface)

   if self:GetVelocity():GetLength() > 2 then
      self:TriggerEffects("grenade_bounce")
   end

end

if Client then

   function NapalmGrenade:OnUpdateRender()

      PredictedProjectile.OnUpdateRender(self)

      if self.releaseGas and not self.clientGasReleased then

         self:TriggerEffects("release_napalm", { effethostcoords = Coords.GetTranslation(self:GetOrigin())} )
         self.clientGasReleased = true

      end

   end

elseif Server then

   function NapalmGrenade:OnUpdate(deltatime)
      PredictedProjectile.OnUpdate(self, deltatime)
      if self.releaseGas and Shared.GetTime() >= self.nextRelease then
         self.nextRelease = Shared.GetTime() + 0.2
         local direction = Vector(math.random() - 0.5, 0.5, math.random() - 0.5)
         direction:Normalize()

         local trace = Shared.TraceRay(self:GetOrigin() + Vector(0, 0.2, 0), self:GetOrigin() + direction * 7, CollisionRep.Damage, PhysicsMask.Bullets, EntityFilterAll())
         local gascloudaround = GetEntitiesWithinRange("NapalmCloud", self:GetOrigin(), 0.1)
         if (#gascloudaround == 0) then -- Avoid cloud stacking
            local nervegascloud = CreateEntity(NapalmCloud.kMapName, self:GetOrigin(), self:GetTeamNumber())
            nervegascloud:SetEndPos(trace.endPoint)

            local owner = self:GetOwner()
            if owner then
               nervegascloud:SetOwner(owner)
            end
         end

      end
   end

   function NapalmGrenade:ReleaseGas()
      self.releaseGas = true
   end

   -- function NapalmGrenade:UpdateNerveGas()


   --     return true

   -- end

end

Shared.LinkClassToMap("NapalmGrenade", NapalmGrenade.kMapName, networkVars)

class 'NapalmCloud' (Entity)

NapalmCloud.kMapName = "napalmcloud"
NapalmCloud.kEffectName = PrecacheAsset("cinematics/marine/napalmcloud.cinematic")

local gNerveGasDamageTakers = {} -- [1]: entityID [2]: time of the last damages

local kCloudUpdateRate = 0.1
local kSpreadDelay = 0.64

local kCloudMoveSpeed = 1.8

local networkVars =
   {
   }

AddMixinNetworkVars(TeamMixin, networkVars)

function NapalmCloud:OnCreate()

   Entity.OnCreate(self)

   InitMixin(self, TeamMixin)
   InitMixin(self, DamageMixin)

   self.creationTime = Shared.GetTime()

   if Server then


      self:AddTimedCallback(TimeUp, kFadedNapalmCloudLifetime)
      self:AddTimedCallback(NapalmCloud.DoNerveGasDamage, kCloudUpdateRate)

      InitMixin(self, OwnerMixin)

   end

   if Client then
      self.light = Client.CreateRenderLight()
      self.light:SetType( RenderLight.Type_Point )
      self.light:SetColor( Color(0xe2, 0x58, 0x22) )
      self.light:SetIntensity( 0.008 + math.random(1, 30) * 0.0005)
      self.light:SetRadius( 1 )

      self.light:SetAtmosphericDensity(3)
      self.light:SetCastsShadows(false)
      self.light:SetSpecular(false)

      self.light:SetIsVisible(true)

      self.lightTimer = nil
   end

   self:SetUpdates(true)
   self:SetRelevancyDistance(kMaxRelevancyDistance)

end

function NapalmCloud:SetEndPos(endPos)
   self.endPos = Vector(endPos)
end

if Client then

   function NapalmCloud:OnInitialized()

      local cinematic = Client.CreateCinematic(RenderScene.Zone_Default)
      cinematic:SetCinematic(NapalmCloud.kEffectName)
      cinematic:SetParent(self)
      cinematic:SetCoords(Coords.GetIdentity())

   end

end

local function GetRecentlyDamaged(entityId, time)

   for index, pair in ipairs(gNerveGasDamageTakers) do
      if (pair[1] == entityId and pair[2] > time) then
         return true
      end
   end

   return false

end

local function SetRecentlyDamaged(entityId)
   for index, pair in ipairs(gNerveGasDamageTakers) do
      if pair[1] == entityId then
         table.remove(gNerveGasDamageTakers, index)
      end
   end
   table.insert(gNerveGasDamageTakers, {entityId, Shared.GetTime()})
end

local function GetIsInCloud(self, entity, radius)

   local targetPos = entity.GetEyePos and entity:GetEyePos() or entity:GetOrigin()
   return ((self:GetOrigin() - targetPos):GetLength() <= radius)

end

-- if (HasMixin(entity, "Fire")) then
--    entity:SetOnFire(self:GetOwner(), self)
-- end


function NapalmCloud_burnEntity(entity)
   for _, napalmCloud in ipairs(GetEntitiesWithinRange("NapalmCloud",
                                                       entity:GetOrigin(),
                                                       kFadedNapalmCloudRadius))
   do
      if (GetIsInCloud(napalmCloud, entity, napalmCloud:GetRadius())) then
         if (HasMixin(entity, "Fire")) then
            entity:SetOnFire(napalmCloud:GetOwner(), napalmCloud)
         end
      end
   end
   return false
end

function NapalmCloud:GetRadius()
   local radius = math.min(1, (Shared.GetTime() - self.creationTime) / kSpreadDelay) * kFadedNapalmCloudRadius
   return (radius)
end

function NapalmCloud:DoNerveGasDamage()
   -- hurt both marines and the Faded
   for _, entity in ipairs(GetEntitiesWithMixinWithinRange("Live", self:GetOrigin(), 2*kFadedNapalmCloudRadius)) do

      if GetIsInCloud(self, entity, self:GetRadius()) then

         if not GetRecentlyDamaged(entity:GetId(), (Shared.GetTime() - kCloudUpdateRate)) then

            local damages = kFadedNapalmDamagePerSecond * kCloudUpdateRate
            if (self:GetTeamNumber() == entity:GetTeamNumber()) then
               damages = damages * kFadedOnMarineDamageMultiplyer
            end
            self:DoDamage(damages, entity, entity:GetOrigin(), GetNormalizedVector(self:GetOrigin() - entity:GetOrigin()), "none")
            SetRecentlyDamaged(entity:GetId())
            Entity.AddTimedCallback(entity, NapalmCloud_burnEntity, kFadedNapalmPutInFireDelay)
         end

      end
   end

   -- for _, entity in ipairs(GetEntitiesWithMixinForTeamWithinRange("Live", GetEnemyTeamNumber(self:GetTeamNumber()), self:GetOrigin(), kFadedBurnRadius)) do
   --    if (HasMixin(entity, "Fire")) then
   --       entity:SetOnFire(self:GetOwner(), self)
   --    end
   -- end

   -- Put in fire enemies that were in the cloud for too long

   return true

end

function NapalmCloud:GetDeathIconIndex()
   return kDeathMessageIcon.GasGrenade
end

if (Client) then

   function NapalmCloud:OnUpdateRender()
      if (Entity.OnUpdateRender) then
         Entity.OnUpdateRender(self)
      end
      if self.light ~= nil then
         if ((self.creationTime + kFadedNapalmCloudLifetime)-1 <= Shared.GetTime())
         then
            local intensity = 0
            intensity = (self.creationTime + kFadedNapalmCloudLifetime)
            intensity = (intensity - Shared.GetTime()) / 100
            self.light:SetIntensity(intensity)
         else
            self.light:SetRadius(self:GetRadius() * 1.3)
         end
         -- self.light:SetIntensity(self.light:GetIntensity()
         --                            + (math.random(1, 10) - 3) / 200)
         self.light:SetCoords(
            Coords.GetLookIn(
               self:GetOrigin() + Vector(0, 1.1, 0),
               self:GetCoords().zAxis))
         self.lightTimer = Shared.GetTime()
      end
   end

   function NapalmCloud:OnDestroy()
      if (Entity.OnDestroy) then
         Entity.OnDestroy(self)
      end
      if self.light ~= nil then
         Client.DestroyRenderLight(self.light)
         self.light = nil
      end
   end
end

if Server then

   function NapalmCloud:OnUpdate(deltaTime)
      if self.endPos then
         local newPos = SlerpVector(self:GetOrigin(), self.endPos, deltaTime * kCloudMoveSpeed)
         self:SetOrigin(newPos)
      end

   end

end

function NapalmCloud:GetDamageType()
   return kDamageType.Flame
end

GetTexCoordsForTechId(kTechId.GasGrenade)
gTechIdPosition[kTechId.NapalmGrenade] = kDeathMessageIcon.GasGrenade

-- GetTexCoordsForTechId(GetMaterialXYOffset(kTechId.MedPack))
-- gTechIdPosition[kTechId.NapalmGrenade] = kDeathMessageIcon.GasGrenade

Shared.LinkClassToMap("NapalmCloud", NapalmCloud.kMapName, networkVars)
