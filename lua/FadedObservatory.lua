// ===================== Faded Mod =====================
//
// lua\FadedObservatory.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
//
// ===================== Faded Mod =====================

Script.Load("lua/Observatory.lua")


if (Server) then
   function notifyAllMarines(state)
      for _, marine in pairs(GetGamerules():GetTeam(kTeam1Index):GetPlayers()) do
     Server.SendNetworkMessage( -- Client side
        marine, "SetObsState",
        {
           kFadedIsObsUp = state,
        }, true)
     marine.kFadedIsObsUp = state -- Server side
      end
   end
end

-- Detection for the obs disable
local observatoryGetDetectionRange = Observatory.GetDetectionRange
function Observatory:GetDetectionRange()
   if (kFadedObsEnablePassiveDetection == true) then
      return (observatoryGetDetectionRange(self))
   else
      return (0)
   end
end

local observatoryOverrideVisionRadius = Observatory.OverrideVisionRadius
function Observatory:OverrideVisionRadius()
   if (kFadedObsEnablePassiveDetection == true) then
      return (observatoryOverrideVisionRadius(self))
   else
      return (0)
   end
end

-- Check for Sentry Battery and power up if one is around
local observatoryOnUpdate = Observatory.OnUpdate
function Observatory:OnUpdate(deltaTime)
   observatoryOnUpdate(self, deltaTime)
   local oldPowerStatue = self:GetIsPowered()
   local bState = buildingCheckForBattery(self)

   -- See FadedMapBlip.lua for minimap effect
   if (oldPowerStatue == false and bState == true) then
      if (Server and kFadedObsGiveMinimap) then
     notifyAllMarines(true)
      end
   elseif (oldPowerStatue == true and bState == false
       and self:GetIsPowered() == false) then
      if (Server and kFadedObsGiveMinimap) then
         notifyAllMarines(false)
      end
   end
end

function Observatory:OnTakeDamage(damage, attacker, doer, point, direction, damageType, preventAlert)
   BuildingOnTakeDamage(self, damage, attacker, doer, point, direction, damageType, preventAlert)
end

Shared.LinkClassToMap("Observatory", Observatory.kMapName, {})
