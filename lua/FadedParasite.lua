// ===================== Faded Mod =====================
//
// lua\FadedParasite.lua
//
//    Created by: rio (rio@myrio.de)
//    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

-- function ParasiteMixin:GetIsParasited()
--     -- if (Client) then
--     --     local player = Client.GetLocalPlayer()
--     --     return player:isa("Alien")
--     -- elseif (Server) then
--         return true
--     -- end
-- end

-- local function SharedUpdate(self)
--    if ((Server or Client) and (self:isa("Marine"))) then
--       self.parasited = true
--    end

--    -- Display the parasited marine on screen if we are the Faded
--    if Client then
--       local player = Client.GetLocalPlayer()

--       if (player:isa("Alien") and player:GetDarkVisionEnabled()
--       and self:isa("Marine")) then
--      self:_CreateParasiteEffect()
--       end
--    end
-- end

-- function ParasiteMixin:OnUpdate(deltaTime)
--     SharedUpdate(self)
-- end

-- function ParasiteMixin:OnProcessMove(input)
--     SharedUpdate(self)
-- end

-- if (Client) then
--     local parasiteMixinCreateParasiteEffect = ParasiteMixin._CreateParasiteEffect
--     function ParasiteMixin:_CreateParasiteEffect()
--         local player = Client.GetLocalPlayer()

--         if (player:isa("Alien") and player:GetDarkVisionEnabled()) then
--             parasiteMixinCreateParasiteEffect(self)
--         end
--     end
-- end


if (Client) then
    function HiveVisionMixin:OnUpdate(deltaTime)
        local player = Client.GetLocalPlayer()
        local model = self:GetRenderModel()

        if model ~= nil then
       if (((player:isa("Alien") and player:GetDarkVisionEnabled())
           or player:isa("Marine") and player:GetTeamNumber() == 2)
           and self:isa("Marine")) then -- Only parasite marine, not mines
          HiveVision_AddModel(model)
       else
          HiveVision_RemoveModel(model)
       end
        end
    end
end
