// ===================== Faded Mod =====================
//
// lua\FadedSpawnHallucination.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

local phaseGateOnInitialized = PhaseGate.OnInitialized
function PhaseGate:OnInitialized()
   phaseGateOnInitialized(self)
   if (self.SetPowerOn) then
      self:SetPowerOn()
   end
end

function PhaseGate:GetRequiresPower()
   if (self.SetPowerOn) then
      self:SetPowerOn()
   end
   return false
end

----
-- We want fake marine to be able to take the phase gate
local function SharedUpdate(self)

   if self:GetCanPhase() then

      local ents = nil
      if (kFadedAllowFakeMarineToTakePG == 1) then
     ents = GetEntitiesWithinRange("PhaseGate", self:GetOrigin(), 0.5)
      else
     ents = GetEntitiesForTeamWithinRange("PhaseGate", self:GetTeamNumber(), self:GetOrigin(), 0.5)
      end
      for _, phaseGate in ipairs(ents) do

     if phaseGate:GetIsDeployed() and GetIsUnitActive(phaseGate) and phaseGate:Phase(self) then

        self.timeOfLastPhase = Shared.GetTime()

                if Client then
           self.timeOfLastPhaseClient = Shared.GetTime()
           local viewAngles = self:GetViewAngles()
           Client.SetYaw(viewAngles.yaw)
           Client.SetPitch(viewAngles.pitch)
                end
                /*
                if HasMixin(self, "Controller") then
                   self:SetIgnorePlayerCollisions(1.5)
                end
                */
                break

     end

      end

   end

end


function PhaseGateUserMixin:OnProcessMove(input)
   SharedUpdate(self)
end

// for non players
if Server then

   function PhaseGateUserMixin:OnUpdate(deltaTime)
      SharedUpdate(self)
   end

end
