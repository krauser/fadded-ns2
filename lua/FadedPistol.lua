// ======= Copyright (c) 2003-2011, Unknown Worlds Entertainment, Inc. All rights reserved. =======
//
// lua\Weapons\Pistol.lua
//
//    Created by:   Charlie Cleveland (charlie@unknownworlds.com) and
   //                  Max McGuire (max@unknownworlds.com)
//
// ========= For more information, visit us at http://www.unknownworlds.com =====================

Script.Load("lua/Weapons/Marine/Pistol.lua")

-- kPistolDamage = 5

local networkVars =
{
      lastAltAttack = "time",
}

-- Script.Load("lua/PickupableWeaponMixin.lua")
-- Script.Load("lua/LaserMixin.lua")

-- class 'LaserPistol' (Pistol)

local pistolOnInitialized = Pistol.OnInitialized
function Pistol:OnInitialized()
   -- Print("Laser Pistol init called")
   pistolOnInitialized(self)

   self.lastAltAttack = Shared.GetTime()
   -- if Client then
   --    InitMixin(self, LaserMixin)
   -- end
end

-- function Pistol:GetIsLaserActive()
--    return self.altMode and self:GetIsActive()
-- end

-- function Pistol:GetIsLaserActive()
--    return self:GetIsActive()
-- end

-- function Pistol:GetIsLaserActive()
--     return true
-- end

-- function Pistol:OverrideLaserLength()
--         return 100
-- end

-- function Pistol:GetLaserMaxLength()
--     return 100
-- end

-- function Pistol:GetLaserWidth()
--     return 0.15
-- end
------------

-- function Pistol:SetEndPoint()

--    local coords = self:GetLaserAttachCoords()
--    local maxLength = self:GetLaserMaxLength()

--    local trace = Shared.TraceRay(coords.origin, coords.origin + coords.zAxis * maxLength,
--                  CollisionRep.Default, PhysicsMask.Bullets, EntityFilterAll())
--     local length = math.abs( (trace.endPoint - coords.origin):GetLength() )

--     if length < 1 then
--         // start a bit away from the mine
--         local newStart = coords.origin
--         newStart.z = newStart.z + 0.3

--         trace = Shared.TraceRay(newStart, coords.origin + coords.zAxis * maxLength, CollisionRep.Default, PhysicsMask.Bullets, EntityFilterAll())
--         length = math.abs( (trace.endPoint - coords.origin):GetLength() )
--     end

--     self.endPoint = trace.endPoint
--     self.length = length
-- end

-- function Pistol:InitializeLaser()

--    self:SetEndPoint()

--     if not self.dynamicMesh1 then
--        self.dynamicMesh1 = DynamicMesh_Create()
--        self.dynamicMesh1:SetMaterial(LaserMixin.kLaserMaterial)
--     end

--     if not self.dynamicMesh2 then
--        self.dynamicMesh2 = DynamicMesh_Create()
--        self.dynamicMesh2:SetMaterial(LaserMixin.kLaserMaterial)
--     end

--     local coords = self:GetLaserAttachCoords()
--     local width = self:GetLaserWidth()

--     local coordsLeft = Coords.GetIdentity()
--     coordsLeft.origin = coords.origin
--     coordsLeft.zAxis = coords.zAxis
--     coordsLeft.yAxis = coords.xAxis
--     coordsLeft.xAxis = -coords.yAxis

--     local coordsRight = Coords.GetIdentity()
--     coordsRight.origin = coords.origin
--     coordsRight.zAxis = coords.zAxis
--     coordsRight.yAxis = -coords.xAxis
--     coordsRight.xAxis = coords.yAxis

--     local startColor = Color(1, 0, 0, 0.7)
--     local endColor = Color(1, 0, 0, 0.07)

--     DynamicMesh_SetLine(self.dynamicMesh1, coordsLeft, width, self.length, startColor, endColor)
--     DynamicMesh_SetLine(self.dynamicMesh2, coordsRight, width, self.length, startColor, endColor)

-- end

-- function Pistol:UninitializeLaser()

--     if self.dynamicMesh1 then
--        DynamicMesh_Destroy(self.dynamicMesh1)
--     end

--     if self.dynamicMesh2 then
--        DynamicMesh_Destroy(self.dynamicMesh2)
--     end

--     if self.laserLight then
--        Client.DestroyRenderLight(self.laserLight)
--     end
-- end

-- local function cricketInfernalAttack(player)

--    local player = player:GiveDualRailgunExo(player:GetOrigin())
--    Print("Shooting with the railgun")
--    player:OnPrimaryAttack(player)
--    player:Replace(Marine.kMapName)
-- end

-- Hook to use our own "ShootGrenade" function with the hook
-- local pistolFirePrimary = Pistol.FirePrimary
-- function Pistol:FirePrimary(player)
--    pistolFirePrimary(self, player)
-- end

function SpecialWeaponHandling(self, player, createEntityFct, range)
   local    created_ent_nb = 0
   local    first_ent = nil

   if Server and player then
      -- Shared:FadedMessage("Pistol:OnPrimaryAttack")
      local eyePos  = player:GetEyePos()
      local startPoint = Vector(eyePos)
      local fireDirection = player:GetViewCoords().zAxis
      local kConeWidth = 0.1
      local filterEnts = {self, player}
      for i = 1, 1 do
         -- Shared:FadedMessage("Pistol:OnPrimaryAttack in loop")
         local extents = Vector(kConeWidth, kConeWidth, kConeWidth)
         local trace = TraceMeleeBox(self, startPoint, fireDirection, extents, range, PhysicsMask.Flame, EntityFilterList(filterEnts))
         local lineTrace = Shared.TraceRay(startPoint,
                                           startPoint + range * fireDirection,
                                           CollisionRep.Damage,
                                           PhysicsMask.Flame, EntityFilterOne(player))

         if lineTrace.fraction < 0.8 then
            -- Shared:FadedMessage("Pistol:OnPrimaryAttack line trace OK")
            fireDirection = fireDirection + trace.normal * 0.55
            fireDirection:Normalize()

            if Server then
               -- Shared:FadedMessage("==> Pistol:OnPrimaryAttack FLAME OK")
               -- CreateMine(self, player, lineTrace.endPoint, lineTrace.normal, fireDirection)
               local ent = createEntityFct(self, player,
                                           lineTrace.endPoint,
                                           lineTrace.normal, fireDirection,
                                           trace.entity)
               startPoint = trace.endPoint
               if (ent) then
                  if (first_ent == nil) then
                     first_ent = ent
                  end
                  created_ent_nb = created_ent_nb + 1
               end
            end

         end

      end -- endfor
   end
   return created_ent_nb, first_ent
end

local function callbackDetonation(self)
   local params = { surface = "organic" }
   local y_extents = 1
   if (self.GetExtents) then
      y_extents = self:GetExtents().y
   end
   params[kEffectHostCoords] = Coords.GetLookIn( self:GetOrigin() + y_extents / 2,
                                                 self:GetCoords().zAxis)

   if (self.ExploCountDown > 0) then

      self:TriggerEffects("clusterfragment_residue", params)
      self.ExploCountDown = self.ExploCountDown - 0.5
      return true
   else
      if (self.doExploDamages == true) then
         self.doExploDamages = false
         local direct_damages = kFadedMedicPistolDmg
         local hitRadius = kFadedMedicPistolBlastRadius
         local hitEntities = GetEntitiesWithMixinWithinRange("Live", self:GetOrigin(),
                                                             hitRadius)

         local orig = self:GetOrigin()
         local attacker = Shared.GetEntity(self.glueFragmentPlayerId)
         for _, ent in ipairs(hitEntities) do
            local dmg_percent = ((hitRadius - orig:GetDistanceTo(ent:GetOrigin())) / hitRadius)
            local dmg = direct_damages * dmg_percent

            if (ent:GetTeamNumber() == 1) then
               dmg = dmg * kFriendlyFireScalar
            end
            if (dmg > 0) then
               ent:DeductHealth(dmg, attacker)
            end
         end
         -- self:DeductHealth(direct_damages, attacker)

         self:TriggerEffects("cluster_fragment_explode")
         -- GlueFragmentDetonation(self)
         -- player:DeductHealth(input.time * kFadedBlinkHpCost, player, self)
      end

   end
   return false
end

function _CreateClusterFragment(self, player, position, normal, direction, hitObject, explo_delay, do_damage)

   if (hitObject and hitObject:isa("Entity")) then
      local delay = kFadedMedicPistolDetonationDelay
      if (explo_delay ~= nil) then
         delay = explo_delay
      end
      hitObject.ExploCountDown = 10 * delay
      hitObject.glueFragmentPlayerId = player:GetId()
      hitObject.doExploDamages = true
      if (do_damage ~= nil) then
         hitObject.doExploDamages = do_damage
      end

      Entity.AddTimedCallback(hitObject, callbackDetonation, 0.05)
   end

end

function _CreateMine(self, player, position, normal, direction)

      local mine = CreateEntity(Mine.kMapName, position, player:GetTeamNumber())
      mine:SetOwner(player)

      local coords = Coords.GetTranslation(position)
      coords.yAxis = normal
      coords.zAxis = direction

      coords.xAxis = coords.yAxis:CrossProduct(coords.zAxis)
      coords.xAxis:Normalize()

      coords.zAxis = coords.xAxis:CrossProduct(coords.yAxis)
      coords.zAxis:Normalize()

      mine:SetCoords(coords)
      return (true)
      -- mine:Kill()
end

-- local Shared_GetModel                           = Shared.GetModel
-- local Shared_GetAnimationGraph                  = Shared.GetAnimationGraph
-- local Shared_GetTime                            = Shared.GetTime

-- local function CalculateBoneVelocities(self, velocity)

--    local model = Shared_GetModel(self.modelIndex)
--    local graph = Shared_GetAnimationGraph(self.animationGraphIndex)
--     local velocities = nil

--     if model ~= nil and graph ~= nil then

--         local deltaTime = 1 / 10
--         local time = Shared_GetTime()
--         local prevTime = time - deltaTime

--         local state = self.animationState
--         local poseParams = self.poseParams
--         local previousBoneCoords = CoordsArray()

--         state:Update(graph, model, poseParams, time, prevTime, { })
--         state:GetBoneCoords(model, poseParams, previousBoneCoords)
--         state:Update(graph, model, poseParams, prevTime, time, { })

--         -- Make sure the previous bone coords are correct. In the case where an entity
--         -- is created initially as a dynamic object, the bone coords will not have been
--         -- updated.
--         if self.boneCoords:GetSize() ~= previousBoneCoords:GetSize() then
--            self.animationState:GetBoneCoords(model, self.poseParams, self.boneCoords)
--         end

--         velocities = VelocityArray()
--         Shared.CalculateBoneVelocities(self:GetCoords(), velocity, previousBoneCoords, self.boneCoords, deltaTime, velocities)

--     end

--     return velocities

-- end

function KillHim(player)
   -- player:Kill()
end

function _GetCoord(position, normal, direction, meter)
   local coords = Coords.GetTranslation(position + direction * meter)
   coords.yAxis = normal
   coords.zAxis = direction

   coords.xAxis = coords.yAxis:CrossProduct(coords.zAxis)
   coords.xAxis:Normalize()

   coords.zAxis = coords.xAxis:CrossProduct(coords.yAxis)
   coords.zAxis:Normalize()
   return (coords)
end

function _CreateRagdoll(self, player, position, normal, direction)

   local ragdoll = nil
   local c = CreateEntity(SentryBattery.kMapName, Vector(-100, -100, -100), 2)
   -- local c = CreateEntity(Clog.kMapName, Vector(-100, -100, -100), 2)
   if (c) then
      local coords = _GetCoord(position, normal, direction, 0.5)
      -- local coords_clog = _GetCoord(position, normal, direction, 0)
      c:SetConstructionComplete()

      -- ragdoll:SetParent(c)
      c:SetCoords(coords)
      ragdoll = CreateRagdoll(player)
      ragdoll:SetCoords(coords)
      -- clog:SetCoords(coords_clog)
      -- ragdoll:SetOrigin(c:GetOrigin())
      -- ragdoll:SetAttachPoint(
   end
   return (ragdoll)
   -- if (ragdoll.physicsModel) then
   --    ragdoll.physicsModel:SetCollisionEnabled(true)
      -- ragdoll.physicsModel:SetBoneVelocities(CalculateBoneVelocities(ragdoll, Vector(15, 15, 15)))
   -- end
end

-- Allow alternative fire
function Pistol:GetHasSecondary(player)
   -- if (player and (player.kFadedReserved or kFadedPistolAltCheatEnable)) then
   --    return true
   -- else
   --    return false
   -- end
   return (true)
end

function Pistol:GetIsLaserActive(player)
   return (true)
end

local flareAttackRate = 0.5
function Pistol:OnSecondaryAttack(player)
   ClipWeapon.OnSecondaryAttack(self, player)
   if (player and (player.kFadedReserved or kFadedPistolAltCheatEnable)) then
      -- SpecialWeaponHandling(self, player, _CreateClusterFragment, 50)
      SpecialWeaponHandling(self, player, _CreateMine, 50)
   elseif (self.secondaryAttacking) then
      if (kFadedFlareEnable
          and self.lastAltAttack + flareAttackRate <= Shared.GetTime()) then
         if (self:GetClip() >= kFadedFlareAmmoCost) then
            self.lastAltAttack = Shared.GetTime()
            fire_flare_grenade(player)
            self:SetClip(self:GetClip() - kFadedFlareAmmoCost)
            self:TriggerEffects("grenadelauncher_attack")
         end
      end
   end
end

function Pistol:GetIsLaserActive()
   return true
end

local pistolGetBulletDamage = Pistol.GetBulletDamage
function Pistol:GetBulletDamage(target, endPoint)
   local dmg = pistolGetBulletDamage(self, target, endPoint)
   if (self:GetParent() and self:GetParent():IsMedic() == true) then
      -- Shared:FadedMessage("Damages changed")
      dmg = 5
   end
   return (dmg)
end

local firePrimary = Pistol.FirePrimary
function Pistol:FirePrimary(player)
   firePrimary(self, player)
   -- SpecialWeaponHandling(self, player, _CreateRagdoll, 60)
   -- _CreateRagdoll(self, player, player:GetOrigin() + Vector(2, 2, 2),
   --                0, Vector(10, 10, 10))

   if (player and player:IsMedic() == true) then
      SpecialWeaponHandling(self, player, _CreateClusterFragment, 50)
   end

   if (player and (player.kFadedReserved or kFadedPistolAltCheatEnable)) then
      SpecialWeaponHandling(self, player, _CreateRagdoll, 50)
   end
end

local onPrimaryAttack = Pistol.OnPrimaryAttack
function Pistol:OnPrimaryAttack(player)
   -- if (player and (player.kFadedReserved or kFadedPistolAltCheatEnable)) then
   --    SpecialWeaponHandling(self, player, _CreateRagdoll, 50)
   --    self.primaryAttacking = true
   --    self.shooting = false
   -- else
   onPrimaryAttack(self, player)
   -- end
end

Shared.LinkClassToMap("Pistol", Pistol.kMapName, networkVars)
