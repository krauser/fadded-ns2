// ===================== Faded Mod =====================
//
// lua\FadedPlayer_Server.lua
//
//    Created by: Rio (rio@myrio.de)
//
// =====================================================

local locale = LibCache:GetLibrary("LibLocales-1.0")
local strformat = string.format

function Player:Reset()
    ScriptActor.Reset(self)
end


-- Return a valid spawn point when the fade become marine again
function GetValidFadeSpawnPlaceAround(origin)
   -- Choose something big to be sure
   local newAlienExtents = LookupTechData(kTechId.Fade, kTechDataMaxExtents)
   local capsuleHeight, capsuleRadius = GetTraceCapsuleFromExtents(newAlienExtents)
   --function GetRandomSpawnForCapsule(capsuleHeight, capsuleRadius, origin, minRange, maxRange, filter, validationFunc)
   if (GetHasRoomForCapsule(newAlienExtents, origin, CollisionRep.Default,
               PhysicsMask.AllButPCsAndRagdolls)) then
      return (origin)
   else
      local new_origin = GetRandomSpawnForCapsule(newAlienExtents.y,
                          capsuleRadius,
                          origin, 0, 1.5)
      return (new_origin)
   end
end

local function performFadeStabAttack(player)
   if (player and player:isa("Fade")) then
      -- player:GiveItem(StabBlink.kMapName, true)
      local activeWeapon = player:GetActiveWeapon()
      -- Shared.PlaySound(player,
      --                "sound/NS2.fev/alien/fade/swipe_both",
      --                1)
      -- Shared.PlaySound(player,
      --                "sound/NS2.fev/alien/fade/stab_marine",
      --                1)
      -- player:PrimaryAttack()
      -- if (Server) then
     -- Trix to make the Faded attack only once
      --      player:AddTimedCallback(SwipeBlink.PrimaryAttackEnd, 0.8)
      -- end

      -- player:PrimaryAttackEnd()
      -- activeWeapon:OnTag("stab_start")
      -- Server.SendNetworkMessage(
      --      player, "StabMarine",
      --      {
      --      }, true)
   end

   -- player:RemoveWeapon(StabBlink.kMapName)
end

-- Return a valid corpse around the player
local function getCorpseUsed(self)
   local dead_marine = GetEntities("Ragdoll")
   -- Destroy the corpse used
   for _, corpse in ipairs(dead_marine) do
      if (corpse:GetPlayerName() == self.kFadedCorpseUsedName) then
     return (corpse)
      end
     -- newform:SetFakeMarineName(corpse:GetName())

     -- See FadedRagdoll.lua for the destruction
     -- kFadedForceCorpseDestruction[corpse:GetName()] = true
     -- corpse:SetHasBeenUsed()


     -- networkBroadCastName(newform:GetName(), corpse:GetName())
     -- DestroyEntitySafe(corpse)
   end
   return (nil)
   -- if (FadedRevealByFire and attacker) then
   --    newform:SetOnFire(attacker, attacker:GetActiveWeapon())
   -- end
end

-- Init Fake marine name/weapon and remove the corpse
local function initFakeMarineData(fakeMarine, corpse)
   local random_weapon = {Rifle.kMapName,
              -- More chance to have shotgun
              -- There is not the GL because if the fake marine shoot and goes fade before grenade explode, they deal damage
              Shotgun.kMapName,
              Shotgun.kMapName}
              --Flamethrower.kMapName} -- No flame, printed with GUIScoreboard for some reason + faded can hurt himself by accident (and undisguise)
   if (fakeMarine and corpse) then
      fakeMarine:GiveItem(random_weapon[math.random(#random_weapon)])
      if (math.random(0, 100) % 2 == 0) then
     fakeMarine:GiveItem(Welder.kMapName)
      end
      if (math.random(0, 100) % 2 == 0) then
     fakeMarine:PromoteToHunter()
      end
      fakeMarine:SetFakeMarineName(corpse:GetPlayerName())
      corpse:TimeUp()
      -- local Drop = Client.GetOptionString("input/Drop", "Drop")
      -- fakeMarine:FadedMessage(string.format("Press %s to use your Faded back"), Drop)
      -- fakeMarine:FadedMessage("Try to blend in and sneak behind enemy lines.")

      -- corpse:SetHasBeenUsed()
   end
end


-- Server Side transformation of an Alien player
-- * From Fade to Marine (using corpse)
-- * From Marine to Fade (if revealed by flame or attack)
function Player:CheckTransformation()
   if (Server) then
      local FadedRevealByFire = nil
      local attacker = nil
      local newform = nil
      local maxpv, pv = self:GetMaxHealth(), self:GetHealth()
      local maxarmor, armor = self:GetMaxArmor(), self:GetArmor()
      if (self.kFadedTransformation) then
     local origin = GetValidFadeSpawnPlaceAround(self:GetOrigin())
     -- Tranform to a Fake Marine
     if (origin and self:isa("Fade")) then
        newform = self:Replace(Marine.kMapName, self:GetTeamNumber(), false, origin)
        local corpse = getCorpseUsed(self)
        if (corpse) then
           if (newform) then
          -- if (self.kFadedCorpseMessageHelp) then
          --    local msg_drop = StringReformat(
          --     locale:ResolveString("FADED_DROP_CORPSE_HINT"),
          --     { key = GetPrettyInputName("Drop") })
          --     -- locale:ResolveString("FADED_DROP_CORPSE_HINT"))
          --    local msg_stab = StringReformat(
          --     locale:ResolveString("FADED_STAB_HINT"),
          --     { key = GetPrettyInputName("Use") })
          --     -- locale:ResolveString("FADED_STAB_HINT"))

          --    faded:FadedMessage(msg_drop)
          --    faded:FadedMessage(msg_stab)
          -- end

          initFakeMarineData(newform, corpse)
           end
        end
        -- Transform to a Fade back
     elseif (origin and self:isa("Marine")) then
        local isStabTrigger = self.kFadedStabTrigger
        FadedRevealByFire = self.kFadedRevealByFire
        -- attacker = Shared:GetPlayerByName(self.kFadedRevealAttackerName)
        kFadedStabTriggered = isStabTrigger
        kFadedSpawnFadeHallucination = false
        self:DestroyWeapons()
        newform = self:Replace(Fade.kMapName, self:GetTeamNumber(), false, origin)
        if (newform) then
           local energy_left = kFadedTransformationEnergyPenality
           if (isStabTrigger) then
              CreateEntity(StormCloud.kMapName, newform:GetOrigin(), 2)
          performFadeStabAttack(newform)
          energy_left = kFadedTransformationOnStabEnergyPenality
          -- newform.kFadedDeniedSwipeAfterStab = Shared.GetTime()
          -- Server.SendNetworkMessage(newform,
          --         "deniedSwipeAfterStab",
          --         {time = newform.kFadedDeniedSwipeAfterStab}, true)
           end
           newform:SetEnergy(newform:GetMaxEnergy() * energy_left)
           newform.kFadedCorpseMessageHelp = false
        end
        kFadedSpawnFadeHallucination = true

        -- for _, m in ipairs(GetGamerules():GetTeam1():GetPlayer()) do
        --    if (m and m:GetIsAlive()) then
        --       attacker = m
        --    end
        --    break
        -- end
     else
        Print("Not enought space to transform back")
     end
     if (newform) then
        newform:SetMaxHealth(maxpv)
        newform:SetHealth(pv)
        newform:SetMaxArmor(maxarmor)
        newform:SetArmor(armor)
        DestroyEntity(self)
     end
      end
   end
end

-- local playerOnKill = Player.OnKill
-- function Player:OnKill(killer, doer, point, direction)
--    playerOnKill(self, nil, doer, point, direction)
-- end
