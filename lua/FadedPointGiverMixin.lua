// ===================== Faded Mod =====================
//
// lua\FadedPointGiverMixin.lua
//
//    Created by: Rio (rio@myrio.de)
//
// =====================================================

local floor = math.floor

local pointGiverMixinGetPointValue = PointGiverMixin.GetPointValue
function PointGiverMixin:GetPointValue()
   if (self:isa("Skulk")) then
      return (kFadedBabblerOnKillScoreReward)
   elseif (self:isa("Player")) then
      return (0)
   else
      return (pointGiverMixinGetPointValue(self))
   end
end

-- Update kill/death/assist values + remove vanilla scoring system
local pointGiverMixinPreOnKill = PointGiverMixin.PreOnKill
local function addAssistKillDeath(self, attacker, doer, point, direction)
   local old_score = 0
   if (attacker and attacker:isa("Player")) then
      old_score = attacker:GetScore()
   end
   -- Do not change player point (only kill/assist/death)
   pointGiverMixinPreOnKill(self, attacker, doer, point, direction)
   if (attacker and attacker:isa("Player")) then
      attacker.score = old_score
   end
end

-- Give assist Point to marine
-- Add point to the Faded when he kill a marine
function PointGiverMixin:PreOnKill(attacker, doer, point, direction)
   addAssistKillDeath(self, attacker, doer, point, direction)
   -- Give point to the Faded on kill
   if (self:isa("Skulk") and attacker and attacker.AddScore) then
      attacker:AddScore(kFadedBabblerOnKillScoreReward)
   end
   if (self:isa("Marine") and attacker
          and attacker.AddScore
       and (attacker:isa("Fade") or attacker:isa("Skulk"))) then
      local score = 10
      if (kMarineNb > 1) then
         score = math.floor(kFadedModScorePointsForAllMarineKill / (kMarineNb - 1))
      end
      attacker:AddScore(score)
   end

   -- Count Faded kill
   -- Count assist as a Marine
   if (self:isa("Fade")) then
      local damageDoneList = self.damageDoneList or {}

      local damageDone = 0
      for _, damage in pairs(damageDoneList) do
         damageDone = damageDone + damage
      end
      local scorePerDamage = kFadedModScorePointsForFadeKill / damageDone
      for playerName, damage in pairs(damageDoneList) do
         -- Get player entity
         for index, playerEntity in ientitylist(Shared.GetEntitiesWithClassname("Player")) do
            if (playerEntity:GetName() == playerName) then
               local score = math.floor(damage * scorePerDamage)
               if (self.isHallucination) then
                  score = math.random(1, 40)
               end
               playerEntity:AddScore(score)
               -- Fake score for hallucination
               if (self.isHallucination) then
                  playerEntity.score = playerEntity.score - score
               end
               break
            end
         end
      end
   end
end

-- Save damage to add point for Fade kill assist on Marine side
function Fade:OnTakeDamage(damage, attacker, doer, point)
   -- if (self.blinkDance == true) then
   --    local weap = self:GetWeaponInHUDSlot(2)
   --    weap:SetEthereal(self, true, self:GetViewCoords().zAxis)
   --    self:SetActiveWeapon(SwipeBlink.kMapName)
   -- end
   if (attacker ~= nil and attacker:isa("Marine")) then
      local damageDoneList = self.damageDoneList or {}
      local damageDoneAttacker = damageDoneList[attacker:GetName()] or 0

      damageDoneAttacker = damageDoneAttacker + damage

      damageDoneList[attacker:GetName()] = damageDoneAttacker
      self.damageDoneList = damageDoneList
      self:OnTakeDamageMentalHealthBonus(damage, attacker, doer, point)
   end
end


-------------------------------------------------------

-- Hook to increase Veteran damage
-- Self is the weapon
-- Attacker is the entity attacking
-- Target is the entity who receive damage
local damageMixinDoDamage = DamageMixin.DoDamage
function DamageMixin:DoDamage(damage, target, point, direction, surface, altMode, showtracer)

   local attacker = nil
   -- Get the attacker
   if self:GetParent() and self:GetParent():isa("Player") then
      attacker = self:GetParent()
   elseif HasMixin(self, "Owner") and self:GetOwner() and self:GetOwner():isa("Player") then
      attacker = self:GetOwner()
   end

   -- Increase veteran Damage
   if (attacker and attacker:isa("Marine") and attacker.kFadedIsVeteran)
   then
      damage = damage * kFadedVeteranDamageBonusFactor
   end
   -- If the faded is a fake marine or a Faded attack a Faded, do almost no damages
   if (attacker and target and attacker:isa("Player") and target:isa("Player")) then
      if ((attacker:GetTeamNumber() == 2 and attacker:isa("Marine"))
          or (attacker:GetTeamNumber() == 2 and target:GetTeamNumber() == 2)) then
         damage = 0.01 -- so the damages display as a fake marine is "0" and not nothing
      end
   end
   -- Double pulse grenade dmg on owner to avoid GL kamikaze
   -- Print("Target = " .. target:GetName())
   -- Print("Attacker = " .. attacker:GetName())
   if (attacker and target and attacker:isa("Marine") and target:isa("Marine")
          and target:GetName() == attacker:GetName()
       and self:isa("PulseGrenade")) then
      damage = damage * 2;
   end

   -- -- No friendly fire reduction on babbler
   -- if (attacker and target and attacker:isa("Skulk")) then
   --    damage = kBiteDamage
   --    if (Server) then
   --      SendDamageMessage(attacker, self, damage, self:GetOrigin(), damage)
   --    end
   -- end

   damageMixinDoDamage(self, damage, target, point, direction, surface, altMode, showtracer)
end
