-- forsaken mod
Script.Load("lua/Class.lua")
Script.Load("lua/PowerPointLightHandler.lua")
local originalFunction
local originalFunction2
local originalFunction3


-- La couleur �mise par les PowerNodes doivent �tre noir, afin d'avoir une ambiance sombre pour les marines
PowerPoint.kDisabledColor = Color(0.001, 0.001, 0.001)

-- Les aliens voient mieux dans le noir (couleur verte)
PowerPoint.kDisabledColorForAlien = Color(0.0015*2, 0.015*2, 0.0015*2)
-- PowerPoint.kDisabledColorForMarine = Color(0.014, 0.014, 0.014)
PowerPoint.kDisabledColorForMarine = Color(0.0015*2, 0.015*2, 0.0015*2)

-- Les aliens doivent pouvoir voir dans le noir (fonction 1/2)
-- Le changement d'etat de lumiere (de "Aucun" � "Faible") doit etre instantane
-- [NOT UPDATE SAFE]
function NoPowerLightWorker:Run()

    PROFILE("NoPowerLightWorker:Run")

    local player = Client.GetLocalPlayer()
    if player:GetTeamType() == kAlienTeamType then
       PowerPoint.kDisabledColor = PowerPoint.kDisabledColorForAlien
    else
       PowerPoint.kDisabledColor = PowerPoint.kDisabledColorForMarine
    end

    -- --------------------------------------------------------------------
    -- --------------------------------------------------------------------

    local kPowerDownTime = 1
    local kAuxPowerCycleTime = 3
    local kAuxPowerMinCommanderIntensity = 3

    local function SetLight(renderLight, intensity, color)


        if intensity then
            renderLight:SetIntensity(intensity)
        end
        if color then
            renderLight:SetColor(color)
            if renderLight:GetType() == RenderLight.Type_AmbientVolume then
                renderLight:SetDirectionalColor(RenderLight.Direction_Right,    color)
                renderLight:SetDirectionalColor(RenderLight.Direction_Left,     color)
                renderLight:SetDirectionalColor(RenderLight.Direction_Up,       color)
                renderLight:SetDirectionalColor(RenderLight.Direction_Down,     color)
                renderLight:SetDirectionalColor(RenderLight.Direction_Forward,  color)
                renderLight:SetDirectionalColor(RenderLight.Direction_Backward, color)
            end
        end
    end

    local timeOfChange = self.handler.powerPoint:GetTimeOfLightModeChange()
    local time = Shared.GetTime()
    local timePassed = time - timeOfChange

    local startAuxLightTime = kPowerDownTime
    local fullAuxLightTime = startAuxLightTime + kAuxPowerCycleTime
    local startAuxLightFailTime = fullAuxLightTime + PowerPoint.kAuxLightSafeTime
    local totalAuxLightFailTime = startAuxLightFailTime + PowerPoint.kAuxLightDyingTime

    local probeTint

    if timePassed < kPowerDownTime then
        local intensity = math.sin(Clamp(timePassed / kPowerDownTime, 0, 1) * math.pi / 2)
        probeTint = Color(intensity, intensity, intensity, 1)
    elseif timePassed < startAuxLightTime then
        probeTint = Color(0, 0, 0, 1)
    elseif timePassed < fullAuxLightTime then

        -- Fade red in smoothly. t will stay at zero during the individual delay time
        local t = timePassed - startAuxLightTime
        -- angle goes from zero to 90 degres in one kAuxPowerCycleTime
        local angleRad = (t / kAuxPowerCycleTime) * math.pi / 2
        -- and scalar goes 0->1
        local scalar = math.sin(angleRad)

        probeTint = Color(PowerPoint.kDisabledColor.r * scalar,
                          PowerPoint.kDisabledColor.g * scalar,
                          PowerPoint.kDisabledColor.b * scalar,
                          1)

    else
        self.activeProbes = false
    end

    if self.activeProbes then
        for probe,_ in pairs(self.handler.probeTable) do
            probe:SetTint( probeTint )
        end
    end


    for renderLight,_ in pairs(self.activeLights) do

        local randomValue = renderLight.randomValue
        -- aux light starting to come on
        local startAuxLightTime = kPowerDownTime + randomValue * PowerPoint.kMaxAuxLightDelay
        -- ... fully on
        local fullAuxLightTime = startAuxLightTime + kAuxPowerCycleTime
        -- aux lights starts to fade
        local startAuxLightFailTime = fullAuxLightTime + PowerPoint.kAuxLightSafeTime + randomValue * PowerPoint.kAuxLightFailTime
        -- ... and dies completly
        local totalAuxLightFailTime = startAuxLightFailTime + PowerPoint.kAuxLightDyingTime

        local intensity = nil
        local color = nil

        local showCommanderLight = false

        local player = Client.GetLocalPlayer()
        if player and player:isa("Commander") then
            showCommanderLight = true
        end

        if timePassed < kPowerDownTime then

            local scalar = math.sin(Clamp(timePassed / kPowerDownTime, 0, 1) * math.pi / 2)
            scalar = (1 - scalar)
            if showCommanderLight then
                scalar = math.max(kMinCommanderLightIntensityScalar, scalar)
            end
            intensity = renderLight.originalIntensity * (1 - scalar)

        elseif timePassed < startAuxLightTime then

            if showCommanderLight then
                intensity = renderLight.originalIntensity * kMinCommanderLightIntensityScalar
            else
                intensity = 0
            end

        elseif timePassed < fullAuxLightTime then

            -- Fade red in smoothly. t will stay at zero during the individual delay time
            local t = timePassed - startAuxLightTime
            -- angle goes from zero to 90 degres in one kAuxPowerCycleTime
            local angleRad = (t / kAuxPowerCycleTime) * math.pi / 2
            -- and scalar goes 0->1
            local scalar = math.sin(angleRad)

            if showCommanderLight then
                scalar = math.max(kMinCommanderLightIntensityScalar, scalar)
            end

            intensity = scalar * renderLight.originalIntensity

            intensity = intensity * self:CheckFlicker(renderLight,PowerPoint.kAuxFlickerChance, scalar)

            if showCommanderLight then
                color = PowerPoint.kDisabledCommanderColor
            else
                color = PowerPoint.kDisabledColor
            end

        else

            -- Deactivate from initial state
            self.activeLights[renderLight] = nil

            -- in steady state, we shift lights between a constant state and a varying state.
            -- We assign each light to one of several groups, and then randomly start/stop cycling for each group.
            local lightGroupIndex = math.floor(math.random() * NoPowerLightWorker.kNumGroups)
            self.lightGroups[lightGroupIndex].lights[renderLight] = true

        end

        SetLight(renderLight, intensity, color)

    end

    -- handle the light-cycling groups.
    for _,lightGroup in pairs(self.lightGroups) do
        lightGroup:Run(timePassed)
    end

end


-- Les aliens doivent pouvoir voir dans le noir (fonction 2/2)
originalFunction3 = Class_ReplaceMethod( "LightGroup", "RunCycle",
    function(self, time)

        local player = Client.GetLocalPlayer()
        if player:GetTeamType() == kAlienTeamType then
            PowerPoint.kDisabledColor = PowerPoint.kDisabledColorForAlien
        else
            PowerPoint.kDisabledColor = PowerPoint.kDisabledColorForMarine
        end

        originalFunction3(self, time)
    end
)


if (Server) then
   -- Same as ns2 except the death messages removed
   function PowerPoint:OnKill(attacker, doer, point, direction)

      ScriptActor.OnKill(self, attacker, doer, point, direction)

      self:StopDamagedSound()

      self:MarkBlipDirty()

      -- self:PlaySound(kDestroyedSound)
      -- self:PlaySound(kDestroyedPowerDownSound)

      -- self:SetInternalPowerState(PowerPoint.kPowerState.destroyed)

      self:SetLightMode(kLightMode.NoPower)

        // Remove effects such as parasite when destroyed.
        self:ClearGameEffects()

        -- if attacker and attacker:isa("Player") and GetEnemyTeamNumber(self:GetTeamNumber()) == attacker:GetTeamNumber() then
        --    attacker:AddScore(self:GetPointValue())
        -- end

        // Faded- No death message
        -- SendTeamMessage(self:GetTeam(), kTeamMessageTypes.PowerLost, self:GetLocationId())

        // A few seconds later, switch on aux power.
        -- self:AddTimedCallback(PlayAuxSound, 4)
        self.timeOfDestruction = Shared.GetTime()

   end

end

-- local function SetupWithInitialSettings(self)

--    self.startSocketed = true
--    -- self:SetInternalPowerState(PowerPoint.kPowerState.destroyed)
--    self:SetConstructionComplete()
--    self:SetLightMode(kLightMode.NoPower)
--    -- self:SetPoweringState(false)

--    self:Kill()
-- end

-- local powerPointOnInitialized = PowerPoint.OnInitialized
-- function PowerPoint:OnInitialized()
--    powerPointOnInitialized(self)
--    SetupWithInitialSettings(self)
-- end

function PowerPoint:GetPowerState()
   return PowerPoint.kPowerState.destroyed
end

function PowerPoint:GetIsDisabled()
   return true
end

function PowerPoint:GetCanBeWeldedOverride(player)
   return (false)
end

function PowerPoint:OnTakeDamage(damage, attacker, doer, direction, damageType, preventAlert)
end

function PowerPoint:GetSendDeathMessageOverride()
   return false
end
