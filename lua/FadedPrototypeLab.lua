// ===================== Faded Mod =====================
//
// lua\FadedArmsLab.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
//
// ===================== Faded Mod =====================

local locale = LibCache:GetLibrary("LibLocales-1.0")
local strformat = string.format

Script.Load("lua/PrototypeLab.lua")

-- Denie the legacy buy menu
function PrototypeLab:GetCanBeUsed(player, useSuccessTable)
   useSuccessTable.useSuccess = false
end

-- Check for Sentry Battery and power up if one is around
local prototypelabOnUpdate = PrototypeLab.OnUpdate
function PrototypeLab:OnUpdate(deltaTime)
   prototypelabOnUpdate(self, deltaTime)
   local oldPowerStatue = self:GetIsPowered()
   local bState = buildingCheckForBattery(self)

   if (oldPowerStatue == false and bState == true) then
      if (Server) then
         local nb_marine_alive = 0
         for _, marine in ipairs(GetGamerules():GetTeam1():GetPlayers()) do
            if (marine and marine:GetIsAlive()) then
               nb_marine_alive = nb_marine_alive + 1
            end
         end
         for nb_drop = 1, nb_marine_alive do
            local max_range = 5
            local extents = GetExtents(kTechId.Armory)
            local entity = nil

            -- Team.RespawnPlayer(player:GetTeam(), player, orig, Angles(0, 0, 0)
            local spawnPoint = GetRandomBuildPosition(kTechId.Exo,
                                                      self:GetOrigin(),
                                                      max_range)
            if (spawnPoint) then
               if (nb_drop % 4 == 1) then
                  entity = CreateEntity(Exosuit.kMapName, spawnPoint + Vector(0, -0.6, 0), 1)
               else
                  CreateEntity(Jetpack.kMapName, spawnPoint, 1)
               end
            end
         end
      end -- end Server
      -- See FadedUpdateServer.lua for the "detection" code
   end -- endif
end -- endfct

function PrototypeLab:OnTakeDamage(damage, attacker, doer, point, direction, damageType, preventAlert)
   BuildingOnTakeDamage(self, damage, attacker, doer, point, direction, damageType, preventAlert)
end
