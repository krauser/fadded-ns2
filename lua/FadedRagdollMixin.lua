--[[
   ===================== Faded Mod =====================

   lua\FadedRagdollMixin.lua

   Created by: JB (jeanbaptiste.laurent.pro@gmail.com)

   Faded note: I create by hand a Ragdoll entity and link
   to the player name for FakeMarine feature

   =====================================================
--]]

-- Script.Load("lua/InfestationMixin.lua")

-- local function SetRagdoll(self, deathTime)

--     if Server then

--        if self:GetPhysicsGroup() ~= PhysicsGroup.RagdollGroup then

--       self:SetPhysicsType(PhysicsType.Dynamic)

--       self:SetPhysicsGroup(PhysicsGroup.RagdollGroup)

--       -- Apply landing blow death impulse to ragdoll (but only if we didn't play death animation).
--             if self.deathImpulse and self.deathPoint and self:GetPhysicsModel() and self:GetPhysicsType() == PhysicsType.Dynamic then

--                 self:GetPhysicsModel():AddImpulse(self.deathPoint, self.deathImpulse)
--                 self.deathImpulse = nil
--                 self.deathPoint = nil
--                 self.doerClassName = nil

--             end

--             if deathTime then
--                 self.timeToDestroy = deathTime
--             end

--         end

--     end
-- end

if Server then
   local ragdollMixinOnTag = RagdollMixin.OnTag
   function RagdollMixin:OnTag(tagName)
      if (self:isa("Player")) then
         PROFILE("RagdollMixin:OnTag")
         if not self.GetHasClientModel or not self:GetHasClientModel() then
            if tagName == "death_end" then
               if self.bypassRagdoll then
                  self:SetModel(nil)
               else
                  local ragdoll = CreateRagdoll(self)
                  if (ragdoll) then
                     self:SetModel(nil)
                  end
                  -- DestroyEntitySafe(self)
                  -- SetRagdoll(self, kRagdollTime)
               end
            elseif tagName == "destroy" then
               DestroyEntitySafe(self)
            end
            -- else
            --    ragdollMixinOnTag(self, tagName)
         end
      else
         ragdollMixinOnTag(self, tagName)
      end --endif player
      return true
   end -- fct
end -- Server
