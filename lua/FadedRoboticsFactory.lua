// ===================== Faded Mod =====================
//
// lua\FadedRoboticsFactory.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
//
// ===================== Faded Mod =====================

Script.Load("lua/RoboticsFactory.lua")

-- Check for Sentry Battery and power up if one is around
local roboticsFactoryOnUpdate = RoboticsFactory.OnUpdate
function RoboticsFactory:OnUpdate(deltaTime)
   roboticsFactoryOnUpdate(self, deltaTime)
   local oldPowerStatue = self:GetIsPowered()
   local bState = buildingCheckForBattery(self)

   -- See FadedMapBlip.lua for minimap effect
   if (oldPowerStatue == false and bState == true and kFadedRoboticsFactoryGiveMac)
   then
      local allmarines = GetGamerules():GetTeam(kTeam1Index):GetPlayers()
      for _, marine in ipairs(allmarines) do
     if (marine and marine:GetIsAlive()) then
        local max_range = 7
        local extents = GetExtents(kTechId.MAC)
        local entity = nil

        -- Team.RespawnPlayer(player:GetTeam(), player, orig, Angles(0, 0, 0)
        local spawnPoint = GetRandomBuildPosition(kTechId.MAC,
                              self:GetOrigin(),
                              max_range)
        if spawnPoint then
           entity = CreateEntity(MAC.kMapName, spawnPoint, 1)
           if (entity) then
          entity:GiveOrder(kTechId.FollowAndWeld, marine:GetId(),
                   marine:GetOrigin(), nil, false, false)
           end
        end
     end
      end
   end
end

function RoboticsFactory:OnTakeDamage(damage, attacker, doer, point, direction, damageType, preventAlert)
   BuildingOnTakeDamage(self, damage, attacker, doer, point, direction, damageType, preventAlert)
end
