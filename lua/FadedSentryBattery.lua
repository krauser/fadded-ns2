// ===================== Faded Mod =====================
//
// lua\FadedGamerules.lua
//
//    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

Script.Load("lua/Armory.lua")
Script.Load("lua/ArmsLab.lua")
Script.Load("lua/Observatory.lua")
Script.Load("lua/PrototypeLab.lua")
Script.Load("lua/Entity.lua")
Script.Load("lua/CloakableMixin.lua")

Script.Load("lua/SentryBattery.lua")
Script.Load("lua/LaySentryBattery.lua")

local locale = LibCache:GetLibrary("LibLocales-1.0")
local strformat = string.format
local tinsert = table.insert

-- -- Already in script actor
-- Script.Load("lua/UsableMixin.lua")

local networkVars =
   {
      kIsAttachedToBuilding = "boolean",
      kIsAttachedTo = "string",
      -- modelsize = "float (0 to 10 by .1)",
   }

AddMixinNetworkVars(CloakableMixin, networkVars)

-- local sentryBatteryOnCreate = SentryBattery.OnCreate
function SentryBattery:OnCreate()

   ScriptActor.OnCreate(self)

   InitMixin(self, BaseModelMixin)
   InitMixin(self, ClientModelMixin)
   InitMixin(self, LiveMixin)
   InitMixin(self, GameEffectsMixin)
   InitMixin(self, FlinchMixin)
   InitMixin(self, TeamMixin)
   InitMixin(self, PointGiverMixin)
   InitMixin(self, SelectableMixin)
   InitMixin(self, EntityChangeMixin)
   InitMixin(self, LOSMixin)
   -- InitMixin(self, CorrodeMixin)
   InitMixin(self, ConstructMixin)
   InitMixin(self, ResearchMixin)
   InitMixin(self, RecycleMixin)
   InitMixin(self, CombatMixin)
   InitMixin(self, ObstacleMixin)
   InitMixin(self, DissolveMixin)
   InitMixin(self, GhostStructureMixin)
   InitMixin(self, VortexAbleMixin)
   InitMixin(self, PowerConsumerMixin)
   InitMixin(self, ParasiteMixin)

   if Client then
      InitMixin(self, CommanderGlowMixin)
   end

   self:SetLagCompensated(false)
   self:SetPhysicsType(PhysicsType.Kinematic)
   self:SetPhysicsGroup(PhysicsGroup.BigStructuresGroup)
   -----------
   -- sentryBatteryOnCreate(self)
   ------------
   self.kIsAttachedToBuilding = false
   self.kIsAttachedTo = ""

   if Client then
      self.light = Client.CreateRenderLight()
      self.light:SetType( RenderLight.Type_Point )
      self.light:SetColor( kFadedBatteryLightColor )
      self.light:SetIntensity( kFadedBatteryLightIntensity )
      self.light:SetRadius( kFadedBatteryLightRadius )

      self.light:SetAtmosphericDensity(3)
      self.light:SetCastsShadows(true)
      self.light:SetSpecular(false)

      self.light:SetIsVisible(false)

      self.lightTimer = nil
   end
   InitMixin(self, CloakableMixin)

   if (Server) then -- Has to be greater than ragdoll relevancy
      self:SetRelevancyDistance(kMaxRelevancyDistance)
   end
   -- self.modelsize = 1
   -- self:SetPropagate(Entity.Propagate_Callback)
end


-- function Cyst:OnAdjustModelCoords(modelCoords)
--    local scale = self.modelsize
--    local coords = modelCoords
--    coords.xAxis = coords.xAxis * 3
--    coords.yAxis = coords.yAxis * 3
--    coords.zAxis = coords.zAxis * 3
--    return coords
-- end

-- -- if (Client) then
--    function SentryBattery:OnAdjustModelCoords(modelCoords)
--       local scale = self.modelsize
--       local coords = modelCoords
--       if (self:GetTeamNumber() == 2) then
--          coords.xAxis = coords.xAxis * 0.1
--          coords.yAxis = coords.yAxis * 0.1
--          coords.zAxis = coords.zAxis * 0.1
--       end
--       return coords
--    end
-- -- end


-- -- Not working
-- function SentryBattery:GetExtentsOverride()
--    return Vector(10, 10, 10)
-- end

-- function SentryBattery:OnGetIsRelevant(player)
--    if (self:GetTeamNumber() == 2) then
--       return (false)
--    end
--    return (true)
-- end

-- function SentryBatteryCompareBuildingProxymity(ent1, ent2)
--    local sb = GetEntitiesWithinRange("SentryBattery", SentryBattery.kRange*2)
--    return (ent1:GetOrigin():GetDistance(sb:GetOrigin())
--           < ent2:GetOrigin():GetDistance(sb:GetOrigin()))
-- end


local sentryBatteryOnInitialized = SentryBattery.OnInitialized
function SentryBattery:OnInitialized()
   sentryBatteryOnInitialized(self)
   if (Server) then
      local entity = {}
      for _, str in ipairs({"ArmsLab", "Armory", "AdvancedArmory",
                            "Observatory",
                            "PrototypeLab", "RoboticsFactory"})
      do
         for _, ent in ipairs(GetEntitiesWithinRange(str, self:GetOrigin(),
                                                     SentryBattery.kRange))
         do
            if (not ent:GetIsPowered()) then -- only add offline building
               if (#entity > 0
                      and ent:GetOrigin():GetDistance(self:GetOrigin()) <
                   entity[1]:GetOrigin():GetDistance(self:GetOrigin())) then
                  tinsert(entity, 1, ent)
               else
                  tinsert(entity, ent)
               end
            end
         end
      end

      if (#entity > 0) then
         self.kIsAttachedToBuilding = true
         self.kIsAttachedTo = EntityToString(entity[1])
      end
   end

end

function SentryBattery:OnTouch(recipient)
end

-- Check if the sentry is already attached (can't be moved if attached)
-- It denied marines to use multiple building with only one battery
function SentryBattery:isAttachedToBuilding()
   -- Check at each try if the building has been REALLY powered up
   -- if (self.kIsAttachedToBuilding) then
   --    local attached_building_is_up = false
   --    for _, ent in ipairs(GetEntitiesWithinRange(self:isAttachedToBuildingName(),
   --                           self:GetOrigin(),
   --                           SentryBattery.kRange))
   --    do
   --      if (ent and ent:GetIsPowered()) then
   --         attached_building_is_up = true
   --      end
   --    end
   --    -- If not, just reset
   --    if (attached_building_is_up == false) then
   --      self.kIsAttachedToBuilding = false
   --      self.kIsAttachedTo = ""
   --    end
   -- end
   if (Server) then
      return self.kIsAttachedToBuilding
   else
      return false -- client can always take the battery, check only server side
   end
end

-- Check if the sentry is already attached (can't be moved if attached)
-- It denied marines to use multiple building with only one battery
function SentryBattery:isAttachedToBuildingName()
   return self.kIsAttachedTo
end

-- function SentryBattery:GetIsPermanent()
--    return false
-- end
local last_warning = 0
function SentryBattery:GetCanBeUsed(player, useSuccessTable)
   local ret = (player:isa("Marine")
                   and player:GetTeamNumber() == 1
                   and self:isAttachedToBuilding() == false)
   local ret_powered_msg = (player:isa("Marine")
                               and player:GetTeamNumber() == 1
                               and self:isAttachedToBuilding() == true)
   if (ret) then
      if (Server) then
         local isActive = false
         -- Shared:FadedMessage(strformat(
         --             locale:ResolveString("FADED_MARINE_PICK_BATTERY"),
         --             tostring(player:GetName()),
         --             tostring(player:GetLocationName())))
         player:FadedMessage(locale:ResolveString("FADED_PICK_BATTERY_TO_BASE"))
         player:GiveItem(LaySentryBattery.kMapName, isActive)
         local marine_building = nil
         marine_building = GetEntitiesWithMixinForTeam("PowerConsumer", 1)
         for _, building in ipairs(marine_building) do
            if (building and building:GetIsPowered() == false) then
               player:GiveOrder(kTechId.Move, nil, building:GetOrigin(),
                                nil, true, true)
            end
         end
         DestroyEntity(self)
      end
   elseif (Server and ret_powered_msg and last_warning < Shared.GetTime()) then
      player:FadedMessage("Battery used by the " .. self:isAttachedToBuildingName())
      player:FadedMessage("You can't take it")
      last_warning = Shared.GetTime() + 1
   end
   -- Do not not known what this is for, but other NS2 lua getCanBeUsed() use it, so ...
   useSuccessTable.useSuccess = ret
end

if Server then

   local oldSentryBatteryOnkill = SentryBattery.OnKill
   function SentryBattery:OnKill()
      SpawnSentryBatteryOnMap(GetGamerules(), true, true)
      -- Sentry dies, drop an other one
      oldSentryBatteryOnkill(self)
   end

end

-- Tricky patch alien battery not visible on minimap
local sentrybatteryOnGetMapBlipInfo = SentryBattery.OnGetMapBlipInfo
function SentryBattery:OnGetMapBlipInfo()
   local blipType = 0
   local isAttacked = HasMixin(self, "Combat") and self:GetIsInCombat()
   local isParasited = HasMixin(self, "ParasiteAble") and self:GetIsParasited()

   if (not (self:GetTeamNumber() == 2)) then
      if rawget( kMinimapBlipType, self:GetClassName() ) ~= nil then
         blipType = kMinimapBlipType[self:GetClassName()]
      end
      return true, blipType, 1, isAttacked, isParasited
   end
   return false
end

if Client then
   local sentryBatteryOnUpdateRender = SentryBattery.OnUpdateRender
   function SentryBattery:OnUpdateRender()
      PROFILE("Flare:OnUpdateRender")
      if (sentryBatteryOnUpdateRender) then
         sentryBatteryOnUpdateRender(self)
      end
      if self.light ~= nil then
         if (not (self:GetTeamNumber() == 2)) then
            self.light:SetIsVisible(true)
         end
         self.light:SetCoords(
            Coords.GetLookIn(
               self:GetOrigin() + Vector(0, 1.1, 0),
               self:GetCoords().zAxis))
         self.lightTimer = Shared.GetTime()
      end
   end

   local sentryBatteryOnDestroy = SentryBattery.OnDestroy
   function SentryBattery:OnDestroy()
      if (sentryBatteryOnDestroy) then
         sentryBatteryOnDestroy(self)
      end
      if self.light ~= nil then
         Client.DestroyRenderLight(self.light)
         self.light = nil
      end
   end
end

-- Sentries can only be destroyed if attached
-- function SentryBattery:OnTakeDamage(damage, attacker, doer, point, direction, damageType, preventAlert)
-- if (self.kIsAttachedToBuilding == false) then
--    self:ActivateNanoShield()
-- end
-- end

-- -- Already implemented in scriptActor
-- function SentryBattery:GetUsablePoints(player, useSuccessTable)
--    player:FadedMessage("GetUsablePoints called\n")
-- end


-- function SentryBattery:GetIsValidRecipient(recipient)
--    return recipient:isa("Marine")
-- end

-- function SentryBattery:OnUse(player, elapsedTime, useSuccessTable)
-- No called for some reason
-- if Server then
--    player:FadedMessage("Trying to pick sentry battery\n")
-- end
-- end
