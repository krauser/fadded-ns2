// ===================== Faded Mod =====================
//
// lua\FadedSpawnHallucination.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

-- TODO: Add documentation about ("lua/ConfigFileUtility.lua") what this file is about
Script.Load("lua/ConfigFileUtility.lua")

all_change_string_log = {}

local configFileName = "FadedConfig_0.22.11.json"

local defaultConfig = {
   config_version = kFadedModVersion,
   enable_hint = kFadedHintEnable,
   ambiant_sound = kFadedAmbiantSound,
   faded_selection_chance =
      {
         on_kill_as_marine = kFadedModFadedSelectionChance,
         on_win_as_faded = kFadedModFadedNextSelectionChance,
      },
   round_timer =
      {
         base = kFadedModBaseRoundTimerInSecs,
         max = kFadedMaxRoundTimer,
         bonus_per_marine = kFadedModBonusTimePerMarine,
         on_change_map_waiting_delay = kFadedNextMapWaitingTime,
         on_next_round_waiting_delay = kFadedModTimeTillNewRound,
      },
   spawn_protection =
      {
         enable = kFadedModSpawnProtectionEnabled,
         time = kFadedModSpawnProtectionTime,
      },
   friendly_fire =
      {
         enable = kFadedModFriendlyFireEnabled,
         scalar = kFriendlyFireScalar,
      },
   all_talk_enable = kFadedModAllTalkEnabled,
   weapon_purchase_time_limit = kFadedModTimeInSecondsSelectingEquipmentIsAllowed,
   multi_faded =
      {
         marine_per_faded = kFadedAddFadeMarineScale,
      },
   veteran =
      {
         marine_per_veteran = kFadedVeteranScale,
         fake_marine_detection_radius = kVeteranFakeMarineRangeDetection,
      },
   anti_camp =
      {
         enable = kFadedAntiCampEnable,
         radius = kAntiCampRadius,
         trigger_time = kCampMaxTime,
         max_babblers = kMaxBabblerOnMap,
         num_babbler = kFadedNumBabblersPerEgg,
         egs_build_time = kBabblerEggBuildTime,
      },
   balance =
      {
         faded =
            {
               health = kFadeBaseHealth,
               armor = kFadeBaseArmor,
               bonus_hp_per_marine = kFadeHealthPerMarine,
               bonus_ap_per_marine = kFadeArmorPerMarine,
               energy_bonus_onkill = kFadedAdreBonusOnKill,
               energy_rege_rate = kEnergyUpdateRate,
               eating_health_bonus = kFadedEatingRegen,
               eating_armor_bonus = kFadedEatingRegenArmor,
               eating_revive_time = kFadedEatingTime,
               revive_enable = kFadedRespawnedEnable,
               revive_health_cost = kFadedRespawnedFadeHealthCost,
               revive_faded_health = kFadedRespawnedFadeHealth,
               revive_faded_energy = kFadedRespawnedFadeEnergy,
               respawn_health = kFadedRespawnedFadeHealth,
               respawn_energy = kFadedRespawnedFadeEnergy,
               respawn_health_cost = kFadedRespawnedFadeHealthCost,
               can_disguise_take_pg = kFadedAllowFakeMarineToTakePG,
            },
         veteran =
            {
               health = kFadedVeteranHealth,
               armor = kFadedVeteranArmor,
               speed_bonus = kFadedVeteranSpeedBonus,
               damage_bonus = kFadedVeteranDamageBonusFactor,
            }, -- veteran
         sanity =
            {
               distance_vision_malus =
                  {
                     min_missing_sanity_for_malus = kFadedMinMissingSanityForMalus,
                  },
               growl =
                  {
                     damage = kFadedGrowlMentalHealthDamage,
                     damage_reduction_per_meter = kFadedGrowlDiminutionByMeter,
                  }, -- growl
               swipe =
                  {
                     damage = kFadedSwipMentalHealthDamage,
                  }, -- swipe
               stab =
                  {
                     poison_damage_per_sec = kFadedPoisonSanityDamagePerSec,
                  },
               on_hit_bonus =
                  {
                     shotgun_per_bullet = kFadedShotgunOnHitMentalHealthBonus,
                     pistol = kFadedPistolOnHitMentalHealthBonus,
                     rifle = kFadedRifleOnHitMentalHealthBonus,
                     flamethrower = kFadedFlameOnHitMentalHealthBonus,
                     GL = kFadedGLOnHitMentalHealthBonus,
                     cluster_grenade = kFadedClusterOnHitMentalHealthBonus,
                     nerve_grenade = kFadedGazOnHitMentalHealthBonus,
                     napalm_grenade = kFadedNapalmOnHitMentalHealthBonus,
                  }, -- on_hit_bonus
               marine_death =
                  {
                     damage = kFadedMarineMentalHealthDeathDamage,
                     radius = kFadedMarineDeathRadius,
                  },
               marine_group =
                  {
                     radius = kFadedMarineGroupRadius,
                  },
            }, -- sanity
         weapons =
            {
               babbler_player =
                  {
                     health = kFadedSpectatorBabblerHealth,
                     armor = kFadedSpectatorBabblerArmor,
                     energy_regen_per_sec = kFadedBabblerPlayerEnergyRegen,
                     bite_energy_cost = kBiteEnergyCost,
                     bite_damages = kBiteDamage,
                     jump_energy_cost = kFadedBabblerPlayerJumpCost,
                     spawn_wave_added_time_per_babbler = kFadedDelayOnSpawnWavePerBabbler,
                     spawn_wave_max_delay = kFadedMaxBabblerPlayerSpawnDelay,
                     instant_spawn_with_proto_up = kFadedInstantBabblerSpawnWithProto,
                     walk_speed = kFadedBabblerPlayerWalkSpeed,
                     jump_speed = kFadedBabblerPlayerJumpSpeed,

                     on_kill_point_reward = kFadedBabblerOnKillScoreReward,
                     drop_ammo_chances_percent = kFadedBabblerAmmoPackSpawnChances,
                  },
               alien =
                  {
                     play_sound_if_nearby_a_marine = kFadedSoundIfFadeNearby,
                     camo =
                        {
                           max_meter_marine_can_see_fade = kFadedVisuDistance,
                           max_camo_bonus_percent_when_standing = kFadedMaxCloakBonusPercentSlowMovement,
                           leap_cloak_malus = kFadedLeapCloakMalus,
                           holes_in_skin_starting_at_speed = kFadedMinSpeedForOpacity,
                        },
                     babbler_ability =
                        {
                           enable = kFadedBabblerAbilityEnable,
                           heath_cost = kFadedBabblerAbilityHealthCost,
                           num_babbler = kFadedBabblerAbilityNumBabbler,
                        }, -- babbler_ability
                     bile_bomb =
                        {
                           energy_cost = kBileBombEnergyCost,
                           fire_rate = kBileBombFireRate,
                           velocity = kFadedModBombVelocity,
                        }, -- bile_bomb
                     leap =
                        {
                           energy_cost = kLeapEnergyCost,
                        }, -- leap
                     growl =
                        {
                           energy_cost = kFadedGrowlEnergyCost,
                           rate = kFadedGrowlInterval,
                        }, -- growl
                     swipe =
                        {
                           damage = kSwipeDamage,
                           energy_cost = kSwipeEnergyCost,
                        }, -- swipe
                     blink =
                        {
                           per_sec_cost = kBlinkEnergyCost,
                           on_use_cost = kStartBlinkEnergyCost,
                           per_sec_hp_cost = kFadedBlinkHpCost,
                           enzime_after_blink = kFadedEnzimeAfterBlink,
                        }, -- blink
                     stab =
                        {
                           hit_damage = kFadedStabDamage,
                           denied_time_after_stab = kFadedDeniedSwipeAfterStab,
                           poison_damage_per_sec = kFadedStabPoisonDamage,
                           poison_duration = kPoisonBiteDuration,
                           poison_damage_threshhold = kPoisonDamageThreshhold,
                        }, -- stab
                  },
               marine =
                  {
                     welder =
                        {
                           can_be_drop = kFadedCanDropWelder,
                        }, -- welder
                     hunt_ability =
                        {
                           max_pts_on_path = kFadedHuntMaxPointOnPath,
                           hunt_charge_duration = kFadedHuntTime,
                           path_display_duration = kFadedHuntPathDuration,
                        }, -- hunt_ability
                     flashlight =
                        {
                           ns2_flashlight = kFadedNoCustomFlashlight,
                           maxUsageTime = kFadedLightTime,
                           regePerSec = kFadedLightRegePerSec,
                           fadedDetectionRange = kFadedLightDetectDist,
                        }, -- flashlight
                     dropped_item_stay_time = kItemStayTime,
                     death_notification_radius = kFadedDeathMessageDistance,
                     mine =
                        {
                           damage = kMineDamage,
                           trigger_range = kMineTriggerRange,
                           nb_per_marine = kNumMines,
                           restriction_range = kFadedModMinesRestrictionRange,
                        }, -- mine
                     flamethrower =
                        {
                           clip = kFlamethrowerClipSize,
                           direct_hit_damage = kFlamethrowerDamage,
                           burn_damage = kBurnDamagePerSecond,
                           burn_duration = kFlamethrowerBurnDuration,
                           energy_damage = kFlameThrowerEnergyDamage,
                        }, -- flamethrower
                     rifle =
                        {
                           clip = kRifleClipSize,
                           damage = kRifleDamage,
                        }, -- rifle
                     heavy_shotgun =
                        {
                           mysterious_option = kFadedHSGCheatEnable,
                           damage = kFadedHeavyShotgunDamage,
                           knock_back = kFadedHeavyShotgunKnockBack,
                           pellet_per_shot = kFadedHeavyShotgunPelletPerShot,
                           fire_rate = kFadedHeavyShotgunFireRate,
                           extra_pellets_on_double_shot = kFadedHeavyShotgunDoubleShotPelletRatio,
                        }, -- heavy_shotgun
                     hand_grenade =
                        {
                           allow_wall_stick = kFadedGrenadeSecondary,
                           grenades_nb = kMaxHandGrenades,
                           pulse =
                              {
                                 radius = kPulseGrenadeDamageRadius,
                                 damage = kPulseGrenadeDamage,
                                 energy_damage = kPulseGrenadeEnergyDamage,
                              }, -- pulse
                           napalm =
                              {
                                 amount = kFadedMaxNapalmGrenades,
                                 cloud_dps = kFadedNapalmDamagePerSecond,
                                 cloud_lifetime = kFadedNapalmCloudLifetime,
                                 cloud_radius = kFadedNapalmCloudRadius,
                                 -- cloud_burn_radius = kFadedBurnRadius,
                                 cloud_min_delay_to_put_in_fire = kFadedNapalmPutInFireDelay,
                                 cloud_dps_multiplyer_on_marine = kFadedOnMarineDamageMultiplyer,
                              }, -- napalm
                           heal =
                              {
                                 amount = kFadedMaxHealGrenades,
                                 refresh_rate = kFadedHealCloudRefreshRate,
                                 hp_per_refresh = kFadedHealCloudHpPerRefresh,
                                 medic_GL_hp_per_refresh = kFadedGLHealCloudHpPerRefresh,
                              }, -- heal
                        },
                     pistol =
                        {
                           mysterious_option = kFadedPistolAltCheatEnable,
                           enable_flare = kFadedFlareEnable,
                           flare_ammo_cost = kFadedFlareAmmoCost,
                           flare_lifetime = kFlareLifeTime,
                           flare_light_intensity = kFadedFlareIntensity,
                           flare_light_radius = kFadedFlareRadius,
                           medic =
                              {
                                 bullet_blast_max_dmg = kFadedMedicPistolDmg,
                                 bullet_blast_radius = kFadedMedicPistolBlastRadius,
                                 detonation_delay = kFadedMedicPistolDetonationDelay,
                              },
                        },
                     GL =
                        {
                           mysterious_option = kFadedGLCheatEnable,
                           has_secondary = kFadedGLSecondary,
                           explosive =
                              {
                                 radius = kGrenadeLauncherGrenadeDamageRadius,
                              },
                        }, -- GL
                     jetpack =
                        {
                           fuel_regen = kJetpackUseFuelRate,
                        }, -- jetpack
                     exo =
                        {
                           speed_bonus = kFadedExoSpeedBonusFactor,
                        }, -- exp
                  }, -- Marine
            }, -- weapons
         corpse =
            {
               use_eat_burn_radius = kFadedEatUseDistance,
               infestation_radius = kFadedDeadBodyInfestationRadius,
               spores_rate = kFadedDeadBodyToxicGazChance,
               spores_damage = kSporesDustDamagePerSecond,
               spores_radius = kSporesDustCloudRadius,
               spores_lifetime = kSporesDustCloudLifetime,
            }, -- corpse
         hallucination =
            {
               lifetime = kHallucinationLifeTime,
               health = kHallucinationMaxHealth,
               spawn_chance_in_stress = kFadedStressLvlHallucinationChance,
               spawn_chance_in_panic = kFadedPanicLvlHallucinationChance,
               spawn_chance_in_heavypanic = kFadedHeavyPanicLvlHallucinationChance,
            },
         objective_mod =
            {
               disable = kFadedDisableObjectivMod,
               armory =
                  {
                     can_drop_weapon = kFadedArmoryAllowWeapon,
                     can_drop_supplies = kFadedArmoryAllowSupply,
                     nb_ressuply_allowed = kFadedArmoryHealthAmmoSupply,
                     drop_per_marine_alive = kFadedArmorySupplyAmount,
                     can_buy_weapon = kFadedCanMarinesBuyWeapons,
                     weapons_cost =
                        {
                           shotgun = kShotgunCost,
                           cluster_grenade = kClusterGrenadeCost,
                           gas_grenade = kGasGrenadeCost,
                           pulse_grenade = kPulseGrenadeCost,
                           mines = kMineCost,
                           flamethrower = kFlamethrowerCost,
                           grenade_launcher = kGrenadeLauncherCost,
                           welder = kWelderCost,
                        }, -- weapon_cost
                  }, -- armory
               observatory =
                  {
                     enable_minimap = kFadedObsGiveMinimap,
                     enable_damages_display = kFadedObsGiveDamageDisplay,
                     enable_passive_detection = kFadedObsEnablePassiveDetection,
                  },
               robotics_factory =
                  {
                     give_mac = kFadedRoboticsFactoryGiveMac,
                  },
               arms_lab =
                  {
                     give_weap1 = kFadedArmslabGiveWeapons1,
                     give_armor1 = kFadedArmslabGiveArmor1,
                  },

               can_use_primary_weap_with_battery = kFadedCanUsePrimaryWithBattery,
               sentry_battery_weight = kFadedSentryBatteryWeight,
               battery_spawn_delay = kFadedBatterySpawnDelay,
               battery_spawned_at_round_start = kFadedBatterySpawnAtRoundStart,
               battery_spawn_on_building_power_up = kFadedSpawnBatteryOnBuildingPowerUp,
            }, -- objective_mod
      }, -- balance
}

-- Generate a default json file for server configuration
WriteDefaultConfigFile(configFileName, defaultConfig)

local config = LoadConfigFile(configFileName)
if (not config or config.config_version ~= kFadedModVersion) then
   Print("Faded Warning: Configuration file ["
            .. configFileName
            .. "] is outdated, you may remove it and configure it again")
   Print("Faded Warning: Your configuration file will work but won't be able to manage new globals")
   if (config and config.config_version) then
      Print("Faded Warning: Your config version: "
               .. tostring(config.config_version))
   end
   Print("Faded Warning:  New config version: "
            .. defaultConfig.config_version)
   config = defaultConfig
end


-- local function check_blind(global, new_value, isBool)
--    loadstring("if ("..global .. " ~= "..tostring(new_value)..") then\
--                   local str = 'Faded: Global ["..global.."] has been set to "..tostring(new_value)..". (Default: '.. tostring("..global..") ..')'\
--                   ".. global .. " = ".. tostring(new_value) .."\
--                   Print(str)\
--               end")()
-- end

function custom_log_print(str)
   Print(str)
   table.insert(all_change_string_log, "Server " .. str)
end

-- Check if the global exist, and print in console if any change Occured
-- This prevent globals to be changed without noticing here
local function check_if_not_out_of_date(global, new_value, isBool)
   -- if (isBool ~= true) then
   global = tostring(global)
   new_value = tostring(new_value)
   local str = "if (" .. global .. " ~= nil and " .. new_value .. " ~= nil and " .. global .. " ~= " .. new_value .. ") then\
                  local str = 'Faded: Global [" .. global .. "] has been set to "..new_value .. ". (Default: '..tostring(" .. global .. ")..')'\
                  ".. global .. " = ".. new_value .."\
                  custom_log_print(str)\
              elseif (" .. global .. " ~= nil and " .. new_value .. " ~= nil and "..global.." == " .. new_value ..") then\
                  --custom_log_print('Equal')\
              else\
                  local str = 'Faded Warning: Global [" .. global .. "] does not exist anymore or the new value is nil'\
                  custom_log_print(str)\
                  custom_log_print('Faded: Maybe a new configuration file just came out')\
                  custom_log_print('Faded: Remove the old one and configure it again')\
              end"
   -- Print(str)
   loadstring(str)()
   -- else
   --    check_blind(global, new_value, isBool)
   -- end
end

local _c = check_if_not_out_of_date
-- local _c = check_blind
local root = config

_c("kFadedModTimeInSecondsSelectingEquipmentIsAllowed", root.weapon_purchase_time_limit)
-- Faded selection chance
_c("kFadedModFadedSelectionChance", root.faded_selection_chance.on_kill_as_marine)
_c("kFadedModFadedNextSelectionChance", root.faded_selection_chance.on_win_as_faded)

_c("kFadedAmbiantSound", root.ambiant_sound)
-- round_timer
_c("kFadedModBaseRoundTimerInSecs", root.round_timer.base)
_c("kFadedMaxRoundTimer", root.round_timer.max)
_c("kFadedModBonusTimePerMarine", root.round_timer.bonus_per_marine)
_c("kFadedNextMapWaitingTime", root.round_timer.on_change_map_waiting_delay)
_c("kFadedModTimeTillNewRound", root.round_timer.on_next_round_waiting_delay)


-- spawn_protection
_c("kFadedModSpawnProtectionEnabled", root.spawn_protection.enable, true)
_c("kFadedModSpawnProtectionTime", root.spawn_protection.time)

-- friendly_fire
_c("kFadedModFriendlyFireEnabled", root.friendly_fire.enable, true)
_c("kFriendlyFireScalar", root.friendly_fire.scalar)

-- Other
_c("kFadedHintEnable", root.enable_hint, true)

-- all_talk_enable
_c("kFadedModAllTalkEnabled", root.all_talk_enable, true)

-- Multi faded
_c("kFadedAddFadeMarineScale", root.multi_faded.marine_per_faded)

-- Veteran
_c("kFadedVeteranScale", root.veteran.marine_per_veteran)
_c("kVeteranFakeMarineRangeDetection", root.veteran.fake_marine_detection_radius)

-- Anti camp
_c("kFadedAntiCampEnable", root.anti_camp.enable)
_c("kAntiCampRadius", root.anti_camp.radius)
_c("kCampMaxTime", root.anti_camp.trigger_time)
_c("kMaxBabblerOnMap", root.anti_camp.max_babblers)
_c("kFadedNumBabblersPerEgg", root.anti_camp.num_babbler)
_c("kBabblerEggBuildTime", root.anti_camp.egs_build_time)

-- Balance
root = config.balance.faded
_c("kFadeBaseHealth", root.health)
_c("kFadeBaseArmor", root.armor)
_c("kFadeHealthPerMarine", root.bonus_hp_per_marine)
_c("kFadeArmorPerMarine", root.bonus_ap_per_marine)
_c("kFadedAdreBonusOnKill", root.energy_bonus_onkill)

_c("kEnergyUpdateRate", root.energy_rege_rate)
_c("kFadedEatingRegen", root.eating_health_bonus)
_c("kFadedEatingRegenArmor", root.eating_armor_bonus)
_c("kFadedEatingTime", root.eating_revive_time)

_c("kFadedRespawnedEnable", root.revive_enable)
_c("kFadedRespawnedFadeHealthCost", root.revive_health_cost)
_c("kFadedRespawnedFadeHealth", root.revive_faded_health)
_c("kFadedRespawnedFadeEnergy", root.revive_faded_energy)

_c("kFadedRespawnedFadeHealth", root.respawn_health)
_c("kFadedRespawnedFadeEnergy", root.respawn_energy)
_c("kFadedRespawnedFadeHealthCost", root.respawn_health_cost)
_c("kFadedAllowFakeMarineToTakePG", root.can_disguise_take_pg)

root = config.balance.veteran
_c("kFadedVeteranHealth", root.health)
_c("kFadedVeteranArmor", root.armor)
_c("kFadedVeteranSpeedBonus", root.speed_bonus)
_c("kFadedVeteranDamageBonusFactor", root.damage_bonus)

-- Sanity
root = config.balance.sanity.distance_vision_malus
_c("kFadedMinMissingSanityForMalus", root.min_missing_sanity_for_malus)

root = config.balance.sanity.growl
_c("kFadedGrowlMentalHealthDamage", root.damage)
_c("kFadedGrowlDiminutionByMeter", root.damage_reduction_per_meter)

root = config.balance.sanity.swipe
_c("kFadedSwipMentalHealthDamage", root.damage)


root = config.balance.sanity.stab
_c("kFadedPoisonSanityDamagePerSec", root.poison_damage_per_sec)

root = config.balance.sanity.on_hit_bonus
_c("kFadedShotgunOnHitMentalHealthBonus", root.shotgun_per_bullet)
_c("kFadedPistolOnHitMentalHealthBonus", root.pistol)
_c("kFadedRifleOnHitMentalHealthBonus", root.rifle)
_c("kFadedFlameOnHitMentalHealthBonus", root.flamethrower)
_c("kFadedGLOnHitMentalHealthBonus", root.GL)

_c("kFadedClusterOnHitMentalHealthBonus", root.cluster_grenade)
_c("kFadedGazOnHitMentalHealthBonus", root.nerve_grenade)
_c("kFadedNapalmOnHitMentalHealthBonus", root.napalm_grenade)

root = config.balance.sanity.marine_death
_c("kFadedMarineMentalHealthDeathDamage", root.damage)
_c("kFadedMarineDeathRadius", root.radius)

-- weapons.babbler_player
root = config.balance.weapons.babbler_player

_c("kFadedSpectatorBabblerHealth", root.health)
_c("kFadedSpectatorBabblerArmor", root.armor)
_c("kFadedBabblerPlayerEnergyRegen", root.energy_regen_per_sec)
_c("kBiteEnergyCost", root.bite_energy_cost)
_c("kBiteDamage", root.bite_damages)
_c("kFadedBabblerPlayerJumpCost", root.jump_energy_cost)
_c("kFadedBabblerPlayerWalkSpeed", root.walk_speed)
_c("kFadedBabblerPlayerJumpSpeed", root.jump_speed)
_c("kFadedBabblerOnKillScoreReward", root.on_kill_point_reward)
_c("kFadedBabblerAmmoPackSpawnChances", root.drop_ammo_chances_percent)

_c("kFadedDelayOnSpawnWavePerBabbler", root.spawn_wave_added_time_per_babbler)
_c("kFadedMaxBabblerPlayerSpawnDelay", root.spawn_wave_max_delay)
_c("kFadedInstantBabblerSpawnWithProto", root.instant_spawn_with_proto_up)

-- weapons.aliens

root = config.balance.weapons.alien
_c("kFadedSoundIfFadeNearby", root.play_sound_if_nearby_a_marine)

root = config.balance.weapons.alien.camo
_c("kFadedVisuDistance", root.max_meter_marine_can_see_fade)
_c("kFadedMaxCloakBonusPercentSlowMovement", root.max_camo_bonus_percent_when_standing)
_c("kFadedLeapCloakMalus", root.leap_cloak_malus)
_c("kFadedMinSpeedForOpacity", root.holes_in_skin_starting_at_speed)

root = config.balance.weapons.alien.babbler_ability
_c("kFadedBabblerAbilityEnable", root.enable)
_c("kFadedBabblerAbilityHealthCost", root.heath_cost)
_c("kFadedBabblerAbilityNumBabbler", root.num_babbler)

root = config.balance.weapons.alien.bile_bomb
_c("kBileBombEnergyCost", root.energy_cost)
_c("kBileBombFireRate", root.fire_rate)
_c("kFadedModBombVelocity", root.velocity)

root = config.balance.weapons.alien.leap
_c("kLeapEnergyCost", root.energy_cost)

root = config.balance.weapons.alien.growl
_c("kFadedGrowlEnergyCost", root.energy_cost)
_c("kFadedGrowlInterval", root.rate)

root = config.balance.weapons.alien.swipe
_c("kSwipeDamage", root.damage)
_c("kSwipeEnergyCost", root.energy_cost)

root = config.balance.weapons.alien.blink
_c("kBlinkEnergyCost", root.per_sec_cost)
_c("kStartBlinkEnergyCost", root.on_use_cost)
_c("kFadedBlinkHpCost", root.per_sec_hp_cost)
_c("kFadedEnzimeAfterBlink", root.enzime_after_blink)

root = config.balance.weapons.alien.stab
_c("kFadedStabDamage", root.hit_damage)
_c("kFadedDeniedSwipeAfterStab", root.denied_time_after_stab)
_c("kFadedStabPoisonDamage", root.poison_damage_per_sec)
_c("kPoisonBiteDuration", root.poison_duration)
_c("kPoisonDamageThreshhold", root.poison_damage_threshhold)

-- weapons.marine

root = config.balance.weapons.marine
_c("kItemStayTime", root.dropped_item_stay_time)
_c("kFadedDeathMessageDistance", root.death_notification_radius)

root = config.balance.weapons.marine.welder
_c("kFadedCanDropWelder", root.can_be_drop)

root = config.balance.weapons.marine.hunt_ability
_c("kFadedHuntMaxPointOnPath", root.max_pts_on_path)
_c("kFadedHuntTime", root.hunt_charge_duration)
_c("kFadedHuntPathDuration", root.path_display_duration)

root = config.balance.weapons.marine.flashlight
_c("kFadedNoCustomFlashlight", root.ns2_flashlight)
_c("kFadedLightTime", root.maxUsageTime)
_c("kFadedLightRegePerSec", root.regePerSec)
_c("kFadedLightDetectDist", root.fadedDetectionRange)

root = config.balance.weapons.marine.mine
_c("kMineDamage", root.damage)
_c("kMineTriggerRange", root.trigger_range)
_c("kNumMines", root.nb_per_marine)
_c("kFadedModMinesRestrictionRange", root.restriction_range)

root = config.balance.weapons.marine.flamethrower
_c("kFlamethrowerClipSize", root.clip)
_c("kFlamethrowerDamage", root.direct_hit_damage)
_c("kBurnDamagePerSecond", root.burn_damage)
_c("kFlamethrowerBurnDuration", root.burn_duration)
_c("kFlameThrowerEnergyDamage", root.energy_damage)

root = config.balance.weapons.marine.rifle
_c("kRifleClipSize", root.clip)
_c("kRifleDamage", root.damage)

root = config.balance.weapons.marine.heavy_shotgun
_c("kFadedHSGCheatEnable", root.mysterious_option)
_c("kFadedHeavyShotgunDamage", root.damage)
_c("kFadedHeavyShotgunKnockBack", root.knock_back)
_c("kFadedHeavyShotgunPelletPerShot", root.pellet_per_shot)
_c("kFadedHeavyShotgunFireRate", root.fire_rate)
_c("kFadedHeavyShotgunDoubleShotPelletRatio", root.extra_pellets_on_double_shot)

root = config.balance.weapons.marine.hand_grenade
_c("kFadedGrenadeSecondary", root.allow_wall_stick)
_c("kMaxHandGrenades", root.grenades_nb)
_c("kPulseGrenadeDamageRadius", root.pulse.radius)
_c("kPulseGrenadeDamage", root.pulse.damage)
_c("kPulseGrenadeEnergyDamage", root.pulse.energy_damage)

root = config.balance.weapons.marine.hand_grenade.napalm
_c("kFadedMaxNapalmGrenades", root.amount)
_c("kFadedNapalmDamagePerSecond", root.cloud_dps)
_c("kFadedNapalmCloudLifetime", root.cloud_lifetime)
_c("kFadedNapalmCloudRadius", root.cloud_radius)
-- _c("kFadedBurnRadius", root.cloud_burn_radius)
_c("kFadedNapalmPutInFireDelay", root.cloud_min_delay_to_put_in_fire)
_c("kFadedOnMarineDamageMultiplyer", root.cloud_dps_multiplyer_on_marine)

root = config.balance.weapons.marine.hand_grenade.heal
_c("kFadedMaxHealGrenades", root.amount)
_c("kFadedHealCloudRefreshRate", root.refresh_rate)
_c("kFadedHealCloudHpPerRefresh", root.hp_per_refresh)
_c("kFadedGLHealCloudHpPerRefresh", root.medic_GL_hp_per_refresh)

root = config.balance.weapons.marine.pistol
_c("kFadedPistolAltCheatEnable", root.mysterious_option)
_c("kFadedFlareEnable", root.enable_flare)
_c("kFadedFlareAmmoCost", root.flare_ammo_cost)

_c("kFlareLifeTime", root.flare_lifetime)
_c("kFadedFlareIntensity", root.flare_light_intensity)
_c("kFadedFlareRadius", root.flare_light_radius)

root = config.balance.weapons.marine.pistol.medic
_c("kFadedMedicPistolDmg", root.bullet_blast_max_dmg)
_c("kFadedMedicPistolBlastRadius", root.bullet_blast_radius)
_c("kFadedMedicPistolDetonationDelay", root.detonation_delay)


root = config.balance.weapons.marine.GL
_c("kFadedGLCheatEnable", root.mysterious_option)
_c("kFadedGLSecondary", root.has_secondary)
_c("kGrenadeLauncherGrenadeDamageRadius", root.explosive.radius)

root = config.balance.weapons.marine.jetpack
_c("kJetpackUseFuelRate", root.fuel_regen)
root = config.balance.weapons.marine.exo
_c("kFadedExoSpeedBonusFactor", root.speed_bonus)

root = config.balance.corpse
_c("kFadedEatUseDistance", root.use_eat_burn_radius)
_c("kFadedDeadBodyInfestationRadius", root.infestation_radius)
_c("kFadedDeadBodyToxicGazChance", root.spores_rate)
_c("kSporesDustDamagePerSecond", root.spores_damage)
_c("kSporesDustCloudRadius", root.spores_radius)
_c("kSporesDustCloudLifetime", root.spores_lifetime)

-- Hallucination
root = config.balance.hallucination
_c("kHallucinationLifeTime", root.lifetime)
_c("kHallucinationMaxHealth", root.health)
_c("kFadedStressLvlHallucinationChance", root.spawn_chance_in_stress)
_c("kFadedPanicLvlHallucinationChance", root.spawn_chance_in_panic)
_c("kFadedHeavyPanicLvlHallucinationChance", root.spawn_chance_in_heavypanic)

-- Objective_mod
root = config.balance.objective_mod
_c("kFadedDisableObjectivMod", root.disable)
_c("kFadedCanUsePrimaryWithBattery", root.can_use_primary_weap_with_battery)
_c("kFadedSentryBatteryWeight", root.sentry_battery_weight)
_c("kFadedBatterySpawnDelay", root.battery_spawn_delay)
_c("kFadedBatterySpawnAtRoundStart", root.battery_spawned_at_round_start)

_c("kFadedSpawnBatteryOnBuildingPowerUp", root.battery_spawn_on_building_power_up)

root = config.balance.objective_mod.armory
_c("kFadedArmoryAllowWeapon", root.can_drop_weapon, true)
_c("kFadedArmoryAllowSupply", root.can_drop_supplies, true)
_c("kFadedArmorySupplyAmount", root.drop_per_marine_alive)
_c("kFadedArmoryHealthAmmoSupply", root.nb_ressuply_allowed)
_c("kFadedCanMarinesBuyWeapons", root.can_buy_weapon)

root = config.balance.objective_mod.armory.weapons_cost
_c("kShotgunCost", root.shotgun)
_c("kClusterGrenadeCost", root.cluster_grenade)
_c("kGasGrenadeCost", root.gas_grenade)
_c("kPulseGrenadeCost", root.pulse_grenade)
_c("kMineCost", root.mines)
_c("kFlamethrowerCost", root.flamethrower)
_c("kGrenadeLauncherCost", root.grenade_launcher)
_c("kWelderCost", root.welder)

root = config.balance.objective_mod.observatory
_c("kFadedObsGiveMinimap", root.enable_minimap)
_c("kFadedObsGiveDamageDisplay", root.enable_damages_display)
_c("kFadedObsEnablePassiveDetection", root.enable_passive_detection)

root = config.balance.objective_mod.robotics_factory
_c("kFadedRoboticsFactoryGiveMac", root.give_mac)

root = config.balance.objective_mod.arms_lab
_c("kFadedArmslabGiveWeapons1", root.give_weap1)
_c("kFadedArmslabGiveArmor1", root.give_armor1)

-- _c("", root.)
-- _c("", root.)
