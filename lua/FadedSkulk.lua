// ===================== Faded Mod =====================
//
// lua\FadedSkulk.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

Script.Load("lua/Skulk.lua")
-- Script.Load("lua/StunMixin.lua")

Skulk.kXExtents = Babbler.kRadius
Skulk.kYExtents = Babbler.kRadius
Skulk.kZExtents = Babbler.kRadius

babbler_XP = {}
babbler_XP_level = {}
babbler_xeno_byte = {}

babbler_byte_before_xeno = 3

babbler_level = {
   {l = 0,  h = 5, dmg_bonus = 0},
   {l = 5,  h = 10, dmg_bonus = 1},
   {l = 10, h = 20, dmg_bonus = 2},
   {l = 20, h = 35, dmg_bonus = 3},
   {l = 35, h = 50, dmg_bonus = 4},
   {l = 50, h = 70, dmg_bonus = 5},
   {l = 70, h = 100, dmg_bonus = 6},
   -- Safety condition (can't reach here)
   {l = 100, h = 100000, dmg_bonus = 7},
}

local networkVars =
   {
   }

local kLeapTime = 0.2
local kMass = Babbler.kMass

-- AddMixinNetworkVars(StunMixin, networkVars)

local kSkulkModel = PrecacheAsset("models/alien/babbler/babbler.model" )
local kSkulkAnimationGraph = PrecacheAsset("models/alien/babbler/babbler.animation_graph")


local kDetonateTime = 2.0

local kXenocideSoundName = PrecacheAsset("sound/NS2.fev/alien/common/xenocide_start")

local function TriggerXenocide(self, player)

    if Server then

       -- -- Reset stats to 0 (Player has to choose between
       -- -- xeno and a squad)
       -- player:initSkulkXP(true)

       _CreateClusterFragment(player, player, nil, nil, nil, player,
                              kDetonateTime, false) -- only the smoke
        if not self.XenocideSoundName then
            self.XenocideSoundName = Server.CreateEntity(SoundEffect.kMapName)
            self.XenocideSoundName:SetAsset(kXenocideSoundName)
            self.XenocideSoundName:SetParent(self)
            self.XenocideSoundName:Start()
        else
            self.XenocideSoundName:Start()
        end
        //StartSoundEffectOnEntity(kXenocideSoundName, player)
        self.xenocideTimeLeft = kDetonateTime * 1.1

    elseif Client and Client.GetLocalPlayer() == player then

        -- if not self.xenocideGui then
        --     self.xenocideGui = GetGUIManager():CreateGUIScript("GUIXenocideFeedback")
        -- end

        -- self.xenocideGui:TriggerFlash(kDetonateTime)
        player:SetCameraShake(.01, 15, kDetonateTime)

    end

end

function XenocideLeap:OnPrimaryAttack(player)

   if player:GetEnergy() >= self:GetEnergyCost() then

        if not self.xenociding then

           TriggerXenocide(self, player)
            self.xenociding = true

        else

            if self.xenocideTimeLeft and self.xenocideTimeLeft < kDetonateTime * 0.8 then
               BiteLeap.OnPrimaryAttack(self, player)
            end

        end

    end

end


function Skulk:initSkulkXP(force)
   if (Client or Server) then
      if (force or babbler_XP[self:GetName()] == nil) then
         babbler_XP[self:GetName()] = 0;
      end
      if (force or babbler_XP_level[self:GetName()] == nil) then
         babbler_XP_level[self:GetName()] = 1;
      end
      if (force or babbler_xeno_byte[self:GetName()] == nil) then
         babbler_xeno_byte[self:GetName()] = 0
      end
   end
end

function Skulk:GetXP()
   if (Client or Server) then
      self:initSkulkXP()
      return (babbler_XP[self:GetName()])
   else
      return (0)
   end
end

function Skulk:GetXPPercent()
   if (Client or Server) then
      self:initSkulkXP()
      current = self:GetXP()
      disp = babbler_level[self:GetXPLevel()].l
      max = babbler_level[self:GetXPLevel()].h

      current = current - disp
      max = max - disp
      return ((current / max) * 100)
   else
      return 0
   end
end

function Skulk:GetXPLevel()
   if (Client or Server) then
      self:initSkulkXP()
      return (babbler_XP_level[self:GetName()])
   else
      return 0
   end
end

function Skulk:AddXP()
   if (Client or Server) then
      self:initSkulkXP()
      babbler_XP[self:GetName()] = self:GetXP() + 1
      babbler_xeno_byte[self:GetName()] = babbler_xeno_byte[self:GetName()] + 1
      if (Server
          and babbler_xeno_byte[self:GetName()] == babbler_byte_before_xeno) then
         self:GiveItem(XenocideLeap.kMapName)
         self:SetActiveWeapon(BiteLeap.kMapName)
         self:FadedMessage("Xenocide unlocked !")
      end

      if (self:GetXP() >= babbler_level[self:GetXPLevel()].h) then
         babbler_XP_level[self:GetName()] = babbler_XP_level[self:GetName()] + 1

         -- if (Server) then
         --    self:TriggerEffects("enzymed")
         -- end

         if (Server) then
            self:FadedMessage("Level Up !")
         end
      end
   end
end

local skulkOnInitialized = Skulk.OnInitialized
function Skulk:OnInitialized()


   if (Client or Server) then
      babbler_xeno_byte[self:GetName()] = 0
   end
    Alien.OnInitialized(self)


    // Note: This needs to be initialized BEFORE calling SetModel() below
    // as SetModel() will call GetHeadAngles() through SetPlayerPoseParameters()
    // which will cause a script error if the Skulk is wall walking BEFORE
    // the Skulk is initialized on the client.
    self.currentWallWalkingAngles = Angles(0.0, 0.0, 0.0)

    -- self:SetModel(self:GetVariantModel(), kSkulkAnimationGraph)
    self:SetModel(kSkulkModel, kSkulkAnimationGraph)

    self.wallWalking = false
    self.wallWalkingNormalGoal = Vector.yAxis

    if Client then

        self.currentCameraRoll = 0
        self.goalCameraRoll = 0

        self:AddHelpWidget("GUIEvolveHelp", 2)
        self:AddHelpWidget("GUISkulkParasiteHelp", 1)
        self:AddHelpWidget("GUISkulkLeapHelp", 2)
        self:AddHelpWidget("GUIMapHelp", 1)
        self:AddHelpWidget("GUITunnelEntranceHelp", 1)

        if (Client.GetLocalPlayer() == self)
        then
           self.GUIXP = GetGUIManager():CreateGUIScript("FadedGUIBabblerXP")
        end
    end

    -- if (Server) then
    --    local squad = {}

    --    for i = 1, 5 do
    --       local babbler = CreateEntity(Babbler.kMapName,
    --                                    self:GetOrigin() + Vector(0, -0.5, 0),
    --                                    self:GetTeamNumber())
    --       if (babbler) then
    --          babbler:SetOwner(self)
    --          table.insert(squad, babbler)
    --       end

    --    end
    --    -- for _, babbler in ipairs(squad) do
    --    --    kTechId.Follow
    --    -- end

    -- end

    self.leaping = false

    self.timeLastWallJump = 0

    InitMixin(self, IdleMixin)
    -------------------------- ns2 legacy skulk above
   -- skulkOnInitialized(self)
   -- InitMixin(self, StunMixin)
   self.timeOfLeap = Shared.GetTime()
   self:SetMaxHealth(kFadedSpectatorBabblerHealth)
   self:SetMaxArmor(kFadedSpectatorBabblerArmor)
   self:SetHealth(kFadedSpectatorBabblerHealth)
   self:SetArmor(kFadedSpectatorBabblerArmor)
   if (Server) then
      self:SetDesiredCamera(0.0, { move = true}, self:GetOrigin(), nil, 2.5)
   end
end

local skulkOnKill = Skulk.OnKill
function Skulk:OnKill(attacker, doer, point, direction)
   if (Server) then -- Simulate explosion effect
      local ent = CreateEntity(Babbler.kMapName, self:GetOrigin())
      if (ent) then
     ent:Kill()
      end
   end
   if (skulkOnKill) then
      skulkOnKill(self, attacker, doer, point, direction)
   end
   if (Server) then
      self:SetBypassRagdoll(true)
      self:SetModel(nil)
      local drop_ammo = (math.random(0, 100) <= kFadedBabblerAmmoPackSpawnChances)
      if (drop_ammo) then
     CreateEntity(AmmoPack.kMapName, self:GetOrigin() + Vector(0, 1, 0), 1)
      end
   end
end

function Skulk:GetRecuperationRate()
   return (kFadedBabblerPlayerEnergyRegen)
end

local skulkOnDestroy = Skulk.OnDestroy
function Skulk:OnDestroy()
   skulkOnDestroy(self)

   if self.hitBox then

      Shared.DestroyCollisionObject(self.hitBox)
      self.hitBox = nil

   end

   if (Client) then
      GetGUIManager():DestroyGUIScript(self.GUIXP)
   end
end

function Skulk:GetBaseArmor()
   return kFadedSpectatorBabblerArmor
end

function Skulk:GetBaseHealth()
   return kFadedSpectatorBabblerHealth
end

-- No leap
function Skulk:OnLeap()
end

function Skulk:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.SensorBlip
   local blipTeam = -1
   return false, blipType, blipTeam, false, false
end

function Skulk:GetMapBlipType()
   return nil
end

-- function Skulk:GetType()
--    return kMinimapBlipType.Undefined
-- end

-- No wall jump
function Skulk:GetCanWallJump()
   return false
end

-- No footsteap
function Skulk:GetPlayFootsteps()
   return (false)
end

-- local function _stunMe(player)
--    local hold_velocity = player:GetVelocity()
--    hold_velocity:Scale(0)
--    player:SetVelocity(hold_velocity)
--    -- if (player.SetStun) then
--    --    player:SetStun(1)
--    -- end
--    player:DisableGroundMove(1)
-- end

function Skulk:GetCanJump()
   if (self.timeOfLeap +3 <= Shared.GetTime()) then
      return (Player.GetCanJump(self))
   else
      return (false)
   end
end

function Skulk:OnJump()

   if (self.GetEnergy and self:GetEnergy() >= kFadedBabblerPlayerJumpCost) then
      local velocity = self:GetVelocity()

      -- velocity.y = 70 * self:GetViewAngles():GetCoords().zAxis.y
      velocity:Scale(5)
      self:SetVelocity(velocity)
      self:DisableGroundMove(0.2)

      self.leaping = true
      self.timeOfLeap = Shared.GetTime()
      self.wallWalking = false
      self:DeductAbilityEnergy(kFadedBabblerPlayerJumpCost)
      if (Server) then
     self:TriggerEffects("babbler_jump")
      end
      -- if (self.SetStun) then
     -- self:AddTimedCallback(_stunMe, 1.3)
      -- end
   end
end

-- -- No dark vision
-- function Skulk:SetDarkVision(st)
-- end


function Skulk:GetMaxSpeed(possible)
   local maxspeed = kFadedBabblerPlayerWalkSpeed

   if (self:GetActiveWeapon() and self:GetActiveWeapon():isa("XenocideLeap")
       and self:GetActiveWeapon().xenociding == true) then
      return (kFadedBabblerPlayerWalkSpeed * 1.8)
   end
   if (self.timeOfLeap >= Shared.GetTime() - kLeapTime) then
      return (kFadedBabblerPlayerJumpSpeed)
   end

   return maxspeed

end

function Skulk:GetMass()
   return kMass
end

function Skulk:GetJumpHeight()
   return kFadedBabblerJumpHeigh
end

function Skulk:GetAcceleration()
   return 100
end

function Skulk:GetAirControl()
   return 27
end

function Skulk:GetAirAcceleration()
   return 100
end

function Skulk:GetIsThirdPerson()
   return (true)
end


local function CreateHitBox(self)

   if not self.hitBox then

      self.hitBox = Shared.CreatePhysicsSphereBody(false, Babbler.kRadius * 1.4, Babbler.kMass, self:GetCoords() )
      self.hitBox:SetGroup(PhysicsGroup.BabblerGroup)
      self.hitBox:SetCoords(self:GetCoords())
      self.hitBox:SetEntity(self)
      self.hitBox:SetPhysicsType(CollisionObject.Kinematic)
      self.hitBox:SetTriggeringEnabled(true)

   end
end

function Skulk:GetHasMovementSpecial()
   return false
end

function Skulk:GetMovementSpecialTechId()
   return nil
end

function Skulk:OnUpdatePhysics()
   CreateHitBox(self)
   self.hitBox:SetCoords(self:GetCoords())
   -- if (Client) then
   --    Print("Physicis update")
   -- elseif (Server) then
   --    Shared:FadedMessage("Phycis updated")
   -- end
end

function Skulk:OnUpdateAnimationInput(modelMixin)
   PROFILE("Skulk:OnUpdateAnimationInput")

   Alien.OnUpdateAnimationInput(self, modelMixin)
   local move = ""

   if self:GetIsLeaping() then
      move = "jump"
   elseif (self:GetSpeedScalar() > 0.2) then
      move = "run"
   elseif (self:GetSpeedScalar() == 0
           and self.timeOfLeap + 2 < Shared.GetTime()) then
      move = "wag"
   else
      move = "idle"
   end
   modelMixin:SetAnimationInput("move", move)
end

Shared.LinkClassToMap("Skulk", Skulk.kMapName, networkVars, true)
