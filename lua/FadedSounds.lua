// ===================== Faded Mod =====================
//
// lua\FadedSounds.lua
//
//    Created by: Rio (rio@myrio.de)
//
// =====================================================

Script.Load("lua/MarineTeam.lua")

// Supress a few messages like "you need a commander"
local avoidMessageTypes = { kTeamMessageTypes.NoCommander, kTeamMessageTypes.SpawningWait, kTeamMessageTypes.Spawning }

local savedSendTeamMessage = SendTeamMessage
function SendTeamMessage(team, messageType, optionalData)
    if (avoidMessageTypes[messageType]) then
        savedSendTeamMessage(team, messageType, optionalData)
    end
end

-- Same as the NS2 one, can copy it again in case of new build
local function GetArmorLevel(self)

    local armorLevels = 0

    local techTree = self:GetTechTree()
    if techTree then

       if techTree:GetHasTech(kTechId.Armor3) then
            armorLevels = 3
       elseif techTree:GetHasTech(kTechId.Armor2) then
            armorLevels = 2
       elseif techTree:GetHasTech(kTechId.Armor1) then
            armorLevels = 1
        end

    end

    return armorLevels

end


// Remove the "No IPs" sound
function MarineTeam:Update(timePassed)
   PROFILE("MarineTeam:Update")

   PlayingTeam.Update(self, timePassed)

   // Update distress beacon mask
   self:UpdateGameMasks(timePassed)

   -- if GetGamerules():GetGameStarted() then
   --    CheckForNoIPs(self)
   -- end

   local armorLevel = GetArmorLevel(self)
   for index, player in ipairs(GetEntitiesForTeam("Player", self:GetTeamNumber())) do
      if (player and player:GetMaxHealth() == kFadedVeteranHealth
         and player:GetMaxArmor() == kFadedVeteranArmor) then
     player:UpdateArmorAmount(1) -- Veteran stay at 1
      else
         player:UpdateArmorAmount(armorLevel)
      end
   end
end

// Remove "Soldier is under attack" sound
function Marine:GetDamagedAlertId()
   return nil
end
