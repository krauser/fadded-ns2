// ===================== Faded Mod =====================
//
// lua\FadedSwipeBlink.lua
//
//    Created by: Rio (rio@myrio.de)
//    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

Script.Load("lua/Weapons/Alien/Ability.lua")
Script.Load("lua/Weapons/Alien/LeapMixin.lua")

local networkVars =
   {
   }

AddMixinNetworkVars(LeapMixin, networkVars)

local onCreate = SwipeBlink.OnCreate
function SwipeBlink:OnCreate()
   onCreate(self)
   self.etherealEndTime = Shared.GetTime()
   InitMixin(self, LeapMixin)
end


-- -- Cannot attack while blinking.
-- function SwipeBlink:GetPrimaryAttackAllowed()
--    local player = self:GetParent()
--    if (player and player.ethereal ~= nil) then
--       Shared:FadedMessage("GetPrimaryAttackAllowed callled, return " .. tostring(not player.ethereal))
--       return not player.ethereal
--    else
--       Shared:FadedMessage("GetPrimaryAttackAllowed callled, TRUE (player: " .. tostring(player))
--       return (true)
--    end
-- end

function SwipeBlink:GetSecondaryTechId()
   return kTechId.Leap
end

function SwipeBlink:GetBlinkAllowed()
   return false
end

function SwipeBlink:GetHUDSlot()
   return 1
end

function SwipeBlink:GetIsBlinking()
   return self.secondaryAttacking
end

-- Overwride the blink by hand
function SwipeBlink:OnSecondaryAttack(player)
   -- Blink.OnSecondaryAttack(self, player)
   if (self.secondaryAttacking == false
       and player and player:GetEnergy() > kLeapEnergyCost) then
      -- player:DeductAbilityEnergy(kLeapEnergyCost)
      self:PerformSecondaryAttack(player)
      self.secondaryAttacking = true
   end
end
function SwipeBlink:OnSecondaryAttackEnd(player)
   -- Blink.OnSecondaryAttackEnd(self, player)
   self.secondaryAttacking = false
end

-- local primaryAttack = Player.PrimaryAttack
-- function SwipeBlink:PrimaryAttack(player)
--    -- The damage are trigger before, this is just for the animation
--    SwipeBlink.kRange = 0.1
--    primaryAttack(self, player)
-- end
-- local primaryAttackEnd = Player.PrimaryAttackEnd
-- function SwipeBlink:PrimaryAttackEnd(player)
--    primaryAttackEnd(self, player)
--    SwipeBlink.kRange = kFadedSwipeRange
-- end

function SwipeBlink:GetEnergyCost(player)
   return (kSwipeEnergyCost)
end

-- Cost of the leap
function SwipeBlink:GetSecondaryEnergyCost(player)
   return kLeapEnergyCost
end

function SwipeBlink:CanFadeTakeCorpse(player)
   return true
end

function SwipeBlink:GetRange()
   return (SwipeBlink.kRange)
end

function SwipeBlink:RagdollUtilities(player)
   ---------------------
   if (not player or not self:CanFadeTakeCorpse()) then
      return
   end

   if (Client and self.holdRagdoll == true) then -- Dropping ragdoll
      local aim_p = player:isAimingEntity("Entity", 4)
      if (aim_p == false) then -- If aiming at entity, do nothing
         self.holdRagdoll = false
         Client.SendNetworkMessage("DropRagdoll",
                                   {
                                      marineRagdoll = self.marineRagdoll,
                                      fadedRagdoll = self.fadedRagdoll,
                                      playerName = self.playerName,
                                      modelName = self.ragdollModelName,
                                      graphName = self.ragdollGraphName,
                                   }, true)
      end
   end

   local status, ragdoll = player:isAimingEntity("Ragdoll", 4)
   -- Taking ragdoll
   if (Client and status and ragdoll.creationTime < Shared.GetTime() - 1)
   then
      if (ragdoll.fadedCorpseAlreadyUsed ~= true
             and not (ragdoll.ragdollDirty == true) and self.holdRagdoll == nil
          and (ragdoll:isAMarine() or ragdoll:isAFaded())) then
         -- Shared:FadedMessage("Taking the corpse")
         local battery = GetEntitiesForTeamWithinRange("SentryBattery", 2,
                                                       ragdoll:GetOrigin(),
                                                       0.2)
         self.holdRagdoll = true
         self.marineRagdoll = ragdoll.marineRagdoll
         self.fadedRagdoll = ragdoll.fadedRagdoll
         self.playerName = ragdoll.playerName
         self.ragdollModelName = ragdoll:GetModelName()
         self.ragdollGraphName = ragdoll:GetGraphName()
         -- Prevent the player to pick the same ragdoll more than once
         -- (in case their is network latency)
         ragdoll.ragdollDirty = true

         local battery_id = 0
         if (#battery == 1) then
            battery_id = battery[1]:GetId()
         end
         Client.SendNetworkMessage("TakeRagdoll",
                                   {
                                      ragdoll_id = ragdoll:GetId(),
                                      battery_id = battery_id
                                   }, true)
      end
   end
   if (self.holdRagdoll == false) then
      self.holdRagdoll = nil
   end

   ---------------------------------
end

-- Degressive damage (25 --> 50 --> 75) depending on the distance and the angle
function SwipeBlink:PerformMeleeAttack()
   local player = self:GetParent()
   local deniedSwipeAfterStab = self.kFadedDeniedSwipeAfterStab
   -- if (deniedSwipeAfterStab) then
   --    deniedSwipeAfterStab = deniedSwipeAfterStab < Shared.GetTime() - kFadedDeniedSwipeAfterStab
   -- else
   --    deniedSwipeAfterStab = true
   -- end
   if player then
      local didHit, hitObject, endPoint, surface = PerformGradualMeleeAttack(self, player, SwipeBlink.kDamage, self:GetRange())
   end
   self:RagdollUtilities(player)
end

local _swipeBlinkRate = nil
local onPrimaryAttack = SwipeBlink.OnPrimaryAttack
-- As the Fade does not have a BileBomb animation,
-- we have to add a manual timer system for the firing rate
function SwipeBlink:OnPrimaryAttack(player)
   local denied = true
   local allowed_time = Shared.GetTime() - kFadedDeniedSwipeAfterStab

   if (player.kFadedDeniedSwipeAfterStab < allowed_time
       and player:GetEnergy() >= kSwipeEnergyCost
       and player.ethereal == false) then
      denied = false
   end
   if (denied == false) then
      --

      -- ent:GetPhysicsModel():AddImpulse(ent:GetOrigin(), z)
      -- end
      -- else
      self.primaryAttacking = true
      -- end
      --
   else
      self.primaryAttacking = false
   end
   -- Print("SwipeBlink:OnPrimaryAttack() called : denied = %s / %s", ToString(denied), ToString(self.primaryAttacking))
end

-- Return the attack origin from the player origin
-- From Utility.lua
-- We make sure that the given range is actually the start/end of the melee volume by moving forward the
-- starting point with the extents (and some extra to make sure we don't hit anything behind us),
-- as well as moving the endPoint back with the extents as well (making sure we dont trace backwards)

-- Prototype: local width, height = weapon:GetMeleeBase()
function SwipeBlink:GetMeleeBase()
   return 1.5, 1.2
end

--------------------


-- -- Given a gorge player's position and view angles, return a position and orientation
-- -- for structure. Used to preview placement via a ghost structure and then to create it.
-- -- Also returns bool if it's a valid position or not.
-- local kPlacementDistance = 3
-- function SwipeBlink:GetPositionForStructure(player)

--     local isPositionValid = false
--     local foundPositionInRange = false
--     local structPosition = nil

--     local origin = player:GetEyePos() + player:GetViewAngles():GetCoords().zAxis * kPlacementDistance

--     -- Trace short distance in front
--     local trace = Shared.TraceRay(player:GetEyePos(), origin, CollisionRep.Default, PhysicsMask.AllButPCsAndRagdolls, EntityFilterTwo(player, self))

--     local displayOrigin = trace.endPoint

--     -- If we hit nothing, trace down to place on ground
--     if trace.fraction == 1 then

--         origin = player:GetEyePos() + player:GetViewAngles():GetCoords().zAxis * kPlacementDistance
--         trace = Shared.TraceRay(origin, origin - Vector(0, kPlacementDistance, 0), CollisionRep.Default, PhysicsMask.AllButPCsAndRagdolls, EntityFilterTwo(player, self))

--     end


--     -- If it hits something, position on this surface (must be the world or another structure)
--     if trace.fraction < 1 then

--         foundPositionInRange = true

--         if trace.entity == nil then
--             isPositionValid = true
--         elseif not trace.entity:isa("ScriptActor") and not trace.entity:isa("Clog") and not trace.entity:isa("Web") then
--             isPositionValid = true
--         end

--         displayOrigin = trace.endPoint

--         -- -- Can not be built on infestation
--         -- if GetIsPointOnInfestation(displayOrigin) then
--         --     isPositionValid = false
--         -- end

--         -- Don't allow dropped structures to go too close to techpoints and resource nozzles
--         if GetPointBlocksAttachEntities(displayOrigin) then
--             isPositionValid = false
--         end

--         -- Don't allow placing above or below us and don't draw either
--         local structureFacing = player:GetViewAngles():GetCoords().zAxis

--         if math.abs(Math.DotProduct(trace.normal, structureFacing)) > 0.9 then
--             structureFacing = trace.normal:GetPerpendicular()
--         end

--         if trace.normal:DotProduct(Vector(0, 1, 0)) < .5 then
--             isPositionValid = false
--         end
--         -- Coords.GetLookIn will prioritize the direction when constructing the coords,
--         -- so make sure the facing direction is perpendicular to the normal so we get
--         -- the correct y-axis.
--         local perp = Math.CrossProduct(trace.normal, structureFacing)
--         structureFacing = Math.CrossProduct(perp, trace.normal)

--         structPosition = Coords.GetLookIn(displayOrigin, structureFacing, trace.normal)

--     end

--     -- Shared:FadedMessage("Found: " .. tostring(foundPositionInRange)
--     --                        .. " is_valid: " .. tostring(isPositionValid)
--     --                        .. "My pos: " .. player:GetOrigin().x .. "/" .. player:GetOrigin().y .. "/" .. player:GetOrigin().z
--     --                        .. "Struct pos: " .. structPosition.origin.x .. "/" .. structPosition.origin.y .. "/" .. structPosition.origin.z
--     -- )
--     return foundPositionInRange, structPosition, isPositionValid

-- end

-- if (Client) then
--    function SwipeBlink:OnProcessIntermediate(input)

--       local player = self:GetParent()

--       if player then

--          self.showGhost, self.ghostCoords, self.placementValid = self:GetPositionForStructure(player)
--          self.showGhost = self.showGhost-- and self.minesLeft > 0

--       end

--    end
-- end

-- function SwipeBlink:GetDropStructureId()
--     return kTechId.Clog
-- end

-- function SwipeBlink:GetSuffixName()
--    return "clog"
-- end

-- function SwipeBlink:GetDropClassName()
--     return "Clog"
-- end

-- function SwipeBlink:GetDropMapName()
--     return Clog.kMapName
-- end

-- function SwipeBlink:GetGhostModelName()
--    return Clog.kModelName
-- end

-- function SwipeBlink:GetShowGhostModel()
--    return self.showGhost
-- end

-- function SwipeBlink:GetGhostModelCoords()
--    return self.ghostCoords
-- end

-- function SwipeBlink:GetIsPlacementValid()
--    return self.placementValid
-- end

-- function SwipeBlink:GetIsValidRecipient(recipient)
--    return (true)
-- end

Shared.LinkClassToMap("SwipeBlink", SwipeBlink.kMapName, networkVars)
