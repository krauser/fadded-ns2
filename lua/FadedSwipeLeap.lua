// ===================== Faded Mod =====================
//
// lua\FadedSwipeLeap.lua
//
//    Created by: Rio (rio@myrio.de)
//    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

Script.Load("lua/Weapons/Alien/Ability.lua")
Script.Load("lua/Weapons/Alien/LeapMixin.lua")
Script.Load("lua/FadedSwipeBlink.lua")

local networkVars =
   {
   }

class 'SwipeLeap' (SwipeBlink)

SwipeLeap.kMapName = "swipeleap"

AddMixinNetworkVars(LeapMixin, networkVars)

local onCreate = SwipeBlink.OnCreate
function SwipeLeap:OnCreate()
   onCreate(self)
end

function SwipeLeap:GetSecondaryTechId()
   return kTechId.Blink
end

function SwipeLeap:GetBlinkAllowed()
   return true
end

function SwipeLeap:GetHUDSlot()
   return 2
end

function SwipeLeap:GetIsBlinking()
   return self.secondaryAttacking
end

function SwipeLeap:GetRange()
   return SwipeLeap.kRange
end

-- Prototype: local width, height = weapon:GetMeleeBase()
function SwipeLeap:GetMeleeBase()
   return 1.6, 1.3--1.5, 1.2
end

-- Degressive damage (25 --> 50 --> 75) depending on the distance and the angle
function SwipeLeap:PerformMeleeAttack()
   local player = self:GetParent()
   local deniedSwipeAfterStab = self.kFadedDeniedSwipeAfterStab
   -- if (deniedSwipeAfterStab) then
   --    deniedSwipeAfterStab = deniedSwipeAfterStab < Shared.GetTime() - kFadedDeniedSwipeAfterStab
   -- else
   --    deniedSwipeAfterStab = true
   -- end
   if player then
      local didHit, hitObject, endPoint, surface = PerformGradualMeleeAttack(self, player, SwipeBlink.kDamage, self:GetRange())
   end

   -- self:RagdollUtilities(player)

end

-- -- Overwride the blink by hand
-- function SwipeLeap:OnSecondaryAttack(player)
--    Blink.OnSecondaryAttack(self, player)
-- end
-- function SwipeLeap:OnSecondaryAttackEnd(player)
--    Blink.OnSecondaryAttackEnd(self, player)
--    -- self.secondaryAttacking = false
-- end
function SwipeLeap:OnSecondaryAttack(player)

   local minTimePassed = not player:GetRecentlyBlinked()
   local hasEnoughEnergy = player:GetEnergy() > kStartBlinkEnergyCost
   if not player.etherealStartTime or minTimePassed and hasEnoughEnergy and player:GetBlinkAllowed() then

      -- Enter "ether" fast movement mode, but don't keep going ethereal when button still held down after
      -- running out of energy.
      if not self.secondaryAttacking then

         self:SetEthereal(player, true)
         

         self.timeBlinkStarted = Shared.GetTime()

         self.secondaryAttacking = true

      end

   end

   -- if (Server) then
   --    Ability.OnSecondaryAttack(self, player)
   -- end

end

function SwipeLeap:CanFadeTakeCorpse(player)
   return (false)
end

function SwipeLeap:OnSecondaryAttackEnd(player)

   if player.ethereal then

      self:SetEthereal(player, false)

   end

   -- if (Server) then
   --    Ability.OnSecondaryAttackEnd(self, player)
   -- end

   self.secondaryAttacking = false
   if (Server and kFadedEnzimeAfterBlink) then
      player:TriggerEnzyme(5)
   end

end

GetTexCoordsForTechId(kTechId.Swipe)
gTechIdPosition[kTechId.SwipeLeap] = kDeathMessageIcon.Swipe

Shared.LinkClassToMap("SwipeLeap", SwipeLeap.kMapName, networkVars)
