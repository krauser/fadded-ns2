// ===================== Faded Mod =====================
//
// lua\FadedTeam.lua
//
//    Created by: Rio (rio@myrio.de)
//    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

// Remove the respawn queue
//function Team:PutPlayerInRespawnQueue(player) end
function AlienTeam:PutPlayerInRespawnQueue(player) end
function MarineTeam:PutPlayerInRespawnQueue(player) end

// Remove player respawn ability
function MarineTeam:GetHasAbilityToRespawn() return false end
function AlienTeam:GetHasAbilityToRespawn() return false end

-- Don't spawn any initial structures
function AlienTeam:SpawnInitialStructures(techPoint) return nil, nil end
function MarineTeam:SpawnInitialStructures(techPoint) return nil, nil end

-- function Marine:ResetTeam()

--    local initialTechPoint = self:GetInitialTechPoint()

--    local tower, commandStructure = self:SpawnInitialStructures(initialTechPoint)

--     self.overflowres = 0
--     self.conceded = false

--     local players = GetEntitiesForTeam("Player", self:GetTeamNumber())
--     for p = 1, #players do
--        Print("Respawn of " .. players:GetName())
--        local player = players[p]
--        player:OnInitialSpawn(initialTechPoint:GetOrigin())
--        player:SetResources(self:GetStartingResources())
--     end

--     return commandStructure

-- end

-- Les PowerNodes sont détruits par défaut (sauf à la zone d'apparition des marines)
-- originalFunction3 = Class_ReplaceMethod( "MarineTeam", "OnResetComplete",
--         function(self)
--                 originalFunction3(self)

--                 local initialTechPoint = self:GetInitialTechPoint()
--                 for index, powerPoint in ientitylist(Shared.GetEntitiesWithClassname("PowerPoint")) do
--                         powerPoint:SetConstructionComplete()
--                         -- if powerPoint:GetLocationName() ~= initialTechPoint:GetLocationName() then
--                                 powerPoint:Kill()
--                                 powerPoint:SetLightMode(kLightMode.LowPower)
--                         -- else
--                         --         powerPoint:AddHealth(kPowerPointHealth + kPowerPointArmor)
--                         --         powerPoint:SetLightMode(kLightMode.Normal)
--                         -- end
--                 end
--         end
-- )
