// ===================== Faded Mod =====================
//
// lua\FadedTeamAttack.lua
//
//    Created by: Rio (rio@myrio.de)
//    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

local locale = LibCache:GetLibrary("LibLocales-1.0")

-- Check spawn Friendly Fire hit
function Marine:FadedOnTakeDamageSpawnProtection(damage, attacker, doer, point)
   if (kFadedModSpawnProtectionEnabled) then
      if (GetGamerules():GetGameState() == kGameState.Started) then
        -- anti friendly fire spawn kill condition
     -- Only use if a player hurt an other (remove warning from eclipse laser damage
     -- Own damage is not count as FriendlyFire
        if (attacker ~= nil and self:GetId() ~= attacker:GetId()) then
           if (damage > 0 and attacker:isa("Marine")
        and attacker:GetTeamNumber() == self:GetTeamNumber()) then
              local gameStartTime = FadedRoundTimer:GetGameStartTime()
              -- Is Spawn protection Over ? if no kill the player
              if (gameStartTime + kFadedModSpawnProtectionTime >= Shared.GetTime()) then
             -- attacker:FadedMessage(locale:ResolveString("FADED_TEAM_ATTACK"))
             -- attacker:Kill()
          self:ActivateNanoShield()
              end
           end
        end
      end
   end
end

-- Lower Marine Mental health on every Faded hit who hurt HP
function Marine:FadedOnTakeDamageSwipUpdateMentalHealth(damage, attacker, doer, point)
   if (attacker and attacker:isa("Fade") and doer
      and doer:isa("SwipeBlink") and damage > 0)
   then
      self:DecreaseMentalHealthByVal(kFadedSwipMentalHealthDamage)
   end
end

--[[
   Brief: Apply damage on a marine

   Note: The bug where a player become invincible when decreasing his
   healtPoint down to 1 (with the eclipse laser for exemple) has been
   removed with the "if (attacker ~= nil)" condition. No error are raise
   and the code execute himself properly. This was because the laser
   was not an attacker entity.
--]]
local marineOnTakeDamage = Marine.OnTakeDamage
function Marine:OnTakeDamage(damage, attacker, doer, point)
   self:FadedOnTakeDamageSpawnProtection(damage, attacker, doer, point)
   marineOnTakeDamage(self, damage, attacker, doer, point)
   self:FadedOnTakeDamageSwipUpdateMentalHealth(damage, attacker, doer, point)
   -- If the Fade is on Fire, transform back into a Fade
   if (self:GetTeamNumber() == 2
       and (doer and doer:isa("Flame") or doer:isa("Flamethrower"))) then
      self.kFadedTransformation = true
      self.kFadedRevealByFire = true
      if (attacker and attacker:isa("Marine")) then
         self.kFadedRevealAttackerName = attacker:GetName()
      end
   end
end

-- local fadeOnTakeDamage = Fade.OnTakeDamage
-- function Fade:OnTakeDamage(damage, attacker, doer, point)
--    Print("===> FadedDamage hook")
--    if (attacker and attacker:isa("Marine") and attacker.kFadedIsVeteran)
--    then
--       damage = damage * kFadedVeteranDamageBonusFactor
--       Print("===> Increade veteran damage")
--    end
--    fadeOnTakeDamage(self, damage, attacker, doer, point)
-- end

--[[
   Brief: Apply damage on the Faded
   It also give a mental health bonus to the marine who hit
--]]

function Fade:OnTakeDamageMentalHealthBonus(damage, attacker, doer, point)
   -- fadeOnTakeDamage(self, damage, attacker, doer, point)
   if (attacker and doer and attacker:isa("Marine") and damage > 0) then
      if (doer:isa("Shotgun")) then
     attacker:IncreaseMentalHealthByVal(kFadedShotgunOnHitMentalHealthBonus)
      elseif (doer:isa("Pistol")) then
     attacker:IncreaseMentalHealthByVal(kFadedPistolOnHitMentalHealthBonus)
      elseif (doer:isa("Rifle")) then
     attacker:IncreaseMentalHealthByVal(kFadedRifleOnHitMentalHealthBonus)
      elseif (doer:isa("Flamethrower")) then
     attacker:IncreaseMentalHealthByVal(kFadedFlameOnHitMentalHealthBonus)
      elseif (doer:isa("Flame")) then
     attacker:IncreaseMentalHealthByVal(kFadedFlameOnHitMentalHealthBonus)
      elseif (doer:isa("Grenade") or doer:isa("PulseGrenade")) then
     attacker:IncreaseMentalHealthByVal(kFadedGLOnHitMentalHealthBonus)
      elseif (doer:isa("ClusterGrenade") or doer:isa("ClusterFragment")) then
     attacker:IncreaseMentalHealthByVal(kFadedClusterOnHitMentalHealthBonus)
      elseif (doer:isa("NerveGasCloud")) then
     attacker:IncreaseMentalHealthByVal(kFadedGazOnHitMentalHealthBonus)
      elseif (doer:isa("NapalmCloud")) then
         attacker:IncreaseMentalHealthByVal(kFadedNapalmOnHitMentalHealthBonus)
      end
   end
end

function ViewModel:OnUpdateAnimationInput(modelMixin)

   PROFILE("ViewModel:OnUpdateAnimationInput")

   local parent = self:GetParent()
   if (parent) then
      parent:OnUpdateAnimationInput(modelMixin)
   end
end
