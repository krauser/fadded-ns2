
-- Thanks compmod for the function
local function AddFadedModTechChanges(techData)
   -- table.insert(techData, {
   --                        }


    // FADED: here added item

    table.insert(techData, { [kTechDataId] = kTechId.SwipeLeap,
      [kTechDataMapName] = SwipeLeap.kMapName,
      [kTechDataDamageType] = kSwipeDamageType,
      [kTechDataDisplayName] = "SWIPE_BLINK",
      [kTechDataTooltipInfo] = "SWIPE_TOOLTIP"})


    table.insert(techData, { [kTechDataId] = kTechId.LaySentryBattery,
      [kTechDataMapName] = LaySentryBattery.kMapName,
      [kTechDataHint] = "SENTRY_BATTERY_HINT",
      [kTechDataDisplayName] = "SENTRY_BATTERY",
      [kTechDataTooltipInfo] = "SENTRY_BATTERY_HINT",
      [kTechDataModel] = SentryBattery.kModelName,
      [kTechDataCostKey] = kMineCost })

    table.insert(techData, { [kTechDataId] = kTechId.LayFlameMines,
      [kTechDataMapName] = LayFlameMines.kMapName,
      [kTechDataPointValue] = kLayMinesPointValue,
      [kTechDataMaxHealth] = kMarineWeaponHealth,
      [kTechDataDisplayName] = "FLAME_MINE",
      [kTechDataModel] = Mine.kModelName,
      [kTechDataCostKey] = kMineCost })

    table.insert(techData, {
       [kTechDataId] = kTechId.FlameMine,
       [kTechDataMapName] = FlameMine.kMapName,
       [kTechDataHint] = "FLAME_MINE_HINT",
       [kTechDataDisplayName] = "FLAME_MINE",
       [kTechDataEngagementDistance] = kMineDetonateRange,
       [kTechDataMaxHealth] = kMineHealth,
       [kTechDataTooltipInfo] = "FLAME_MINE_TOOLTIP",
       [kTechDataMaxArmor] = kMineArmor,
       [kTechDataModel] = Mine.kModelName,
       [kTechDataPointValue] = kMinePointValue, })
    table.insert(techData, {
       [kTechDataId] = kTechId.HeavyShotgun,
       [kTechDataMaxHealth] = kMarineWeaponHealth,
       [kTechDataPointValue] = kShotgunPointValue,
       [kTechDataMapName] = HeavyShotgun.kMapName,
       [kTechDataDisplayName] = "HEAVYSHOTGUN",
       [kTechDataTooltipInfo] =  "HEAVYSHOTGUN_TOOLTIP",
       [kTechDataModel] = HeavyShotgun.kModelName,
       [kTechDataDamageType] = kShotgunDamageType,
       [kTechDataCostKey] = kShotgunCost,
       [kStructureAttachId] = kTechId.Armory,
       [kStructureAttachRange] = kArmoryWeaponAttachRange,
       [kStructureAttachRequiresPower] = true })
------------

    table.insert(techData, { [kTechDataId] = kTechId.NapalmGrenade,
      [kTechDataMapName] = NapalmGrenadeThrower.kMapName,
      [kTechDataDisplayName] = "NAPALM_GRENADE",
      [kTechDataTooltipInfo] =  "NAPALM_GRENADE_TOOLTIP",
      [kTechDataCostKey] = kGasGrenadeCost })


    table.insert(techData, { [kTechDataId] = kTechId.NapalmGrenadeProjectile,
      [kTechDataMapName] = NapalmGrenade.kMapName,
      [kTechDataDisplayName] = "NAPALM_GRENADE",
      [kTechDataTooltipInfo] =  "NAPALM_GRENADE_TOOLTIP",
      [kTechDataDamageType] = kDamageType.Flame })

------------
    table.insert(techData, { [kTechDataId] = kTechId.HealGrenade,
      [kTechDataMapName] = HealGrenadeThrower.kMapName,
      [kTechDataDisplayName] = "HEAL_GRENADE",
      [kTechDataTooltipInfo] =  "HEAL_GRENADE_TOOLTIP",
      [kTechDataCostKey] = kGasGrenadeCost })


    table.insert(techData, { [kTechDataId] = kTechId.HealGrenadeProjectile,
      [kTechDataMapName] = HealGrenade.kMapName,
      [kTechDataDisplayName] = "HEAL_GRENADE",
      [kTechDataTooltipInfo] =  "HEAL_GRENADE_TOOLTIP",
      [kTechDataDamageType] = kDamageType.Flame })
------------


    for index, record in ipairs(techData) do
       -- faded babbler player (to reduce extend)
       if record[kTechDataId] == kTechId.Skulk then
          record[kTechDataId] = kTechId.Skulk
          record[kTechDataUpgradeCost] = kSkulkUpgradeCost
          record[kTechDataMapName] = Skulk.kMapName
          record[kTechDataGestateName] = Skulk.kMapName
          record[kTechDataGestateTime] = kSkulkGestateTime
          record[kTechDataDisplayName] = "SKULK"
          record[kTechDataTooltipInfo] = "SKULK_TOOLTIP"
          record[kTechDataModel] = Skulk.kModelName
          record[kTechDataCostKey] = kSkulkCost
          record[kTechDataMaxHealth] = Skulk.kHealth
          record[kTechDataMaxArmor] = Skulk.kArmor
          record[kTechDataEngagementDistance] = kPlayerEngagementDistance
          record[kTechDataMaxExtents] = Vector(Babbler.kRadius + 0.06,
                                               Babbler.kRadius + 0.06,
                                               Babbler.kRadius + 1)
          record[kTechDataPointValue] = kSkulkPointValue
       end
       --    if record[kTechDataId] == kTechId.BabblerTech then
       --       record[kTechDataCostKey] = kBabblersResearchCost
       --       record[kTechDataResearchTimeKey] = kBabblersResearchTime
       --       record[kTechDataDisplayName] = "Babblers"
       --       record[kTechDataTooltipInfo] = "Allows gorges to create babblers."
       --    end
       --    if record[kTechDataId] == kTechId.DropFlamethrower then
       --       record[kStructureAttachId] = { kTechId.Armory, kTechId.AdvancedArmory }
       --    end
    end
end

local oldBuildTechData = BuildTechData
function BuildTechData()
   local techData = oldBuildTechData()
   AddFadedModTechChanges(techData)
   return techData
end


-- { [kTechDataId] = kTechId.Skulk, [kTechDataUpgradeCost] = kSkulkUpgradeCost, [kTechDataMapName] = Skulk.kMapName, [kTechDataGestateName] = Skulk.kMapName,                      [kTechDataGestateTime] = kSkulkGestateTime, [kTechDataDisplayName] = "SKULK",  [kTechDataTooltipInfo] = "SKULK_TOOLTIP",        [kTechDataModel] = Skulk.kModelName, [kTechDataCostKey] = kSkulkCost, [kTechDataMaxHealth] = Skulk.kHealth, [kTechDataMaxArmor] = Skulk.kArmor, [kTechDataEngagementDistance] = kPlayerEngagementDistance, [kTechDataMaxExtents] = Vector(Skulk.kXExtents, Skulk.kYExtents, Skulk.kZExtents), [kTechDataPointValue] = kSkulkPointValue},
