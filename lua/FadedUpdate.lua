// ===================== Faded Mod =====================
//
// lua\FadedUpdate.lua
//
//    Create by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

local sound_no_battery = "sound/NS2.fev/common/door_inoperable"
function updateFlashLightBattery(time_counter, marine)
   local lb = marine.kFadedLightBatteryLeft

   if (kFadedNoCustomFlashlight == true or Shared.GetCheatsEnabled()
      or (Server and GetGamerules():GetGameStarted() == false))
   then
      return
   end

   if (Client and PlayerUI_GetGameStartTime() == 0) then
      return
   end

   if (marine:GetFlashlightOn()) then
      marine.kFadedLightBatteryLeft = Clamp(lb - time_counter * 1, 0, kFadedLightTime)
   else
      marine.kFadedLightBatteryLeft = Clamp(lb + time_counter * kFadedLightRegePerSec, 0, kFadedLightTime)
   end
   -- if ((math.abs(lb) % (kFadedLightTime / 4)) < 1
   --       and math.abs(marine.kFadedLightLastNotification - lb) > 5)
   -- then
   --    if (lb == kFadedLightTime) then
   --      marine:FadedMessage("Battery full: " ..
   --                 string.format("%d", (math.abs(lb))) .. "s")
   --    elseif (marine:GetFlashlightOn()) then
   --      marine:FadedMessage("Battery left: " ..
   --                 string.format("%d", (math.abs(lb))) .. "s")
   --    end
   --    marine.kFadedLightLastNotification = lb
   -- end
   if (Server and lb <= 0 and marine:GetFlashlightOn()) then
      marine.kFadedLightBatteryLeft = 0
      marine:SetFlashlightOn(false)
      Shared.PlayPrivateSound(marine, sound_no_battery,
                              nil, 1.0, marine:GetOrigin())
      -- else
      -- local status = marine:isAimingEntity("Fade", kFadedLightDetectDist)
      -- if (status) then
      --    marine.flashlight:SetColor( kFadedLightDetectColor )
      -- else
      --    marine.flashlight:SetColor( kFadedLightNoDetectColor )
      -- end
   end
end
