// ===================== Faded Mod =====================
//
// lua\FadedUpdateClient.lua
//
//    Create by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================


local function isSameColor(c1, c2)
   return (c1.r == c2.r and
          c1.g == c2.g and
          c1.b == c2.b and
          c1.a == c2.a)
end

local function trackEntities(player)
   -- local trackEntities = GetEntitiesWithinRange("Alien", PlayerUI_GetOrigin(), kFadedLightDetectDist)
   local closestToCrosshair = nil
   local closestDistToCrosshair = math.huge
   local closestToCrosshairScale = nil
   local closestToCrosshairOpacity = nil
   local redFlashLightOn = (isSameColor(player.flashlight:GetColor()
                                        , kFadedLightDetectColor))
   local found = false

   -- for t = 1, #trackEntities do
   --    -- No red flashlight for babbler player
   --    if (not trackEntities[t]:isa("Skulk")) then
   --       local trackEntity = trackEntities[t]
   --       local inFrontData = player:GetViewCoords().zAxis:DotProduct(GetNormalizedVector(trackEntity:GetModelOrigin() - player:GetEyePos()))
   --       local isOnFlashLight = inFrontData >= 0.978
   --       if isOnFlashLight and trackEntity:GetIsAlive() then
   --          if (GetWallBetween(player:GetEyePos(), trackEntity:GetOrigin() + Vector(0, 0.8, 0)) == false) then
   --             found = true
   --             break
   --          end
   --       end
   --    end
   -- end -- endfor

   local sigted_ent = GetEntitiesForTeamWithinRangeInLOS(player, 0.978, "Fade", 2, PlayerUI_GetOrigin(), kFadedLightDetectDist)
   if (#sigted_ent > 0) then
      found = true
   end

   if (found and redFlashLightOn == false) then
      player.flashlight:SetColor( kFadedLightDetectColor )
   elseif (found == false and redFlashLightOn == true) then
      player.flashlight:SetColor( kFadedLightNoDetectColor )
   end
end

local function OnUpdateClient(deltaTime)
   local local_player = Client.GetLocalPlayer()

   FadedAmbiantSound(local_player, deltaTime)
   -- player:FadedMessage(locale:ResolveString(v))


   for index, player in ipairs(GetEntities("Marine")) do
      updateFlashLightBattery(deltaTime, player)
      if (player and kFadedNoCustomFlashlight == false
      and player:GetIsAlive() and player:GetFlashlightOn()
      and player:hasHunterAbility()) then
     -- player:FadedMessage("Battery: " .. tostring(player.kFadedLightBatteryLeft))
     -- local status = player:isAimingEntity("Fade", kFadedLightDetectDist)
     -- if (status) then
     --    player.flashlight:SetColor( kFadedLightDetectColor )
     -- else
     --    player.flashlight:SetColor( kFadedLightNoDetectColor )
     -- end
     trackEntities(player)
      end
      if (player.kFadedConvertToHeavyShotgun == true) then
     local hsg = player:GetWeaponInHUDSlot(1)
     if (hsg and hsg:isa("Shotgun")) then
        convertToHeavyShotgun(hsg)
     end
      end
   end
   -- FadedOnUpdate(player, deltaTime)
   -- clientUpdateMentalHealth(deltaTime)
end

Event.Hook("UpdateClient", OnUpdateClient)
