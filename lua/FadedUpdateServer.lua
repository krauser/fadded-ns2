// ===================== Faded Mod =====================
//
// lua\FadedUpdateServer.lua
//
//    Create by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

local time_counter = 0

local function updateDetectableFadeOnObjWithProto()
   local proto = GetEntitiesForTeam("PrototypeLab", 1)
   if (#proto > 0 and proto[1]:GetIsPowered() == true) then
      for _, faded in pairs(GetGamerules():GetTeam2():GetPlayers()) do
         -- No need to flood minimap with player babbler position
         if (faded) then
            if (not faded:isa("Skulk") and faded:GetIsAlive())
            then
               faded:SetIsSighted(true)
            end
         end
      end
   end
end

local function upHeavyShotgunPlayers(marine)
   if (marine.kFadedConvertToHeavyShotgun == true) then
      local hsg = marine:GetWeaponInHUDSlot(1)
      if (hsg and hsg:isa("Shotgun")) then
     convertToHeavyShotgun(hsg)
      end
   end
end



local function OnUpdateServer(deltaTime)
   local update_time = 0.5
   time_counter = time_counter + deltaTime
   -- -- Testing the blink loop code
   -- for index, player in ipairs(GetEntities("Fade")) do
   --    FadedOnUpdate(player, deltaTime)
   -- end
   if (time_counter > update_time
       and GetGamerules() and GetGamerules().team1) then
      local allPlayers = GetGamerules().team1:GetPlayers()

      updateDetectableFadeOnObjWithProto()
      for _, marine in pairs(allPlayers) do
     if (marine.SetIsSighted) then
        marine:SetIsSighted(true) -- Faded can see marine on minimap
     end

     if (marine and marine:GetIsAlive() and marine:isa("Marine")) then
        upHeavyShotgunPlayers(marine)
        updateFlashLightBattery(update_time, marine)
     end
      end
      time_counter = time_counter - update_time
   end
end

Event.Hook("UpdateServer", OnUpdateServer)
