// ===================== Faded Mod =====================
//
// lua\FadedVeteranMarine.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

-- Script.Load("lua/Marine.lua")
-- Script.Load("lua/FadedMarine.lua")

-- If true, the next marine created will be a veteran
-- kFadedIsNextMarineVeteran = false

class 'VeteranMarine' (Marine)

VeteranMarine.kMapName = "veteranmarine"
VeteranMarine.kModelName = PrecacheAsset("models/marine/male/male.model")

local networkVars =
{
}

function VeteranMarine:OnInitialized()
   Marine.OnInitialized(self)
end

function VeteranMarine:OnCreate()
   Marine.OnCreate(self)
end

function VeteranMarine:UpgradeMarine(player)
   -- local values = { origin = player:GetOrigin(), teamNumber = 1 }
   -- local entity = Server.CreateEntity(VeteranMarine.kMapName, values)
   -- Print("Upgraded marine entity = " .. tostring(entity))
   -- self.flashlight1 = Client.CreateRenderLight()

   -- self.flashlight1:SetType( RenderLight.Type_Spot )
   -- self.flashlight1:SetColor( Color(.8, .8, 1) )
   -- self.flashlight1:SetInnerCone( math.rad(30) )
   -- self.flashlight1:SetOuterCone( math.rad(35) )
   -- self.flashlight1:SetIntensity( 10 )
   -- self.flashlight1:SetRadius( 15 )
   -- self.flashlight1:SetGoboTexture("models/marine/male/flashlight1.dds")

   -- local coords = Coords(self:GetOrigin())
   -- coords.origin = coords.origin + coords.zAxis * 0.75

   -- self.flashlight1:SetCoords(coords)
   -- self.flashlight1:SetIsVisible(true)

   -- Print("VeteranMarine:UpgradeMarine called")
   player.kFadedMentalStrenght = kFadedVeteranMarineMentalStrenght
   player.kFadedIsVeteran = true
   -- self:SetSelected(1, true, true)
   -- self:SetSelected(2, true, true)

   -- Overload the marine function
   -- player.GetArmorLevel = VeteranMarine.GetArmorLevel
   -- player.GetWeaponLevel = VeteranMarine.GetWeaponLevel
   -- player.GetMaxSpeed = VeteranMarine.GetMaxSpeed
   -- player:GiveUpgrade(kTechId.Weapons3)
   -- player:GiveUpgrade(kTechId.Armor3)
   -- Print("Weapon level is now at " .. player:GetWeaponLevel())
   -- Print("Armor level is now at " .. player:GetArmorLevel())
   -- player.kWalkMaxSpeed = player.kWalkMaxSpeed * 10
   -- player.weaponUpgradeLevel = 3
   player:SetMaxArmor(kFadedVeteranArmor)
   player:SetArmor(kFadedVeteranArmor)
   player:SetMaxHealth(kFadedVeteranHealth)
   player:SetHealth(kFadedVeteranHealth)

   -- local jetpack = CreateEntity(Shotgun.kMapName, player:GetAttachPointOrigin(Jetpack.kAttachPoint), player:GetTeamNumber())
   -- local jetpack = CreateEntity(JetpackOnBack.kMapName, player:GetAttachPointOrigin(Jetpack.kAttachPoint), player:GetTeamNumber())
   -- jetpack:SetParent(player)
   -- jetpack:SetAttachPoint(Jetpack.kAttachPoint)
end

-- Force printing of the GUI of marine armslabs upgrade
-- even whithout arms labs
if (Client) then
   local playerUI_GetArmorLevel = PlayerUI_GetArmorLevel
   function PlayerUI_GetArmorLevel(researched)
      local p = Client.GetLocalPlayer()
      if (p and p:GetMaxHealth() == kFadedVeteranHealth
      and p:GetMaxArmor() == kFadedVeteranArmor) then
     return (1)
      else
     return (playerUI_GetArmorLevel(researched))
      end
   end

   local playerUI_GetWeaponLevel = PlayerUI_GetWeaponLevel
   function PlayerUI_GetWeaponLevel(researched)
      local p = Client.GetLocalPlayer()
      if (p and p:GetMaxHealth() == kFadedVeteranHealth
      and p:GetMaxArmor() == kFadedVeteranArmor) then
     return (1)
      else
     return (playerUI_GetWeaponLevel(researched))
      end
   end
end

-- local marineGetArmorLevel = Marine.GetArmorLevel
-- function Marine:GetArmorLevel()
--    local armor_lvl = marineGetArmorLevel(self)
--    if (kFadedIsNextMarineVeteran == true) then
--       armor_lvl = 1
--    else
--       armor_lvl = 3
--    end
--    return (armor_lvl)
-- end

-- local marineGetWeaponLevel = Marine.GetWeaponLevel
-- function Marine:GetWeaponLevel()
--    local weapon_lvl = marineGetWeaponLevel(self)
--    if (kFadedIsNextMarineVeteran == true) then
--       weapon_lvl = 1
--    else
--       weapon_lvl = 3
--    end
--    return (weapon_lvl)
-- end

-- Increase max walking speed of Veteran marine
local marineGetMaxSpeed = Marine.GetMaxSpeed
function Marine:GetMaxSpeed(possible)
   -- Print("VeteranMarine:GetMaxSpeed()")
   local speed_boost = 1
   if (self.GetMaxArmor and self.GetMaxHealth
      and self:GetMaxArmor() == kFadedVeteranArmor
          and self:GetMaxHealth() == kFadedVeteranHealth)
   then
      speed_boost = 1 + kFadedVeteranSpeedBonus
   end
   return (marineGetMaxSpeed(self, possible) * speed_boost)
end

Shared.LinkClassToMap("VeteranMarine", VeteranMarine.kMapName, networkVars, true)

-- Class_Reload("VeteranMarine", networkVars)
