// ===================== Faded Mod =====================
//
// lua\FadedHeavyShotgun.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

Script.Load("lua/WeaponOwnerMixin.lua")

local locale = nil
if (Server) then
   locale = LibCache:GetLibrary("LibLocales-1.0")
end

-- Denie taking the primary weapon if we carry a sentry battery
local weaponOwnerMixinSetActiveWeapon = WeaponOwnerMixin.SetActiveWeapon
function WeaponOwnerMixin:SetActiveWeapon(weaponMapName, keepQuickSwitchSlot)
   if (kFadedCanUsePrimaryWithBattery == false) then
      local foundWeapon = nil
      local foundBattery = nil
      for i = 0, self:GetNumChildren() - 1 do

     local child = self:GetChildAtIndex(i)
     if (child:isa("Weapon")) then
        if (child:GetMapName() == weaponMapName) then
           foundWeapon = child
        elseif (child:GetMapName() == LaySentryBattery.kMapName) then
           foundBattery = true
        end
     end

      end

      if foundWeapon then
     if (not (foundBattery == true)) then
        return (weaponOwnerMixinSetActiveWeapon(self, weaponMapName, keepQuickSwitchSlot))
     else
        if (Server) then
           self:FadedMessage(locale:ResolveString("FADED_NO_WEAPON_WITH_BATTERY"))
        end
     end
      end
      return (false)
   else -- Allows the primary weapon use
      return (weaponOwnerMixinSetActiveWeapon(self, weaponMapName, keepQuickSwitchSlot))
   end
end
