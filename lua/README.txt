##
## \file README.txt
## 
## \author Jean-Baptiste Laurent
## \<jeanbaptiste.laurent.pro@gmail.com\>
## 
## \date Started on  Tue Apr  8 16:12:47 2014 Jean-Baptiste Laurent
## \date Last update Thu Apr 10 16:52:47 2014 Jean-Baptiste Laurent
## \brief README
##

The Faded moded source code is now hosted on the GIT versioning server bitbucket.org
At: www.bitbucket.org/krauser/fadded-ns2/

Send me a mail if you want to contribute to the code, I will then be able to give you
read and write access to the code.

------------

If you want to custom the mod and change globals, overwrite them
in "FadedCustomGlobals.lua". You can find globals in "FadedGlobals.lua".

------------
