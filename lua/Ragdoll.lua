// ======= Copyright (c) 2003-2012, Unknown Worlds Entertainment, Inc. All rights reserved. =======
//
// lua\Ragdoll.lua
//
//    Created by:   Andreas Urwalek (andi@unknownworlds.com)
//    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
//    A fake ragdoll that dissolves after kRagdollDuration.
//
// ========= For more information, visit us at http://www.unknownworlds.com =====================

Script.Load("lua/Entity.lua")
Script.Load("lua/Mixins/BaseModelMixin.lua")
Script.Load("lua/InfestationMixin.lua")
Script.Load("lua/MapBlipMixin.lua")
Script.Load("lua/TeamMixin.lua")

function CreateRagdoll(fromEntity)

    local useModelName = fromEntity:GetModelName()
    local useGraphName = fromEntity:GetGraphName()
    local ragdoll = nil

    if useModelName and string.len(useModelName) > 0 and useGraphName and string.len(useGraphName) > 0 then

       ragdoll = CreateEntity(Ragdoll.kMapName, fromEntity:GetOrigin(), 2)
       if (ragdoll == nil) then return end
       if (fromEntity:isa("Player")) then
          ragdoll:SetPlayerName(fromEntity:GetName())
          if (fromEntity:isa("Marine")) then
             ragdoll:SetAsMarine()
          elseif (fromEntity:isa("Fade") and fromEntity.forPlayer == nil) then
             ragdoll:SetAsFaded()
          else -- hallucination corpses disapear
             if (Server) then
                ragdoll:AddTimedCallback(Ragdoll.TimeUp, 10)
             end
          end
       elseif (fromEntity:isa("Ragdoll") and fromEntity.playerName) then
          -- Shared:FadedMessage("Copy from ragdoll to an other")
          ragdoll.marineRagdoll = fromEntity.marineRagdoll
          ragdoll.fadedRagdoll = fromEntity.fadedRagdoll
          ragdoll.playerName = fromEntity.playerName
          ragdoll.mapBlipRefreshDone = false
          ragdoll.creationTime = fromEntity.creationTime
          -- Shared:FadedMessage("New ragdoll: "
          --              .. "is marine: " .. tostring(ragdoll.marineRagdoll)
          --              .. "is faded: " .. tostring(ragdoll.fadedRagdoll)
          --              .. "with name: " .. ragdoll.playerName)
       end

       ragdoll:SetCoords(fromEntity:GetCoords())
       ragdoll:SetModel(useModelName, useGraphName)

       -- ---
       -- local id = FindNearestEntityId("Marine", ragdoll:GetOrigin())
       -- local nearest_marine = Shared.GetEntity(id)
       -- if (nearest_marine) then
       --    local orig = nearest_marine:GetAttachPointOrigin(Jetpack.kAttachPoint)
       --    ragdoll:SetParent(nearest_marine)
       --    ragdoll:SetAttachPoint(Jetpack.kAttachPoint)
       -- end
       -- ---

       if fromEntity.GetPlayInstantRagdoll and fromEntity:GetPlayInstantRagdoll() then
          ragdoll:SetPhysicsType(PhysicsType.Dynamic)
          ragdoll:SetPhysicsGroup(PhysicsGroup.RagdollGroup)
       else
          ragdoll:SetPhysicsGroup(PhysicsGroup.SmallStructuresGroup)
       end
       -- ragdoll:SetPhysicsGroup(PhysicsGroup.CollisionGeometryGroup)
       ragdoll:CopyAnimationState(fromEntity)

    end
    return (ragdoll)
end


class 'Ragdoll' (Entity)

-- local kRagdollDuration = 60*5

Ragdoll.kMapName = "ragdoll"

local networkVars =
   {
      marineRagdoll = "boolean",
      fadedRagdoll = "boolean",
      creationTime = "float",
      refresh_time = "float",
      playerName = "string (32)",
      mapBlipRefreshDone = "boolean",
   }

AddMixinNetworkVars(BaseModelMixin, networkVars)
AddMixinNetworkVars(ModelMixin, networkVars)
AddMixinNetworkVars(TeamMixin, networkVars)

function Ragdoll:OnCreate()

   Entity.OnCreate(self)

   self.marineRagdoll = false
   self.fadedRagdoll = false
   self.creationTime = Shared.GetTime()
   self.refresh_time = Shared.GetTime()
   self.playerName = "Kill me I am a spy"
   self.mapBlipRefreshDone = false
   -- if Server then
   --     self:AddTimedCallback(Ragdoll.TimeUp, kRagdollDuration)
   -- end

   self:SetUpdates(true)

   InitMixin(self, BaseModelMixin)
   InitMixin(self, ModelMixin)
   InitMixin(self, InfestationMixin)
   InitMixin(self, TeamMixin)
   -- self.attachPointOrigin = self:GetOrigin()
   if (Server) then
      -- Used with the "battery collision" trick so corpse stay
      -- attached even if we are going outside the relevancy range
      self:SetRelevancyDistance(kMaxRelevancyDistance - 10)
   end
end

function Ragdoll:OnInitialized()
   if (Entity.OnInitialized) then
      Entity.OnInitialized(self)
   end
   if (Server) then
      // This Mixin must be inited inside this OnInitialized() function.
      if not HasMixin(self, "MapBlip") then
         InitMixin(self, MapBlipMixin)
      end
   end
   -- self.attachPointOrigin = self:GetOrigin()
end

-- function Ragdoll:GetModelOrigin()
--    return (self:GetOrigin())
-- end


-- function Ragdoll:OnAdjustModelCoords(self, modelCoords)
--    -- Shared:FadedMessage("OnAdjustModelCoords Called")
--    return (self._modelCoords)
-- end

-- function Ragdoll:GetAttachPointOrigin()
--    if (self.attachPointOrigin) then
--       Shared:FadedMessage("GetAttachPointOrigin Called")
--       return (self.attachPointOrigin)
--    else
--       return (self:GetOrigin())
--    end
-- end

-- function Ragdoll:GetAttachPointCoords()
--    if (self.attachPointOrigin) then
--       Shared:FadedMessage("GetAttachPointCoords Called")
--       return (self.attachPointOrigin)
--    else
--       return (self:GetOrigin())
--    end
-- end

if (Server) then
   -- Refresh the mapblip once after a sec (do not have it on creation)
   function Ragdoll:OnUpdate(deltaTime)
      if ((not self.mapBlipRefreshDone and self.creationTime + 1 < Shared.GetTime())
             or self.refresh_time + 0.5 < Shared.GetTime())
      then
         -- Shared:FadedMessage("Refresh done")
         -- -- end
         self.mapBlipRefreshDone = true
         self:MarkBlipDirty()
         self.refresh_time = Shared.GetTime()
      end
   end
end

-- So corpses are displayed on the map
function Ragdoll:OnGetMapBlipInfo()
   -- if (Server) then

   -- if (Client) then
   --    local player = Client.GetLocalPlayer()
   --    if (player and player:GetTeamNumber() == 1) then
   --       return false
   --    end
   -- end
   local blipType = nil
   local blipTeam = nil
   -- For some reason, blipTeam = -1 is auto converted to a neutral team
   -- and setting it to neutral directly is not working ...
   if (self:isAFaded()) then
      blipType = kMinimapBlipType.Fade
      blipTeam = -1
   elseif (self:isAMarine()) then
      blipType = kMinimapBlipType.Marine
      blipTeam = -1
   else
      blipType = kMinimapBlipType.Undefined
      blipTeam = -1
   end
   --return success, blipType, blipTeam, isAttacked, isParasited
   return true, blipType, blipTeam, false, false
   -- end
   -- return (false)
end

-- SetOrigin = "Sets the location of an entity",^M
-- SetAngles = "Sets the angles of an entity",^M
-- SetCoords = "Sets both both location and angles"^M


function Ragdoll:OnUpdateAnimationInput(modelMixin)
   PROFILE("Ragdoll:OnUpdateAnimationInput")
   modelMixin:SetAnimationInput("alive", false)
   modelMixin:SetAnimationInput("built", true)
   modelMixin:SetAnimationInput("active", true)
end

function Ragdoll:GetInfestationRadius()
   return (kFadedDeadBodyInfestationRadius)
end

function Ragdoll:OnUpdatePoseParameters()
   self:SetPoseParam("grow", 1)
end

function Ragdoll:TimeUp()
   DestroyEntity(self)
end

function Ragdoll:SetAsMarine()
   self.marineRagdoll = true
end
function Ragdoll:SetAsFaded()
   self.fadedRagdoll = true
end

function Ragdoll:isAMarine()
   return (self.marineRagdoll)
end
function Ragdoll:isAFaded()
   return (self.fadedRagdoll)
end

-- Use to set and get the original player name
-- in order to free the Player entity and set the
-- fake marine name
function Ragdoll:SetPlayerName(name)
   self.playerName = name
end
function Ragdoll:GetPlayerName()
   return (self.playerName)
end

function Ragdoll:GetTechId()
   return (nil)
end

-- function Ragdoll:OnUpdateRender()

--     PROFILE("Ragdoll:OnUpdateRender")

--     local remainingLifeTime = kRagdollDuration - (Shared.GetTime() - self.creationTime)
--     if remainingLifeTime <= 1 then

--         local dissolveAmount = Clamp(1 - remainingLifeTime, 0, 1)
--         self:SetOpacity(1-dissolveAmount, "dissolveAmount")

--     end

-- end

function Ragdoll:TakeDamage(damage, attacker, doer, point, direction)
   if (doer and (doer:isa("Flame") or doer:isa("Flamethrower"))) then
      self:DeductHealth(damage, attacker, doer, false, false, true)
   end
end

function Ragdoll:GetAttached()
   return (self)
end

function Ragdoll:GetTeamNumber()
   return (2) -- Dead body are on the faded team (for infestation)
end
function Ragdoll:GetIsAlive()
   return (false)
end

if Server then

   function Ragdoll:OnTag(tagName)

      PROFILE("Ragdoll:OnTag")

      if tagName == "death_end" then
         self:SetPhysicsType(PhysicsType.Dynamic)
         self:SetPhysicsGroup(PhysicsGroup.RagdollGroup)
         -- self:SetPhysicsType(PhysicsType.DynamicServer)
         -- self:SetPhysicsGroup(PhysicsGroup.RagdollGroup)
      end

   end

end

Shared.LinkClassToMap("Ragdoll", Ragdoll.kMapName, networkVars)
