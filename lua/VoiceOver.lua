--[[
   ======= Copyright (c) 2003-2011, Unknown Worlds Entertainment, Inc. All rights reserved. =======

   file: lua\VoiceOver.lua

   Created by: Andreas Urwalek (andi@unknownworlds.com)
   Updated by JB for the Faded mode
   * To ease future change we directly overload the entire VoiceOver File
   * Change:
   ** 'AlienGrowl' has been added to kVoiceId
   ** The following line has been added to kSoundData
   [kVoiceId.AlienGrowl] = { Sound = ...}
   ** Function FadedGrowl added to support MentalHealth reduction
   ** 'local kAlienMenu' has been changed
   ** Ping time has been reduced (PingInViewDirection)
   ** Growl usage is limited every 10s depending on kFadedGrowlInterval
   ** Line 167: Change the need_order by "sound/NS2.fev/alien/common/hatch"
   ** Code for the devour sound

   ========= For more information, visit us at http://www.unknownworlds.com =====================
--]]

LEFT_MENU = 1
RIGHT_MENU = 2
kMaxRequestsPerSide = 5

kVoiceId = enum ({

                    'None', 'VoteEject', 'VoteConcede', 'Ping',

                    'RequestWeld', 'MarineRequestMedpack', 'MarineRequestAmmo', 'MarineRequestOrder',
                    'MarineTaunt', 'MarineTauntExclusive', 'MarineCovering', 'MarineFollowMe', 'MarineHostiles', 'MarineLetsMove',

                    'AlienRequestHarvester', 'AlienRequestHealing', 'AlienRequestMist', 'AlienRequestDrifter',
                    'AlienTaunt', 'AlienFollowMe', 'AlienChuckle', 'EmbryoChuckle',
                    -- Faded
                    'AlienGrowl', 'AlienWhisper', 'AlienDevour', 'BabblerPlayer', 'SpectateFade', 'NoFade', 'ToggleBeeper',
                    'BabblerTaunt', 'BabblerAttack'


                 })

local kAlienTauntSounds =
   {
      [kTechId.Skulk] = "sound/NS2.fev/alien/voiceovers/chuckle",
      [kTechId.Gorge] = "sound/NS2.fev/alien/gorge/taunt",
      [kTechId.Lerk] = "sound/NS2.fev/alien/lerk/taunt",
      [kTechId.Fade] = "sound/NS2.fev/alien/fade/taunt",
      [kTechId.Onos] = "sound/NS2.fev/alien/onos/taunt",
      [kTechId.Embryo] = "sound/NS2.fev/alien/common/swarm",
   }
for _, tauntSound in pairs(kAlienTauntSounds) do
   PrecacheAsset(tauntSound)
end
PrecacheAsset("sound/NS2.fev/alien/babbler/idle")
PrecacheAsset("sound/NS2.fev/alien/babbler/fetch")
PrecacheAsset("sound/NS2.fev/alien/babbler/attack_jump")
PrecacheAsset("sound/faded.fev/alien/voiceovers/growl")

local function VoteEjectCommander(player)

   if player then
      GetGamerules():CastVoteByPlayer(kTechId.VoteDownCommander1, player)
   end

end

local function VoteConcedeRound(player)

   if player then
      GetGamerules():CastVoteByPlayer(kTechId.VoteConcedeRound, player)
   end

end

local function GetLifeFormSound(player)

   if player and player:isa("Alien") then
      return kAlienTauntSounds[player:GetTechId()] or ""
   end

   return ""

end

local function PingInViewDirection(player)

   if player and (not player.lastTimePinged or player.lastTimePinged + 15 < Shared.GetTime()) then

      local startPoint = player:GetEyePos()
      local endPoint = startPoint + player:GetViewCoords().zAxis * 40
      local trace = Shared.TraceRay(startPoint, endPoint,  CollisionRep.Default, PhysicsMask.Bullets, EntityFilterOne(player))

      // seems due to changes to team mixin you can be assigned to a team which does not implement SetCommanderPing
      local team = player:GetTeam()
      if team and team.SetCommanderPing then
         player:GetTeam():SetCommanderPing(trace.endPoint)
      end

      player.lastTimePinged = Shared.GetTime()

   end

end

local function GiveWeldOrder(player)

   if ( player:isa("Marine") or player:isa("Exo") ) and player:GetArmor() < player:GetMaxArmor() then

      for _, marine in ipairs(GetEntitiesForTeamWithinRange("Marine", player:GetTeamNumber(), player:GetOrigin(), 8)) do

         if player ~= marine and marine:GetWeapon(Welder.kMapName) then
            marine:GiveOrder(kTechId.AutoWeld, player:GetId(), player:GetOrigin(), nil, true, false)
         end

      end

   end

end

local function FadedNoFade(player)
   NoFadedChatCommand(player)
   return (nil)
end

local function FadedToggleBeeper(player)
   TogglerBeeperChatCommand(player)
   return (nil)
end

local function FadedSpectateFaded(player)
   SpectateFadedChatCommand(player)
   return (nil)
end

local function FadedBabblerPlayer(player)
   BabblerPlayerChatCommand(player)
   return (nil)
end

local function FadedRequestMedpack(player)
   local marines = GetEntitiesForTeamWithinRange("Marine", 1, player:GetOrigin(), 25)

   for _, medic in ipairs(marines) do
      if (medic and medic:IsMedic() and medic:GetIsAlive()) then
         if (medic ~= player and player:GetMaxHealth() > player:GetHealth()) then
            medic:GiveOrder(kTechId.Defend, player:GetId(), nil)
         end
      end
   end
   -- return "sound/NS2.fev/marine/voiceovers/medpack"
end

-- Perform a growl and alter mental health of nearby marine
local function FadedGrowl(player)
   local sound = nil

   if (player and player:isa("Fade")) then
      local next_growl = player.lastTimeGrowled + kFadedGrowlInterval
      if (Shared.GetTime() > next_growl) then
         sound = player:FadedGrowl()
         if (sound) then
            player.lastTimeGrowled = Shared.GetTime()
         end
      end
   end
   if (sound) then
      return (sound)
   else
      return kAlienTauntSounds[kTechId.Fade]
   end
end

local function FadedWhisper(player)
   if (player) then
      local marines = GetEntitiesForTeamWithinRange("Marine", 1, player:GetOrigin(), 30)
      if (#marines > 0) then
         for index, marine in ipairs(marines) do
            if (marine and marine:GetIsAlive()) then
               Server.SendNetworkMessage(marine, "PlayWhisperSound", {}, true)
               -- Shared.PlaySound(marine, s.sound, s.volume)
            end
         end
         Server.SendNetworkMessage(player, "PlayWhisperSound", {}, true)
         -- Shared.PlaySound(player, s.sound, s.volume)
      end
   end
   return (nil)
end

local function BabblerTaunt(player)
   return ("sound/NS2.fev/alien/babbler/fetch")
end
local function BabblerAttack(player)
   return ("sound/NS2.fev/alien/babbler/attack_jump")
end


-- local function RequestDevourMarineCorpse(player)
--    if (player and player:isa("Fade")) then
--       local sound = player:DevourMarineCorpse() -- ("sound/NS2.fev/alien/structures/death_large")
--       if (sound) then
--         return (sound)
--       end
--    end
--    return kAlienTauntSounds[kTechId.Fade]
-- end

local kSoundData =
   {

      // always part of the menu
      [kVoiceId.VoteEject] = { Function = VoteEjectCommander },
      [kVoiceId.VoteConcede] = { Function = VoteConcedeRound },

      [kVoiceId.Ping] = { Function = PingInViewDirection, Description = "REQUEST_PING", KeyBind = "PingLocation" },

      // marine vote menu
      [kVoiceId.RequestWeld] = { Sound = "sound/NS2.fev/marine/voiceovers/weld", Function = GiveWeldOrder, Description = "REQUEST_MARINE_WELD", KeyBind = "RequestWeld", AlertTechId = kTechId.None },
      [kVoiceId.MarineRequestMedpack] = { Function = FadedRequestMedpack, Description = "REQUEST_MARINE_MEDPACK", KeyBind = "RequestHealth", Sound = "sound/NS2.fev/marine/voiceovers/medpack", AlertTechId = kTechId.MarineAlertNeedMedpack },
      [kVoiceId.MarineRequestAmmo] = { Sound = "sound/NS2.fev/marine/voiceovers/ammo", Description = "REQUEST_MARINE_AMMO", KeyBind = "RequestAmmo", AlertTechId = kTechId.MarineAlertNeedAmmo },
      [kVoiceId.MarineRequestOrder] = { Sound = "sound/NS2.fev/marine/voiceovers/ack", Description = "REQUEST_MARINE_ORDER",  KeyBind = "RequestOrder", AlertTechId = kTechId.MarineAlertNeedOrder },

      [kVoiceId.MarineTaunt] = { Sound = "sound/NS2.fev/marine/voiceovers/taunt", Description = "REQUEST_MARINE_TAUNT", KeyBind = "Taunt", AlertTechId = kTechId.None },
      [kVoiceId.MarineTauntExclusive] = { Sound = "sound/NS2.fev/marine/voiceovers/taunt_exclusive", Description = "REQUEST_MARINE_TAUNT", KeyBind = "Taunt", AlertTechId = kTechId.None },
      [kVoiceId.MarineCovering] = { Sound = "sound/NS2.fev/marine/voiceovers/covering", Description = "REQUEST_MARINE_COVERING", AlertTechId = kTechId.None },
      [kVoiceId.MarineFollowMe] = { Sound = "sound/NS2.fev/marine/voiceovers/follow_me", Description = "REQUEST_MARINE_FOLLOWME", AlertTechId = kTechId.None },
      [kVoiceId.MarineHostiles] = { Sound = "sound/NS2.fev/marine/voiceovers/hostiles", Description = "REQUEST_MARINE_HOSTILES", AlertTechId = kTechId.None },
      [kVoiceId.MarineLetsMove] = { Sound = "sound/NS2.fev/marine/voiceovers/lets_move", Description = "REQUEST_MARINE_LETSMOVE", AlertTechId = kTechId.None },


      // alien vote menu
      [kVoiceId.AlienRequestHarvester] = { Sound = "sound/NS2.fev/alien/voiceovers/follow_me", Description = "REQUEST_ALIEN_HARVESTER", KeyBind = "RequestOrder", AlertTechId = kTechId.AlienAlertNeedHarvester },
      [kVoiceId.AlienRequestMist] = { Sound = "sound/NS2.fev/alien/common/hatch", Description = "REQUEST_ALIEN_MIST", KeyBind = "RequestHealth", AlertTechId = kTechId.AlienAlertNeedMist },
      [kVoiceId.AlienRequestDrifter] = { Sound = "sound/NS2.fev/alien/voiceovers/follow_me", Description = "REQUEST_ALIEN_DRIFTER", KeyBind = "RequestAmmo", AlertTechId = kTechId.AlienAlertNeedDrifter },   
      [kVoiceId.AlienTaunt] = { Sound = "", Function = GetLifeFormSound, Description = "REQUEST_ALIEN_TAUNT", KeyBind = "Taunt", AlertTechId = kTechId.None },
      [kVoiceId.AlienFollowMe] = { Sound = "sound/NS2.fev/alien/voiceovers/follow_me", Description = "REQUEST_ALIEN_FOLLOWME", AlertTechId = kTechId.None },
      [kVoiceId.AlienChuckle] = { Sound = "sound/NS2.fev/alien/voiceovers/chuckle", Description = "REQUEST_ALIEN_CHUCKLE", AlertTechId = kTechId.None },
      [kVoiceId.EmbryoChuckle] = { Sound = "sound/NS2.fev/alien/structures/death_large", Description = "REQUEST_ALIEN_CHUCKLE", AlertTechId = kTechId.None },

      [kVoiceId.AlienGrowl] = { Sound = "sound/NS2.fev/alien/fade/taunt", Function = FadedGrowl, Description = "REQUEST_ALIEN_GROWL", KeyBind = "Taunt", AlertTechId = kTechId.None },
      [kVoiceId.AlienWhisper] = { Function = FadedWhisper, Description = "REQUEST_ALIEN_WHISPER"},
      [kVoiceId.BabblerPlayer] = { Function = FadedBabblerPlayer, Description = "FADED_REQUEST_BABBLERPLAYER" },
      [kVoiceId.SpectateFade] = { Function = FadedSpectateFaded, Description = "FADED_REQUEST_SPECTATEFADE" },
      [kVoiceId.NoFade] = { Function = FadedNoFade, Description = "FADED_REQUEST_NO_FADE" },
      [kVoiceId.ToggleBeeper] = { Function = FadedToggleBeeper, Description = "FADED_REQUEST_TOGGLE_BEEPER" },

      [kVoiceId.BabblerTaunt] = { Sound = "sound/NS2.fev/alien/babbler/fetch", Description = "REQUEST_ALIEN_TAUNT", KeyBind = "Taunt", AlertTechId = kTechId.None },
      [kVoiceId.BabblerAttack] = { Sound = "sound/NS2.fev/alien/babbler/attack_jump", Description = "REQUEST_ALIEN_CHUCKLE", KeyBind = "RequestAmmo", AlertTechId = kTechId.None },
      -- [kVoiceId.AlienDevour] = { Sound = "", Function = RequestDevourMarineCorpse, Description = "REQUEST_ALIEN_DEVOUR", KeyBind = "RequestHealth", AlertTechId = kTechId.None },
   }

-- Initialize the female variants of the voice overs and precache.
for _, soundData in pairs(kSoundData) do

   if soundData.Sound ~= nil and string.len(soundData.Sound) > 0 then

      PrecacheAsset(soundData.Sound)

      -- Do not look for female versions of alien sounds.
      if string.find(soundData.Sound, "sound/NS2.fev/alien/", 1) == nil then

         soundData.SoundFemale = soundData.Sound .. "_female"
         PrecacheAsset(soundData.SoundFemale)

      end

   end

end

function GetVoiceSoundData(voiceId)
   return kSoundData[voiceId]
end

local kMarineMenu =
   {
      [LEFT_MENU] = { kVoiceId.RequestWeld,
                      kVoiceId.MarineRequestMedpack,
                      -- kVoiceId.MarineRequestAmmo,
                      kVoiceId.MarineTaunt,
                      kVoiceId.MarineCovering,
                      kVoiceId.MarineLetsMove,
      },

      [RIGHT_MENU] = { kVoiceId.Ping,
                       kVoiceId.NoFade,
                       kVoiceId.SpectateFade,
                       kVoiceId.BabblerPlayer,
                       kVoiceId.ToggleBeeper,
      }
   }

-----------------------------------------------------
-----------------------------------------------------
-- Hook
local kAlienMenu =
   {
      [LEFT_MENU] =
         {
            -- Marine voice
            -- kVoiceId.MarineTaunt,
            kVoiceId.RequestWeld,
            kVoiceId.MarineRequestAmmo,
            kVoiceId.MarineRequestOrder,
            kVoiceId.MarineCovering,
            kVoiceId.MarineLetsMove,
         },
      [RIGHT_MENU] =
         {
            kVoiceId.MarineFollowMe,
            kVoiceId.MarineHostiles,
            kVoiceId.AlienWhisper,
            -- Alien voice
            kVoiceId.AlienGrowl,
            kVoiceId.AlienChuckle,
         }
   }

local kSpectatorMenu =
   {
      [LEFT_MENU] =
         {
            kVoiceId.BabblerPlayer,
            kVoiceId.SpectateFade
         },
      [RIGHT_MENU] =
         {
            kVoiceId.NoFade,
         }
   }

--------------------------------------
--------------------------------------
--------------------------------------
-- local kAlienMenu =
-- {
--     [LEFT_MENU] = { kVoiceId.AlienRequestHealing, kVoiceId.AlienRequestDrifter, kVoiceId.Ping },
--     [RIGHT_MENU] = { kVoiceId.AlienTaunt, kVoiceId.AlienChuckle }
-- }
-- local kAlienMenu = kFadedAlienMenuHook

local kRequestMenus =
   {
      ["Spectator"] = kSpectatorMenu, -- spectator menu here
      ["AlienSpectator"] = kSpectatorMenu,
      ["MarineSpectator"] = kSpectatorMenu,

      ["Marine"] = kMarineMenu,
      ["JetpackMarine"] = kMarineMenu,
      ["Exo"] =
         {
            [LEFT_MENU] = { kVoiceId.RequestWeld, kVoiceId.MarineRequestOrder, kVoiceId.Ping },
            [RIGHT_MENU] = { kVoiceId.MarineTaunt, kVoiceId.MarineCovering, kVoiceId.MarineFollowMe, kVoiceId.MarineHostiles, kVoiceId.MarineLetsMove }
         },

      ["Skulk"] =
         {
            [LEFT_MENU] = { kVoiceId.BabblerTaunt,
                            kVoiceId.BabblerAttack
            },
            [RIGHT_MENU] = {kVoiceId.NoFade,
                            kVoiceId.SpectateFade,
                            kVoiceId.BabblerPlayer
            }

         }, -- no menu for the skulk: TODO: ajouter taut of the babbler

      ["Gorge"] =
         {
            [LEFT_MENU] = { kVoiceId.AlienRequestHealing, kVoiceId.AlienRequestDrifter, kVoiceId.AlienRequestHarvester, kVoiceId.Ping },
            [RIGHT_MENU] = { kVoiceId.AlienTaunt, kVoiceId.AlienChuckle }
         },

      ["Lerk"] = {},
      ["Fade"] = kAlienMenu,
      ["Onos"] = {},
      ["Embryo"] =
         {
            [LEFT_MENU] = { kVoiceId.AlienRequestMist },
            [RIGHT_MENU] = { kVoiceId.AlienTaunt, kVoiceId.EmbryoChuckle }
         }
   }

function GetRequestMenu(side, className)

   local menu = kRequestMenus[className]
   if menu and menu[side] then
      return menu[side]
   end

   return { }

end

if Client then

   function GetVoiceDescriptionText(voiceId)

      local descriptionText = ""

      local soundData = kSoundData[voiceId]
      if soundData then
         descriptionText = Locale.ResolveString(soundData.Description)
      end

      return descriptionText

   end

   function GetVoiceKeyBind(voiceId)

      local soundData = kSoundData[voiceId]
      if soundData then
         return soundData.KeyBind
      end

   end

end


local kAutoMarineVoiceOvers = {}
local kAutoAlienVoiceOvers = {}
