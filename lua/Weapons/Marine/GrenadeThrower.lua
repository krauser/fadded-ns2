--  ======= Copyright (c) 2003-2011, Unknown Worlds Entertainment, Inc. All rights reserved. =======
--
--  lua\Weapons\Marine\GrenadeThrower.lua
--
--     Created by:   Andreas Urwalek (andi@unknownworlds.com)
--
--     Base class for hand grenades. Override GetViewModelName and GetGrenadeMapName in implementation.
-- // Faded changes:  ALlow grenade to be planted on wall and detonated
-- //
-- //
-- //
--
--  ========= For more information, visit us at http:-- www.unknownworlds.com =====================

Script.Load("lua/LiveMixin.lua") -- Faded

local kGrenadeAnimationSpeedIncrease = 2.75
local kDefaultVariantData = kMarineVariantData[ kDefaultMarineVariant ]
function GenerateMarineGrenadeViewModelPaths(grenadeType)

    local viewModels = { male = { }, female = { } }

    local function MakePath(prefix, suffix)
        return "models/marine/grenades/" .. prefix .. grenadeType .. "_view" .. suffix .. ".model"
    end

    for variant, data in pairs(kMarineVariantData) do
        viewModels.male[variant] = PrecacheAssetSafe(MakePath("", data.viewModelFilePart), MakePath("", kDefaultVariantData.viewModelFilePart))
    end

    for variant, data in pairs(kMarineVariantData) do
        viewModels.female[variant] = PrecacheAssetSafe(MakePath("female_", data.viewModelFilePart), MakePath("female_", kDefaultVariantData.viewModelFilePart))
    end

    return viewModels

end

class 'GrenadeThrower' (Weapon)

GrenadeThrower.kMapName = "grenadethrower"

kMaxHandGrenades = 2

local kGrenadeVelocity = 18

local networkVars =
{
    grenadesLeft = "integer (0 to ".. kMaxHandGrenades ..")",
    lastPlant = "time", -- Faded
}

AddMixinNetworkVars(LiveMixin, networkVars)

local function ThrowGrenade(self, player)

    if Server or (Client and Client.GetIsControllingPlayer()) then

        local viewCoords = player:GetViewCoords()
        local eyePos = player:GetEyePos()

        local startPointTrace = Shared.TraceCapsule(eyePos, eyePos + viewCoords.zAxis, 0.2, 0, CollisionRep.Move, PhysicsMask.PredictedProjectileGroup, EntityFilterTwo(self, player))
        local startPoint = startPointTrace.endPoint

        local direction = viewCoords.zAxis

        if startPointTrace.fraction ~= 1 then
            direction = GetNormalizedVector(direction:GetProjection(startPointTrace.normal))
        end

        local grenadeClassName = self:GetGrenadeClassName()
        local grenade = player:CreatePredictedProjectile(grenadeClassName, startPoint, direction * kGrenadeVelocity, 0.7, 0.45)

    end

end

function GrenadeThrower:OnCreate()

    Weapon.OnCreate(self)
    InitMixin(self, LiveMixin) -- Faded

    self.grenadesLeft = kMaxHandGrenades

    self:SetModel(self:GetThirdPersonModelName())
    self.lastPlant = Shared.GetTime() -- Faded
    self:SetHealth(1)
    self:SetArmor(0)

end

function GrenadeThrower:OnDraw(player, previousWeaponMapName)

    Weapon.OnDraw(self, player, previousWeaponMapName)

    --  Attach weapon to parent's hand.
    self:SetAttachPoint(Weapon.kHumanAttachPoint)

end

function GrenadeThrower:OnPrimaryAttack(player)

    if self.grenadesLeft > 0 then

        if not self.primaryAttacking then
            self:TriggerEffects("grenade_pull_pin")
        end

        self.primaryAttacking = true
    else
        self.primaryAttacking = false
    end

end

function GrenadeThrower:OnPrimaryAttackEnd(player)
    self.primaryAttacking = false
end

function GrenadeThrower:OnTag(tagName)

    local player = self:GetParent()

    if tagName == "throw" then

        if player then

            ThrowGrenade(self, player)
            if not player:GetDarwinMode() then
                self.grenadesLeft = math.max(0, self.grenadesLeft - 1)
            end
            self:SetIsVisible(false)
            self:TriggerEffects("grenade_throw")

        end

    elseif tagName == "attack_end" then

        if self.grenadesLeft == 0 then
            self.readyToDestroy = true
        else
            self:SetIsVisible(true)
        end

    end

end

function GrenadeThrower:GetHUDSlot()
    return 5
end

function GrenadeThrower:GetViewModelName()
    assert(false)
end

function GrenadeThrower:GetAnimationGraphName()
    assert(false)
end

function GrenadeThrower:GetWeight()
    return kHandGrenadeWeight
end

function GrenadeThrower:GetGrenadeClassName()
    assert(false)
end

function GrenadeThrower:OnUpdateAnimationInput(modelMixin)

    modelMixin:SetAnimationInput("activity", self.primaryAttacking and "primary" or "none")
    modelMixin:SetAnimationInput("grenadesLeft", self.grenadesLeft)

end

function GrenadeThrower:OverrideWeaponName()
    return "grenades"
end

if Server then

    function GrenadeThrower:OnProcessMove(input)

        Weapon.OnProcessMove(self, input)

        local player = self:GetParent()
        if player then

            local activeWeapon = player:GetActiveWeapon()
            local allowDestruction = self.readyToDestroy or (activeWeapon ~= self and self.grenadesLeft == 0)

            if allowDestruction then

                if activeWeapon == self then

                    self:OnHolster(player)
                    player:SwitchWeapon(1)

                end

                player:RemoveWeapon(self)
                DestroyEntity(self)

            end

        end

    end

end

function GrenadeThrower:GetCatalystSpeedBase()
    return self.primaryAttacking and kGrenadeAnimationSpeedIncrease or 1
end


------------- Faded update

-- local function PlantGrenade(self, player)
--    -- Shared:FadedMessage("Plant grenade called")
-- end

function GrenadeThrower:GetGhostModelName()
    return LookupTechData(self:GetDropStructureId(), kTechDataModel)
end

function GrenadeThrower:GetIsValidRecipient(recipient)

   -- if self:GetParent() == nil and recipient and not GetIsVortexed(recipient) and recipient:isa("Marine") then

   --     local laymines = recipient:GetWeapon(LayMines.kMapName)
   --     return laymines == nil

   -- end

   return false

end

function _CreateGrenade(self, player, position, normal, direction)

   local mine = CreateEntity(self:GetMapName(), position, player:GetTeamNumber())
   mine:SetOwner(player)

   local coords = Coords.GetTranslation(position)
   coords.yAxis = normal
   coords.zAxis = direction

   coords.xAxis = coords.yAxis:CrossProduct(coords.zAxis)
   coords.xAxis:Normalize()

   coords.zAxis = coords.xAxis:CrossProduct(coords.yAxis)
   coords.zAxis:Normalize()

   mine:SetCoords(coords)
   return (true)
   -- mine:Kill()
end

local function explode_grenade(self, player)
   local detonate = false
   local next_plant = player.kFadedLastChatWarning + 1
   if (Shared.GetTime() > next_plant) then
      player.kFadedLastChatWarning = Shared.GetTime()
      if (player and Server) then
         for _, stick_grenade in ipairs(GetEntitiesForTeam("GrenadeThrower", 1))
         do
            -- Do not detonate the one the player is holding in his hands
            if (stick_grenade and stick_grenade:GetOwner()
                and stick_grenade:GetOwner():GetId() == player:GetId()) then
               local grenadeClassName = stick_grenade:GetGrenadeClassName()
               player:CreatePredictedProjectile(grenadeClassName,
                                                stick_grenade:GetOrigin(),
                                                Vector(0, 0, 0), 0.7, 0.45)
               DestroyEntity(stick_grenade)
               detonate = true
            end
         end
      end

      if (detonate == false) then
         player:FadedMessage("No sticked grenade in range to detonate")
      else
         player:TriggerEffects("mine_arm")
      end
   end
end

function Axe:OnSecondaryAttack(player)
   explode_grenade(self, player)
end
function Axe:GetHasSecondary(player)
   return (true)
end
function Welder:OnSecondaryAttack(player)
   explode_grenade(self, player)
end
function Welder:GetHasSecondary(player)
   return (true)
end

-- Replace temporaly the Grenade type for the alternative fire
-- and restore it after
function GrenadeThrower:OnSecondaryAttack(player)
   local next_plant = self.lastPlant + 0.7
   if (Shared.GetTime() > next_plant) then
      self.lastPlant = Shared.GetTime()

      local success = SpecialWeaponHandling(self, player, _CreateGrenade, 2.5)
      if success == 1 and player then
         self:SetIsVisible(false)
         self:TriggerEffects("grenade_throw")
         player:TriggerEffects("mine_explode")

         if not player:GetDarwinMode() then
            self.grenadesLeft = math.max(0, self.grenadesLeft - 1)
         end

         local activeWeapon = player:GetActiveWeapon()
         local allowDestruction = (self.grenadesLeft == 0)

         if allowDestruction then

            if activeWeapon == self then

               self:OnHolster(player)
               player:SwitchWeapon(1)

            end

            player:RemoveWeapon(self)
            if (Server) then
               DestroyEntity(self)
            end

         end
      end

   end
end

function GrenadeThrower:GetIsDroppable()
   return false
end

function GrenadeThrower:GetDropStructureId()
   return kTechId.Mine
end


-- Allow alternative fire
function GrenadeThrower:GetHasSecondary(player)
   return kFadedGrenadeSecondary
end

function GrenadeThrower:GetDropClassName()
    return "Mine"
end

function GrenadeThrower:GetGhostModelName()
   return LookupTechData(self:GetDropStructureId(), kTechDataModel)
end

function GrenadeThrower:GetShowGhostModel()
   return self.showGhost
end

function GrenadeThrower:GetGhostModelCoords()
   return self.ghostCoords
end

function GrenadeThrower:GetIsPlacementValid()
--   return self.placementValid
   return (true)
end

function GrenadeThrower:OnTakeDamage(damage, attacker, doer, point, direction, damageType, preventAlert)
   if (self:GetOwner()) then
      if (attacker) then
         attacker:TriggerEffects("mine_arm")
      end
      if (Server) then
         DestroyEntity(self)
      end
   end
end

function GrenadeThrower:OnKill(attacker, doer, point, direction)
   if (attacker) then
      attacker:TriggerEffects("mine_arm")
   end
   if (Server) then
      DestroyEntity(self)
   end
end

function GrenadeThrower:GetReceivesStructuralDamage()
   return (self:GetOwner() ~= nil) -- If owner, is planted
end

function GrenadeThrower:GetCanDieOverride()
   return (self:GetOwner() ~= nil)
end
-------------

Shared.LinkClassToMap("GrenadeThrower", GrenadeThrower.kMapName, networkVars)
