// ===================== Faded Mod =====================
//
// lua\FadedFade.lua
//
//    Created by: Rio (rio@myrio.de)
//    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

Script.Load("lua/FadedSwipeLeap.lua")
Script.Load("lua/FadedAcidRockets.lua")

local locale = nil
local strformat = string.format
-- Prevent chat message flood (1msg per sec at most)
local canSeeYouHelpTime = Shared.GetTime()
if (Server) then
   locale = LibCache:GetLibrary("LibLocales-1.0")

   Script.Load("lua/FadedFade_Server.lua")
elseif (Client) then
   Script.Load("lua/FadedFade_Client.lua")
end

Fade.kLeapForce = 22
Fade.kLeapTime = 0.2
-- Id of the marine who has an hallucination
-- Use in the OnCreate
kFadedHallucinationMarineId = nil
kFadedStabTriggered = false

local networkVars =
   {
      isScanned = "boolean",
      shadowStepping = "boolean",
      timeShadowStep = "private time",
      shadowStepDirection = "private vector",
      hasDoubleJumped = "private compensated boolean",
      landedAfterBlink = "private compensated boolean",
      landedAfterBlink = "compensated boolean",

      etherealStartTime = "private time",
      etherealEndTime = "private time",

      -- True when we're moving quickly "through the ether"
      ethereal = "boolean",

      -- Wall grip. time == 0 no grip, > 0 when grip started.
      wallGripTime = "private compensated time",
      -- the normal that the model will use. Calculated the same way as the skulk
      wallGripNormalGoal = "private compensated vector",
      wallGripAllowed = "private compensated boolean",

      leaping = "compensated boolean",
      timeOfLeap = "private compensated time",
      timeOfLastJumpLand = "private compensated time",

      forPlayer = "integer",
      lastTimeGrowled = "time",

      kFadedLocalVocalChat = "boolean",
      -- En lien avec FadedMarine.lua
      kFadedTransformation = "boolean",
      kFadedTimeout = "time",
      kFadedCode1 = "boolean",
      kFadedCode2 = "boolean",
      kFadedCode3 = "boolean",

      kFadedCorpseMessageHelp = "boolean",
      kFadedEatingStartTime = "time",
      kFadedDeniedSwipeAfterStab = "time",
   }

AddMixinNetworkVars(BaseMoveMixin, networkVars)
AddMixinNetworkVars(GroundMoveMixin, networkVars)
AddMixinNetworkVars(CameraHolderMixin, networkVars)
AddMixinNetworkVars(DissolveMixin, networkVars)

-- if the user hits a wall and holds the use key and the resulting speed is < this, grip starts
Fade.kWallGripMaxSpeed = 10
-- once you press grip, you will slide for this long a time and then stop. This is also the time you
-- have to release your movement keys, after this window of time, pressing movement keys will release the grip.
Fade.kWallGripSlideTime = 0.2
-- after landing, the y-axis of the model will be adjusted to the wallGripNormal after this time.
Fade.kWallGripSmoothTime = 0.6

-- how to grab for stuff ... same as the skulk tight-in code
Fade.kWallGripRange = 0.2
Fade.kWallGripFeelerSize = 0.25



local onCreate = Fade.OnCreate
function Fade:OnCreate()
   onCreate(self)
   InitMixin(self, WallMovementMixin)
   -- self:SetHasUmbra(true, 1000)
   -- self:SetMaxHealth(100)
   -- self:SetHealth(13)

   self.wallGripTime = 0
   -- self.silenceLevel = 3

   self.leaping = false
   self.lastTimeGrowled = Shared.GetTime()
   self.darkVisionEnabled = false
   self:SetDarkVision(true)
   self.kFadedLocalVocalChat = false
   self.kFadedTransformation = false
   self.kFadedTimeout = Shared.GetTime()
   self.kFadedCode1 = false
   self.kFadedCode2 = false
   self.kFadedCode3 = false
   self.kFadedCorpseMessageHelp = true
   self.kFadedEating = false
   kFadedEatingStartTime = nil
   -- if (kFadedStabTriggered and not kFadedSpawnFadeHallucination) then
      self.kFadedDeniedSwipeAfterStab = Shared.GetTime()
   --    kFadedStabTriggered = false
   -- else
      -- self.kFadedDeniedSwipeAfterStab = Shared.GetTime() - kFadedDeniedSwipeAfterStab
   -- end
   if (Client) then
      self.GUIVeteran = GetGUIManager():CreateGUIScript("FadedGUIVeteran")
      self.GUICloak = GetGUIManager():CreateGUIScript("FadedGUICloak")
   end
   if (kFadedSpawnFadeHallucination == true) then
      -- Print("Hallu has callback")
      -- self:SetName("Hallucination")
      -- self.kMapName = "Hallucination"
      -- self:SetMaxHealth(26)
      -- self:SetHealth(26)
      -- self:SetMaxArmor(0)
      -- self:SetArmor(0)
      self.forPlayer = kFadedHallucinationMarineId
      self:SetPropagate(Entity.Propagate_Callback)
      -- else
      -- Print("Real faded called")
   end
end

local fadeOnDestroy = Fade.OnDestroy
function Fade:OnDestroy()
   fadeOnDestroy(self)
   if (Client) then
      GetGUIManager():DestroyGUIScript(self.GUIVeteran)
      GetGUIManager():DestroyGUIScript(self.GUICloak)
   end
end

-- Start amount of armor of the Faded
-- The faded receive a bonus for each additional marine in game
function Fade:GetBaseArmor()
   local fadeArmor = kFadeBaseArmor + kMarineNb * kFadeArmorPerMarine
   fadeArmor = fadeArmor / kFadeNb
   if (kMultiFadeMaluce) then
      if (kFadeNb > 1) then
	 fadeArmor = fadeArmor / kFadedHPReductionPerFade
      end
   end
   return fadeArmor
end

-- Start amount of Health of the Faded
-- The faded receive a bonus for each additional marine in game
function Fade:GetBaseHealth()
   local fadeHealth = kFadeBaseHealth + kMarineNb * kFadeHealthPerMarine
   fadeHealth = fadeHealth / kFadeNb
   if (kMultiFadeMaluce) then
      if (kFadeNb > 1) then
	 fadeHealth = fadeHealth / kFadedHPReductionPerFade
      end
   end
   return fadeHealth
end

-- function Fade:GetArmorFullyUpgradedAmount()
--     return kFadeBaseArmor + kMarineNb * kFadeArmorPerMarine
-- end

local fadeOnInitialize = Fade.OnInitialized
function Fade:OnInitialized()
   fadeOnInitialize(self)

   if (Client) then
      self:AddHelpWidget("FadedGUIFadeLeapHelp", kFadedModGUIHelpLimit)
      self:AddHelpWidget("FadedGUIFadeWallgripHelp", kFadedModGUIHelpLimit)
      self:AddHelpWidget("FadedGUIFadeVisionHelp", kFadedModGUIHelpLimit)
   end
end

-- TODO: a checker
function Fade:SetDarkVision(state)
   Alien.SetDarkVision(self, state)
   self.darkVisionEnabled = state
end

function Fade:GetIsLeaping()
   return self.leaping
end

function Fade:GetIsWallGripping()
   return self.wallGripTime ~= 0
end

function Fade:OverrideUpdateOnGround(onGround)
   return (onGround or self:GetIsWallGripping())
end

function Fade:GetCanStep()
   return self:GetIsOnGround() and not self:GetIsWallGripping()
end

function Fade:ModifyGravityForce(gravityTable)
   if self:GetIsWallGripping() or self:GetIsOnGround() then
      gravityTable.gravity = 0
   elseif self:GetCrouching() then
      gravityTable.gravity = gravityTable.gravity * 4

   end
end

function Fade:OnWorldCollision(normal)
   self.wallGripAllowed = normal.y < 0.5 and not self:GetCrouching()
end

function Fade:HandleButtons(input)
   local wallGripPressed = bit.band(input.commands, Move.MovementModifier) ~= 0 and bit.band(input.commands, Move.Jump) == 0

   if not self:GetIsWallGripping() and wallGripPressed and self.wallGripAllowed then

      -- check if we can grab anything around us
      local wallNormal = self:GetAverageWallWalkingNormal(Fade.kWallGripRange, Fade.kWallGripFeelerSize)

      if wallNormal then

	 self.wallGripTime = Shared.GetTime()
	 self.wallGripNormalGoal = wallNormal
	 self:SetVelocity(Vector(0,0,0))

      end

   else

      -- we always abandon wall gripping if we flap (even if we are sliding to a halt)
      local breakWallGrip = bit.band(input.commands, Move.Jump) ~= 0 or input.move:GetLength() > 0 or self:GetCrouching() or bit.band(input.commands, Move.SecondaryAttack) ~= 0

      if breakWallGrip then

	 self.wallGripTime = 0
	 self.wallGripNormal = nil
	 self.wallGripAllowed = false

      end

   end

   Alien.HandleButtons(self, input)
end

function Fade:CalcWallGripSpeedFraction()

   local dt = (Shared.GetTime() - self.wallGripTime)
   if dt > Fade.kWallGripSlideTime then
      return 0
   end
   local k = Fade.kWallGripSlideTime
   return (k - dt) / k

end

local postUpdateMove = Fade.PostUpdateMove
function Fade:PostUpdateMove(input, runningPrediction)
   postUpdateMove(self, input, runningPrediction)

   if (self:GetIsWallGripping()) then
      if (Shared.GetTime() > (self.wallGripTime + 0.08)) then
	 local energyCost = input.time * kWallGripEnergyCost
	 if (self:GetEnergy() < energyCost) then
	    self.wallGripTime = 0
	    self.wallGripOrigin = nil
	 else
	    self:DeductAbilityEnergy(energyCost)
	 end
      end
   end

   -- wallgrip, recheck wallwalknormal as soon as the slide has stopped
   if self:GetIsWallGripping() and not self.wallGripRecheckDone and self:CalcWallGripSpeedFraction() == 0 then
      self.wallGripNormalGoal = self:GetAverageWallWalkingNormal(Lerk.kWallGripRange, Lerk.kWallGripFeelerSize)
      self.wallGripRecheckDone = true

      if not self.wallGripNormalGoal then
	 self.wallGripTime = 0
	 self.wallGripOrigin = nil
	 self.wallGripRecheckDone = false
      end
   end

   if self.leaping and self:GetIsOnGround() and (Shared.GetTime() > self.timeOfLeap + Fade.kLeapTime) then
      self.leaping = false
   end
end

-- local velocity = self:GetVelocity() * 0.5
-- local forwardVec = self:GetViewAngles():GetCoords().zAxis
-- local newVelocity = velocity + GetNormalizedVectorXZ(forwardVec) * kLeapForce

-- // Add in vertical component.
-- newVelocity.y = kLeapVerticalForce * forwardVec.y + kLeapVerticalForce * 0.5 + ConditionalValue(velocity.y < 0, velocity.y, 0)

-- self:SetVelocity(newVelocity)

-- self.leaping = true
-- self.wallWalking = false
-- self:DisableGroundMove(0.2)

-- self.timeOfLeap = Shared.GetTime()

function Fade:GetBlinkAllowed()
   return false
end

function Fade:OnLeap()
   if (self:GetEnergy() > kLeapEnergyCost) then
      local newVelocity = self:GetViewCoords().zAxis * self.kLeapForce * 1
      self:SetVelocity(newVelocity)

      self.leaping = true
      self.onGround = false
      self.onGroundNeedsUpdate = true

      self.timeOfLeap = Shared.GetTime()
      self.timeOfLastJump = Shared.GetTime()
      self:DeductAbilityEnergy(kLeapEnergyCost)
   end
end

function Fade:GetAirMoveScalar()

   if self:GetVelocityLength() < 8 then
      return 1.0
   elseif self.leaping then
      return 0.3
   end

   return 0
end

function Fade:GetCanBeSetOnFire()
   return kFadedModFadeCanBeSetOnFire
end

-- No shadow step!
function Fade:TriggerShadowStep(direction)
end

-- Make the Faded silent
function Alien:UpdateSilenceLevel()
   self.silenceLevel = 3
end

function Fade:UpdateSilenceLevel()
   self.silenceLevel = 3
end

function Fade:GetEffectParams(tableParams)
   tableParams[kEffectFilterSilenceUpgrade] = true
end

function Alien:GetEffectParams(tableParams)
   tableParams[kEffectFilterSilenceUpgrade] = true
end

-- Speed of the Fade
local fadeGetMaxSpeed = Fade.GetMaxSpeed
function Fade:GetMaxSpeed(possible)
   local max_speed = 6.2

   return (max_speed)
end

-- Return the number of Fade alive in the team
function Fade:getNbFadedAlive()
   local kFadeNbAlive = 0
   local faded_team = GetEntitiesForTeamWithinRange("Fade", 2,
				    self:GetOrigin(),
				    kMultiFadedGrowlMalusDistance)
   for _, fade in ipairs(faded_team) do
      if (fade and fade:GetIsAlive()) then
	 kFadeNbAlive = kFadeNbAlive + 1
      end
   end
   -- Safety check
   if (kFadeNbAlive == 0) then
      kFadeNbAlive = 1
   end
   return (kFadeNbAlive)
end

-- MentalHealth impact on the growl
-- The Growl reduce nearby marine MentalHealth
-- Return the growl sound if the faded has enough energy, taunt otherwise
function Fade:FadedGrowl()
   local	damage
   local	distance

   if (self:GetEnergy() > kFadedGrowlEnergyCost) then
      self:DeductAbilityEnergy(kFadedGrowlEnergyCost)
      distance = kFadedGrowlMentalHealthDamage / kFadedGrowlDiminutionByMeter
      -- Make a round for each range level and applie to all marine
      -- So a marine nearby will receive N*level he is in.
      -- ex: for 30dmg and 2dmg reduction
      -- * 30/2 => 15 loop, each loop substract 2 damages
      -- * Marine at 1 meter will receive 15 time 2 damage ==> 30dmg
      local kFadeNbAlive = self:getNbFadedAlive()
      while (distance > 0) do
	 local marines_around = GetEntitiesWithinRange("Marine",
						       self:GetOrigin(),
						       distance)
	 for index, marine in ipairs(marines_around) do
	    -- Divise growl damage, so its not Over powered
	    local growl_damage = kFadedGrowlDiminutionByMeter / kFadeNbAlive
	    marine:DecreaseMentalHealthByVal(growl_damage)
	 end
	 distance = distance - kFadedGrowlDiminutionByMeter
      end
      return ("sound/faded.fev/alien/voiceovers/growl")
   end
   return (nil)
end

-- function ViewModel:OnGetIsRelevant(player)
--     return self:GetParent() == player
-- end

function Fade:OnGetIsRelevant(player)
   -- Allow or not the Faded to see marines hallucination
   if (kFadedAllowFadedToSeeHallucination) then
      if (player:GetTeamNumber() == 2) then
	 return (true)
      end
   end
   return (self.forPlayer == player:GetId())
end

function Alien:IsUsingTeamVocalChat()
   return (self.kFadedLocalVocalChat)
end

-- Custom FOV for real view detection (160 degrees)
-- local function customPlayerFOV()
--    return (160)
-- end

-- Return true if there is a marine of team1
-- at less than distance who view the Player
function Player:isAliveMarineAroundInView(distance)
   local function _isSeen(seen, viewver)
      for _, entitySeen in ipairs(seen) do
	 if (entitySeen:isa("Player")
	     and GetAreEnemies(viewver, entitySeen)) then
		return true, viewver
	 end
      end
      return false, nil
   end

   local alive_marine = GetEntitiesWithinRange("Marine",
					       self:GetOrigin(),
					       distance)
   -- Is there an alive marine nearby who see a Faded ?
   for _, amarine in ipairs(alive_marine) do
      if (amarine and amarine:GetIsAlive()
	  and GetAreEnemies(amarine, self)) then

	 -- Trix to use a 160 field of view
	 -- local old_GetFov = amarine.GetFov
	 -- local old_GetMinimapFov = amarine.GetMinimapFov
	 -- amarine.GetFov = customPlayerFOV
	 -- amarine.GetMinimapFov = customPlayerFOV
	 local seen = GetEntitiesWithinRangeInView("Player",
						   distance,
						   amarine)
	 -- amarine.GetFov = old_GetFov
	 -- amarine.GetMinimapFov = old_GetMinimapFov
	 local ret, entity = _isSeen(seen, amarine)
	 --------- If not found check just around
	 --------- Because "beeing glue" on a marine is considered as not viewed
	 if (ret == false) then
	    seen = GetEntitiesWithinRange("Player",
				  amarine:GetOrigin() + Vector(0, 0, 2),
				  1.6)
	    ret, entity = _isSeen(seen, amarine)
	 end
	 if (ret) then
	    return true, entity
	 end
	 ---------

      end
   end
   return false, nil
end

-- Check for nearby corpse and change the Faded
function Fade:useMarineCorpse(input)
   local marineWhoSeeYou = nil
   local marine_alive_around = false
   local key = (bit.band(input.commands, Move.Use) ~= 0)
   local status, ent = false
   if (key and self.kFadedTransformation == false) then
      -- local dead_marine = GetEntitiesWithinRange("Marine",
      -- 						 self:GetOrigin(),
      -- 						 kFadedEatUseDistance)
      status, ent = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
      -- Is there a body to take ?
      -- for _, marine in ipairs(dead_marine) do
	 if (status) then
	    haveCorpseAround = true
	    -- marine_alive_around, marineWhoSeeYou = self:isAliveMarineAroundInView(kFadedTakeBodyDistance)
	    -- -- If no, transform and destroy the body
	    -- if (marine_alive_around == false) then
	       if (Client) then
		  local msg = {corpse_name = ent:GetPlayerName()}
		  Client.SendNetworkMessage("OnTriggerTransform", msg)
	       end
	       self.kFadedTransformation = true
	    end
	    -- break
	 -- end
      -- end
      -- if (Server and self.kFadedTransformation == false
      -- 	  and status == true
      -- 	  and canSeeYouHelpTime < Shared.GetTime() - 1) then
      -- 	 self:FadedMessage(strformat(locale:ResolveString("FADED_CANT_USE_CORPSE"), marineWhoSeeYou:GetName()))
      -- 	 -- Server.SendNetworkMessage(self, "ResetEatingTimer", {}, true)
      -- 	 canSeeYouHelpTime = Shared.GetTime()
      -- end

      -- self:CheckTransformation()
   end
end


-- Called when the eating process is completed
-- Get the corpse by hand to be sure there are no confusion
function Fade:DevourMarineCorpse()
   local status, corpse = nil
   -- local dead_marine = GetEntitiesWithinRange("Marine",
   -- 					      self:GetOrigin(),
   -- 					      -- +0.5 for safety
   -- 					      kFadedEatUseDistance + 0.5)
   -- -- Is there a body to take ?
   -- for _, marine in ipairs(dead_marine) do
   --    if (marine and marine:GetIsAlive() == false) then
   -- 	 corpse = marine
   --    end
   -- end
   status, corpse = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
   if (status) then
      -- Play SOUND

      -- if (Server) then
      -- 	 self:FadedMessage(strformat(locale:ResolveString("FADED_CANT_USE_CORPSE"), marineWhoSee:GetName()))
      -- end

      -- local msg = {corpse_name = marine:GetId()}
      -- Client.SendNetworkMessage("TriggerEating", msg)
      -- corpse:SetHasBeenUsed()
      self:ResetEatingFraction()
      if (Client) then
	 local msg = {corpse_name = corpse:GetPlayerName()}
	 Client.SendNetworkMessage("OnTriggerEating", msg)
      end
   end
end

-- Process to eat a corpse
-- the Server notify
function Fade:eatMarineCorpse()
   -- local dead_marine = GetEntitiesWithinRange("Marine",
   -- 					      self:GetOrigin(),
   -- 					      kFadedEatUseDistance)
   -- Is there à corpse to devour
   local status = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
   if (not status) then
      self:ResetEatingFraction()
   else
      if (self.kFadedEatingStartTime == nil) then
	 local start = Shared.GetTime()
	 self:SetEatingStartTime(start)
      else
	 if (self:HasFinishedToEat()) then
	    self:DevourMarineCorpse(marine)
	 end
      end
   end
   return (nil)
end

-- See PlayerInput.lua (of NS2) for the key id
-- Note: Do not use this function anymore, try to use SendKeyEvent()
--       unless you need to run Server side code
local last_change = Shared.GetTime()
local playerHandleButtons = Fade.HandleButtons
function Fade:HandleButtons(input)
   playerHandleButtons(self, input)
   -- alienHandleButtons(self, input)
   -- Print("OnSendKeyEvent called")
   local msg = nil
   -- local player = Client.GetLocalPlayer()
   local buyButtonPressed = bit.band(input.commands, Move.Drop) ~= 0
   if (buyButtonPressed and last_change < Shared.GetTime() + 0.5) then
      self.kFadedLocalVocalChat = not self.kFadedLocalVocalChat
      if (self.kFadedLocalVocalChat == true) then
	 msg = "Team vocal chat enable"
      else
	 msg = "Team vocal chat disable"
      end
      if (Server) then
	 self:FadedMessage(msg)
      end
      last_change = Shared.GetTime()
   end

   self:useMarineCorpse(input)
end

-- Return the eating progress of the Faded (0.00 --> 1.00)
-- Reset if someone see him
function Fade:GetEatingFraction()
   if (self:GetIsOnFire() == false) then
      if (self.kFadedEatingStartTime) then
	 local current = Shared.GetTime() - self.kFadedEatingStartTime
	 local end_time = kFadedEatingTime
	 local progress = ((current / end_time) * 100) / 100
	 if (progress > 1) then
	    progress = 1
	 end
	 return (progress)
      end
   else
      self:ResetEatingFraction()
   end
   return (0)
end

-- Return true if the Faded has finished to eat a corpse
function Fade:HasFinishedToEat()
   if (self:GetEatingFraction() >= 1) then
      return (true)
   else
      return (false)
   end
end

-- Set the timer when the Faded start eating and notify the Server
function Fade:SetEatingStartTime(_time)
   self.kFadedEatingStartTime = _time
   -- if (Client) then
   --    Client.SendNetworkMessage("IsFadeEating", {time = _time})
   -- end

end

-- Reset the eating timer and notify the Server
function Fade:ResetEatingFraction()
   -- if (Client) then
   --    Client.SendNetworkMessage("IsFadeEating", {time = nil})
   -- end
   self.kFadedEatingStartTime = nil
end

if (Client) then
   -- This function manage the display of Help icon for the Faded
   -- Its Display:
   -- * "Press 'use'" / Stab / Eat / Eat progress Bar Button
   local switch_icon_time = Shared.GetTime()
   local icon = false
   local fadeOnProcessMove = AlienActionFinderMixin.OnProcessMove
   function AlienActionFinderMixin:OnProcessMove(input)
      fadeOnProcessMove(self, input)
      local player = Client.GetLocalPlayer()
      if (switch_icon_time < Shared.GetTime() - 1) then
	 switch_icon_time = Shared.GetTime()
	 icon = not(icon)
      end
      -- for _, corpse in ipairs(GetEntitiesWithinRange("Marine",
      -- 						     player:GetOrigin(),
      -- 						     kFadedEatUseDistance)) do
      local status = self:isAimingEntity("Ragdoll", kFadedEatUseDistance)
      if (status) then
	 local eatingFraction = player:GetEatingFraction()
	 if (eatingFraction == 0) then
	    if (icon == false) then
	       self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("Use"), nil, "FADED_DISGUISE_HELP", nil)
	    else
	       self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("RequestHealth"), nil, "FADED_HEAL_HELP", nil)
	    end
	 else -- Show eating progress
	    self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("RequestHealth"), nil, "FADED_HEAL_HELP", eatingFraction)
	 end
	 -- break
      end
   end
end
-- end

-- Called when a key is pressed
-- Note: GetIsBinding() is only available on Client side
-- Note2: If the key is overloaded in VoiceOver.lua, the event
--        when the key is pressed will not be catched here.
--        (Only when its released)

-- '_hasReleasetheKey' is here to force the "eatMarineCorpse()" to be
-- called. If the player press an other key, the key is not the same,
-- (and the eating stop) so we have to track event "down" and "up"
-- and not "until".
function Fade:SendKeyEvent(key, down)
   if (GetIsBinding(key, "RequestHealth") and down) then
      self:eatMarineCorpse()
   else
      self:ResetEatingFraction()
   end
   -- elseif (down == false and GetIsBinding(key, "RequestHealth")) then
   --    Print("Request Heal Event called Client up")
   -- end
   Player.SendKeyEvent(self, key, down)
end

function Fade:GetHasMetabolizeAnimationDelay()
   return 0
end

function Fade:GetMovementSpecialTechId()
   return kTechId.Leap
end

function Fade:GetCanMetabolizeHealth()
--   return self:GetHasTwoHives()
   return false
end

-- Check if a marine see the Faded and reset the eating timer client side
-- Only send the message at most once per second
-- local _notifyOncePerSec = 0
-- local function _CheckIfAMarineSeeMe(self, deltatime)
--    _notifyOncePerSec = _notifyOncePerSec + deltatime
--    local dead_marine = GetEntitiesWithinRange("Marine",
-- 					      self:GetOrigin(),
-- 					      kFadedEatUseDistance + 0.5)
--    if (#dead_marine > 0) then
--       local marine_alive_around, marineWhoSee = self:isAliveMarineAroundInView(kFadedTakeBodyDistance)
--       if (marine_alive_around == true) then
-- 	 -- Reset client side timer
-- 	 if (Server) then
-- 	    Server.SendNetworkMessage(self, "IsFadeEating",
-- 				      {time = nil}, true)
-- 	    if (_notifyOncePerSec > 1 and marineWhoSee) then
-- 	       _notifyOncePerSec = 0
-- 	       self:FadedMessage(strformat(locale:ResolveString("FADED_CANT_USE_CORPSE"), marineWhoSee:GetName()))
-- 	    end
-- 	 end
--       end
--    end
-- end

-- Update function for every Faded entity
-- Check if someone see us (Server side) or if the process is finished
-- Called by OnUpdateClient()
-- function FadedOnUpdate(self, deltatime)
--    if (self and self:isa("Fade") and self:GetIsAlive()) then
--       local eatingProgress = self:GetEatingFraction()
--       -- if (Server) then
-- 	 _CheckIfAMarineSeeMe(self, deltatime)
--       -- end
--       if (Client and eatingProgress >= 1) then
-- 	 self:DevourMarineCorpse()
--       end
--    end
-- end

-- Event.Hook("SendKeyEvent", OnSendKeyEvent)

-- function Fade:SetTargetPlayer(forPlayerId)
--    Print("SetTargetPlayer called with :" .. tostring(forPlayerId))
--    self.forPlayer = forPlayerId
-- end

function Fade:MovementModifierChanged(newMovementModifierState, input)

   -- if newMovementModifierState and self:GetActiveWeapon() ~= nil then
   --    local weaponMapName = self:GetActiveWeapon():GetMapName()
   --    local metabweapon = self:GetWeapon(Metabolize.kMapName)
   --    if metabweapon and not metabweapon:GetHasAttackDelay() and self:GetEnergy() >= metabweapon:GetEnergyCost() then
   -- 	 self:SetActiveWeapon(Metabolize.kMapName)
   -- 	 self:PrimaryAttack()
   --          if weaponMapName ~= Metabolize.kMapName then
   --              self.previousweapon = weaponMapName
   --          end
   --      end
   --  end

end

Shared.LinkClassToMap("Fade", Fade.kMapName, networkVars)
