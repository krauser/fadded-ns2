// ===================== Faded Mod =====================
//
// lua\FadedGUIDeadBody.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
// Notes: This file is the same as "GUIPickups" except:
// Each line with "This line has been changed"
--[[ In short:
   * Change 'Update' function to loop over Dead Marine
     instead of 'pickup' entity.
   * Only display if the marine as a Flamthrower
   * Decrease kPickupsVisibleRange to 10
   * Change 'GetPickupTextureCoordinates' to print a Flamethrower icon
--]]
//
// =====================================================

class 'FadedGUIDeadBody' (GUIPickups)

-- This line has been change
local kPickupsVisibleRange = kFadedBurnOrderRadius

local kPickupTextureYOffsets = { }
kPickupTextureYOffsets["AmmoPack"] = 0
kPickupTextureYOffsets["CatPack"] = 11
kPickupTextureYOffsets["MedPack"] = 1
kPickupTextureYOffsets["Rifle"] = 2
kPickupTextureYOffsets["HeavyRifle"] = 2
kPickupTextureYOffsets["Shotgun"] = 3
kPickupTextureYOffsets["Pistol"] = 4
kPickupTextureYOffsets["Flamethrower"] = 5
kPickupTextureYOffsets["GrenadeLauncher"] = 6
kPickupTextureYOffsets["Welder"] = 7
kPickupTextureYOffsets["Builder"] = 7
kPickupTextureYOffsets["Jetpack"] = 8
kPickupTextureYOffsets["LayMines"] = 9
kPickupTextureYOffsets["Exosuit"] = 10
kPickupTextureYOffsets["CatPack"] = 11
-- Faded: Line added
kPickupTextureYOffsets["Egg"] = 34


local kPickupIconHeight = 64
local kPickupIconWidth = 64

-- Change for the Faded mode
local function GetPickupTextureCoordinates(pickup)

   local yOffset = kPickupTextureYOffsets["Flamethrower"]
   return 0, yOffset * kPickupIconHeight, kPickupIconWidth, (yOffset + 1) * kPickupIconHeight

end

local kMinPickupSize = 16
local kMaxPickupSize = 48
-- Note: This graphic can probably be smaller as we don't need the icons to be so big.
local kTextureName = "ui/drop_icons.dds"
local kIconWorldOffset = Vector(0, 0.5, 0)
local kBounceSpeed = 2
local kBounceAmount = 0.05
local kFontName = "fonts/AgencyFB_small.fnt"
local kFontScale = GUIScale(Vector(1, 1, 1))

function FadedGUIDeadBody:Initialize()

   self.allPickupGraphics = { }

end

function FadedGUIDeadBody:Uninitialize()

   for i, pickupGraphic in ipairs(self.allPickupGraphics) do
      GUI.DestroyItem(pickupGraphic)
   end
   self.allPickupGraphics = { }

end

function FadedGUIDeadBody:GetFreePickupGraphic()

   for i, pickupGraphic in ipairs(self.allPickupGraphics) do

      if pickupGraphic:GetIsVisible() == false then
	 return pickupGraphic
      end

   end
   local newPickupGraphic = GUIManager:CreateGraphicItem()
   newPickupGraphic:SetAnchor(GUIItem.Left, GUIItem.Top)
   newPickupGraphic:SetTexture(kTextureName)
   newPickupGraphic:SetIsVisible(false)


   table.insert(self.allPickupGraphics, newPickupGraphic)

   return newPickupGraphic

end

local _corpseGUIIcon = 0
function FadedGUIDeadBody:Update(deltaTime)

   PROFILE("GUIPickups:Update")

   local localPlayer = Client.GetLocalPlayer()
   -- Line added
   local origin = localPlayer:GetOrigin()

   if localPlayer then

      for i, pickupGraphic in ipairs(self.allPickupGraphics) do
	 pickupGraphic:SetIsVisible(false)
      end

      -- Line Changed
      -- local nearbyPickups = GetNearbyPickups()
      -- for i, pickup in ipairs(nearbyPickups) do
      for _, pickup in ipairs(GetEntitiesWithinRange("Ragdoll", origin, kPickupsVisibleRange))
      do
	 -- Only display
	 if (pickup) then
	    // Check if the pickup is in front of the player.
	    local playerForward = localPlayer:GetCoords().zAxis
	    local playerToPickup = GetNormalizedVector(pickup:GetOrigin() - localPlayer:GetOrigin())
	    local dotProduct = Math.DotProduct(playerForward, playerToPickup)

	    if dotProduct > 0 then

	       local freePickupGraphic = self:GetFreePickupGraphic()
	       freePickupGraphic:SetIsVisible(true)

	       local distance = pickup:GetDistanceSquared(localPlayer)
	       distance = distance / (kPickupsVisibleRange * kPickupsVisibleRange)
	       distance = 1 - distance
	       freePickupGraphic:SetColor(Color(1, 1, 1, distance))

	       local pickupSize = kMinPickupSize + ((kMaxPickupSize - kMinPickupSize) * distance)
	       freePickupGraphic:SetSize(Vector(pickupSize, pickupSize, 0))

	       local bounceAmount = math.sin(Shared.GetTime() * kBounceSpeed) * kBounceAmount
	       -- Changed
	       -- local pickupWorldPosition = pickup:GetOrigin() + kIconWorldOffset + Vector(0, bounceAmount, 0)
	       local pickupWorldPosition = pickup:GetOrigin() + kIconWorldOffset + Vector(0, bounceAmount, 0)
	       local pickupInScreenspace = Client.WorldToScreen(pickupWorldPosition)
	       // Adjust for the size so it is in the middle.
	       pickupInScreenspace = pickupInScreenspace + Vector(-pickupSize / 2, -pickupSize / 2, 0)
	       freePickupGraphic:SetPosition(pickupInScreenspace)

	       -- Changing icon
	       -- if (_corpseGUIIcon > 1 and localPlayer and localPlayer:GetTeamNumber() == 1) then
	       -- self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("Use"), nil, hintText, nil)
		  freePickupGraphic:SetTexturePixelCoordinates(GetPickupTextureCoordinates(pickup))
	       -- else
	       -- 	  freePickupGraphic:SetTexturePixelCoordinates(unpack(GetTextureCoordinatesForIcon(kTechId.Marine)))
	       -- end
	       -- _corpseGUIIcon = _corpseGUIIcon + deltaTime
	       -- if (_corpseGUIIcon > 2) then
	       -- 	  _corpseGUIIcon = 0
	       -- end

	       -- Set an orange color
	       freePickupGraphic:SetColor(Color(255, 127, 0, distance))

	    end

	 end
      end
   end
end
