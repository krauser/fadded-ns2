// ===================== Faded Mod =====================
//
// lua\FadedGUIDepressionScreenEffect.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
//
// =====================================================

Script.Load("lua/GUIAnimatedScript.lua")

local kBackgroundTexture = PrecacheAsset("ui/objective_banner_alien.dds")
local kBackgroundNoiseTexture = PrecacheAsset("ui/alien_commander_bg_smoke.dds")
local kBackgroundSize = Vector(1024, 128, 0)
local kBackgroundPosition = Vector(-kBackgroundSize.x / 2, -300, 0)
// Color starts faded away and fades in.
local kMessageFontColor = Color(1, 0.8, 0.2)

local kMessageFontName = "fonts/Stamp_large.fnt"

local kDropDistance = Vector(0, 10, 0)

-----------

GUIMinimapFrame.kModeMini = 0
GUIMinimapFrame.kModeBig = 1
GUIMinimapFrame.kModeZoom = 2
local kMapBackgroundXOffset = 28
local kMapBackgroundYOffset = 28

local kBigSizeScale = 3
local kZoomedBlipSizeScale = 1 // 0.8
local kMiniBlipSizeScale = 0.5

local kMarineZoomedIconColor = Color(191 / 255, 226 / 255, 1, 1)

local kFrameTextureSize = GUIScale(Vector(473, 354, 0))
-- local kMarineFrameTexture = PrecacheAsset("ui/marine_commander_textures.dds")
-- local kMarineFrameTexture = PrecacheAsset("ui/insight_minimap.dds")
local kMarineFrameTexture = PrecacheAsset("ui/minimapbackground.dds")
local kFramePixelCoords = { 466, 250 , 466 + 473, 250 + 354 }

local kBackgroundNoiseTexture = PrecacheAsset("ui/alien_commander_bg_smoke.dds")
local kSmokeyBackgroundSize = GUIScale(Vector(480, 640, 0))

local kMarineIconsFileName = PrecacheAsset("ui/marine_minimap_blip.dds")
-----------
class 'FadedGUIDepressionScreenEffect' (GUIAnimatedScript)

function FadedGUIDepressionScreenEffect:Initialize()

   GUIAnimatedScript.Initialize(self)
   self.background = self:CreateAnimatedGraphicItem()
   -- self.background:SetAnchor(GUIItem.Left, GUIItem.Bottom)
   self.background:SetSize(Vector(Client.GetScreenWidth() * 2, Client.GetScreenHeight() * 2, 0))
   self.background:SetPosition(Vector(0, 0, 0))
   self.background:SetTexture(kMarineFrameTexture)
   -- self.background:SetAdditionalTexture("noise", kBackgroundNoiseTexture)
   self.background:SetTexturePixelCoordinates(unpack(kFramePixelCoords))
   self.background:SetColor(Color(1, 1, 1, 0.3))
   self.background:SetIsVisible(true)
   self.background:SetLayer(-1)

   -- Print("FadedGUIDepressionScreenEffect:init called")
   -- GUIScript.Initialize(self)

   -- self.background = GUIManager:CreateGraphicItem()
   -- self.background:SetSize(Vector(Client.GetScreenWidth(), Client.GetScreenHeight(), 0))
   -- self.background:SetPosition(Vector(0, 0, 0))
   -- self.background:SetIsVisible(true)
   -- self.background:SetLayer(kGUILayerPlayerHUDBackground)
   -- self.background:SetColor(Color(1, 1, 1, 0))
   -- -- self.background:SetTexture(kElectricTexture)

   -- GUIAnimatedScript.Initialize(self)

   -- self.background = self:CreateAnimatedGraphicItem()
   -- self.background:SetAnchor(GUIItem.Middle, GUIItem.Center)
   -- self.background:SetPosition(kBackgroundPosition)
   -- self.background:SetSize(kBackgroundSize)
   -- self.background:SetTexture(kBackgroundTexture)
   -- -- self.background:SetShader("shaders/GUISmoke.surface_shader")
   -- self.background:SetShader("shaders/ExoScreen.surface_shader")
   -- self.background:SetAdditionalTexture("noise", kBackgroundNoiseTexture)
   -- self.background:SetFloatParameter("correctionX", 3)
   -- self.background:SetFloatParameter("correctionY", 0.2)
   -- self.background:SetIsVisible(true)

   -- self.messageText = self:CreateAnimatedTextItem()
   -- self.messageText:SetFontName(kMessageFontName)
   -- self.messageText:SetAnchor(GUIItem.Middle, GUIItem.Center)
   -- self.messageText:SetTextAlignmentX(GUIItem.Align_Center)
   -- self.messageText:SetTextAlignmentY(GUIItem.Align_Center)
   -- self.messageText:SetColor(kMessageFontColor)
   -- self.background:AddChild(self.messageText)

   -- self.messageTextScale = Vector(1, 1, 1)
   --  self.messageTextDir = 1

end

function FadedGUIDepressionScreenEffect:Update(deltaTime)
   -- Print("FadedGUIDepressionScreenEffect:Updated called")
end

function FadedGUIDepressionScreenEffect:OnResolutionChanged(oldX,
							    oldY,
							    newX,
							    newY)
   self.background:SetSize(Vector(newX, newY, 0))
end
