// ===================== Faded Mod =====================
//
// lua\FadeGUIScoreBoard.lua
//
//    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
// Note: This GUI has been changed to allow the Faded to
//       see dead marine and to change veteran color.
//
// =====================================================

local kDeadColor = Color(1,0,0,1)
-- Give orange color for veteran
local kVeteranColor = Color(255,127,0,1)

local GUIScoreboardUpdateTeam = GUIScoreboard.UpdateTeam
function GUIScoreboard:UpdateTeam(updateTeam)
   GUIScoreboardUpdateTeam(self, updateTeam)

   -- local playerList = updateTeam["PlayerList"]
   -- local teamScores = updateTeam["GetScores"]()
   -- local playerRecord = teamScores[currentPlayerIndex]

   -- for index, player in ipairs(updateTeam:GetPlayers()) do
   --    Print("==> Loop")
   --    local isDead = player:GetIsAlive()
   --    local isVeteran = player.kFadedIsVeteran == true
   --    -- Do not allow marine to see dead faded
   --    if (isDead and player:GetTeamNumber() == 1) then
   -- 	 player["Name"]:SetColor(kDeadColor)
   -- 	 player["Status"]:SetColor(kDeadColor)
   -- 	 Print("==> Loop Set Dead")
   --    elseif (isVeteran) then
   -- 	 player["Name"]:SetColor(kVeteranColor)
   -- 	 player["Status"]:SetColor(kVeteranColor)
   -- 	 Print("==> Loop Set Veteran")
   --    end
   -- end
end
